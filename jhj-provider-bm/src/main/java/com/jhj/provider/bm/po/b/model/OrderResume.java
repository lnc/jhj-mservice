package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "order_resume")
public class OrderResume {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;
    
    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 人才库id
     */
    @Column(name = "resume_id")
    private Integer resumeId;
    
    /**
     * 人才库编号
     */
    @Column(name = "resume_no")
    private String resumeNo;    

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 姓名
     */
    private String name;
    
    /**
     * 面试时间
     */
    @Column(name = "interview_time")
    private Integer interviewTime;
    
    /**
     * 面试地点
     */
    @Column(name = "interview_addr")
    private String interviewAddr;

    /**
     * choose = 推荐阿姨 interview = 面试 assign = 用户预约 nochoose = 不满意
     */
    @Column(name = "action_type")
    private String actionType;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取人才库id
     *
     * @return resume_id - 人才库id
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才库id
     *
     * @param resumeId 人才库id
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取姓名
     *
     * @return name - 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取interview  = 面试black = 黑名单
     *
     * @return action_type - interview  = 面试black = 黑名单
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * 设置interview  = 面试black = 黑名单
     *
     * @param actionType interview  = 面试black = 黑名单
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

	public String getResumeNo() {
		return resumeNo;
	}

	public void setResumeNo(String resumeNo) {
		this.resumeNo = resumeNo;
	}

	public Integer getInterviewTime() {
		return interviewTime;
	}

	public void setInterviewTime(Integer interviewTime) {
		this.interviewTime = interviewTime;
	}

	public String getInterviewAddr() {
		return interviewAddr;
	}

	public void setInterviewAddr(String interviewAddr) {
		this.interviewAddr = interviewAddr;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
package com.jhj.provider.bm.vo;

import java.math.BigDecimal;

import com.jhj.provider.bm.po.b.model.Sale;

public class SaleStatVo extends Sale {
	
	// 总订单数
	private Integer total;

	// 正在进行中订单数
	private Integer totalProcess;

	// 已经签约成功订单数
	private Integer totalDone;

	// 总签约金额
	private BigDecimal totalOrderPay;
	
	// 额外订单数 = 正在进行订单数 - 系统登记饱和数.
	private Integer totalExtra;

	public Integer getTotalProcess() {
		return totalProcess;
	}

	public void setTotalProcess(Integer totalProcess) {
		this.totalProcess = totalProcess;
	}

	public Integer getTotalDone() {
		return totalDone;
	}

	public void setTotalDone(Integer totalDone) {
		this.totalDone = totalDone;
	}

	public BigDecimal getTotalOrderPay() {
		return totalOrderPay;
	}

	public void setTotalOrderPay(BigDecimal totalOrderPay) {
		this.totalOrderPay = totalOrderPay;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getTotalExtra() {
		return totalExtra;
	}

	public void setTotalExtra(Integer totalExtra) {
		this.totalExtra = totalExtra;
	}

}

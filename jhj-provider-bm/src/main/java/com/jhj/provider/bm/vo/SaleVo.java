package com.jhj.provider.bm.vo;

import com.jhj.provider.bm.po.b.model.Sale;

public class SaleVo extends Sale{
	
	private String startTimeStr;
	
	private String endTimeStr;
	
	private String levels;

	public String getStartTimeStr() {
		return startTimeStr;
	}

	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}
	
}

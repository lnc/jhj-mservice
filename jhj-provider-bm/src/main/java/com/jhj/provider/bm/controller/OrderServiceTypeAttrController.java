package com.jhj.provider.bm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.util.StringUtil;
import com.jhj.provider.bm.po.b.model.OrderServiceType;
import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;
import com.jhj.provider.bm.service.OrderServiceTypeAttrService;
import com.jhj.provider.bm.service.OrderServiceTypeService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;

@RestController
public class OrderServiceTypeAttrController {

	@Autowired
	private OrderServiceTypeAttrService orderServiceTypeAttrServie;

	@Autowired
	private OrderServiceTypeService orderServiceTypeService;

	/**
	 * 订单属性添加
	 * 
	 * @param orderId
	 *            订单id
	 * @param orderNo
	 *            订单号
	 * @param serviceTypeId
	 *            服务类别id
	 * @param salary
	 *            薪资
	 * @param workYear
	 *            工作经验
	 * @param age
	 *            服务年龄
	 * @param addr
	 *            地址
	 * @param birthPlace
	 *            出生地
	 * @param serviceAge
	 *            服务对象年龄
	 * @param predicted
	 *            预产期
	 * @param serviceDate
	 *            服务日期
	 * @param workHome
	 *            是否住家 0=不在，1=住家'
	 * 
	 * @return
	 * 
	 */
	@RequestMapping(value = "/api/bm/attr/add.json", method = RequestMethod.POST)
	public AppResultData<Object> addAttr(@RequestParam(value = "orderId") Integer orderId,
			@RequestParam(value = "orderNo") String orderNo,
			@RequestParam(value = "serviceTypeId", defaultValue = "0") Integer serviceTypeId,
			@RequestParam(value = "workHome", required = false, defaultValue = "") String workHomeStr,
			@RequestParam(value = "serviceAge", required = false, defaultValue = "") String serviceAge,
			@RequestParam(value = "resumeAge", required = false, defaultValue = "") String resumeAge,
			@RequestParam(value = "workYear", required = false, defaultValue = "") String workYear,
			@RequestParam(value = "predicted", required = false, defaultValue = "") String predicted,
			@RequestParam(value = "serviceAddr", required = false, defaultValue = "") String serviceAddr,
			@RequestParam(value = "serviceDate", required = false, defaultValue = "") String serviceDate,
			@RequestParam(value = "salary", required = false, defaultValue = "") String salary,
			@RequestParam(value = "birthPlace", required = false, defaultValue = "") String birthPlace,
			@RequestParam(value = "addr", required = false, defaultValue = "") String addr
			) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		
		// 是否住家
		if (!StringUtil.isEmpty(workHomeStr)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "workHome", workHomeStr, "是否住家");
			
			Integer workHome = Integer.valueOf(workHomeStr);
			OrderServiceType ost = new OrderServiceType();
			ost.setOrderId(orderId);
			ost.setOrderNo(orderNo);
			ost.setWorkHome(workHome);
			orderServiceTypeService.update(ost);
		}
		
		// 服务对象年龄
		if (!StringUtil.isEmpty(serviceAge)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "serviceAge", serviceAge, "服务对象年龄");
		}
				
		
		// 保姆/月嫂年龄
		if (!StringUtil.isEmpty(resumeAge)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "resumeAge", resumeAge, "保姆/月嫂年龄");
		}
		
		// 工作年限
		if (!StringUtil.isEmpty(workYear)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "workYear", workYear, "工作年限");
		}
		

		
		// 预产期
		if (!StringUtil.isEmpty(predicted)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "predicted", predicted, "预产期");
		}
		
		// 服务地址
		if (!StringUtil.isEmpty(serviceAddr)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "serviceAddr", serviceAddr, "服务地址");
		}
		
		// 服务日期
		if (!StringUtil.isEmpty(serviceDate)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "serviceDate", serviceDate, "服务日期");
		}
				
		// 期望薪资
		if (!StringUtil.isEmpty(salary)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "salary", salary, "期望薪资");
		}

		// 籍贯要求
		if (!StringUtil.isEmpty(birthPlace)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "birthPlace", birthPlace, "籍贯要求");
		}
		
		//服务区域
		if (!StringUtil.isEmpty(addr)) {
			this.addServiceAttr(orderId, orderNo, serviceTypeId, "addr", addr, "服务区域");
		}

		return data;
	}
	
	
	public int addServiceAttr(Integer orderId, String orderNo, Integer serviceTypeId, String attrName, String attrValue, String attrDesc) {
		OrderServiceTypeAttr search = new OrderServiceTypeAttr();
		search.setOrderId(orderId);
		search.setOrderNo(orderNo);
		search.setAttrType(attrName);
		OrderServiceTypeAttr orderServiceTypeAttr = orderServiceTypeAttrServie.selectOne(search);
		if (orderServiceTypeAttr == null) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(orderId);
			attr.setOrderNo(orderNo);
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName(attrDesc);
			attr.setAttrType(attrName);
			attr.setAttrValue(attrValue);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			return orderServiceTypeAttrServie.insert(attr);
		} else {
			return this.updateAttrValue(orderServiceTypeAttr, attrValue);
		}

	}
	/**
	 * 更新属性值
	 */
	public int updateAttrValue(OrderServiceTypeAttr attr, String attrValue) {
		attr.setAttrValue(attrValue);
		return orderServiceTypeAttrServie.update(attr);
	}

}

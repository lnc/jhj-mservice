package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "imgs")
public class Imgs {
    /**
     * 图片id
     */
    @Id
    @Column(name = "img_id")
    @GeneratedValue(generator = "JDBC")
    private Integer imgId;

    /**
     * 图片类型sale_head_img  = 销售老师头像sale =  销售老师其他照片contract  = 订单合同照片interview  = 面试照片
     */
    @Column(name = "img_type")
    private String imgType;

    /**
     * 图片地址
     */
    @Column(name = "img_url")
    private String imgUrl;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;
    
    /**
     * 关联ID
     */
    @Column(name = "link_id")
    private Integer linkId;
    
    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取图片id
     *
     * @return img_id - 图片id
     */
    public Integer getImgId() {
        return imgId;
    }

    /**
     * 设置图片id
     *
     * @param imgId 图片id
     */
    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    /**
     * 获取图片类型sale_head_img  = 销售老师头像sale =  销售老师其他照片contract  = 订单合同照片interview  = 面试照片
     *
     * @return img_type - 图片类型sale_head_img  = 销售老师头像sale =  销售老师其他照片contract  = 订单合同照片interview  = 面试照片
     */
    public String getImgType() {
        return imgType;
    }

    /**
     * 设置图片类型sale_head_img  = 销售老师头像sale =  销售老师其他照片contract  = 订单合同照片interview  = 面试照片
     *
     * @param imgType 图片类型sale_head_img  = 销售老师头像sale =  销售老师其他照片contract  = 订单合同照片interview  = 面试照片
     */
    public void setImgType(String imgType) {
        this.imgType = imgType;
    }

    /**
     * 获取图片地址
     *
     * @return img_url - 图片地址
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * 设置图片地址
     *
     * @param imgUrl 图片地址
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

	public Integer getLinkId() {
		return linkId;
	}

	public void setLinkId(Integer linkId) {
		this.linkId = linkId;
	}
}
package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.Dict;

import tk.mybatis.mapper.common.Mapper;

public interface DictMapper extends Mapper<Dict> {
}
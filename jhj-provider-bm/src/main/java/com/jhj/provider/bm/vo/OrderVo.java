package com.jhj.provider.bm.vo;

import java.math.BigDecimal;
import java.util.List;

import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.po.b.model.Imgs;
import com.jhj.provider.bm.po.b.model.OrderResume;
import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;

/**
 * @author hulj
 *
 */
public class OrderVo extends BmOrder {

	private List<Integer> orderIdList;

	private String orderIds;

	private String orderStatusName;

	private String addTimeStr;

	private String addFromName;

	private BigDecimal orderPay;

	private String startAddTimeStr;

	private String endAddTimeStr;

	private List<Integer> saleIdList;
	
	private String saleName;

	private String orderStatuss;

	private String serviceTypeId;

	private String serviceTypeIds;

	private String serviceTypeName;

	private String serviceAge;

	private String serviceAgeName;

	private String resumeAge;

	private String workYear;

	private String predicted;

	private String serviceAddr;

	private String serviceDate;

	private String salary;

	private String birthPlaceName;

	private String addr;

	private Integer resumeId;

	private String resumeNo;

	private String resumeName;

	private List<OrderResume> orderResumeList;

	private List<Imgs> imgList;

	private String interviewTimeStr;

	private String interviewAddr;

	private String saleMobile;

	/**
	 * 订单属性集合
	 */
	private List<OrderServiceTypeAttr> orderServiceTypeAttrList;
	

	public String getSaleMobile() {
		return saleMobile;
	}

	public void setSaleMobile(String saleMobile) {
		this.saleMobile = saleMobile;
	}

	public List<Integer> getOrderIdList() {
		return orderIdList;
	}

	public void setOrderIdList(List<Integer> orderIdList) {
		this.orderIdList = orderIdList;
	}

	public String getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(String orderIds) {
		this.orderIds = orderIds;
	}

	public String getOrderStatusName() {
		return orderStatusName;
	}

	public void setOrderStatusName(String orderStatusName) {
		this.orderStatusName = orderStatusName;
	}

	public String getAddTimeStr() {
		return addTimeStr;
	}

	public void setAddTimeStr(String addTimeStr) {
		this.addTimeStr = addTimeStr;
	}

	public String getAddFromName() {
		return addFromName;
	}

	public void setAddFromName(String addFromName) {
		this.addFromName = addFromName;
	}

	public String getStartAddTimeStr() {
		return startAddTimeStr;
	}

	public void setStartAddTimeStr(String startAddTimeStr) {
		this.startAddTimeStr = startAddTimeStr;
	}

	public String getEndAddTimeStr() {
		return endAddTimeStr;
	}

	public void setEndAddTimeStr(String endAddTimeStr) {
		this.endAddTimeStr = endAddTimeStr;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public String getOrderStatuss() {
		return orderStatuss;
	}

	public void setOrderStatuss(String orderStatuss) {
		this.orderStatuss = orderStatuss;
	}

	public String getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(String serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public BigDecimal getOrderPay() {
		return orderPay;
	}

	public void setOrderPay(BigDecimal orderPay) {
		this.orderPay = orderPay;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	public String getServiceTypeIds() {
		return serviceTypeIds;
	}

	public void setServiceTypeIds(String serviceTypeIds) {
		this.serviceTypeIds = serviceTypeIds;
	}

	/**
	 * 订单属性集合
	 */
	public List<OrderServiceTypeAttr> getOrderServiceTypeAttrList() {
		return orderServiceTypeAttrList;
	}

	/**
	 * 订单属性集合
	 */
	public void setOrderServiceTypeAttrList(List<OrderServiceTypeAttr> orderServiceTypeAttrList) {
		this.orderServiceTypeAttrList = orderServiceTypeAttrList;
	}

	public String getBirthPlaceName() {
		return birthPlaceName;
	}

	public void setBirthPlaceName(String birthPlaceName) {
		this.birthPlaceName = birthPlaceName;
	}

	public Integer getResumeId() {
		return resumeId;
	}

	public void setResumeId(Integer resumeId) {
		this.resumeId = resumeId;
	}

	public String getResumeNo() {
		return resumeNo;
	}

	public void setResumeNo(String resumeNo) {
		this.resumeNo = resumeNo;
	}

	public String getResumeName() {
		return resumeName;
	}

	public void setResumeName(String resumeName) {
		this.resumeName = resumeName;
	}

	public List<OrderResume> getOrderResumeList() {
		return orderResumeList;
	}

	public void setOrderResumeList(List<OrderResume> orderResumeList) {
		this.orderResumeList = orderResumeList;
	}

	public List<Imgs> getImgList() {
		return imgList;
	}

	public void setImgList(List<Imgs> imgList) {
		this.imgList = imgList;
	}

	public String getInterviewTimeStr() {
		return interviewTimeStr;
	}

	public void setInterviewTimeStr(String interviewTimeStr) {
		this.interviewTimeStr = interviewTimeStr;
	}

	public String getInterviewAddr() {
		return interviewAddr;
	}

	public void setInterviewAddr(String interviewAddr) {
		this.interviewAddr = interviewAddr;
	}

	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getServiceAddr() {
		return serviceAddr;
	}

	public void setServiceAddr(String serviceAddr) {
		this.serviceAddr = serviceAddr;
	}

	public String getServiceAge() {
		return serviceAge;
	}

	public void setServiceAge(String serviceAge) {
		this.serviceAge = serviceAge;
	}

	public String getResumeAge() {
		return resumeAge;
	}

	public void setResumeAge(String resumeAge) {
		this.resumeAge = resumeAge;
	}

	public String getWorkYear() {
		return workYear;
	}

	public void setWorkYear(String workYear) {
		this.workYear = workYear;
	}

	public String getPredicted() {
		return predicted;
	}

	public void setPredicted(String predicted) {
		this.predicted = predicted;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getServiceAgeName() {
		return serviceAgeName;
	}

	public void setServiceAgeName(String serviceAgeName) {
		this.serviceAgeName = serviceAgeName;
	}

	public List<Integer> getSaleIdList() {
		return saleIdList;
	}

	public void setSaleIdList(List<Integer> saleIdList) {
		this.saleIdList = saleIdList;
	}
	
	

}

package com.jhj.provider.bm.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.bm.client.ResumeClient;
import com.jhj.provider.bm.client.vo.ResumeListVo;
import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.po.b.model.Imgs;
import com.jhj.provider.bm.po.b.model.OrderContract;
import com.jhj.provider.bm.po.b.model.OrderLog;
import com.jhj.provider.bm.po.b.model.OrderResume;
import com.jhj.provider.bm.po.b.model.OrderServiceType;
import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;
import com.jhj.provider.bm.po.b.model.Sale;
import com.jhj.provider.bm.service.BmOrderService;
import com.jhj.provider.bm.service.ImgService;
import com.jhj.provider.bm.service.OrderContractService;
import com.jhj.provider.bm.service.OrderLogService;
import com.jhj.provider.bm.service.OrderResumeService;
import com.jhj.provider.bm.service.OrderServiceTypeAttrService;
import com.jhj.provider.bm.service.OrderServiceTypeService;
import com.jhj.provider.bm.service.SaleService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.provider.bm.vo.OrderBaseVo;
import com.jhj.provider.bm.vo.OrderVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.OrderNoUtil;
import com.jhj.utils.SmsUtil;
import com.jhj.utils.StringUtil;
import com.jhj.utils.TimeStampUtil;
import com.jhj.utils.sign.JhjSignConfig;
import com.jhj.utils.sign.JhjSignUtils;

@RestController
public class BmOrderController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private BmOrderService orderService;

	@Autowired
	private OrderServiceTypeService orderServiceTypeService;

	@Autowired
	private OrderServiceTypeAttrService orderServiceTypeAttrService;

	@Autowired
	private OrderResumeService orderResumeService;

	@Autowired
	private OrderLogService orderLogService;

	@Autowired
	private SaleService saleService;

	@Autowired
	private ImgService imgService;

	@Autowired
	private OrderContractService orderContractService;

	@Autowired
	public ResumeClient resumeClient;

	@Value("${spring.profiles.active}")
	private String applicationActive;

	/**
	 * 下单接口
	 * 
	 * 保姆需要的信息未  workHome   
	 * 
	 * @param userId
	 *            用户id
	 * @param mobile
	 *            用户手机号
	 * @param userName
	 *            用户手机号
	 * @param saleId
	 *            销售老师id
	 * @param saleMobile
	 *            销售老师手机号
	 * @param workHome
	 *            是否住家
	 * @param cityId
	 *            服务城市id
	 * @param regionId
	 *            服务区县id
	 * @param orderStatus
	 *            订单状态
	 * @param lat
	 *            经纬度
	 * @param lng
	 *            经纬度
	 * @param remark
	 *            备注
	 * @param addFrom
	 *            订单来源
	 * @param serviceTypeId
	 *            服务类型
	 * @param serviceAge
	 *            服务年龄
	 * @param predicted
	 *            预产期
	 * @param resumeAge
	 *            保姆年龄
	 * @param workYear
	 *            工作年限
	 * @param addrId
	 *            地址
	 * @param serviceDate
	 *            服务日期
	 * @param actionId
	 *            操作人id
	 * @param actionName
	 *            操作人员姓名
	 * @param resumeNo
	 *            用户下单时指定的阿姨编号.
	 * @param workContent
	 *            工作内容
	 * @return sale
	 */
	@RequestMapping(value = "/api/bm/order/add.json", method = RequestMethod.POST)
	public AppResultData<Object> add(@RequestParam(value = "userId", required = false) Integer userId,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "serviceTypeId", required = false, defaultValue = "0") Integer serviceTypeId,
			@RequestParam(value = "serviceTypeName", required = false, defaultValue = "") String serviceTypeName,
			@RequestParam(value = "workHome", required = false, defaultValue = "") String workHomeStr,
			@RequestParam(value = "serviceAge", required = false, defaultValue = "") String serviceAge,
			@RequestParam(value = "resumeAge", required = false, defaultValue = "") String resumeAge,
			@RequestParam(value = "workYear", required = false, defaultValue = "") String workYear,
			@RequestParam(value = "predicted", required = false, defaultValue = "") String predicted,
			@RequestParam(value = "serviceAddr", required = false, defaultValue = "") String serviceAddr,
			@RequestParam(value = "serviceDate", required = false, defaultValue = "") String serviceDate,
			@RequestParam(value = "workContent", required = false, defaultValue = "") String workContent,
			@RequestParam(value = "remark", required = false, defaultValue = "") String remark,
			@RequestParam(value = "saleId", required = false, defaultValue = "") Integer saleId,
			@RequestParam(value = "saleMobile", required = false, defaultValue = "") String saleMobile,
			@RequestParam(value = "saleName", required = false, defaultValue = "") String saleName,
			@RequestParam(value = "cityId", required = false, defaultValue = "") Integer cityId,
			@RequestParam(value = "regionId", required = false, defaultValue = "") Integer regionId,
			@RequestParam(value = "orderStatus", required = false, defaultValue = "1") Integer orderStatus,
			@RequestParam(value = "lat", required = false, defaultValue = "0") String lat,
			@RequestParam(value = "lng", required = false, defaultValue = "0") String lng,
			@RequestParam(value = "addFrom", required = false, defaultValue = "0") Integer addFrom,
			@RequestParam(value = "actionId", required = false, defaultValue = "") Integer actionId,
			@RequestParam(value = "actionName", required = false, defaultValue = "") String actionName,
			@RequestParam(value = "resumeNo", required = false, defaultValue = "") String resumeNo
			) {

		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		Integer workHome = 0;
		if (!StringUtil.isEmpty(workHomeStr)) workHome = Integer.valueOf(workHomeStr);
		// 必要的信息，或者是直接注册用户
		if (userId == null && StringUtil.isEmpty(mobile)) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("用户信息不完整");
			return data;
		}

		// 一个用户，预约一个阿姨只有一次，多次会提示已预约过.
		if (!StringUtil.isEmpty(resumeNo)) {
			OrderResume searchVo = new OrderResume();
			searchVo.setUserId(userId);
			searchVo.setResumeNo(resumeNo);
			List<OrderResume> list = orderResumeService.selectBySearchVo(searchVo, "");
			if (!list.isEmpty()) {
				data.setStatus(Constants.ERROR_999);
				data.setMsg("您已经预约过此阿姨.");
				return data;
			}
		}

		BmOrder order = orderService.initPo(new BmOrder());
		order.setOrderNo(String.valueOf(OrderNoUtil.genOrderNo()));
		order.setUserId(userId);
		order.setMobile(mobile);
		order.setUserName(userName);
		order.setSaleId(saleId);
		if (saleId != null && saleId > 0 && saleMobile == null) {
			Sale sale = saleService.selectByPrimaryKey(saleId);
			order.setSaleMobile(sale.getMobile());
			order.setOrderDispatch(1);
		} else {
			order.setSaleMobile(saleMobile);
			order.setOrderDispatch(0);
		}
		order.setWorkHome(workHome);
		order.setCityId(cityId);
		order.setRegionId(regionId);
		order.setOrderStatus(orderStatus);
		order.setLat(lat);
		order.setLng(lng);
		order.setRemark(remark);
		order.setAddFrom(addFrom);
		order.setAddTime(TimeStampUtil.getNowSecond().intValue());
		order.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		order.setWorkContent(workContent);
		order.setPublish(0);
		int insert = orderService.insert(order);

		// 服务类别
		if (serviceTypeId != null && serviceTypeId > 0) {
			OrderServiceType orderServiceType = orderServiceTypeService.initPo(new OrderServiceType());
			orderServiceType.setOrderId(order.getOrderId());
			orderServiceType.setOrderNo(order.getOrderNo());
			orderServiceType.setServiceTypeId(serviceTypeId);
			orderServiceType.setServiceTypeName(serviceTypeName + "");
			orderServiceType.setWorkHome(workHome);
			orderServiceType.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeService.insert(orderServiceType);
		}
		
		//是否住家
		if (!StringUtil.isEmpty(workHomeStr)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("是否住家");
			attr.setAttrType("workHome");
			attr.setAttrValue(workHomeStr);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}
		
		//服务对象年龄 baby 或者老人
		if (!StringUtil.isEmpty(serviceAge)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("服务对象年龄");
			attr.setAttrType("serviceAge");
			attr.setAttrValue(serviceAge);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}
		
		//保姆/月嫂年龄
		if (!StringUtil.isEmpty(resumeAge)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("希望保姆年龄");
			attr.setAttrType("resumeAge");
			attr.setAttrValue(resumeAge);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}
		
		//工作年限
		if (!StringUtil.isEmpty(workYear)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("工作年限");
			attr.setAttrType("workYear");
			attr.setAttrValue(workYear);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}
		
		//宝宝预产期
		if (!StringUtil.isEmpty(predicted)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("预产期");
			attr.setAttrType("predicted");
			attr.setAttrValue(predicted);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}

		//服务地址
		if (!StringUtil.isEmpty(serviceAddr)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("服务地址");
			attr.setAttrType("serviceAddr");
			attr.setAttrValue(serviceAddr);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}
		
		//服务日期
		if (!StringUtil.isEmpty(serviceDate)) {
			OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
			attr.setOrderId(order.getOrderId());
			attr.setOrderNo(order.getOrderNo());
			attr.setServiceTypeId(serviceTypeId);
			attr.setAttrName("服务日期");
			attr.setAttrType("serviceDate");
			attr.setAttrValue(serviceDate);
			attr.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeAttrService.insert(attr);
		}

		// 指定服务人员
		if (!StringUtil.isEmpty(resumeNo)) {
			// 获取人才基本信息
			String resumeMobile = "";
			String resumeName = "";
			Integer resumeId = 0;
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("resumeNo", resumeNo);
			params = JhjSignUtils.getSignParams(params);

			String appkey = JhjSignConfig.appkey;
			String oncestr = params.get("oncestr").toString();
			String sign = params.get("sign").toString();

			try {
				AppResultData<ResumeListVo> resumeData = resumeClient.getBaseInfo(resumeNo, resumeMobile, appkey,
						oncestr, sign);
				ResumeListVo resumeListVo = resumeData.getData();
				resumeMobile = resumeListVo.getMobile();
				resumeName = resumeListVo.getName();
				resumeId = resumeListVo.getResumeId();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			OrderResume orderResume = new OrderResume();
			orderResume.setUserId(userId);
			orderResume.setOrderId(order.getOrderId());
			orderResume.setOrderNo(order.getOrderNo());
			orderResume.setResumeId(resumeId);
			orderResume.setResumeNo(resumeNo);
			orderResume.setMobile(resumeMobile);
			orderResume.setName(resumeName);
			orderResume.setInterviewTime(0);
			orderResume.setInterviewAddr("");
			orderResume.setActionType(Constants.ORDER_RESUME_ACTION_TYPE_ASSIGN);
			orderResume.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderResumeService.insert(orderResume);

			order.setAddFrom(Constants.ADD_FROM_3);
			BmOrder o = new BmOrder();
			o.setOrderId(order.getOrderId());
			o.setAddFrom(Constants.ADD_FROM_3);
			orderService.update(o);
		}

		// 日志
		if (insert > 0) {
			OrderLog orderLog = new OrderLog();
			orderLog.setOrderId(order.getOrderId());
			orderLog.setOrderNo(order.getOrderNo());
			orderLog.setLogType("order_add");
			orderLog.setLogName("");
			if (actionId != null && actionId > 0) {
				orderLog.setActionId(actionId);
				orderLog.setActionName(actionName);
			} else {
				orderLog.setActionId(Integer.valueOf(userId));
				orderLog.setActionName(userName);
			}
			orderLog.setRemark("订单添加");
			orderLog.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderLogService.insert(orderLog);
		}
		
		Integer orderId = order.getOrderId();
		//下面进行系统派单
		HashMap<String, Object> dispatchInfo = saleService.orderDispatch(saleId, saleMobile, orderId, addFrom);
		
		if (dispatchInfo != null) {
			Integer dispatchSaleId = Integer.valueOf(dispatchInfo.get("saleId").toString());
			String dispatchBy = dispatchInfo.get("dispatchBy").toString();
			Integer orderDispatch = Integer.valueOf(dispatchInfo.get("orderDispatch").toString());
			Sale sale = saleService.selectByPrimaryKey(dispatchSaleId);
			
			BmOrder o = new BmOrder();
			o.setOrderId(order.getOrderId());
			o.setSaleId(dispatchSaleId);
			o.setOrderDispatch(orderDispatch);
			o.setOrderDispatchBy(dispatchBy);
			orderService.update(o);
			data.setData(sale);
			// 短信提醒销售老师
			// 【叮当到家】系统后台给您派发一条新订单，请抓紧打开后台，及时与客户取得联系
			if (!applicationActive.equals("dev")) {
				saleMobile = sale.getMobile();
				String[] content = new String[] { "" };
				SmsUtil.SendSms(saleMobile, "243124", content);
			}
		}
		

		return data;
	}

	/**
	 * 用户/销售老师订单列表接口
	 * 
	 * @param userId
	 *            用户id
	 * @param saleId
	 *            销售老师id
	 * @param saleName
	 *            销售老师名称
	 * @param mobile
	 *            用户手机号
	 * @param orderStatuss
	 *            订单状态集合
	 * @param serviceTypeId
	 *            服务类型id
	 * @param startAddTimeStr
	 *            下单开始时间
	 * @param endAddTimeStr
	 *            下单完成时间
	 * @param pageNo
	 *            页数
	 * @param pageSize
	 *            分页大小
	 * @param orderDispatch
	 *            空 = 全部， 0 = 系统派单 1 = 自主开发
	 * @param publish
	 *            1 = 只查询已发布， 其他值不作为查询条件
	 * @param sceretPhone
	 *            手机号是否标星 0 = 否 1= 是
	 * @return list<OrderVo>
	 */
	@RequestMapping(value = "/api/bm/order/list.json", method = RequestMethod.GET)
	public AppResultData<Object> userOrderList(@RequestParam(value = "userId", required = false) Integer userId,
			@RequestParam(value = "saleId", required = false) Integer saleId,
			@RequestParam(value = "saleName", required = false) String saleName,
			@RequestParam(value = "saleMobile", required = false) String saleMobile,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "orderStatuss", required = false) String orderStatuss,
			@RequestParam(value = "serviceTypeId", required = false) String serviceTypeId,
			@RequestParam(value = "serviceTypeIds", required = false) String serviceTypeIds,
			@RequestParam(value = "startAddTimeStr", required = false) String startAddTimeStr,
			@RequestParam(value = "endAddTimeStr", required = false) String endAddTimeStr,
			@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
			@RequestParam(value = "orderDispatch", required = false, defaultValue = "") String orderDispatch,
			@RequestParam(value = "publish", required = false, defaultValue = "1") Integer publish,
			@RequestParam(value = "sceretName", required = false, defaultValue = "0") Integer sceretName,
			@RequestParam(value = "sceretPhone", required = false, defaultValue = "1") Integer sceretPhone,
			@RequestParam(value = "needPageInfo", required = false, defaultValue = "1") Integer needPageInfo) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		OrderVo orderVo = new OrderVo();
		orderVo.setUserId(userId);
		orderVo.setSaleId(saleId);
		orderVo.setSaleName(saleName);
		orderVo.setSaleMobile(saleMobile);
		orderVo.setMobile(mobile);
		orderVo.setOrderStatuss(orderStatuss);
		orderVo.setServiceTypeId(serviceTypeId);
		orderVo.setServiceTypeIds(serviceTypeIds);
		orderVo.setStartAddTimeStr(startAddTimeStr);
		orderVo.setEndAddTimeStr(endAddTimeStr);

		if (publish == 1) {
			orderVo.setPublish(1);
		}

		if (!StringUtil.isEmpty(orderDispatch)) {
			orderVo.setOrderDispatch(Integer.valueOf(orderDispatch));
		}

		List<BmOrder> list = orderService.selectByOrderVoDefinedListPage(orderVo, pageNo, pageSize, "add_time desc");
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				BmOrder o = list.get(i);
				OrderVo vo = orderService.transVo(o);

				if (sceretPhone == 1) {
					String m = vo.getMobile();
					StringBuffer bf = new StringBuffer();
					bf.append(m.substring(0, 2)).append("*****").append(m.substring(8));
					vo.setMobile(bf.toString());
				}

				if (sceretName == 1) {
					String name = vo.getUserName();
					if (!StringUtil.isEmpty(name)) {
						name = StringUtil.subStringByByte(name, 2);
						name = name + "**";
						vo.setUserName(name);
					}
				}

				list.set(i, vo);
			}
		}

		if (needPageInfo == 1) {
			data.setData(new PageInfo<>(list));
		} else {
			data.setData(list);
		}

		return data;
	}

	/**
	 * 更新保姆订单
	 * 
	 * @param orderId
	 *            订单号
	 * @param orderStatus
	 *            订单状态
	 * @param serviceTypeId
	 *            服务类别id
	 * @param serviceTypeName
	 *            服务类别名称
	 * @param remark
	 *            备注
	 * @param workContent
	 *            工作内容
	 * @param adminId
	 *            操作人id
	 * @param adminName
	 *            操作人
	 * 
	 * @return order
	 * 
	 */
	@RequestMapping(value = "/api/bm/order/update.json", method = RequestMethod.POST)
	public AppResultData<Object> update(@RequestParam("orderId") Integer id,
			@RequestParam(value = "orderStatus", required = false, defaultValue = "1") Integer orderStatus,
			@RequestParam(value = "userName", required = false, defaultValue = "") String userName,
			@RequestParam(value = "serviceTypeId", required = false, defaultValue = "0") Integer serviceTypeId,
			@RequestParam(value = "serviceTypeName", required = false, defaultValue = "") String serviceTypeName,
			@RequestParam(value = "remark", defaultValue = "") String remark,
			@RequestParam(value = "workContent", defaultValue = "") String workContent,
			@RequestParam(value = "adminId", required = false, defaultValue = "0") Integer actionId,
			@RequestParam(value = "adminName", required = false, defaultValue = "") String actionName,
			@RequestParam(value = "addFrom", required = false, defaultValue = "")  Integer addFrom
			) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "0k", "");

		BmOrder order = orderService.selectByPrimaryKey(id);
		if (order == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("数据错误");
			return result;
		}
		order.setOrderStatus(orderStatus);
		order.setAddFrom(addFrom);
		if (!StringUtil.isEmpty(userName)) order.setUserName(userName);
		
		order.setRemark(remark);
		order.setWorkContent(workContent);
		int update = orderService.update(order);

		OrderServiceType t = new OrderServiceType();
		t.setOrderId(id);
		OrderServiceType orderServiceType = orderServiceTypeService.selectOne(t);
		if (orderServiceType != null) {
			if (serviceTypeId > 0) {
				orderServiceType.setServiceTypeId(serviceTypeId);
			}
			orderServiceType.setServiceTypeName(serviceTypeName);
			orderServiceTypeService.update(orderServiceType);
		} else {
			orderServiceType = new OrderServiceType();
			orderServiceType.setOrderId(id);
			orderServiceType.setOrderNo(order.getOrderNo());
			orderServiceType.setServiceTypeId(serviceTypeId);
			orderServiceType.setServiceTypeName(serviceTypeName);
			orderServiceType.setWorkHome(0);
			orderServiceType.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderServiceTypeService.insert(orderServiceType);
		}

		if (update > 0) {
			OrderLog orderLog = new OrderLog();
			orderLog.setOrderId(order.getOrderId());
			orderLog.setOrderNo(order.getOrderNo());
			orderLog.setLogType("order_update");
			orderLog.setLogName("");
			orderLog.setActionId(actionId);
			orderLog.setActionName(actionName);
			orderLog.setRemark("订单修改");
			orderLog.setAddTime(TimeStampUtil.getNowSecond().intValue());
			orderLogService.insert(orderLog);
		}

		result.setData(order);
		return result;
	}

	/**
	 * 订单详情
	 * 
	 * @param orderId
	 *            订单id
	 * @param sceretPhone
	 *            手机号是否标星 0 = 否 1= 是
	 * @return orderVo
	 * 
	 */
	@RequestMapping(value = "/api/bm/order/detail.json", method = RequestMethod.GET)
	public AppResultData<Object> bmOrderDetail(
			@RequestParam(value = "orderId", required = false, defaultValue = "") String id,
			@RequestParam(value = "orderNo", required = false, defaultValue = "") String orderNo,
			@RequestParam(value = "sceretPhone", required = false, defaultValue = "1") Integer sceretPhone) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		Integer orderId = 0;
		BmOrder order = null;

		if (StringUtil.isEmpty(orderNo) && StringUtil.isEmpty(id)) {
			return data;
		}

		if (!StringUtil.isEmpty(id)) {
			orderId = Integer.valueOf(id.trim());
			order = orderService.selectByPrimaryKey(orderId);
		}
		if (!StringUtil.isEmpty(orderNo)) {
			BmOrder orderSearchVo = new BmOrder();
			orderSearchVo.setOrderNo(orderNo);
			order = orderService.selectOne(orderSearchVo);
			orderId = order.getOrderId();
		}

		OrderVo vo = orderService.transVo(order);

		// OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
		// attr.setOrderId(id);
		// List<OrderServiceTypeAttr> orderServiceTypeAttrList =
		// orderServiceTypeAttrService.select(attr);
		// vo.setOrderServiceTypeAttrList(orderServiceTypeAttrList);

		OrderResume or = new OrderResume();
		or.setOrderId(orderId);
		List<OrderResume> orderResumeList = orderResumeService.selectBySearchVo(or, "add_time desc");
		vo.setOrderResumeList(orderResumeList);
		String interviewTimeStr = "";
		String interviewAddr = "";

		if (orderResumeList != null && orderResumeList.size() > 0) {
			for (OrderResume o : orderResumeList) {
				if (o.getActionType() == Constants.ORDER_RESUME_ACTION_TYPE_INTEGER) {
					Integer interviewTime = o.getInterviewTime() * 1000;
					interviewTimeStr = TimeStampUtil.timeStampToDateStr(interviewTime.longValue());
					interviewAddr = o.getInterviewAddr();
				}
			}
		}
		vo.setInterviewTimeStr(interviewTimeStr);
		vo.setInterviewAddr(interviewAddr);

		List<Imgs> imgList = new ArrayList<Imgs>();
		// 找出生效的合同信息
		OrderContract orderContractSearchVo = new OrderContract();
		orderContractSearchVo.setOrderId(orderId);
		List<OrderContract> list = orderContractService.selectBySearchVo(orderContractSearchVo, "");
		if (!list.isEmpty()) {
			OrderContract orderContract = list.get(0);
			Integer contractId = orderContract.getId();

			Imgs img = new Imgs();
			img.setOrderId(orderId);
			img.setLinkId(contractId);
			img.setImgType(Constants.IMG_TYPE_ORDER_CONTRACT);
			imgList = imgService.selectBySearchVo(img, "add_time desc");
		}

		vo.setImgList(imgList);

		if (sceretPhone == 1) {
			String m = vo.getMobile();
			StringBuffer bf = new StringBuffer();
			bf.append(m.substring(0, 2)).append("*****").append(m.substring(8));
			vo.setMobile(bf.toString());
		}

		data.setData(vo);
		return data;
	}

	/**
	 * 订单详情
	 * 
	 * @param orderId
	 *            订单id
	 * @param sceretPhone
	 *            手机号是否标星 0 = 否 1= 是
	 * @return orderVo
	 * 
	 */
	@RequestMapping(value = "/api/bm/order/baseinfo.json", method = RequestMethod.GET)
	public AppResultData<Object> bmOrderBaseInfo(@RequestParam("orderNo") String orderNo) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		Integer orderId = 0;
		BmOrder order = null;

		BmOrder orderSearchVo = new BmOrder();
		orderSearchVo.setOrderNo(orderNo);
		order = orderService.selectOne(orderSearchVo);
		orderId = order.getOrderId();

		OrderBaseVo vo = orderService.getBaseInfo(order);

		data.setData(vo);
		return data;
	}

	/**
	 * 服务人员订单列表接口
	 * 
	 * @param resumeId
	 *            服务人员id
	 * @param pageNo
	 *            页数
	 * @param pageSize
	 *            分页大小
	 */
	// @RequestMapping(value="/api/bm/order/po-list.json",method=RequestMethod.GET)
	// public AppResultData<Object> poOrderList(
	// @RequestParam("resumeId") Integer resumeId,
	// @RequestParam("pageNo") Integer pageNo,
	// @RequestParam(value = "pageSize",required = false,defaultValue="10") Integer
	// pageSize){
	// AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,
	// "ok", "");
	//
	// OrderResume orderResume = new OrderResume();
	// orderResume.setResumeId(resumeId);
	// PageInfo<?> page = orderResumeService.selectByListPage(orderResume, pageNo,
	// pageSize, "add_time desc");
	// List list = page.getList();
	//
	// List<Integer> orderIdList = new ArrayList<>();
	// for(int i=0,len=list.size();i<len;i++) {
	// OrderResume r =(OrderResume) list.get(i);
	// orderIdList.add(r.getOrderId());
	// }
	//
	// String str = orderIdList.toString();
	// OrderVo orderVo = new OrderVo();
	// orderVo.setOrderIds(str.substring(1,str.length()-1));
	// List<BmOrder> orderList = orderService.selectByOrderVoDefined(orderVo);
	//
	// page.setList(list);
	// data.setData(page);
	//
	// return data;
	// }

	/**
	 * 更新订单的发布状态
	 * 
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "/api/bm/order/updatePublish.json", method = RequestMethod.POST)
	public AppResultData<Object> updatePublish(@RequestParam(value = "orderId") Integer id) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		BmOrder order = orderService.selectByPrimaryKey(id);
		order.setPublish(1);
		int update = orderService.update(order);
		data.setData(update);
		return data;
	}

	/**
	 * 更新销售老师
	 * 
	 * @param orderId
	 * @param saleId
	 * @param saleMobile
	 * @return
	 */
	@RequestMapping(value = "/api/bm/order/updateSale.json", method = RequestMethod.POST)
	public AppResultData<Object> updateSale(@RequestParam(value = "orderId") Integer id,
			@RequestParam(value = "saleId") Integer saleId, @RequestParam(value = "saleMobile") String saleMobile) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		BmOrder order = orderService.selectByPrimaryKey(id);
		order.setSaleId(saleId);
		order.setSaleMobile(saleMobile);
		int update = orderService.update(order);
		data.setData(update);
		return data;
	}

}

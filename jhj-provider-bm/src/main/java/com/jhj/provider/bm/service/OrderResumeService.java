package com.jhj.provider.bm.service;

import java.util.List;

import com.jhj.provider.bm.po.b.model.OrderResume;
import com.jhj.provider.bm.vo.OrderResumeVo;

public interface OrderResumeService extends BaseService<OrderResume>{

	OrderResumeVo getVo(OrderResume item);

	List<OrderResume> selectBySearchVoSelf(OrderResume searchVo, String orderBy);

}

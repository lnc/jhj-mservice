package com.jhj.provider.bm.po.b.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "sale")
public class Sale {
    @Id
    @GeneratedValue(generator="JDBC")
    @Column(name = "sale_id")
    private Integer saleId;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 姓名
     */
    private String name;

    /**
     * 用户名
     */
    private String username;

    /**
     * md5
     */
    private String passwd;

    /**
     * 微信绑定信息
     */
    private String openid;

    /**
     * 签约成功总金额
     */
    @Column(name = "total_sale")
    private BigDecimal totalSale;

    /**
     * 级别   1 = 白金  2 = 金牌 3 = 银牌 4 = 普通
     */
    private Integer level;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;
    
    /**
     * 状态：1=在职 正常接单  2=暂停接单 3 = 请假
     */
    @Column(name = "status")
    private Integer status;
    
    /**
     * 头像
     */
    @Column(name = "head_img")
    private String headImg;

    
    /**
     * 暂停接单的备注信息
     */
    @Column(name = "remark")
    private String remark;
    
    
    
    /**
     * @return sale_id
     */
    public Integer getSaleId() {
        return saleId;
    }

    /**
     * @param saleId
     */
    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取姓名
     *
     * @return name - 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取用户名
     *
     * @return username - 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     *
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取md5
     *
     * @return passwd - md5
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * 设置md5
     *
     * @param passwd md5
     */
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    /**
     * 获取微信绑定信息
     *
     * @return openid - 微信绑定信息
     */
    public String getOpenid() {
        return openid;
    }

    /**
     * 设置微信绑定信息
     *
     * @param openid 微信绑定信息
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }

    /**
     * 获取签约成功总金额
     *
     * @return total_sale - 签约成功总金额
     */
    public BigDecimal getTotalSale() {
        return totalSale;
    }

    /**
     * 设置签约成功总金额
     *
     * @param totalSale 签约成功总金额
     */
    public void setTotalSale(BigDecimal totalSale) {
        this.totalSale = totalSale;
    }

    /**
     * 获取级别   1 = 白金  2 = 金牌 3 = 银牌 4 = 普通
     *
     * @return level - 级别   1 = 白金  2 = 金牌 3 = 银牌 4 = 普通
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 设置级别   1 = 白金  2 = 金牌 3 = 银牌 4 = 普通
     *
     * @param level 级别   1 = 白金  2 = 金牌 3 = 银牌 4 = 普通
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
    
}
package com.jhj.provider.bm.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.bm.service.BmOrderService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.utils.AppResultData;

@RestController
public class StatOrderCountController {
	
	@Autowired
	private BmOrderService orderService;
	
	/**
	 * 订单分类数量统计
	 * */
	@RequestMapping(value="/api/bm/chart/order-service-type.json",method=RequestMethod.GET)
	public AppResultData<Object> countOrderServiceType(ChartSearchVo vo){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		List<Map<String, String>> countOrderNumGbServiceType = orderService.countOrderNumGbServiceType(vo);
		data.setData(countOrderNumGbServiceType);
		
		return data;
	}

}

package com.jhj.provider.bm.po.b.mapper;

import java.util.List;
import java.util.Map;

import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.provider.bm.vo.OrderVo;

import tk.mybatis.mapper.common.Mapper;

public interface BmOrderMapper extends Mapper<BmOrder> {
	
	/**
	 * 订单分类数量统计
	 * */
	List<Map<String, String>> countOrderNumGbServiceType(ChartSearchVo vo);

	List<Map<Integer, Integer>> countOrderSale(OrderVo searchVo);
}
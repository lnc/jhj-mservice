package com.jhj.provider.bm.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.bm.po.b.model.OrderServiceType;
import com.jhj.provider.bm.service.OrderServiceTypeService;

@Service
public class OrderServiceTypeServiceImpl extends BaseServiceImpl<OrderServiceType> implements OrderServiceTypeService {


}

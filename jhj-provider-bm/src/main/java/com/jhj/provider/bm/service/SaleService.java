package com.jhj.provider.bm.service;

import java.util.HashMap;
import java.util.List;

import com.jhj.provider.bm.po.b.model.Sale;
import com.jhj.provider.bm.vo.SaleVo;

public interface SaleService extends BaseService<Sale>{
	
	/**
	 * 自定义查询分页
	 * 
	 * */
	List<Sale> selectByDefinedListPage(SaleVo  vo,Integer pageNo, Integer pageSize);
	
	HashMap<String, Object> orderDispatch(Integer saleId, String saleMobile, Integer orderId, Integer addFrom);

	int getSaleLevel(Integer saleId);

}

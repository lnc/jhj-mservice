package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "bm_order")
public class BmOrder {
    /**
     * 订单id
     */
    @Id
    @Column(name = "order_id")
    @GeneratedValue(generator = "JDBC")
    private Integer orderId;

    /**
     * 订单编号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 用户手机号
     */
    private String mobile;

    /**
     * 用户称呼
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 销售老师
     */
    @Column(name = "sale_id")
    private Integer saleId;

    /**
     * 老师手机号
     */
    @Column(name = "sale_mobile")
    private String saleMobile;

    /**
     * 是否住家 0=不在，1=住家
     */
    @Column(name = "work_home")
    private Integer workHome;

    /**
     * 服务城市
     */
    @Column(name = "city_id")
    private Integer cityId;

    /**
     * 服务区县，
     */
    @Column(name = "region_id")
    private Integer regionId;

    /**
     * 订单状态  0 = 人工关闭  1 = 已预约  2=  匹配阿姨  3 = 安排面试  4 = 签约成功
     */
    @Column(name = "order_status")
    private Integer orderStatus;

    /**
     * 用户下单经纬度
     */
    private String lat;

    /**
     * 用户下单经纬度
     */
    private String lng;

    /**
     * 备注
     */
    private String remark;

    /**
     * 订单来源  0 = 用户下单  1 = 客服 2 = 销售老师下单
     */
    @Column(name = "add_from")
    private Integer addFrom;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 更新时间戳
     */
    @Column(name = "update_time")
    private Integer updateTime;

    /**
     * 工作内容
     */
    @Column(name = "work_content")
    private String workContent;

    /**
     *  0 = 未发布  1 = 发布
     */
    private Integer publish;

    /**
     *   0 = 系统派单  1 = 自主开发
     */
    @Column(name = "order_dispatch")
    private Integer orderDispatch;
    
    /**
     *   派工依据
     */
    @Column(name = "order_dispatch_by")
    private String orderDispatchBy;

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单编号
     *
     * @return order_no - 订单编号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单编号
     *
     * @param orderNo 订单编号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取用户手机号
     *
     * @return mobile - 用户手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置用户手机号
     *
     * @param mobile 用户手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取用户称呼
     *
     * @return user_name - 用户称呼
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户称呼
     *
     * @param userName 用户称呼
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取销售老师
     *
     * @return sale_id - 销售老师
     */
    public Integer getSaleId() {
        return saleId;
    }

    /**
     * 设置销售老师
     *
     * @param saleId 销售老师
     */
    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    /**
     * 获取老师手机号
     *
     * @return sale_mobile - 老师手机号
     */
    public String getSaleMobile() {
        return saleMobile;
    }

    /**
     * 设置老师手机号
     *
     * @param saleMobile 老师手机号
     */
    public void setSaleMobile(String saleMobile) {
        this.saleMobile = saleMobile;
    }

    /**
     * 获取是否住家 0=不在，1=住家
     *
     * @return work_home - 是否住家 0=不在，1=住家
     */
    public Integer getWorkHome() {
        return workHome;
    }

    /**
     * 设置是否住家 0=不在，1=住家
     *
     * @param workHome 是否住家 0=不在，1=住家
     */
    public void setWorkHome(Integer workHome) {
        this.workHome = workHome;
    }

    /**
     * 获取服务城市
     *
     * @return city_id - 服务城市
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * 设置服务城市
     *
     * @param cityId 服务城市
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    /**
     * 获取服务区县，
     *
     * @return region_id - 服务区县，
     */
    public Integer getRegionId() {
        return regionId;
    }

    /**
     * 设置服务区县，
     *
     * @param regionId 服务区县，
     */
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    /**
     * 获取订单状态  0 = 人工关闭  1 = 已预约  2=  匹配阿姨  3 = 安排面试  4 = 签约成功
     *
     * @return order_status - 订单状态  0 = 人工关闭  1 = 已预约  2=  匹配阿姨  3 = 安排面试  4 = 签约成功
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     * 设置订单状态  0 = 人工关闭  1 = 已预约  2=  匹配阿姨  3 = 安排面试  4 = 签约成功
     *
     * @param orderStatus 订单状态  0 = 人工关闭  1 = 已预约  2=  匹配阿姨  3 = 安排面试  4 = 签约成功
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 获取用户下单经纬度
     *
     * @return lat - 用户下单经纬度
     */
    public String getLat() {
        return lat;
    }

    /**
     * 设置用户下单经纬度
     *
     * @param lat 用户下单经纬度
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * 获取用户下单经纬度
     *
     * @return lng - 用户下单经纬度
     */
    public String getLng() {
        return lng;
    }

    /**
     * 设置用户下单经纬度
     *
     * @param lng 用户下单经纬度
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取订单来源  0 = 用户下单  1 = 客服 2 = 销售老师下单
     *
     * @return add_from - 订单来源  0 = 用户下单  1 = 客服 2 = 销售老师下单
     */
    public Integer getAddFrom() {
        return addFrom;
    }

    /**
     * 设置订单来源  0 = 用户下单  1 = 客服 2 = 销售老师下单
     *
     * @param addFrom 订单来源  0 = 用户下单  1 = 客服 2 = 销售老师下单
     */
    public void setAddFrom(Integer addFrom) {
        this.addFrom = addFrom;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间戳
     *
     * @return update_time - 更新时间戳
     */
    public Integer getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间戳
     *
     * @param updateTime 更新时间戳
     */
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取工作内容
     *
     * @return work_content - 工作内容
     */
    public String getWorkContent() {
        return workContent;
    }

    /**
     * 设置工作内容
     *
     * @param workContent 工作内容
     */
    public void setWorkContent(String workContent) {
        this.workContent = workContent;
    }

    /**
     * 获取 0 = 未发布  1 = 发布
     *
     * @return publish -  0 = 未发布  1 = 发布
     */
    public Integer getPublish() {
        return publish;
    }

    /**
     * 设置 0 = 未发布  1 = 发布
     *
     * @param publish  0 = 未发布  1 = 发布
     */
    public void setPublish(Integer publish) {
        this.publish = publish;
    }

    /**
     * 获取  0 = 系统派单  1 = 自主开发
     *
     * @return order_dispatch -   0 = 系统派单  1 = 自主开发
     */
    public Integer getOrderDispatch() {
        return orderDispatch;
    }

    /**
     * 设置  0 = 系统派单  1 = 自主开发
     *
     * @param orderDispatch   0 = 系统派单  1 = 自主开发
     */
    public void setOrderDispatch(Integer orderDispatch) {
        this.orderDispatch = orderDispatch;
    }

	public String getOrderDispatchBy() {
		return orderDispatchBy;
	}

	public void setOrderDispatchBy(String orderDispatchBy) {
		this.orderDispatchBy = orderDispatchBy;
	}
}
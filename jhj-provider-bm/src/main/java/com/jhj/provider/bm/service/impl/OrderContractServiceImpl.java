package com.jhj.provider.bm.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jhj.provider.bm.client.ResumeClient;
import com.jhj.provider.bm.client.vo.ResumeListVo;
import com.jhj.provider.bm.po.b.mapper.OrderContractMapper;
import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.po.b.model.Imgs;
import com.jhj.provider.bm.po.b.model.OrderContract;
import com.jhj.provider.bm.po.b.model.Sale;
import com.jhj.provider.bm.service.ImgService;
import com.jhj.provider.bm.service.OrderContractService;
import com.jhj.provider.bm.service.SaleService;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.provider.bm.vo.OrderContractVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.BeanUtilsExp;
import com.jhj.utils.DateUtil;
import com.jhj.utils.TimeStampUtil;
import com.jhj.utils.sign.JhjSignConfig;
import com.jhj.utils.sign.JhjSignUtils;

@Service
public class OrderContractServiceImpl extends BaseServiceImpl<OrderContract> implements OrderContractService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	protected OrderContractMapper orderContractMapper;
	
	@Autowired
	private SaleService saleService;
	
	@Autowired
	private ImgService imgService;
	
	@Autowired
	public ResumeClient resumeClient;
		
	@Override
	public OrderContractVo getVo(OrderContract item, BmOrder order) {
		OrderContractVo vo = new OrderContractVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(item, vo);
		Integer orderId = vo.getOrderId();
		Integer userId = order.getUserId();
		String userName = order.getUserName();
		String userMobile = order.getMobile();
		vo.setUserId(userId);
		vo.setUserName(userName);
		vo.setUserMobile(userMobile);
		
		String saleName = "";
		String saleMobile = "";
		Integer saleId = order.getSaleId();
		if (saleId != null && saleId > 0) {
			Sale sale = saleService.selectByPrimaryKey(saleId);
			saleName = sale.getName();
			saleMobile = sale.getMobile();
		}
		vo.setSaleName(saleName);
		vo.setSaleMobile(saleMobile);
		
		Integer resumeId = vo.getResumeId();
		String resumeNo = vo.getResumeNo();
		String resumeMobile = vo.getResumeMobile();
		String resumeName = vo.getResumeName();
		
		if (resumeId > 0) {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("resumeNo", resumeNo);
			params.put("resumeMobile", resumeMobile);
			params = JhjSignUtils.getSignParams(params);
			
			String appkey = JhjSignConfig.appkey;
			String oncestr = params.get("oncestr").toString();
			String sign = params.get("sign").toString();
			String headImg = "";
			try {
				AppResultData<ResumeListVo> resumeData = resumeClient.getBaseInfo(resumeNo, resumeMobile, appkey, oncestr, sign);
				ResumeListVo resumeListVo = resumeData.getData();
				headImg = resumeListVo.getHeadImg();
			} catch(Exception e) {
				logger.error(e.getMessage());
			}
			
			vo.setHeadImg(headImg);
		}
		
		vo.setResumeId(resumeId);
		vo.setResumeMobile(resumeMobile);
		vo.setResumeName(resumeName);
		
		//合同图片资源
		Imgs searchVo = new Imgs();
		searchVo.setOrderId(orderId);
		searchVo.setLinkId(vo.getId());
		List<Imgs> imgs = imgService.selectBySearchVo(searchVo, "img_id desc");
		if (imgs == null) imgs = new ArrayList<Imgs>();
		vo.setContractImgs(imgs);
		
		String contractDay = DateUtil.getToday();
		if (vo.getConcatTime() > 0) {			
			Long t = Long.valueOf(vo.getConcatTime()) * 1000;
			contractDay = TimeStampUtil.timeStampToDateStr(t, DateUtil.DEFAULT_PATTERN);
		}
		vo.setContractDay(contractDay);
		return vo;
	}



	@Override
	public List<Map<String, String>> countSaleOrderMoney(ChartSearchVo vo) {
		
		return orderContractMapper.countSaleOrderMoney(vo);
	}



	@Override
	public List<Map<String, String>> countSaleOrderNum(ChartSearchVo vo) {
		
		return orderContractMapper.countSaleOrderMoney(vo);
	}
	
	@Override
	public List<Map<Integer, BigDecimal>> sumSaleOrderPay(ChartSearchVo searchVo) {
		return orderContractMapper.sumSaleOrderPay(searchVo);
	}
}

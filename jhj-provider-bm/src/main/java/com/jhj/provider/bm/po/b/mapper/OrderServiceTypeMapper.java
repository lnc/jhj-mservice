package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.OrderServiceType;

import tk.mybatis.mapper.common.Mapper;

public interface OrderServiceTypeMapper extends Mapper<OrderServiceType> {
}
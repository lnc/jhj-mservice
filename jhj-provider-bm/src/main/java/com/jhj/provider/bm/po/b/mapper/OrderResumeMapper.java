package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.OrderResume;

import tk.mybatis.mapper.common.Mapper;

public interface OrderResumeMapper extends Mapper<OrderResume> {
}
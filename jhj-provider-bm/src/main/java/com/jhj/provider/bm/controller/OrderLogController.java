package com.jhj.provider.bm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.bm.po.b.model.OrderLog;
import com.jhj.provider.bm.service.OrderLogService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.utils.AppResultData;

@RestController
public class OrderLogController {
	
	@Autowired
	private OrderLogService orderLogService;
	
	/**
	 * 根据订单号查询日志
	 * @param orderId 订单id
	 * @param orderNo 订单编号
	 * 
	 * @param list<OrderLog>
	 * */
	@RequestMapping(value="/api/bm/order-log-list", method=RequestMethod.GET)
	public AppResultData<Object> getOrderList(
			@RequestParam(value="orderId") Integer orderId,
			@RequestParam(value="orderNo") String orderNo){
		
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		OrderLog orderLog = new OrderLog();
		orderLog.setOrderId(orderId);
		orderLog.setOrderNo(orderNo);
		List<OrderLog> orderLogList = orderLogService.selectBySearchVo(orderLog, "add_time desc");
		data.setData(orderLogList);
		
		return data;
	}

}

package com.jhj.provider.bm.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.bm.service.OrderContractService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.utils.AppResultData;

@RestController
public class StatSaleOrderCountController {
	
	@Autowired
	private OrderContractService orderContractService;
	
	@RequestMapping(value="/api/bm/chart/stat-sale-order-money.json",method=RequestMethod.GET)
	public AppResultData<Object> countSaleOrderMoney(ChartSearchVo vo){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		List<Map<String, String>> countSaleOrderMoney = orderContractService.countSaleOrderMoney(vo);
		data.setData(countSaleOrderMoney);
		
		return data;
	}
	
	@RequestMapping(value="/api/bm/chart/stat-sale-order-num.json",method=RequestMethod.GET)
	public AppResultData<Object> countSaleOrderNum(ChartSearchVo vo){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		List<Map<String, String>> countSaleOrderNum = orderContractService.countSaleOrderNum(vo);
		data.setData(countSaleOrderNum);
		
		return data;
	}

}

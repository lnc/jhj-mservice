package com.jhj.provider.bm.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.po.b.model.OrderLog;
import com.jhj.provider.bm.po.b.model.OrderResume;
import com.jhj.provider.bm.service.BmOrderService;
import com.jhj.provider.bm.service.OrderLogService;
import com.jhj.provider.bm.service.OrderResumeService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.provider.bm.vo.OrderResumeVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.JacksonUtil;
import com.jhj.utils.StringUtil;
import com.jhj.utils.TimeStampUtil;

@RestController
public class OrderResumeController {

	@Autowired
	private BmOrderService orderService;
	
	@Autowired
	private OrderResumeService orderResumeService;

	@Autowired
	private OrderLogService orderLogService;

	/***
	 * 销售老师推荐阿姨
	 * @param orderId
	 * @param chooseResume   格式为 {resumeId: , resumeNo : , name: , mobile}
	 * @param actionId
	 * @param actionName
	 * @param actionMobile
	 * @throws Exception 
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/bm/order/resume/choose.json", method = RequestMethod.POST)
	public AppResultData<Object> doChoose(
			@RequestParam(value = "orderId") Integer orderId,
			@RequestParam(value = "orderNo") String orderNo,
			@RequestParam(value = "chooseResume") String chooseResume,
			@RequestParam(value = "actionId", required = false, defaultValue = "0") Integer actionId,
			@RequestParam(value = "actionName", required = false, defaultValue = "") String actionName,
			@RequestParam(value = "actionMobile", required = false, defaultValue = "") String actionMobile
			) throws Exception {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		BmOrder order = orderService.selectByPrimaryKey(orderId);
		Integer userId = order.getUserId();
		//插入订单order_resume表
		List<Map> chooseList = JacksonUtil.json2list(chooseResume, Map.class);
		
		if (chooseList.isEmpty()) {
			return result;
		}
		
		int len = chooseList.size();
		for (int i = 0; i < len; i++) {
			Map<String, Object> item = chooseList.get(i);
			
			Integer resumeId = Integer.valueOf(item.get("resumeId").toString());
			String resumeNo = item.get("resumeNo").toString();
			String name = item.get("name").toString();
			String mobile = item.get("mobile").toString();
			
			//先判断是否存在
			OrderResume searchVo = new OrderResume();
			searchVo.setOrderId(orderId);
			searchVo.setResumeId(resumeId);
			OrderResume orderResumeExist = orderResumeService.selectOne(searchVo);
			
			if (orderResumeExist == null) {
				OrderResume orderResume = new OrderResume();
				orderResume.setId(0);
				orderResume.setUserId(userId);
				orderResume.setOrderId(orderId);
				orderResume.setOrderNo(orderNo);
				orderResume.setResumeId(resumeId);
				orderResume.setResumeNo(resumeNo);
				orderResume.setName(name);
				orderResume.setMobile(mobile);
				orderResume.setActionType(Constants.ORDER_RESUME_ACTION_TYPE_CHOOSE);
				orderResume.setInterviewTime(0);
				orderResume.setInterviewAddr("");
				orderResume.setAddTime(TimeStampUtil.getNowSecond().intValue());
				orderResumeService.insert(orderResume);
			}
		}

		// 更新订单状态
		BmOrder orderUpdate = new BmOrder();
		orderUpdate.setOrderId(orderId);
		orderUpdate.setOrderStatus(Constants.ORDER_STATUS_2);
		orderUpdate.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		orderService.update(orderUpdate);

		

		// 记录日志
		OrderLog log1 = new OrderLog();
		log1.setOrderId(orderId);
		log1.setOrderNo(orderNo);
		log1.setLogType("order_resume");
		log1.setLogName("推荐阿姨");
		log1.setActionId(actionId);
		log1.setActionName(actionName);
		log1.setRemark("推荐阿姨");
		log1.setAddTime(TimeStampUtil.getNowSecond().intValue());
		orderLogService.insert(log1);


		return result;
	}
	
	/***
	 * 用户选择面试阿姨接口
	 * @param userId    用户ID
	 * @param mobile		用户手机号
	 * @param orderId	订单ID
	 * @param orderNo   订单编号
	 * @param chooseResume   resumeNo ,逗号隔开
	 * @param unchooseResume resumeNo, 逗号隔开
	 * @param interviewTime	面试时间， 时间戳精确到秒
	 * @param interviewAddr  面试地点.
	 * 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/bm/order/resume/interview.json", method = RequestMethod.POST)
	public AppResultData<Object> interview(
			@RequestParam(value = "userId") Integer userId,
			@RequestParam(value = "mobile") String mobile,
			@RequestParam(value = "orderId") Integer orderId,
			@RequestParam(value = "orderNo") String orderNo,
			@RequestParam(value = "chooseResume") String chooseResume,
			@RequestParam(value = "unchooseResume") String unchooseResume,
			@RequestParam(value = "interviewTime") Integer interviewTime,
			@RequestParam(value = "interviewAddr") String interviewAddr
			) throws Exception {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		OrderResume searchVo = new OrderResume();
		searchVo.setOrderNo(orderNo);
		List<OrderResume> orderResumes = orderResumeService.selectBySearchVo(searchVo, "");
		
		String[] chooseResumeAry = null;
		String[] unchooseResumeAry = null;
		//更新选择阿姨的状态.
		if (!StringUtil.isEmpty(chooseResume)) {
			chooseResumeAry = StringUtil.convertStrToArray(chooseResume);
		}
		
		if (!StringUtil.isEmpty(unchooseResume)) {
			unchooseResumeAry = StringUtil.convertStrToArray(unchooseResume);
		}
		
		for (OrderResume orderResume : orderResumes) {
			
			//选择的阿姨更新状态
			if (chooseResumeAry != null) {
				for (int i = 0 ; i < chooseResumeAry.length; i++) {
					if (StringUtil.isEmpty(chooseResumeAry[i])) continue;
					String chooseResumeNo = chooseResumeAry[i];
					if (chooseResumeNo.equals(orderResume.getResumeNo())) {
						orderResume.setActionType(Constants.ORDER_RESUME_ACTION_TYPE_INTEGER);
						orderResume.setInterviewTime(interviewTime);
						orderResume.setInterviewAddr(interviewAddr);
						orderResumeService.update(orderResume);
					}
				}
			}
			
			//未选择的阿姨也需要更新状态
			if (unchooseResumeAry != null) {
				for (int i = 0 ; i < unchooseResumeAry.length; i++) {
					if (StringUtil.isEmpty(unchooseResumeAry[i])) continue;
					String unchooseResumeNo = unchooseResumeAry[i];
					if (unchooseResumeNo.equals(orderResume.getResumeNo())) {
						orderResume.setActionType(Constants.ORDER_RESUME_ACTION_TYPE_NOT_CHOOSE);
						orderResumeService.update(orderResume);
					}
				}
			}
		}

		// 更新订单状态
		BmOrder order = new BmOrder();
		order.setOrderId(orderId);
		order.setOrderStatus(Constants.ORDER_STATUS_3);
		order.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		orderService.update(order);

		

		// 记录日志
		OrderLog log1 = new OrderLog();
		log1.setOrderId(orderId);
		log1.setOrderNo(orderNo);
		log1.setLogType("order_interview");
		log1.setLogName("选择面试");
		log1.setActionId(userId);
		log1.setActionName(mobile);
		log1.setRemark("用户选择服务人员面试");
		log1.setAddTime(TimeStampUtil.getNowSecond().intValue());
		orderLogService.insert(log1);


		return result;
	}	
	
	
	/**
	 * 用户都不满意推荐的阿姨，重新推荐
	 * @param userId		用户Id
	 * @param mobile		用户手机号
	 * @param orderId	订单ID
	 * @param orderNo	订单编号
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/api/bm/order/resume/notchoose.json", method = RequestMethod.POST)
	public AppResultData<Object> notChoose(
			@RequestParam(value = "userId") Integer userId,
			@RequestParam(value = "mobile") String mobile,
			@RequestParam(value = "orderId") Integer orderId,
			@RequestParam(value = "orderNo") String orderNo
			) throws Exception {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		OrderResume searchVo = new OrderResume();
		searchVo.setOrderNo(orderNo);
		List<OrderResume> orderResumes = orderResumeService.selectBySearchVo(searchVo, "");
		
		for (OrderResume orderResume : orderResumes) {
			orderResume.setActionType(Constants.ORDER_RESUME_ACTION_TYPE_NOT_CHOOSE);
			orderResumeService.update(orderResume);
		}

		// 更新订单状态
		BmOrder order = new BmOrder();
		order.setOrderId(orderId);
		order.setOrderStatus(Constants.ORDER_STATUS_2);
		order.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		orderService.update(order);

		// 记录日志
		OrderLog log1 = new OrderLog();
		log1.setOrderId(orderId);
		log1.setOrderNo(orderNo);
		log1.setLogType("order_resume");
		log1.setLogName("用户未选择人员面试");
		log1.setActionId(userId);
		log1.setActionName("");
		log1.setRemark("用户未选择人员面试");
		log1.setAddTime(TimeStampUtil.getNowSecond().intValue());
		orderLogService.insert(log1);


		return result;
	}		
	
	
	@RequestMapping(value = "/api/bm/order/resume/del.json", method = RequestMethod.POST)
	public AppResultData<Object> delResume(
			@RequestParam(value = "orderId") Integer orderId,
			@RequestParam(value = "orderNo") String orderNo,
			@RequestParam(value = "resumeId") Integer resumeId,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "actionId", required = false, defaultValue = "0") Integer actionId,
			@RequestParam(value = "actionName", required = false, defaultValue = "") String actionName,
			@RequestParam(value = "actionMobile", required = false, defaultValue = "") String actionMobile
			
			) throws Exception {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		OrderResume searchVo = new OrderResume();
		searchVo.setOrderId(orderId);
		searchVo.setResumeId(resumeId);
		
		orderResumeService.delete(searchVo);
		
		// 记录日志
		OrderLog log1 = new OrderLog();
		log1.setOrderId(orderId);
		log1.setOrderNo(orderNo);
		log1.setLogType("order_resume_del");
		log1.setLogName("删除推荐阿姨");
		log1.setActionId(actionId);
		log1.setActionName(actionName);
		log1.setRemark("删除推荐阿姨" + name);
		log1.setAddTime(TimeStampUtil.getNowSecond().intValue());
		orderLogService.insert(log1);

		return result;
	}		

	/**
	 * 获取阿姨信息
	 * 
	 * @param orderId 订单id
	 * @param actionType  支持多个类别查询，逗号隔开
	 * 
	 */
	@RequestMapping(value = "/api/bm/order/resume/get.json", method = RequestMethod.GET)
	public AppResultData<Object> getOrderResumeList(
			@RequestParam(value = "orderId") Integer orderId,
			@RequestParam(value = "actionType", required = false, defaultValue = "") String actionType
			) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		OrderResume searchVo = orderResumeService.initPo(new OrderResume());
		searchVo.setOrderId(orderId);
		
		List<OrderResume> list = orderResumeService.selectBySearchVoSelf(searchVo, "add_time desc");
		List<OrderResumeVo> listVo = new ArrayList<OrderResumeVo>();
		int len = list.size();
		for (int i = 0 ; i < len; i++) {
			OrderResumeVo vo = orderResumeService.getVo(list.get(i));
			listVo.add(vo);
		}
		
		data.setData(listVo);
		return data;
	}
	
	/**
	 * 面试记录
	 * @param resumeNo
	 * @param pageNo
	 * @param pageSize
	 * 
	 * */
	@RequestMapping(value="/api/bm/order/resume/list.json",method=RequestMethod.GET)
	public AppResultData<Object> orderResumeList(
			@RequestParam(value="resumeNo") String resumeNo,
			@RequestParam(value="pageNo",defaultValue="1") Integer pageNo,
			@RequestParam(value="pageSize",defaultValue="10") Integer pageSize){
		
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		OrderResume or = new OrderResume();
		or.setResumeNo(resumeNo);
		PageInfo page = orderResumeService.selectByListPage(or, pageNo, pageSize, "add_time desc");
		List<OrderResume> list = page.getList();
		for (int i = 0; i < list.size(); i++) {
			OrderResume item = list.get(i);
			OrderResumeVo vo = orderResumeService.getVo(item);
			list.set(i, vo);
		}
		page = new PageInfo(list);
		data.setData(page);
		
		return data;
	}

}

package com.jhj.provider.bm.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jhj.provider.bm.client.vo.ResumeListVo;
import com.jhj.utils.AppResultData;

@FeignClient("jhj-provider-resume")
public interface ResumeClient {
	

	/**
	 * 获取基础人才库信息,params为涉及到请求参数签名
	 * @param resumeNo
	 * @param mobile
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/api/resume/baseinfo.json")
	public AppResultData<ResumeListVo> getBaseInfo(@RequestParam("resumeNo") String resumeNo,
			@RequestParam("mobile") String mobile,
			@RequestParam("appkey") String appkey,
			@RequestParam("oncestr") String oncestr,
			@RequestParam("sign") String sign);
	
	/**
	 * 设置人才库状态,params为涉及到请求参数签名
	 * @param resumeNo
	 * @param status
	 * @param actionId
	 * @param actionName
	 * @param actionMobile
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/api/resume/setStatus.json")
	public String setStatus(@RequestParam("resumeNo") String resumeNo,
			@RequestParam("status") Integer status,
			@RequestParam(value = "actionId", required = false, defaultValue = "0") Integer actionId,
			@RequestParam(value = "actionName", required = false, defaultValue = "") String actionName,
			@RequestParam(value = "actionMobile", required = false, defaultValue = "") String actionMobile,
			@RequestParam("appkey") String appkey,
			@RequestParam("oncestr") String oncestr,
			@RequestParam("sign") String sign);
}

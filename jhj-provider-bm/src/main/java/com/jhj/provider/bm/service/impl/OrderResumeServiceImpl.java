package com.jhj.provider.bm.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jhj.provider.bm.client.ResumeClient;
import com.jhj.provider.bm.client.vo.ResumeListVo;
import com.jhj.provider.bm.po.b.model.OrderResume;
import com.jhj.provider.bm.po.b.model.OrderServiceType;
import com.jhj.provider.bm.service.OrderResumeService;
import com.jhj.provider.bm.service.OrderServiceTypeService;
import com.jhj.provider.bm.utils.BmUtil;
import com.jhj.provider.bm.vo.OrderResumeVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.BeanUtilsExp;
import com.jhj.utils.DateUtil;
import com.jhj.utils.StringUtil;
import com.jhj.utils.TimeStampUtil;
import com.jhj.utils.sign.JhjSignConfig;
import com.jhj.utils.sign.JhjSignUtils;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class OrderResumeServiceImpl extends BaseServiceImpl<OrderResume> implements OrderResumeService {
	
	@Autowired
	public ResumeClient resumeClient;
	
	@Autowired
	private OrderServiceTypeService orderServiceTypeService;
	
	@Override
	public OrderResumeVo getVo(OrderResume item) {
		OrderResumeVo vo = new OrderResumeVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(item, vo);
		
		String actionTypeName = BmUtil.getOrderResumeActionType(vo.getActionType());
		
		String resumeNo = vo.getResumeNo();
		String mobile = vo.getMobile();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("resumeNo", resumeNo);
		params.put("mobile", mobile);
		params = JhjSignUtils.getSignParams(params);
		String appkey = JhjSignConfig.appkey;
		String oncestr = params.get("oncestr").toString();
		String sign = params.get("sign").toString();
		
		AppResultData<ResumeListVo> resumeData = resumeClient.getBaseInfo(resumeNo, mobile, appkey, oncestr, sign);
		ResumeListVo resumeListVo = resumeData.getData();
		String headImg = resumeListVo.getHeadImg();
		vo.setHeadImg(headImg);
		
		vo.setActionTypeName(actionTypeName);
		
		String serviceTypeName = "";
		OrderServiceType orderServiceType = new OrderServiceType();
		orderServiceType.setOrderId(vo.getOrderId());
		OrderServiceType type = orderServiceTypeService.selectOne(orderServiceType);
		if (type != null) {
			serviceTypeName = type.getServiceTypeName();
			
		}
		vo.setServiceTypeName(serviceTypeName);
		
		
		String interviewTimeStr = "待定";
		if (vo.getInterviewTime() > 0) {
			interviewTimeStr = TimeStampUtil.timeStampToDateStr(Long.valueOf(vo.getInterviewTime() * 1000), DateUtil.DEFAULT_PATTERN);
		}
		vo.setInterviewTimeStr(interviewTimeStr);
		return vo;
	}
	
	@Override
	public List<OrderResume> selectBySearchVoSelf(OrderResume searchVo, String orderBy) {

		Example example = new Example(OrderResume.class);
		Criteria criteria = example.createCriteria();
		
		if (searchVo.getId() > 0) {
			criteria.andCondition("id = " + searchVo.getId());
		}
		
		if (searchVo.getOrderId() > 0) {
			criteria.andCondition("order_id = " + searchVo.getOrderId());
		}
		
		if (!StringUtil.isEmpty(searchVo.getOrderNo())) {
			criteria.andCondition("order_no = " + searchVo.getOrderNo());
		}
		
		if (searchVo.getResumeId() > 0) {
			criteria.andCondition("resume_id = " + searchVo.getResumeId());
		}
		
		if (!StringUtil.isEmpty(searchVo.getResumeNo())) {
			criteria.andCondition("resume_no = " + searchVo.getResumeNo());
		}
		
		if (!StringUtil.isEmpty(searchVo.getMobile())) {
			criteria.andCondition("mobile = " + searchVo.getMobile());
		}
		
		if (!StringUtil.isEmpty(searchVo.getName())) {
			criteria.andCondition("name = " + searchVo.getName());
		}
		
		if (!StringUtil.isEmpty(searchVo.getActionType())) {
			
			String[] actionTypeAry = StringUtil.convertStrToArray(searchVo.getActionType());
			String actionTypeIn = "";
			for (int i = 0; i < actionTypeAry.length; i++) {
				actionTypeIn+="'" + actionTypeAry[i] + "',";
			}
			actionTypeIn = actionTypeIn.substring(0, actionTypeIn.length()-1);
			criteria.andCondition("action_type in (" + actionTypeIn + ")");
		}

		// 排序
		if (!StringUtil.isEmpty(orderBy)) {
			example.setOrderByClause(orderBy);
		}
				
		List<OrderResume> list = mapper.selectByExample(example);
		return list;
	}	
}

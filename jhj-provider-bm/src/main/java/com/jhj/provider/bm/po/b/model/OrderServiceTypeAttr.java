package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "order_service_type_attr")
public class OrderServiceTypeAttr {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单编号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 服务类型id
     */
    @Column(name = "service_type_id")
    private Integer serviceTypeId;

    /**
     * 属性名称
     */
    @Column(name = "attr_name")
    private String attrName;

    /**
     * 属性标识service_age = 服务对象年龄predicted = 预产期resume_age = 希望保姆年龄work_year = 工作经验addr_id = 地址IDservice_date = 服务时间
     */
    @Column(name = "attr_type")
    private String attrType;

    /**
     * 属性值
     */
    @Column(name = "attr_value")
    private String attrValue;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单编号
     *
     * @return order_no - 订单编号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单编号
     *
     * @param orderNo 订单编号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取服务类型id
     *
     * @return service_type_id - 服务类型id
     */
    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    /**
     * 设置服务类型id
     *
     * @param serviceTypeId 服务类型id
     */
    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    /**
     * 获取属性名称
     *
     * @return attr_name - 属性名称
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * 设置属性名称
     *
     * @param attrName 属性名称
     */
    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    /**
     * 获取属性标识service_age = 服务对象年龄predicted = 预产期resume_age = 希望保姆年龄work_year = 工作经验addr_id = 地址IDservice_date = 服务时间
     *
     * @return attr_type - 属性标识service_age = 服务对象年龄predicted = 预产期resume_age = 希望保姆年龄work_year = 工作经验addr_id = 地址IDservice_date = 服务时间
     */
    public String getAttrType() {
        return attrType;
    }

    /**
     * 设置属性标识service_age = 服务对象年龄predicted = 预产期resume_age = 希望保姆年龄work_year = 工作经验addr_id = 地址IDservice_date = 服务时间
     *
     * @param attrType 属性标识service_age = 服务对象年龄predicted = 预产期resume_age = 希望保姆年龄work_year = 工作经验addr_id = 地址IDservice_date = 服务时间
     */
    public void setAttrType(String attrType) {
        this.attrType = attrType;
    }

    /**
     * 获取属性值
     *
     * @return attr_value - 属性值
     */
    public String getAttrValue() {
        return attrValue;
    }

    /**
     * 设置属性值
     *
     * @param attrValue 属性值
     */
    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
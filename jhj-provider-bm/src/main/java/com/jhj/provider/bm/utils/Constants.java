package com.jhj.provider.bm.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Constants {
	
	//小程序微信配置 -阿姨端配置
	public static String MP_RESUME_APP_ID = "wx6f00f70ea215500d";
	public static String MP_RESUME_APP_SECERT = "20df3180aed615d481f5a924717947ea";
	
	//小程序微信配置 -用户端配置
	public static String MP_USER_APP_ID = "wx2b2a7241d84bc4c5";
	public static String MP_USER_APP_SECERT = "f07928818b4b917891989aa43ece1c08";
	
	public static String URL_ENCODE = "UTF-8";
	public static int PAGE_MAX_NUMBER = 10;
	/**
	 * 验证码最大值
	 */
	public static int CHECK_CODE_MAX_LENGTH = 999999;

	// '0' COMMENT '用户来源 0 = APP 1 = 微网站 2 = 管理后台 3= 到位用户'
	public static int USER_APP = 0;
	public static int USER_NET = 1;
	public static int USER_BACK = 2;
	public static int USER_DAOWAY = 3;


	// 是否使用 0 = 未使用 1= 已使用
	public static short IS_USER_0 = 0;
	public static short IS_USER_1 = 1;

	// 地址默认 默认地址 1 = 默认 0 = 不默认
	public static int ADDRESS_DEFAULT_1 = 1;
	public static int ADDRESS_DEFAULT_NOT_0 = 0;

	// 0 = 发送失败 1 = 发送成功
	public static short SMS_SUCCESS = 1; // 2015-11-24 15:46:10， 发送成功和失败都是 0 ？？？？
	public static short SMS_FAIL = 0;
	public static String SMS_SUCCESS_CODE = "000000";
	public static String SMS_STATUS_CODE = "statusCode";
	public static String SMS_STATUS_MSG = "msg";

	/*
	 * 短信验证码 类型 2016年3月28日12:13:13
	 */
	public static int SMS_TYPE_0 = 0; // app登录
	public static int SMS_TYPE_1 = 1; // app 支付
	public static int SMS_TYPE_2 = 2;
	public static int SMS_TYPE_3 = 3; // 运营平台。 会员充值

	// 短信模板定义
	public static String PAY_SUCCESS_SMS_TEMPLE_ID = "9282";
	// public static String GET_CODE_TEMPLE_ID= "8429";
	public static String GET_CODE_TEMPLE_ID = "30064";
	public static String STAFF_POST_BEGIN = "64740";// 服务人员点击开始服务 64740
	public static String STAFF_POST_DONE = "64744";// 服务人员点击完成服务 64740
	public static String STAFF_JOIN_BLACK = "80275";// 服务人员欠款大于1000元被加入黑名单
	public static String STAFF_OUT_BLACK = "65016";// 服务人员已经被移除黑名单
	public static String GET_USER_VERIFY_ID = "30064";// jhj 用户获取验证码短信模板Id
	public static String GET_AM_EXP_CLEAN_ORDER_ID = "29167";// jhj 助理获得深度保洁订单通知
	public static String GET_USER_NO_PAY_ORDER_ID = "29163";// jhj 用户收到未支付的预约单通知
	public static String GET_CODE_REMIND_ID = "10923";
	public static String GET_CODE_MAX_VALID = "30";// 短信有效时间
	public static String NOTICE_CUSTOMER_Message_ID = "9280";
	public static String NOTICE_STAFF__Message_ID = "15284";
	public static String GET_USER_ORDER_AM_ID = "29161";// 用户预约下单后给助理发短信
	public static String CANCEL_ORDER_SATUS = "cancel";
	public static String AM_NOTICE_CUSTOMER_Message = "64742";// 助理修改助理预约单后给用户发短信
	public static String AM_CLEAN_NOTICE_CUSTOMER_Message = "29160";// 助理修改助理预约单后给用户发短信
	public static String NOTICE_USER_REST_MONEY_NOT_ENOUGH = "34652";
	public static String STAFF_PAY_DEPT_SUCCESS = " 80274"; // 服务人员支付欠款成功
	public static String STAFF_CASE_REQ = "80272"; // 服务人员申请提现
	public static String PAY_SUCCESS_ORDER_SMS = "151019";

	/**
	 * SUCCESS_0 数据成功状态
	 * ERROR_999 数据失败状态
	 * 
	 * */
	public static int SUCCESS_0 = 0;
	public static int ERROR_999 = 999;
	public static int ERROR_100 = 100;
	public static int ERROR_101 = 101;
	public static int ERROR_102 = 102;
	public static int ERROR_103 = 103;
	public static int ERROR_104 = 104;
	
	/***
	 * 
	 * '订单状态  0 = 人工关闭  1 = 已预约  2=  匹配阿姨  3 = 安排面试  4 = 签约成功',
	 * 
	 * */
	public static int ORDER_STATUS_0 = 0;
	public static int ORDER_STATUS_1 = 1;
	public static int ORDER_STATUS_2 = 2;
	public static int ORDER_STATUS_3 = 3;
	public static int ORDER_STATUS_4 = 4;
	public static Map<String,String> orderStatusMap = new HashMap<>();
	static {
		orderStatusMap.put(String.valueOf(ORDER_STATUS_0), "人工关闭");
		orderStatusMap.put(String.valueOf(ORDER_STATUS_1), "已预约");
		orderStatusMap.put(String.valueOf(ORDER_STATUS_2), "匹配阿姨");
		orderStatusMap.put(String.valueOf(ORDER_STATUS_3), "安排面试");
		orderStatusMap.put(String.valueOf(ORDER_STATUS_4), "签约成功");
	}
	
	/**
	 * '订单来源  0 = 用户下单  1 = 客服 2 = 销售老师下单 3= 用户指定阿姨下单'
	 * 
	 * */
	public static int ADD_FROM_0 =0;
	public static int ADD_FROM_1 =1;
	public static int ADD_FROM_2 =2;
	public static int ADD_FROM_3 =3;
	public static Map<String,String> addFromMap = new HashMap<>();
	static {
		addFromMap.put(String.valueOf(ADD_FROM_0), "用户下单");
		addFromMap.put(String.valueOf(ADD_FROM_1), "客服");
		addFromMap.put(String.valueOf(ADD_FROM_2), "销售老师下单");
		addFromMap.put(String.valueOf(ADD_FROM_3), "用户指定阿姨下单");
	}
	
	/**
	 * 销售老师级别 1 = 白金  2 = 金牌 3 = 银牌 4 = 普通',
	 * 
	 * */
	public static Map<String,String> LEVEL = new HashMap<>();
	public static int LEVEL_1 =1;
	public static int LEVEL_2 =2;
	public static int LEVEL_3 =3;
	public static int LEVEL_4 =4;
	static {
		LEVEL.put(String.valueOf(LEVEL_1), "白金");
		LEVEL.put(String.valueOf(LEVEL_2), "金牌");
		LEVEL.put(String.valueOf(LEVEL_3), "银牌");
		LEVEL.put(String.valueOf(LEVEL_4), "普通");
	}
	
	/**
	 * 
	 * 	LEVEL_VALUE_3
	 *  <10000 普通级别
	 *  >10000 and <30000 银牌级别
	 *  >30000 and <50000 金牌级别
	 *  >50000 白金级别
	 * 
	 * */
	public static BigDecimal LEVEL_VALUE_3 = new BigDecimal(10000);
	public static BigDecimal LEVEL_VALUE_2 = new BigDecimal(30000); 
	public static BigDecimal LEVEL_VALUE_1 = new BigDecimal(50000); 
	
	
	//系统订单饱和量，根据级别定义
	public static Integer LEVE_FULL_TOTAL_ORDER_1 = 4;
	public static Integer LEVE_FULL_TOTAL_ORDER_2 = 3;
	public static Integer LEVE_FULL_TOTAL_ORDER_3 = 2;
	public static Integer LEVE_FULL_TOTAL_ORDER_4 = 1;
	
	/**
	 * 推荐阿姨状态  推荐中
	 */
	public static String ORDER_RESUME_ACTION_TYPE_CHOOSE = "choose";
	
	/**
	 * 推荐阿姨状态  选择面试
	 */
	public static String ORDER_RESUME_ACTION_TYPE_INTEGER = "interview";
	
	/**
	 * 推荐阿姨状态  未选中
	 */
	public static String ORDER_RESUME_ACTION_TYPE_NOT_CHOOSE = "unchoose";
	
	/**
	 * 推荐阿姨状态 用户下单指定
	 */
	public static String ORDER_RESUME_ACTION_TYPE_ASSIGN = "assign";
	
	/**
	 * 图片类型 contract 合同图片
	 */
	public static String IMG_TYPE_ORDER_CONTRACT = "contract";
}

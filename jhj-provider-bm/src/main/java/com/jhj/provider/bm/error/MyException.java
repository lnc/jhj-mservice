package com.jhj.provider.bm.error;

/**
 * 统一异常处理
 */
public class MyException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyException(String message) {
        super(message);
    }

}

package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "dict")
public class Dict {
    @Id
    @Column(name = "dict_id")
    @GeneratedValue(generator = "JDBC")
    private Integer dictId;

    /**
     * 父id
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 名称
     */
    private String name;

    /**
     * service_type = 服务类型level = 级别
     */
    @Column(name = "dict_type")
    private String dictType;

    /**
     * 扩展信息 比如级别的金额数据
     */
    private String ext;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * @return dict_id
     */
    public Integer getDictId() {
        return dictId;
    }

    /**
     * @param dictId
     */
    public void setDictId(Integer dictId) {
        this.dictId = dictId;
    }

    /**
     * 获取父id
     *
     * @return parent_id - 父id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置父id
     *
     * @param parentId 父id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取service_type = 服务类型level = 级别
     *
     * @return dict_type - service_type = 服务类型level = 级别
     */
    public String getDictType() {
        return dictType;
    }

    /**
     * 设置service_type = 服务类型level = 级别
     *
     * @param dictType service_type = 服务类型level = 级别
     */
    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    /**
     * 获取扩展信息 比如级别的金额数据
     *
     * @return ext - 扩展信息 比如级别的金额数据
     */
    public String getExt() {
        return ext;
    }

    /**
     * 设置扩展信息 比如级别的金额数据
     *
     * @param ext 扩展信息 比如级别的金额数据
     */
    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
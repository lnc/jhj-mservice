package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "order_log")
public class OrderLog {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * order_add = 订单添加order_update= 订单修改order_interview = 订单匹配阿姨order_contract = 订单签约
     */
    @Column(name = "log_type")
    private String logType;

    @Column(name = "log_name")
    private String logName;

    /**
     * 操作人id
     */
    @Column(name = "action_id")
    private Integer actionId;

    @Column(name = "action_name")
    private String actionName;

    /**
     * 操作说明
     */
    private String remark;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取order_add = 订单添加order_update= 订单修改order_interview = 订单匹配阿姨order_contract = 订单签约
     *
     * @return log_type - order_add = 订单添加order_update= 订单修改order_interview = 订单匹配阿姨order_contract = 订单签约
     */
    public String getLogType() {
        return logType;
    }

    /**
     * 设置order_add = 订单添加order_update= 订单修改order_interview = 订单匹配阿姨order_contract = 订单签约
     *
     * @param logType order_add = 订单添加order_update= 订单修改order_interview = 订单匹配阿姨order_contract = 订单签约
     */
    public void setLogType(String logType) {
        this.logType = logType;
    }

    /**
     * @return log_name
     */
    public String getLogName() {
        return logName;
    }

    /**
     * @param logName
     */
    public void setLogName(String logName) {
        this.logName = logName;
    }

    /**
     * 获取操作人id
     *
     * @return action_id - 操作人id
     */
    public Integer getActionId() {
        return actionId;
    }

    /**
     * 设置操作人id
     *
     * @param actionId 操作人id
     */
    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    /**
     * @return action_name
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * @param actionName
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    /**
     * 获取操作说明
     *
     * @return remark - 操作说明
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置操作说明
     *
     * @param remark 操作说明
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
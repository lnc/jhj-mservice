package com.jhj.provider.bm.client.vo;

import javax.persistence.*;

@Table(name = "resume")
public class Resume {
    /**
     * 主键
     */
    @Id
    @Column(name = "resume_id")
    @GeneratedValue(generator = "JDBC")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 身份证号
     */
    @Column(name = "id_card")
    private String idCard;

    /**
     * 姓名
     */
    private String name;

    /**
     * 鲜活度分数
     */
    @Column(name = "total_score")
    private Integer totalScore;

    /**
     * 总支持人数
     */
    @Column(name = "total_zan")
    private Integer totalZan;

    /**
     * 最后刷新时间
     */
    @Column(name = "refresh_time")
    private Integer refreshTime;

    /**
     * 资料完整度
     */
    @Column(name = "total_complete")
    private Integer totalComplete;

    /**
     * 性别  0 = 未知 1 = 男  2= 女
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 工作经验
     */
    @Column(name = "work_year")
    private Integer workYear;

    /**
     * 户口所在省份
     */
    @Column(name = "province_id")
    private Integer provinceId;

    /**
     * 户口所在城市
     */
    @Column(name = "city_id")
    private Integer cityId;

    /**
     * 期望薪水
     */
    @Column(name = "hope_salary")
    private Integer hopeSalary;

    /**
     * 民族
     */
    @Column(name = "nation_id")
    private Integer nationId;

    /**
     * 学历
     */
    @Column(name = "degree_id")
    private Integer degreeId;

    /**
     * 婚姻状态ID
     */
    @Column(name = "marital_id")
    private Integer maritalId;

    /**
     * 星座
     */
    private Integer astro;

    /**
     * 常用地址
     */
    private String addr;

    /**
     * 自我介绍
     */
    private String remark;
    
    /**
     * 是否有证书
     */
    @Column(name = "has_cert")
    private Integer hasCert;
    

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 更新时间戳
     */
    @Column(name = "update_time")
    private Integer updateTime;

    /**
     * 获取主键
     *
     * @return resume_id - 主键
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置主键
     *
     * @param resumeId 主键
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取身份证号
     *
     * @return id_card - 身份证号
     */
    public String getIdCard() {
        return idCard;
    }

    /**
     * 设置身份证号
     *
     * @param idCard 身份证号
     */
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    /**
     * 获取姓名
     *
     * @return name - 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取鲜活度分数
     *
     * @return total_score - 鲜活度分数
     */
    public Integer getTotalScore() {
        return totalScore;
    }

    /**
     * 设置鲜活度分数
     *
     * @param totalScore 鲜活度分数
     */
    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    /**
     * 获取总支持人数
     *
     * @return total_zan - 总支持人数
     */
    public Integer getTotalZan() {
        return totalZan;
    }

    /**
     * 设置总支持人数
     *
     * @param totalZan 总支持人数
     */
    public void setTotalZan(Integer totalZan) {
        this.totalZan = totalZan;
    }

    /**
     * 获取最后刷新时间
     *
     * @return refresh_time - 最后刷新时间
     */
    public Integer getRefreshTime() {
        return refreshTime;
    }

    /**
     * 设置最后刷新时间
     *
     * @param refreshTime 最后刷新时间
     */
    public void setRefreshTime(Integer refreshTime) {
        this.refreshTime = refreshTime;
    }

    /**
     * 获取资料完整度
     *
     * @return total_complete - 资料完整度
     */
    public Integer getTotalComplete() {
        return totalComplete;
    }

    /**
     * 设置资料完整度
     *
     * @param totalComplete 资料完整度
     */
    public void setTotalComplete(Integer totalComplete) {
        this.totalComplete = totalComplete;
    }

    /**
     * 获取性别  0 = 未知 1 = 男  2= 女
     *
     * @return sex - 性别  0 = 未知 1 = 男  2= 女
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * 设置性别  0 = 未知 1 = 男  2= 女
     *
     * @param sex 性别  0 = 未知 1 = 男  2= 女
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * 获取年龄
     *
     * @return age - 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 设置年龄
     *
     * @param age 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 获取工作经验
     *
     * @return work_year - 工作经验
     */
    public Integer getWorkYear() {
        return workYear;
    }

    /**
     * 设置工作经验
     *
     * @param workYear 工作经验
     */
    public void setWorkYear(Integer workYear) {
        this.workYear = workYear;
    }

    /**
     * 获取户口所在省份
     *
     * @return province_id - 户口所在省份
     */
    public Integer getProvinceId() {
        return provinceId;
    }

    /**
     * 设置户口所在省份
     *
     * @param provinceId 户口所在省份
     */
    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    /**
     * 获取户口所在城市
     *
     * @return city_id - 户口所在城市
     */
    public Integer getCityId() {
        return cityId;
    }

    /**
     * 设置户口所在城市
     *
     * @param cityId 户口所在城市
     */
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    /**
     * 获取期望薪水
     *
     * @return hope_salary - 期望薪水
     */
    public Integer getHopeSalary() {
        return hopeSalary;
    }

    /**
     * 设置期望薪水
     *
     * @param hopeSalary 期望薪水
     */
    public void setHopeSalary(Integer hopeSalary) {
        this.hopeSalary = hopeSalary;
    }

    /**
     * 获取民族
     *
     * @return nation_id - 民族
     */
    public Integer getNationId() {
        return nationId;
    }

    /**
     * 设置民族
     *
     * @param nationId 民族
     */
    public void setNationId(Integer nationId) {
        this.nationId = nationId;
    }

    /**
     * 获取学历
     *
     * @return degree_id - 学历
     */
    public Integer getDegreeId() {
        return degreeId;
    }

    /**
     * 设置学历
     *
     * @param degreeId 学历
     */
    public void setDegreeId(Integer degreeId) {
        this.degreeId = degreeId;
    }

    /**
     * 获取婚姻状态ID
     *
     * @return marital_id - 婚姻状态ID
     */
    public Integer getMaritalId() {
        return maritalId;
    }

    /**
     * 设置婚姻状态ID
     *
     * @param maritalId 婚姻状态ID
     */
    public void setMaritalId(Integer maritalId) {
        this.maritalId = maritalId;
    }

    /**
     * 获取星座
     *
     * @return astro - 星座
     */
    public Integer getAstro() {
        return astro;
    }

    /**
     * 设置星座
     *
     * @param astro 星座
     */
    public void setAstro(Integer astro) {
        this.astro = astro;
    }

    /**
     * 获取常用地址
     *
     * @return addr - 常用地址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置常用地址
     *
     * @param addr 常用地址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取自我介绍
     *
     * @return remark - 自我介绍
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置自我介绍
     *
     * @param remark 自我介绍
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间戳
     *
     * @return update_time - 更新时间戳
     */
    public Integer getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间戳
     *
     * @param updateTime 更新时间戳
     */
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

	public Integer getHasCert() {
		return hasCert;
	}

	public void setHasCert(Integer hasCert) {
		this.hasCert = hasCert;
	}
}
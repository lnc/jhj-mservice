package com.jhj.provider.bm.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.bm.po.b.model.Sale;
import com.jhj.provider.bm.service.SaleService;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.provider.bm.vo.SaleVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;

@RestController
public class SaleController {
	
	@Autowired
	private SaleService saleService;
	
	/**
	 * 销售老师查询接口
	 * @param saleVo
	 * @param pageNo 页数
	 * @param pageSize 页数大小
	 * 
	 * @return list<Sale>
	 * */
	@RequestMapping(value="/api/bm/sale/list.json", method = RequestMethod.GET)
	public AppResultData<Object> getSaleList(
			SaleVo saleVo,
			@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(value = "PageSize", defaultValue = "10") Integer pageSize){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		List<Sale> list = saleService.selectByDefinedListPage(saleVo, pageNo, pageSize);
		PageInfo<?> page = new PageInfo<>(list);
		data.setData(page);
		
		return data;
	}
	
	/**
	 * 销售老师详情接口
	 * @param id 
	 * @return sale
	 * */
	@RequestMapping(value="/api/bm/sale/get.json", method = RequestMethod.GET)
	public AppResultData<Object> getSaleList(
			@RequestParam("id") Integer id){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Sale sale = saleService.selectByPrimaryKey(id);
		data.setData(sale);
		
		return data;
	} 
	
	/**
	 * 销售老师详情接口
	 * @param mobile 
	 * @return sale
	 * */
	@RequestMapping(value="/api/bm/sale/get_bymobile.json", method = RequestMethod.GET)
	public AppResultData<Object> getByMobile(
			@RequestParam("mobile") String mobile){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Sale searchVo = new Sale();
		searchVo.setMobile(mobile);
		Sale sale = saleService.selectOne(searchVo);
		data.setData(sale);
		
		return data;
	} 
	
	/**
	 * 销售老师添加接口
	 * @param id
	 * @param mobile 手机号
	 * @param name 姓名
	 * @param username 用户名
	 * @param password 密码
	 * @param openid openid
	 * @param totalSale 总销售额
	 * @param level 级别
	 * @param headImg 头像
	 * @return map
	 * */
	@RequestMapping(value="/api/bm/sale/save.json", method = RequestMethod.POST)
	public AppResultData<Object> addSale(
			@RequestParam("id") Integer id,
			@RequestParam("mobile") String mobile,
			@RequestParam("name") String name,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam(value = "openid", required = false,defaultValue="") String openid,
			@RequestParam(value = "totalSale",defaultValue="0") BigDecimal totalSale,
			@RequestParam(value = "level",defaultValue="4") Integer level,
			@RequestParam(value = "headImg") String headImg) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Sale s = new Sale();
		s.setMobile(mobile);
		Sale selectOne = saleService.selectOne(s);
		if(selectOne != null) {
			data.setMsg("手机号已注册过！");
			return data;
		}
		
		Sale sale = saleService.initPo(new Sale());
		sale.setSaleId(id);
		sale.setMobile(mobile);
		sale.setName(name);
		sale.setUsername(username);
		sale.setPasswd(password);
		sale.setOpenid(openid);
		sale.setTotalSale(totalSale);
		sale.setLevel(Integer.valueOf(level));
		sale.setAddTime(TimeStampUtil.getNowSecond().intValue());
		sale.setStatus(1);
		sale.setHeadImg(headImg);
		
		Map<String,Object> map = new HashMap<>();
		int insert = saleService.insert(sale);
		map.put("sale", insert);
		data.setMsg("添加成功");
		data.setData(map);
		
		return data;
	}
	
	/**
	 * 销售老师更新接口
	 * @param id
	 * @param mobile 手机号
	 * @param name 姓名
	 * @param username 用户名
	 * @param password 密码
	 * @param openid openid
	 * @param totalSale 总销售额
	 * @param level 级别
	 * @return map
	 * */
	@RequestMapping(value="/api/bm/sale/update.json", method = RequestMethod.POST)
	public AppResultData<Object> updateSale(
			@RequestParam("id") Integer id,
			@RequestParam("mobile") String mobile,
			@RequestParam("name") String name,
			@RequestParam("username") String username,
			@RequestParam("password") String password,
			@RequestParam(value = "openid", required = false,defaultValue="") String openid,
			@RequestParam(value = "totalSale",defaultValue="0") BigDecimal totalSale,
			@RequestParam("level") Integer level) {
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Sale sale = saleService.selectByPrimaryKey(Integer.valueOf(id));
		sale.setMobile(mobile);
		sale.setName(name);
		sale.setUsername(username);
		sale.setPasswd(password);
		sale.setOpenid(openid);
		sale.setTotalSale(totalSale);
		sale.setLevel(level);
		sale.setAddTime(TimeStampUtil.getNowSecond().intValue());
		Map<String,Object> map = new HashMap<>();
		int update = saleService.update(sale);
		map.put("sale", update);
		data.setMsg("修改成功");
		data.setData(map);
		
		return data;
	}

}

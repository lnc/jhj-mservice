package com.jhj.provider.bm.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;
import com.jhj.provider.bm.service.OrderServiceTypeAttrService;

@Service
public class OrderServiceTypeAttrServiceImpl extends BaseServiceImpl<OrderServiceTypeAttr> implements OrderServiceTypeAttrService {

	@Override
	public List<OrderServiceTypeAttr> select(OrderServiceTypeAttr attr) {
		return mapper.select(attr);
	}

	
}

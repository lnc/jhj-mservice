package com.jhj.provider.bm.service;

import java.util.List;

import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;

public interface OrderServiceTypeAttrService extends BaseService<OrderServiceTypeAttr>{
	
	List<OrderServiceTypeAttr> select(OrderServiceTypeAttr attr);
}

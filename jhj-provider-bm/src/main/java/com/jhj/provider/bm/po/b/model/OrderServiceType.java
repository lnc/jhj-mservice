package com.jhj.provider.bm.po.b.model;

import javax.persistence.*;

@Table(name = "order_service_type")
public class OrderServiceType {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 服务类型id
     */
    @Column(name = "service_type_id")
    private Integer serviceTypeId;
    
    /**
     * 服务类型名称
     */
    @Column(name = "service_type_name")
    private String serviceTypeName;

    /**
     * 是否住家
     */
    @Column(name = "work_home")
    private Integer workHome;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取服务类型id
     *
     * @return service_type_id - 服务类型id
     */
    public Integer getServiceTypeId() {
        return serviceTypeId;
    }
    
    /**
     * 获取服务类型名称
     *
     * @return service_type_name - 服务类型name
     */
    public String getServiceTypeName() {
		return serviceTypeName;
	}

    /**
     * 获取服务类型名称
     *
     * @return service_type_name - 服务类型name
     */
	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	/**
     * 设置服务类型id
     *
     * @param serviceTypeId 服务类型id
     */
    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    /**
     * 获取是否住家
     *
     * @return work_home - 是否住家
     */
    public Integer getWorkHome() {
        return workHome;
    }

    /**
     * 设置是否住家
     *
     * @param workHome 是否住家
     */
    public void setWorkHome(Integer workHome) {
        this.workHome = workHome;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
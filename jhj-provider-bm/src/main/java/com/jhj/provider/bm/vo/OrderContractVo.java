package com.jhj.provider.bm.vo;

import java.util.List;

import com.jhj.provider.bm.po.b.model.Imgs;
import com.jhj.provider.bm.po.b.model.OrderContract;

public class OrderContractVo extends OrderContract {
	
	public String contractDay;
	
	public String headImg;
	
	public Integer userId;
	
	public String userName;
	
	public String userMobile;
	
	public String saleName;
	
	public String saleMobile;
	
	public List<Imgs> contractImgs;
	
	
	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public String getSaleMobile() {
		return saleMobile;
	}

	public void setSaleMobile(String saleMobile) {
		this.saleMobile = saleMobile;
	}

	public List<Imgs> getContractImgs() {
		return contractImgs;
	}

	public void setContractImgs(List<Imgs> contractImgs) {
		this.contractImgs = contractImgs;
	}

	public String getContractDay() {
		return contractDay;
	}

	public void setContractDay(String contractDay) {
		this.contractDay = contractDay;
	}
}

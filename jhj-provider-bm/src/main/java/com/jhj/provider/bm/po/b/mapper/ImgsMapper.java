package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.Imgs;

import tk.mybatis.mapper.common.Mapper;

public interface ImgsMapper extends Mapper<Imgs> {
}
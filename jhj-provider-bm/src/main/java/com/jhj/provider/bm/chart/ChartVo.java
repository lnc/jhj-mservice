package com.jhj.provider.bm.chart;

import java.math.BigDecimal;

public class ChartVo {
	
	private String saleId;
	
	private BigDecimal totalPay;
	
	public String getSaleId() {
		return saleId;
	}

	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}

	public BigDecimal getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
}

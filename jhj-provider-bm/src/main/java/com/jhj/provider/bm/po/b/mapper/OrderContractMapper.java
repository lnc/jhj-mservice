package com.jhj.provider.bm.po.b.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.jhj.provider.bm.chart.ChartVo;
import com.jhj.provider.bm.po.b.model.OrderContract;
import com.jhj.provider.bm.vo.ChartSearchVo;

import tk.mybatis.mapper.common.Mapper;

public interface OrderContractMapper extends Mapper<OrderContract> {
	
	List<ChartVo> chartBySaleId(@Param("saleId")Integer saleId,@Param("startTime")Integer startTime,@Param("endTime")Integer endTime);

	/**
	 * 统计销售老师订单金额
	 * 
	 * */
	List<Map<String,String>> countSaleOrderMoney(ChartSearchVo vo);
	
	/**
	 * 统计销售老师订单数量
	 * 
	 * */
	List<Map<String,String>> countSaleOrderNum(ChartSearchVo vo);
	
	
	List<Map<Integer,BigDecimal>> sumSaleOrderPay(ChartSearchVo vo);
}
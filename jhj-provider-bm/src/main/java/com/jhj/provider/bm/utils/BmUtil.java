package com.jhj.provider.bm.utils;

public class BmUtil {

	public static String getOrderResumeActionType(String actionType) {
		String actionTypeName = "";
		switch (actionType) {
		case "choose":
			actionTypeName = "推荐中";
			break;
		case "interview":
			actionTypeName = "面试中";
			break;
		case "unchoose":
			actionTypeName = "未选中";

		case "assign":
			actionTypeName = "用户指定";
			break;
		default:
			actionTypeName = "";
		}
		return actionTypeName;
	}

	public static String getServiceAgeName(String serviceAge) {
		String serviceAgeName = "";
		switch (serviceAge) {
		case "0":
			serviceAgeName = "0-1岁";
			break;
		case "1":
			serviceAgeName = "1-2岁";
			break;
		case "2":
			serviceAgeName = "2-3岁";

		case "3":
			serviceAgeName = "3岁以上";
			break;
		default:
			serviceAgeName = "";
		}
		return serviceAgeName;
	}

	public static Integer getLeveFullOrder(Integer level) {
		Integer num = 0;
		switch (level) {
		case 1:
			num = Constants.LEVE_FULL_TOTAL_ORDER_1;
			break;
		case 2:
			num = Constants.LEVE_FULL_TOTAL_ORDER_2;
			break;
		case 3:
			num = Constants.LEVE_FULL_TOTAL_ORDER_3;
			break;
		case 4:
			num = Constants.LEVE_FULL_TOTAL_ORDER_4;
			break;
		default:
			num = 0;

		}

		return num;
	}

}

package com.jhj.provider.bm.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.bm.po.b.model.OrderLog;
import com.jhj.provider.bm.service.OrderLogService;

@Service
public class OrderLogServiceImpl extends BaseServiceImpl<OrderLog> implements OrderLogService {

}

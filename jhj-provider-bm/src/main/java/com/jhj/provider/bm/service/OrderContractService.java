package com.jhj.provider.bm.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.po.b.model.OrderContract;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.provider.bm.vo.OrderContractVo;

public interface OrderContractService extends BaseService<OrderContract>{
	
	OrderContractVo getVo(OrderContract item, BmOrder order);
	
	/**
	 * 统计销售老师订单金额
	 * 
	 * */
	List<Map<String,String>> countSaleOrderMoney(ChartSearchVo vo);
	
	/**
	 * 统计销售老师订单数量
	 * 
	 * */
	List<Map<String,String>> countSaleOrderNum(ChartSearchVo vo);

	List<Map<Integer, BigDecimal>> sumSaleOrderPay(ChartSearchVo searchVo);
	
}

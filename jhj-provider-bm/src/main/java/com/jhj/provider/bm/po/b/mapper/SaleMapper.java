package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.Sale;
import tk.mybatis.mapper.common.Mapper;

public interface SaleMapper extends Mapper<Sale> {
}
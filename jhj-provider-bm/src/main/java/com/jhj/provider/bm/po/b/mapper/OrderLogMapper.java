package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.OrderLog;

import tk.mybatis.mapper.common.Mapper;

public interface OrderLogMapper extends Mapper<OrderLog> {
}
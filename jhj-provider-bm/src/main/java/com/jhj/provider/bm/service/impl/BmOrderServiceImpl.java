package com.jhj.provider.bm.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.page.PageMethod;
import com.jhj.provider.bm.po.b.mapper.BmOrderMapper;
import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.po.b.model.OrderContract;
import com.jhj.provider.bm.po.b.model.OrderServiceType;
import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;
import com.jhj.provider.bm.po.b.model.Sale;
import com.jhj.provider.bm.service.BmOrderService;
import com.jhj.provider.bm.service.OrderContractService;
import com.jhj.provider.bm.service.OrderServiceTypeAttrService;
import com.jhj.provider.bm.service.OrderServiceTypeService;
import com.jhj.provider.bm.service.SaleService;
import com.jhj.provider.bm.utils.BmUtil;
import com.jhj.provider.bm.utils.Constants;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.provider.bm.vo.OrderBaseVo;
import com.jhj.provider.bm.vo.OrderVo;
import com.jhj.utils.BeanUtilsExp;
import com.jhj.utils.StringUtil;
import com.jhj.utils.TimeStampUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class BmOrderServiceImpl extends BaseServiceImpl<BmOrder> implements BmOrderService {
	
	@Autowired
	protected BmOrderMapper orderMapper;

	@Autowired
	private OrderServiceTypeAttrService orderServiceTypeAttrService;

	@Autowired
	private OrderContractService orderContractService;

	@Autowired
	private SaleService saleService;

	@Autowired
	private OrderServiceTypeService orderServiceTypeService;

	@Override
	public OrderVo transVo(BmOrder o) {
		OrderVo vo = new OrderVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(o, vo);

		Map<String, String> orderStatusMap = Constants.orderStatusMap;
		vo.setOrderStatusName(orderStatusMap.get(String.valueOf(o.getOrderStatus())));

		Map<String, String> addFromMap = Constants.addFromMap;
		vo.setAddFromName(addFromMap.get(String.valueOf(o.getAddFrom())));
		vo.setAddTimeStr(TimeStampUtil.timeStampToDateStr(o.getAddTime().longValue() * 1000));

		OrderServiceTypeAttr attr = new OrderServiceTypeAttr();
		attr.setOrderId(o.getOrderId());
		List<OrderServiceTypeAttr> attrList = orderServiceTypeAttrService.select(attr);
		vo.setOrderServiceTypeAttrList(attrList);
		
		if (attrList != null && attrList.size() > 0) {
			for (int i = 0; i < attrList.size(); i++) {
				OrderServiceTypeAttr t = attrList.get(i);
				
				if (t.getAttrType().equals("serviceAge")) {
					vo.setServiceAge(t.getAttrValue());
					
					String serviceAgeName = "";
					serviceAgeName = BmUtil.getServiceAgeName(t.getAttrValue());
					vo.setServiceAgeName(serviceAgeName);
				}
				
				if (t.getAttrType().equals("resumeAge")) {
					vo.setResumeAge(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("workYear")) {
					vo.setWorkYear(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("predicted")) {
					vo.setPredicted(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("serviceAddr")) {
					vo.setServiceAddr(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("serviceDate")) {
					vo.setServiceDate(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("salary")) {
					vo.setSalary(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("birthPlace")) {
					vo.setBirthPlaceName(t.getAttrValue());
				}
				
				if (t.getAttrType().equals("addr")) {
					vo.setAddr(t.getAttrValue());
				}
			}
		}

		if (o.getSaleId() != null && o.getSaleId() > 0) {
			Sale s = new Sale();
			s.setSaleId(o.getSaleId());
			Sale sale = saleService.selectOne(s);
			if (sale != null) {
				vo.setSaleName(sale.getName());
			}
		}

		if (o.getOrderId() != null && o.getOrderId() > 0) {
			OrderContract oc = new OrderContract();
			oc.setOrderId(o.getOrderId());
			oc.setStatus(1);
			OrderContract orderContract = orderContractService.selectOne(oc);
			if (orderContract != null) {
				vo.setOrderPay(orderContract.getOrderPay());
				vo.setResumeId(orderContract.getResumeId());
				vo.setResumeNo(orderContract.getResumeNo());
				vo.setResumeName(orderContract.getResumeName());
			}
		}

		OrderServiceType orderServiceType = new OrderServiceType();
		orderServiceType.setOrderId(o.getOrderId());
		OrderServiceType type = orderServiceTypeService.selectOne(orderServiceType);
		if (type != null) {
			vo.setServiceTypeId(String.valueOf(type.getServiceTypeId()));
			vo.setServiceTypeName(type.getServiceTypeName());
		}
		
		return vo;
	}
	
	/**
	 * 订单最基础的信息接口（简化的订单订单详情）
	 */
	@Override
	public OrderBaseVo getBaseInfo(BmOrder order) {
		OrderBaseVo vo = new OrderBaseVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(order, vo);

		Map<String, String> orderStatusMap = Constants.orderStatusMap;
		vo.setOrderStatusName(orderStatusMap.get(String.valueOf(order.getOrderStatus())));

		vo.setAddTimeStr(TimeStampUtil.timeStampToDateStr(order.getAddTime().longValue() * 1000));

	
		OrderServiceType orderServiceType = new OrderServiceType();
		orderServiceType.setOrderId(vo.getOrderId());
		OrderServiceType type = orderServiceTypeService.selectOne(orderServiceType);
		if (type != null) {
			vo.setServiceTypeId(String.valueOf(type.getServiceTypeId()));
			vo.setServiceTypeName(type.getServiceTypeName());
		}
		
		return vo;
	}
	
	
	@Override
	public List<BmOrder> selectBySearchVoSelf(OrderVo orderVo) {
		Example ex = this.getSearchExample(orderVo);
		return mapper.selectByExample(ex);
	}
	
//	@Override
	public List<BmOrder> selectBySaleIdAndOrderStatus(List<Integer> saleIdList, String orderStatus) {

		Example ex = new Example(BmOrder.class);
		Criteria createCriteria = ex.createCriteria();

		if (saleIdList != null && saleIdList.size() > 0) {
			String saleIds = saleIdList.toString();
			saleIds = saleIds.substring(1, saleIds.length() - 1);
			createCriteria.andCondition("sale_id in (" + saleIds + ")");
		}
		if (orderStatus != null && !"".equals(orderStatus)) {
			createCriteria.andCondition("order_status in (" + orderStatus + ")");
		}

		return mapper.selectByExample(ex);
	}

	@Override
	public List<BmOrder> selectByOrderVoDefinedListPage(OrderVo orderVo, Integer pageNum, Integer pageSize, String orderBy) {
		PageMethod.startPage(pageNum, pageSize);

		Example ex = this.getSearchExample(orderVo);

		// 排序
		if (!StringUtil.isEmpty(orderBy)) {
			ex.setOrderByClause(orderBy);
		}
		
		return mapper.selectByExample(ex);
	}
	
	private Example getSearchExample(OrderVo orderVo) {
		Example ex = new Example(BmOrder.class);
		Criteria createCriteria = ex.createCriteria();
		
		Integer userId = orderVo.getUserId();
		if (userId != null && userId > 0) {
			createCriteria.andCondition("user_id =" + userId);
		}

		Integer saleId = orderVo.getSaleId();
		if (saleId != null && saleId > 0) {
			createCriteria.andCondition("sale_id =" + saleId);
		}
		
		List<Integer> saleIdList = orderVo.getSaleIdList();
		if (saleIdList != null && saleIdList.size() > 0) {
			String saleIds = saleIdList.toString();
			saleIds = saleIds.substring(1, saleIds.length() - 1);
			createCriteria.andCondition("sale_id in (" + saleIds + ")");
		}

		String saleName = orderVo.getSaleName();
		if (saleName != null && !"".equals(saleName)) {
			createCriteria.andCondition("sale_id in (select sale_id from sale where instr(name, " + saleName + ")>0)");
		}
		
		String saleMobile = orderVo.getSaleMobile();
		if (saleName != null && !"".equals(saleMobile)) {
			createCriteria.andCondition("sale_id in (select sale_id from sale where instr(mobile, " + saleMobile + ")>0)");
		}

		String mobile = orderVo.getMobile();
		if (mobile != null && !"".equals(mobile)) {
			createCriteria.andCondition("mobile = '"+ mobile +"'");
		}

		String orderStatuss = orderVo.getOrderStatuss();
		if (orderStatuss != null && !"".equals(orderStatuss)) {
			createCriteria.andCondition("order_status in(" + orderStatuss + ")");
		}

		String serviceTypeId = orderVo.getServiceTypeId();
		if (serviceTypeId != null && !"".equals(serviceTypeId)) {
			createCriteria.andCondition("order_id in (select order_id from order_service_type where service_type_id="
					+ serviceTypeId + ")");
		}
		String serviceTypeIds = orderVo.getServiceTypeIds();
		if (serviceTypeIds != null && !"".equals(serviceTypeIds)) {
			createCriteria.andCondition("order_id in (select order_id from order_service_type where service_type_id in ("
					+ serviceTypeIds + "))");
		}

		String startAddTimeStr = orderVo.getStartAddTimeStr();
		if (startAddTimeStr != null && !"".equals(startAddTimeStr)) {
			long startAddTime = TimeStampUtil.getMillisOfDayFull(startAddTimeStr + " 00:00:00");
			createCriteria.andCondition("add_time >=" + startAddTime / 1000);
		}

		String endAddTimeStr = orderVo.getEndAddTimeStr();
		if (endAddTimeStr != null && !"".equals(endAddTimeStr)) {
			long endAddTime = TimeStampUtil.getMillisOfDayFull(endAddTimeStr + " 23:59:59");
			createCriteria.andCondition("add_time <=" + endAddTime / 1000);
		}
		
		Integer publish = orderVo.getPublish();
		if(publish!=null) {
			createCriteria.andCondition("publish =" + publish);
		}
		
		Integer orderDispatch = orderVo.getOrderDispatch();
		if(orderDispatch!=null) {
			createCriteria.andCondition("order_dispatch =" + orderDispatch);
		}
		
		return ex;
	}

	@Override
	public List<Map<String, String>> countOrderNumGbServiceType(ChartSearchVo vo) {
		
		return orderMapper.countOrderNumGbServiceType(vo);
	}
	
	
	/**
	 * 订单统计，按照销售老师统计
	 * @param searchVo   
	 * @return
	 * 		total  			: 总订单数
	 * 		totalProcess 	: 正在进行的订单数
	 * 		totalDone 		: 已签约订单数
	 */
	@Override
	public List<Map<Integer, Integer>> countOrderSale(OrderVo searchVo) {
		return orderMapper.countOrderSale(searchVo);
	}
}

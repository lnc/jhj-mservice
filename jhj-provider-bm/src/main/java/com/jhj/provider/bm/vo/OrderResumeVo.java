package com.jhj.provider.bm.vo;

import com.jhj.provider.bm.po.b.model.OrderResume;

public class OrderResumeVo extends OrderResume {
	
	public String headImg;
	
	public String actionTypeName;
	
	private String serviceTypeName;
	
	private String interviewTimeStr;

	public String getActionTypeName() {
		return actionTypeName;
	}

	public void setActionTypeName(String actionTypeName) {
		this.actionTypeName = actionTypeName;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	public String getInterviewTimeStr() {
		return interviewTimeStr;
	}

	public void setInterviewTimeStr(String interviewTimeStr) {
		this.interviewTimeStr = interviewTimeStr;
	}
}

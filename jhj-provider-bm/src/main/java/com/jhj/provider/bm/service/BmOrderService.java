package com.jhj.provider.bm.service;

import java.util.List;
import java.util.Map;

import com.jhj.provider.bm.po.b.model.BmOrder;
import com.jhj.provider.bm.vo.ChartSearchVo;
import com.jhj.provider.bm.vo.OrderBaseVo;
import com.jhj.provider.bm.vo.OrderVo;

public interface BmOrderService extends BaseService<BmOrder>{
	
	OrderVo transVo(BmOrder o);
	
	/**
	 * 销售老师订单状态 order_status>0 and order_status<4
	 * @param saleIdList 销售老师id集合
	 * @param orderStatus 订单状态
	 * */
//	List<BmOrder> selectBySaleIdAndOrderStatus(List<Integer> saleIdList,String orderStatus);

	/**
	 * 自定义分页查询
	 * */
	List<BmOrder> selectByOrderVoDefinedListPage(OrderVo orderVo, Integer pageNum, Integer pageSize, String orderBy);
	
	/**
	 * 订单分类数量统计
	 * */
	List<Map<String,String>> countOrderNumGbServiceType(ChartSearchVo vo);

	OrderBaseVo getBaseInfo(BmOrder order);

	List<BmOrder> selectBySearchVoSelf(OrderVo orderVo);

	List<Map<Integer, Integer>> countOrderSale(OrderVo searchVo);
	
}

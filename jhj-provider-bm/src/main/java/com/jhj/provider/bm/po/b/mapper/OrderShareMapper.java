package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.OrderShare;

import tk.mybatis.mapper.common.Mapper;

public interface OrderShareMapper extends Mapper<OrderShare> {
}
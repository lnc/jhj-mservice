package com.jhj.provider.bm.po.b.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "order_contract")
public class OrderContract {
    @Id
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 销售老师Id
     */
    @Column(name = "sale_id")
    private Integer saleId;

    /**
     * 合同签订时间
     */
    @Column(name = "concat_time")
    private Integer concatTime;

    /**
     * 人才库id
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * 手机号
     */
    @Column(name = "resume_mobile")
    private String resumeMobile;

    /**
     * 姓名
     */
    @Column(name = "resume_name")
    private String resumeName;

    /**
     * 身份证信息
     */
    @Column(name = "id_card")
    private String idCard;

    /**
     * 订单金额
     */
    @Column(name = "order_pay")
    private BigDecimal orderPay;

    /**
     * 服务地址
     */
    private String addr;

    /**
     * 备注
     */
    private String remark;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;
    
    /**
     * 服务费
     */
    @Column(name = "order_fee")
    private BigDecimal orderFee;

    /**
     * 员工工资
     */
    @Column(name = "salary")
    private BigDecimal salary;
    
    /**
     * 单位月
     */
    @Column(name = "contract_end")
    private Integer contractEnd;

    /**
     *  无效 = 0    1 = 有效  （默认）
     */
    private Integer status;

    /**
     * 有效期开始
     */
    @Column(name = "from_date")
    private String fromDate;
    /**
     * 有效期结束
     */
    @Column(name = "to_date")
    private String toDate;
    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取销售老师Id
     *
     * @return sale_id - 销售老师Id
     */
    public Integer getSaleId() {
        return saleId;
    }

    /**
     * 设置销售老师Id
     *
     * @param saleId 销售老师Id
     */
    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    /**
     * 获取合同签订时间
     *
     * @return concat_time - 合同签订时间
     */
    public Integer getConcatTime() {
        return concatTime;
    }

    /**
     * 设置合同签订时间
     *
     * @param concatTime 合同签订时间
     */
    public void setConcatTime(Integer concatTime) {
        this.concatTime = concatTime;
    }

    /**
     * 获取人才库id
     *
     * @return resume_id - 人才库id
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才库id
     *
     * @param resumeId 人才库id
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取手机号
     *
     * @return resume_mobile - 手机号
     */
    public String getResumeMobile() {
        return resumeMobile;
    }

    /**
     * 设置手机号
     *
     * @param resumeMobile 手机号
     */
    public void setResumeMobile(String resumeMobile) {
        this.resumeMobile = resumeMobile;
    }

    /**
     * 获取姓名
     *
     * @return resume_name - 姓名
     */
    public String getResumeName() {
        return resumeName;
    }

    /**
     * 设置姓名
     *
     * @param resumeName 姓名
     */
    public void setResumeName(String resumeName) {
        this.resumeName = resumeName;
    }

    /**
     * 获取身份证信息
     *
     * @return id_card - 身份证信息
     */
    public String getIdCard() {
        return idCard;
    }

    /**
     * 设置身份证信息
     *
     * @param idCard 身份证信息
     */
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    /**
     * 获取订单金额
     *
     * @return order_pay - 订单金额
     */
    public BigDecimal getOrderPay() {
        return orderPay;
    }

    /**
     * 设置订单金额
     *
     * @param orderPay 订单金额
     */
    public void setOrderPay(BigDecimal orderPay) {
        this.orderPay = orderPay;
    }

    /**
     * 获取服务地址
     *
     * @return addr - 服务地址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置服务地址
     *
     * @param addr 服务地址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * @return order_fee
     */
    public BigDecimal getOrderFee() {
        return orderFee;
    }

    /**
     * @param orderFee
     */
    public void setOrderFee(BigDecimal orderFee) {
        this.orderFee = orderFee;
    }

    /**
     * 获取单位月
     *
     * @return contract_end - 单位月
     */
    public Integer getContractEnd() {
        return contractEnd;
    }

    /**
     * 设置单位月
     *
     * @param contractEnd 单位月
     */
    public void setContractEnd(Integer contractEnd) {
        this.contractEnd = contractEnd;
    }

    /**
     * 获取 无效 = 0    1 = 有效  （默认）
     *
     * @return status -  无效 = 0    1 = 有效  （默认）
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置 无效 = 0    1 = 有效  （默认）
     *
     * @param status  无效 = 0    1 = 有效  （默认）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	

	
	
}
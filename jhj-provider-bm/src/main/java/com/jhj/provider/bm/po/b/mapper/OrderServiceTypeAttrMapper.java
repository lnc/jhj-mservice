package com.jhj.provider.bm.po.b.mapper;

import com.jhj.provider.bm.po.b.model.OrderServiceTypeAttr;
import tk.mybatis.mapper.common.Mapper;

public interface OrderServiceTypeAttrMapper extends Mapper<OrderServiceTypeAttr> {
}
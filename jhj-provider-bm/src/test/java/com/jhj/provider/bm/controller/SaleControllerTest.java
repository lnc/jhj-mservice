package com.jhj.provider.bm.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.jhj.provider.bm.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class, MockServletContext.class })
@WebAppConfiguration()
public class SaleControllerTest {
	
	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(
				new HelloController()).build();
	}
	
	@Test
	public void getSaleList() throws Exception {
		String url = "/api/bm/sale/list.json";

		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		RequestBuilder e = get(url);
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);
	}
	
	@Test
	public void saveSale() throws Exception {
		String url = "/api/bm/sale/add.json";

		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("id", "1");
		params.add("mobile", "15201023689");
		params.add("name", "张三");
		params.add("password", "123456");
		params.add("openid", "");
		params.add("totalSale", "11");
		params.add("level", "1");
		RequestBuilder e = post(url).params(params);
		
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);
	}

}

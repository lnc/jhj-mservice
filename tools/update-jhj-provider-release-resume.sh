cd /data/src/jhj-mservice/
git reset --hard HEAD
git pull

sed -i "s#^spring.profiles.active=.*#spring.profiles.active=production#g"  /data/src/jhj-mservice/jhj-provider-resume/src/main/resources/application.properties

mvn clean package
sleep 1
\cp -rf /data/src/jhj-mservice/jhj-provider-resume/target/jhj-provider-resume.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-provider-resume.sh /data/api-server/
chmod +x /data/api-server/*.sh

supervisorctl restart jhj-provider-resume
sleep 5






#!/bin/sh
#
#
cd /data/src/jhj-mservice/
git reset --hard HEAD
git pull
APP_DIR=/data/api-server/
APP_NAME=jhj-provider-user
APP_CONF=$APP_DIR/application.properties

$APP_DIR/jhj-provider-user.sh stop
$APP_DIR/jhj-provider-resume.sh stop
$APP_DIR/jhj-provider-bm.sh stop
$APP_DIR/jhj-api-gateway.sh stop
$APP_DIR/jhj-eureka-server.sh stop
sleep 1

$APP_DIR/jhj-eureka-server.sh start
sleep 1
$APP_DIR/jhj-api-gateway.sh start
sleep 1
$APP_DIR/jhj-provider-bm.sh start
sleep 1
$APP_DIR/jhj-provider-resume.sh start
sleep 1
$APP_DIR/jhj-provider-user.sh start
sleep 1
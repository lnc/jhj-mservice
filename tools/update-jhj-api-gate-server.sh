cd /data/src/jhj-mservice/
git reset --hard HEAD
git pull
mvn clean package
sleep 1
\cp -rf /data/src/jhj-mservice/jhj-api-gateway/target/jhj-api-gateway.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-api-gateway.sh /data/api-server/
chmod +x /data/api-server/*.sh

supervisorctl restart jhj-api-gateway
sleep 5


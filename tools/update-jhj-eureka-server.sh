cd /data/src/jhj-mservice/
git reset --hard HEAD
git pull
mvn clean package

sleep 1
\cp -rf /data/src/jhj-mservice/jhj-eureka-server/target/jhj-eureka-server.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-eureka-server.sh /data/api-server/
chmod +x /data/api-server/*.sh

supervisorctl restart jhj-eureka-server
sleep 5


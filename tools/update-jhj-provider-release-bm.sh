cd /data/src/jhj-mservice/
git reset --hard HEAD
git pull

sed -i "s#^spring.profiles.active=.*#spring.profiles.active=production#g"  /data/src/jhj-mservice/jhj-provider-bm/src/main/resources/application.properties

mvn clean package
sleep 1
\cp -rf /data/src/jhj-mservice/jhj-provider-bm/target/jhj-provider-bm.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-provider-bm.sh /data/api-server/
chmod +x /data/api-server/*.sh

supervisorctl restart jhj-provider-bm
sleep 5






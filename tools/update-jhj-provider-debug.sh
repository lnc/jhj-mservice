cd /data/src/jhj-mservice/
git reset --hard HEAD
git pull
mvn clean package
sleep 1
#jhj-proivder-user

\cp -rf /data/src/jhj-mservice/jhj-provider-user/target/jhj-provider-user.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-provider-user.sh /data/api-server/
chmod +x /data/api-server/*.sh

#jhj-provider-resume
\cp -rf /data/src/jhj-mservice/jhj-provider-resume/target/jhj-provider-resume.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-provider-resume.sh /data/api-server/
chmod +x /data/api-server/*.sh

#jhj-provider-bm
\cp -rf /data/src/jhj-mservice/jhj-provider-bm/target/jhj-provider-bm.jar /data/api-server/

\cp -rf /data/src/jhj-mservice/tools/jhj-provider-bm.sh /data/api-server/
chmod +x /data/api-server/*.sh


supervisorctl restart jhj-provider-user
sleep 5

supervisorctl restart jhj-provider-bm
sleep 5

supervisorctl restart jhj-provider-resume
sleep 5



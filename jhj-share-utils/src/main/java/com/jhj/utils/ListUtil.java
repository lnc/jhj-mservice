package com.jhj.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ListUtil {

	/**
	 * 对list的元素按照多个属性名称排序,
	 * list元素的属性可以是数字（byte、short、int、long、float、double等，支持正数、负数、0）、char、String、
	 * java.util.Date
	 * 
	 * 
	 * @param lsit
	 * @param sortname
	 *            list元素的属性名称
	 * @param isAsc
	 *            true升序，false降序
	 */
	public static <E> void sort(List<E> list, final boolean isAsc, final String... sortnameArr) {
		Collections.sort(list, new Comparator<E>() {

			public int compare(E a, E b) {
				int ret = 0;
				try {
					for (int i = 0; i < sortnameArr.length; i++) {
						ret = ListUtil.compareObject(sortnameArr[i], isAsc, a, b);
						if (0 != ret) {
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return ret;
			}
		});
	}

	/**
	 * 给list的每个属性都指定是升序还是降序
	 * 
	 * @param list
	 * @param sortnameArr
	 *            参数数组
	 * @param typeArr
	 *            每个属性对应的升降序数组， true升序，false降序
	 */

	public static <E> void sort(List<E> list, final String[] sortnameArr, final boolean[] typeArr) {
		if (sortnameArr.length != typeArr.length) {
			throw new RuntimeException("属性数组元素个数和升降序数组元素个数不相等");
		}
		Collections.sort(list, new Comparator<E>() {
			public int compare(E a, E b) {
				int ret = 0;
				try {
					for (int i = 0; i < sortnameArr.length; i++) {
						ret = ListUtil.compareObject(sortnameArr[i], typeArr[i], a, b);
						if (0 != ret) {
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return ret;
			}
		});
	}

	/**
	 * 对2个对象按照指定属性名称进行排序
	 * 
	 * @param sortname
	 *            属性名称
	 * @param isAsc
	 *            true升序，false降序
	 * @param a
	 * @param b
	 * @return
	 * @throws Exception
	 */
	private static <E> int compareObject(final String sortname, final boolean isAsc, E a, E b) throws Exception {
		int ret;
		Object value1 = ListUtil.forceGetFieldValue(a, sortname);
		Object value2 = ListUtil.forceGetFieldValue(b, sortname);
		String str1 = value1.toString();
		String str2 = value2.toString();
		if (value1 instanceof Number && value2 instanceof Number) {
			int maxlen = Math.max(str1.length(), str2.length());
			str1 = ListUtil.addZero2Str((Number) value1, maxlen);
			str2 = ListUtil.addZero2Str((Number) value2, maxlen);
		} else if (value1 instanceof Date && value2 instanceof Date) {
			long time1 = ((Date) value1).getTime();
			long time2 = ((Date) value2).getTime();
			int maxlen = Long.toString(Math.max(time1, time2)).length();
			str1 = ListUtil.addZero2Str(time1, maxlen);
			str2 = ListUtil.addZero2Str(time2, maxlen);
		}
		if (isAsc) {
			ret = str1.compareTo(str2);
		} else {
			ret = str2.compareTo(str1);
		}
		return ret;
	}

	/**
	 * 给数字对象按照指定长度在左侧补0.
	 * 
	 * 使用案例: addZero2Str(11,4) 返回 "0011", addZero2Str(-18,6)返回 "-000018"
	 * 
	 * @param numObj
	 *            数字对象
	 * @param length
	 *            指定的长度
	 * @return
	 */
	public static String addZero2Str(Number numObj, int length) {
		NumberFormat nf = NumberFormat.getInstance();
		// 设置是否使用分组
		nf.setGroupingUsed(false);
		// 设置最大整数位数
		nf.setMaximumIntegerDigits(length);
		// 设置最小整数位数
		nf.setMinimumIntegerDigits(length);
		return nf.format(numObj);
	}

	/**
	 * 获取指定对象的指定属性值（去除private,protected的限制）
	 * 
	 * @param obj
	 *            属性名称所在的对象
	 * @param fieldName
	 *            属性名称
	 * @return
	 * @throws Exception
	 */
	public static Object forceGetFieldValue(Object obj, String fieldName) throws Exception {
		Field field = obj.getClass().getDeclaredField(fieldName);
		Object object = null;
		boolean accessible = field.isAccessible();
		if (!accessible) {
			// 如果是private,protected修饰的属性，需要修改为可以访问的
			field.setAccessible(true);
			object = field.get(obj);
			// 还原private,protected属性的访问性质
			field.setAccessible(accessible);
			return object;
		}
		object = field.get(obj);
		return object;
	}

	/**
	 * Create a new list which contains the specified number of elements from
	 * the source list, in a
	 * random order but without repetitions.
	 *
	 * @param sourceList
	 *            the list from which to extract the elements.
	 * @param itemsToSelect
	 *            the number of items to select
	 * @param random
	 *            the random number generator to use
	 * @return a new list containg the randomly selected elements
	 */
	public static <T> List<T> chooseRandomly(List<T> sourceList, int n) {
		Random random = new Random();
		int sourceSize = sourceList.size();

		if (sourceSize < n)
			n = sourceSize;
		// Generate an array representing the element to select from 0... number
		// of available
		// elements after previous elements have been selected.
		int[] selections = new int[n];

		// Simultaneously use the select indices table to generate the new
		// result array
		ArrayList<T> resultArray = new ArrayList<T>();

		for (int count = 0; count < n; count++) {

			// An element from the elements *not yet chosen* is selected
			int selection = random.nextInt(sourceSize - count);
			selections[count] = selection;
			// Store original selection in the original range 0.. number of
			// available elements

			// This selection is converted into actual array space by iterating
			// through the elements
			// already chosen.
			for (int scanIdx = count - 1; scanIdx >= 0; scanIdx--) {
				if (selection >= selections[scanIdx]) {
					selection++;
				}
			}
			// When the first selected element record is reached all selections
			// are in the range
			// 0.. number of available elements, and free of collisions with
			// previous entries.

			// Write the actual array entry to the results
			resultArray.add(sourceList.get(selection));
		}
		return resultArray;
	}
	
	//列表深度拷贝
	public static <T> List<T> deepCopy(List<T> src) {  
	   
		try {
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();  
			ObjectOutputStream out;
			out = new ObjectOutputStream(byteOut);
		
			out.writeObject(src);  

			ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());  
			ObjectInputStream in = new ObjectInputStream(byteIn);  
			@SuppressWarnings("unchecked")  
			List<T> dest = (List<T>) in.readObject();  
			return dest; 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return src; 
		}  
	    return src;  
	}
	
	public static String getTimeList(List<Map<String, String>> datas) {
		System.out.println(GsonUtil.GsonString(datas));
		String timeList = "";
		int len = datas.size();
		for (int i = 0; i < len; i++) {
			String isFull = "";
			Map<String, String> item = datas.get(i);
			
			isFull = item.get("is_full").toString();
			System.out.println(item.get("service_hour").toString() + "--" + isFull);
			timeList+=isFull;
			System.out.println(timeList);
		}
		return timeList;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
//
//		for (int i = 0; i < 1; i++) {
//			Map<String, String> item = new HashMap<String, String>();
//			item.put("aaa", String.valueOf(i));
//			item.put("aaa", String.valueOf(i));
//			list.add(item);
//		}
//
//		List<Map<String, String>> randList = ListUtil.chooseRandomly(list, 2);
//		for (Map item : randList) {
//			System.out.println(item.get("aaa"));
//		}
		
		String json = "[{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"00:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"00:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"01:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"01:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"02:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"02:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"03:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"03:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"04:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"04:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"05:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"05:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"06:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"06:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[117]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"0\",\"total_dispatched\":\"0\",\"is_real_full\":\"0\",\"service_hour\":\"07:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[117]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"0\",\"total_dispatched\":\"0\",\"is_real_full\":\"0\",\"service_hour\":\"07:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 238, 207, 205, 174, 117, 24]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"215,226,302\",\"is_full\":\"0\",\"total_dispatched\":\"3\",\"is_real_full\":\"0\",\"service_hour\":\"08:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 238, 207, 205, 174, 24]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,215,226,302,352\",\"is_full\":\"0\",\"total_dispatched\":\"5\",\"is_real_full\":\"0\",\"service_hour\":\"08:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,215,226,280,302,308,325,328,333,352\",\"is_full\":\"0\",\"total_dispatched\":\"11\",\"is_real_full\":\"0\",\"service_hour\":\"09:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,280,302,308,325,328,333,352\",\"is_full\":\"0\",\"total_dispatched\":\"12\",\"is_real_full\":\"0\",\"service_hour\":\"09:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,280,302,308,325,328,333,352,365\",\"is_full\":\"0\",\"total_dispatched\":\"13\",\"is_real_full\":\"0\",\"service_hour\":\"10:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,280,302,308,325,328,333,352,365\",\"is_full\":\"0\",\"total_dispatched\":\"13\",\"is_real_full\":\"0\",\"service_hour\":\"10:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,270,280,302,308,325,328,333,352,365\",\"is_full\":\"0\",\"total_dispatched\":\"14\",\"is_real_full\":\"0\",\"service_hour\":\"11:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,270,280,302,308,325,328,333,352,365\",\"is_full\":\"0\",\"total_dispatched\":\"14\",\"is_real_full\":\"0\",\"service_hour\":\"11:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,270,280,302,308,315,325,328,333,352,365\",\"is_full\":\"0\",\"total_dispatched\":\"15\",\"is_real_full\":\"0\",\"service_hour\":\"12:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 320, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,136,176,215,226,270,280,302,308,315,325,328,333,352,365\",\"is_full\":\"1\",\"total_dispatched\":\"15\",\"is_real_full\":\"0\",\"service_hour\":\"12:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 302, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"117,133,136,176,204,226,235,270,280,308,315,323,325,328,333,352,365\",\"is_full\":\"0\",\"total_dispatched\":\"17\",\"is_real_full\":\"0\",\"service_hour\":\"13:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 327, 302, 298, 240, 207, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,136,176,204,226,235,238,270,280,308,315,323,325,328,333,365\",\"is_full\":\"0\",\"total_dispatched\":\"17\",\"is_real_full\":\"0\",\"service_hour\":\"13:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 328, 327, 302, 298, 240, 207, 205, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,176,204,226,235,238,270,315,323,325,333,365\",\"is_full\":\"0\",\"total_dispatched\":\"14\",\"is_real_full\":\"0\",\"service_hour\":\"14:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 328, 327, 302, 298, 240, 207, 205, 176, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,270,315,323,325,333,365\",\"is_full\":\"0\",\"total_dispatched\":\"13\",\"is_real_full\":\"0\",\"service_hour\":\"14:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 365, 363, 357, 351, 328, 327, 302, 298, 240, 207, 205, 176, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,270,315,323,325,333\",\"is_full\":\"0\",\"total_dispatched\":\"12\",\"is_real_full\":\"0\",\"service_hour\":\"15:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 365, 363, 357, 351, 328, 327, 302, 298, 240, 207, 205, 176, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,270,315,323,325,333\",\"is_full\":\"0\",\"total_dispatched\":\"12\",\"is_real_full\":\"0\",\"service_hour\":\"15:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 365, 363, 357, 351, 328, 327, 302, 298, 270, 240, 207, 205, 176, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,315,323,325,333,356\",\"is_full\":\"0\",\"total_dispatched\":\"12\",\"is_real_full\":\"0\",\"service_hour\":\"16:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 365, 363, 357, 351, 328, 327, 302, 298, 270, 240, 207, 205, 176, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,280,315,323,325,333,356\",\"is_full\":\"0\",\"total_dispatched\":\"13\",\"is_real_full\":\"0\",\"service_hour\":\"16:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 365, 363, 357, 351, 328, 327, 315, 302, 298, 270, 240, 207, 205, 176, 174, 136]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,280,323,325,333,356\",\"is_full\":\"0\",\"total_dispatched\":\"12\",\"is_real_full\":\"0\",\"service_hour\":\"17:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 328, 327, 302, 298, 270, 240, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,204,226,235,238,280,320,323,325,333,356\",\"is_full\":\"0\",\"total_dispatched\":\"13\",\"is_real_full\":\"0\",\"service_hour\":\"17:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[372, 366, 363, 357, 351, 328, 327, 302, 298, 270, 240, 205, 174]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"24,117,133,238,280,320,323,325,333,356\",\"is_full\":\"0\",\"total_dispatched\":\"10\",\"is_real_full\":\"0\",\"service_hour\":\"18:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"117,133,280,320,323,325,333,356\",\"is_full\":\"0\",\"total_dispatched\":\"8\",\"is_real_full\":\"0\",\"service_hour\":\"18:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,280,320,323,356\",\"is_full\":\"0\",\"total_dispatched\":\"5\",\"is_real_full\":\"0\",\"service_hour\":\"19:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,280,320,323,356\",\"is_full\":\"0\",\"total_dispatched\":\"5\",\"is_real_full\":\"0\",\"service_hour\":\"19:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"133,280,320,323,356\",\"is_full\":\"0\",\"total_dispatched\":\"5\",\"is_real_full\":\"0\",\"service_hour\":\"20:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"280,320,323,356\",\"is_full\":\"0\",\"total_dispatched\":\"4\",\"is_real_full\":\"0\",\"service_hour\":\"20:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"280,320,323\",\"is_full\":\"0\",\"total_dispatched\":\"3\",\"is_real_full\":\"0\",\"service_hour\":\"21:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"320,323\",\"is_full\":\"0\",\"total_dispatched\":\"2\",\"is_real_full\":\"0\",\"service_hour\":\"21:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"320,323\",\"is_full\":\"0\",\"total_dispatched\":\"2\",\"is_real_full\":\"0\",\"service_hour\":\"22:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[320, 298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"323\",\"is_full\":\"0\",\"total_dispatched\":\"1\",\"is_real_full\":\"0\",\"service_hour\":\"22:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[320, 298]\",\"is_logic_full\":\"0\",\"total\":\"43\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"0\",\"total_dispatched\":\"0\",\"is_real_full\":\"0\",\"service_hour\":\"23:00\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"},{\"can_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 270, 268, 265, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\",\"is_logic_full\":\"1\",\"total\":\"43\",\"reason\":\"过滤配置后无可用服务人员\",\"order_service_hour\":\"3.0\",\"leave_staffs\":\"[275, 259, 228, 299, 260, 300, 11, 231, 322, 93, 229, 289]\",\"staffs\":\"\",\"is_full\":\"1\",\"total_dispatched\":\"0\",\"is_real_full\":\"1\",\"service_hour\":\"23:30\",\"total_staffs\":\"[373, 372, 366, 365, 363, 362, 357, 356, 353, 352, 351, 333, 328, 317, 327, 323, 325, 320, 315, 312, 308, 302, 298, 280, 275, 270, 268, 265, 259, 240, 238, 235, 226, 215, 207, 205, 204, 176, 174, 136, 133, 117, 24]\"}]";
		List<Map<String,String>> datas = GsonUtil.GsonToListMaps(json);
		System.out.println(ListUtil.getTimeList(datas));
	}

}

package com.jhj.utils.sign;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.jhj.utils.StringUtil;


public class JhjSignUtils {
	public static String map2String(SortedMap<String, String> map) {
		StringBuilder builder = new StringBuilder();
		if (map.size() == 0) {
			return "";
		}
		for (Entry<String, String> kv : map.entrySet()) {
//				builder.append(kv.getKey() + "=" + URLEncoder.encode(kv.getValue(), "UTF-8") + "&");
			if (!StringUtil.isEmpty(kv.getValue())) builder.append(kv.getKey() + "=" + kv.getValue() + "&");
		}

		if (builder.length() > 0) {
			return builder.substring(0, builder.length() - 1);
		} else {
			return "";
		}
	}

	private static String md5(String str) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(str.getBytes("UTF-8"));
			byte[] byteArray = messageDigest.digest();
			StringBuffer md5StrBuff = new StringBuffer();
			for (int i = 0; i < byteArray.length; i++) {
				if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
					md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
				else
					md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
			}
			return md5StrBuff.toString();
		} catch (Exception e) {
			throw new RuntimeException("md5 failed!");
		}
	}

	private static void dumpParams(Map<String, String[]> params) {
		for (Entry<String, String[]> kv : params.entrySet()) {
			System.out.println(kv.getKey() + "=" + kv.getValue()[0]);
		}

	}

	public static boolean verifySign(HttpServletRequest request, String appkey, String appsecret) {
		SortedMap<String, String> map = new TreeMap<String, String>();
		@SuppressWarnings("unchecked")
		Map<String, String[]> params = request.getParameterMap();

		// for debug only
		dumpParams(params);

		String expSign = null;
		String[] appkeyParam = params.get("appkey");
		if (appkeyParam == null) {
			return false;
		}
		if (!appkey.equals(appkeyParam[0])) {
			return false;
		}
		for (Entry<String, String[]> pv : params.entrySet()) {
			String param = pv.getKey();
			String[] value = pv.getValue();
			if (!param.equals("sign")) {
				map.put(param, value[0]);
			} else {
				expSign = value[0];
			}
		}
		String string1 = map2String(map);
		String stringSignTemp = string1 + "&secret=" + appsecret;
//		System.out.println(stringSignTemp);
		String actualSign = MD5Utils.MD5(stringSignTemp).toUpperCase();
//		System.out.println("server sign = " + actualSign);
		return expSign.equals(actualSign);
	}

	public static String getUniqueId() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String createSign(SortedMap<String, String> map, String appsecret) {
		String string1 = map2String(map);
		String stringSignTemp = string1 + "&secret=" + appsecret;
		return md5(stringSignTemp).toUpperCase();
	}

	public static String sign(SortedMap<String, String> map, String appkey, String appsecret) {
		String oncestr = getUniqueId();

		map.put("appkey", appkey);
		map.put("oncestr", oncestr);
		String sign = createSign(map, appsecret);
		map.put("sign", sign);

		return map2String(map);
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<String, String> getSignParams(HashMap<String, String> params) {
		String oncestr = getUniqueId();
		SortedMap<String, String> map = JhjSignUtils.ConvertToSortMap(params);
		map.put("appkey", JhjSignConfig.appkey);
		map.put("oncestr", oncestr);
		String sign = JhjSignUtils.createSign(map, JhjSignConfig.appsecret);
		map.put("sign", sign);
		params = JhjSignUtils.ConvertToMap(map);
		return params;
	}
	
	@SuppressWarnings("rawtypes")
	public static SortedMap<String, String>  ConvertToSortMap(HashMap<String, String> params) {
		SortedMap<String, String> map = new TreeMap<String, String>();
		Iterator it = params.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            String value = params.get(key);
			map.put(key, value);
        }
		return map;
	}
	
	@SuppressWarnings("rawtypes")
	public static HashMap<String, String>  ConvertToMap(SortedMap<String, String> map) {
		HashMap<String, String> params = new HashMap<String, String>();
		Iterator it = map.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            String value = map.get(key);
			params.put(key, value);
        }
		return params;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		System.out.println(JhjSignUtils.getUniqueId());
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("mobile", "18612514665");
		params.put("token", "1234");
		params = JhjSignUtils.getSignParams(params);
		System.out.println(params);
	}
}

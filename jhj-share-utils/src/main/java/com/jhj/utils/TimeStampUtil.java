package com.jhj.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimeStampUtil {

	private static final String DEFAULT_PATTERN = "yyyy-MM-dd";
		
	private static final String DEFAULT_FULL_PATTERN = "yyyy-MM-dd HH:mm:ss";

	private static final String DEFAULT_TIME_PATTERN = "HH:mm:ss";

	private static final Calendar DEFAULT_CALENDAR = Calendar.getInstance();

	/**
	 * 当前时间戳, 注意为精确到毫秒
	 * @return long
	 */
	public static Long getNow() {
		return getMillisOfDate(DateUtil.getNowOfDate());
	}
	
	/**
	 * 当前时间戳, 注意为精确到秒
	 * @return long
	 */
	public static Long getNowSecond() {
		return getMillisOfDate(DateUtil.getNowOfDate())/1000;
	}
	
	/**
	 * 当前时间戳分钟, 注意为精确到秒
	 * @return long
	 */
	public static Long getNowMin() {
		
		String format = "yyyy-MM-dd HH:mm";
		SimpleDateFormat df = new SimpleDateFormat(format);
		String str = df.format(getNow());
		Date pDate = DateUtil.parse(str, format);
		return getMillisOfDate(pDate)/1000;
	}

	/**
	 * 返回日期字符串的毫秒数
	 * @param date
	 * @return
	 */
	public static long getMillisOfDay(String strDate) {
		Date date = DateUtil.parse(strDate);
		return getMillisOfDate(date);
	}

	/**
	 * 返回日期字符串的毫秒数
	 * @param date
	 * @return
	 */
	public static long getMillisOfDayFull(String strDate) {
		Date date = DateUtil.parseFull(strDate);
		return getMillisOfDate(date);
	}

	/**
	 * 返回日期对象的毫秒数
	 * @param date
	 * @return
	 */
	public static long getMillisOfDate(Date date) {
		DEFAULT_CALENDAR.setTime(date);
		return DEFAULT_CALENDAR.getTimeInMillis();
	}

	/**
	 * 返回两个时间戳相差毫秒
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static long compareTimeStr(Long startTime, Long endTime) {
		return endTime - startTime;
	}

	/**
	 * 今天开始时间戳，注意为精确到毫秒
	 * @return long
	 */
	public static Long getBeginOfToday() {
		String today = DateUtil.getBeginOfDay();
		Date d = DateUtil.parse(today);
		return getMillisOfDate(d) / 1000;
	}

	/**
	 * 结束开始时间戳，注意为精确到毫秒
	 * @return long
	 */
	public static Long getEndOfToday() {
		String today = DateUtil.getEndOfDay();
		Date d = DateUtil.parseFull(today);
		return getMillisOfDate(d) / 1000;
	}

	/**
	 *  根据时间戳 -> String yyyy-MM-dd HH:MM:ss
	 */
	public static String timeStampToDateStr(Long t) {
		SimpleDateFormat df = new SimpleDateFormat(DEFAULT_FULL_PATTERN);
		String str = df.format(t);
		return str;
	}

	/**
	 *  根据时间戳 -> String yyyy-MM-dd HH:MM:ss
	 */
	public static String timeStampToDateStr(Long t, String patten) {
		SimpleDateFormat df = new SimpleDateFormat(patten);
		String str = df.format(t);
		return str;
	}

	/**
	 *  根据时间戳 -> String yyyy-MM-dd HH:MM:ss
	 */
	public static Date timeStampToDate(Long t) {
		SimpleDateFormat df = new SimpleDateFormat(DEFAULT_FULL_PATTERN);
		String str = df.format(t);
		return DateUtil.parse(str);
	}

	public static Date timeStampToDateFull(Long t, String format) {
		if (format == null) format = DEFAULT_FULL_PATTERN;
		SimpleDateFormat df = new SimpleDateFormat(format);
		String str = df.format(t);
		return DateUtil.parse(str, format);
	}
	
	public static Long timeStampToDateHour(Long t) {
		String format = "yyyy-MM-dd HH:mm";
		SimpleDateFormat df = new SimpleDateFormat(format);
		String str = df.format(t);
		Date pDate = DateUtil.parse(str, format);
		return getMillisOfDate(pDate);
	}
	
	/**
	 * @return long
	 */
	public static Long getBeginOfMonth(int year, int month) {
		String today = DateUtil.getFirstDayOfMonth(year, month);
		Date d = DateUtil.parse(today);
		return getMillisOfDate(d) / 1000;
	}
	
	/**
	 *
	 * @return long
	 */
	public static Long getEndOfMonth(int year, int month) {
		String today = DateUtil.getLastDayOfMonth(year, month);
		Date d = DateUtil.parseFull(today + " 23:59:59");
		return getMillisOfDate(d) / 1000;
	}
	
	
	
	
	
	
	public static void main(String[] args) {
		String serviceDateStr = "2017-08-02";
		String startAm = "";
		String endAm = "";
		String startPm = "13:00";
		String endPm = "19:00";
		
		List<String> rangDateTime = new ArrayList<String>();
		
		int startAmHour = 0;
		int endAmHour = 0;
		int startPmHour = 0;
		int endPmHour = 0;
		if (!StringUtil.isEmpty(startAm)) {
			Long startAmTime = TimeStampUtil.getMillisOfDayFull(serviceDateStr + " " + startAm + ":00");
			startAmHour = Integer.valueOf(TimeStampUtil.timeStampToDateStr(startAmTime, "HH"));
		}
		
		if (!StringUtil.isEmpty(endAm)) {
			Long endAmTime = TimeStampUtil.getMillisOfDayFull(serviceDateStr + " " + endAm + ":00");
			endAmHour = Integer.valueOf(TimeStampUtil.timeStampToDateStr(endAmTime, "HH"));
		}
		
		if (!StringUtil.isEmpty(startPm)) {
			Long startPmTime = TimeStampUtil.getMillisOfDayFull(serviceDateStr + " " + startPm + ":00");
			startPmHour = Integer.valueOf(TimeStampUtil.timeStampToDateStr(startPmTime, "HH"));
		}
		
		if (!StringUtil.isEmpty(endPm)) {
			Long endPmTime = TimeStampUtil.getMillisOfDayFull(serviceDateStr + " " + endPm + ":00");
			endPmHour = Integer.valueOf(TimeStampUtil.timeStampToDateStr(endPmTime, "HH"));
		}
		
		
		int startHour = startAmHour;
		if (StringUtil.isEmpty(startAm)) startHour = startPmHour;
		
		int endHour = endPmHour;
		if (StringUtil.isEmpty(endPm)) endHour = endAmHour;
		
		for (int i = startHour; i <= endHour; i++) {
			String h = String.valueOf(i);
			if (h.length() == 1) h = "0" + h;
			
			String tmpTimeZero = h + ":00";
			rangDateTime.add(tmpTimeZero);
			
			
			String tmpTimeHalf = h + ":30";
			if (i != endHour)
			rangDateTime.add(tmpTimeHalf);
			
		}
		
		for(String item : rangDateTime) {
			System.out.println(item);
		}
		
	}
}
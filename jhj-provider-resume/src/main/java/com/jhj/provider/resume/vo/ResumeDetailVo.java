package com.jhj.provider.resume.vo;

import java.util.List;

import com.jhj.provider.resume.po.b.model.ResumeCert;
import com.jhj.provider.resume.po.b.model.ResumeResource;

public class ResumeDetailVo extends ResumeListVo {
	
	//完整度百分比
	public String totalPercent;
	
//	//最后更新时间格式化为yyyy-MM-dd HH:mm:ss
//	public String refreshTimeFormat;
	
	//添加时间格式化为yyyy-MM-dd HH:mm:ss
	public String addTimeFormat;
	
	//排名
	public Integer rowNum;
	
	//总人数
	public Integer total;
	
	
	
	//民族名称
	public String nationName;
	
	//学历名称
	public String degreeName;
	
	//婚否名称
	public String maritalName;
	
	//星座名称
	public String astroName;
	
	//推荐人来源渠道
	public String resumeFromName;
	
	//推荐人ID
	public Integer fromId;
	
	//推荐人手机号
	public String fromMobile;
	
	//推荐人名称
	public String fromName;
	
	//推荐人评价
	public String fromRate;
	
	//推荐时间
	public String fromTimeFormat;
	
	//服务城市
	public Integer serviceCityId;
	
	public String regionIds;
	
	//服务区域
	public List<ResumeServiceAreaVo> resumeServiceAreas;
	
	//服务技能
	public List<ResumeSkillVo> resumeSkills;
	
	//图片视频资源信息
	public List<ResumeResource> resumeResource;
	
	//证书信息
	public List<ResumeCert> resumeCerts;

	public String getTotalPercent() {
		return totalPercent;
	}

	public void setTotalPercent(String totalPercent) {
		this.totalPercent = totalPercent;
	}

	@Override
	public String getRefreshTimeFormat() {
		return refreshTimeFormat;
	}

	@Override
	public void setRefreshTimeFormat(String refreshTimeFormat) {
		this.refreshTimeFormat = refreshTimeFormat;
	}

	public String getNationName() {
		return nationName;
	}

	public void setNationName(String nationName) {
		this.nationName = nationName;
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public String getMaritalName() {
		return maritalName;
	}

	public void setMaritalName(String maritalName) {
		this.maritalName = maritalName;
	}

	public String getAstroName() {
		return astroName;
	}

	public void setAstroName(String astroName) {
		this.astroName = astroName;
	}

	public String getResumeFromName() {
		return resumeFromName;
	}

	public void setResumeFromName(String resumeFromName) {
		this.resumeFromName = resumeFromName;
	}

	public Integer getFromId() {
		return fromId;
	}

	public void setFromId(Integer fromId) {
		this.fromId = fromId;
	}

	public String getFromMobile() {
		return fromMobile;
	}

	public void setFromMobile(String fromMobile) {
		this.fromMobile = fromMobile;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getFromTimeFormat() {
		return fromTimeFormat;
	}

	public void setFromTimeFormat(String fromTimeFormat) {
		this.fromTimeFormat = fromTimeFormat;
	}

	public List<ResumeServiceAreaVo> getResumeServiceAreas() {
		return resumeServiceAreas;
	}

	public void setResumeServiceAreas(List<ResumeServiceAreaVo> resumeServiceAreas) {
		this.resumeServiceAreas = resumeServiceAreas;
	}

	public List<ResumeSkillVo> getResumeSkills() {
		return resumeSkills;
	}

	public void setResumeSkills(List<ResumeSkillVo> resumeSkills) {
		this.resumeSkills = resumeSkills;
	}

	public List<ResumeCert> getResumeCerts() {
		return resumeCerts;
	}

	public void setResumeCerts(List<ResumeCert> resumeCerts) {
		this.resumeCerts = resumeCerts;
	}

	public String getAddTimeFormat() {
		return addTimeFormat;
	}

	public void setAddTimeFormat(String addTimeFormat) {
		this.addTimeFormat = addTimeFormat;
	}

	public Integer getServiceCityId() {
		return serviceCityId;
	}

	public void setServiceCityId(Integer serviceCityId) {
		this.serviceCityId = serviceCityId;
	}

	public String getRegionIds() {
		return regionIds;
	}

	public void setRegionIds(String regionIds) {
		this.regionIds = regionIds;
	}

	public List<ResumeResource> getResumeResource() {
		return resumeResource;
	}

	public void setResumeResource(List<ResumeResource> resumeResource) {
		this.resumeResource = resumeResource;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getFromRate() {
		return fromRate;
	}

	public void setFromRate(String fromRate) {
		this.fromRate = fromRate;
	}
	
	
	
	
}

package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.vo.ResumeListVo;
import com.jhj.provider.resume.vo.ResumeSearchVo;
import com.jhj.utils.AppResultData;
import com.github.pagehelper.PageInfo;


/**
 *
 */
@RestController
public class ResumeStatController {
	
	@Autowired
	ResumeService resumeService;
		
	/**
	 * 查询统计接口
	 * @param serviceCityId		服务城市
	 * @param serviceTypes		服务类别
	 * @param cityId				籍贯
	 * @param hopeSalary			期望薪水
	 * @param workYear			工作年限
	 * @param degreeId			学历
	 * @param ageId				年龄范围
	 * @param mobile				手机号
	 * @param name				姓名
	 * @param sex				性别
	 * @param publish      0 = 显示全部 1 = 仅显示已推荐的
	 * @return
	 * 		totalNum  总人数
	 * 		totalPublic 总已推荐人数
	 * 		totalMale	男性人数
	 * 		totalFemail	女性人数
	 */
	
	@RequestMapping(value = "/api/resume/statNum.json", method = RequestMethod.GET)
	public AppResultData<Object> list(
		@RequestParam(value = "serviceCityId", required = false, defaultValue = "0") Integer serviceCityId,
		@RequestParam(value = "serviceRegionIds", required = false, defaultValue = "") String serviceRegionIds,
		@RequestParam(value = "serviceTypes", required = false, defaultValue = "") String serviceTypes,
		@RequestParam(value = "provinceId", required = false, defaultValue = "0") Integer provinceId,
		@RequestParam(value = "salaryId", required = false, defaultValue = "0") Integer salaryId,
		@RequestParam(value = "workYearId", required = false, defaultValue = "0") Integer workYearId,
		@RequestParam(value = "degreeId", required = false, defaultValue = "0") Integer degreeId,
		@RequestParam(value = "ageId", required = false, defaultValue = "0") Integer ageId,
		@RequestParam(value = "mobile", required = false, defaultValue = "") String mobile,
		@RequestParam(value = "name", required = false, defaultValue = "") String name,
		@RequestParam(value = "sex", required = false, defaultValue = "0") Integer sex,
		@RequestParam(value = "hasCert", required = false, defaultValue = "0") Integer hasCert,
		@RequestParam(value = "status", required = false, defaultValue = "1") Integer status,
		@RequestParam(value = "publish", required = false, defaultValue = "0") Integer publish
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		ResumeSearchVo searchVo = new ResumeSearchVo();
		searchVo.setServiceCityId(serviceCityId);
		searchVo.setServiceRegionIds(serviceRegionIds);
		searchVo.setServiceTypes(serviceTypes);
		searchVo.setProvinceId(provinceId);
		searchVo.setHopeSalary(salaryId);
		searchVo.setWorkYear(workYearId);
		searchVo.setDegreeId(degreeId);
		searchVo.setAgeId(ageId);
		searchVo.setMobile(mobile);
		searchVo.setName(name);
		searchVo.setSex(sex);
		searchVo.setHasCert(hasCert);
		searchVo.setStatus(status);
		
		// 0 = 显示全部 1 = 仅显示已推荐的
		if (publish == 1) {
			searchVo.setPublish(1);
		}
		
		
		Map<String, Integer> resultMap = resumeService.statNumBySearchVo(searchVo);
		
		if (resultMap == null) {
			resultMap = new HashMap<String, Integer>();
			resultMap.put("totalNum", 0);
			resultMap.put("totalPublic", 0);
			resultMap.put("totalMale", 0);
			resultMap.put("totalFemale", 0);
		}
		
		result.setData(resultMap);
		return result;
	}
	
	
	
	/**
	 *  获取服务人员总人数和特定地理位置的人数
	 *  @param serviceTypeId
	 *  @param lat
	 *  @param lng
	 *  @return map
	 */
	@RequestMapping(value = "/api/resume/get_total.json", method = RequestMethod.GET)
	public AppResultData<Object> getTotal(
			@RequestParam(value = "serviceTypeId", required = false, defaultValue = "0") Integer serviceTypeId,
			@RequestParam(value = "lat", required = false, defaultValue = "") String lat,
			@RequestParam(value = "lng", required = false, defaultValue = "") String lng) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		ResumeSearchVo resumeSearchVo = new ResumeSearchVo();
		if (serviceTypeId > 0) resumeSearchVo.setServiceTypeId(serviceTypeId);
		Integer total = resumeService.selectCountBySearchVo(resumeSearchVo);
		
		//排名
		Integer serviceCount = resumeService.getServiceCount(serviceTypeId, lat, lng);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("total", total);
		data.put("serviceCount", serviceCount);
		
		result.setData(data);
		return result;
	}	
}
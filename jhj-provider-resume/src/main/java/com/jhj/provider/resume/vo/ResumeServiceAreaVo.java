package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.ResumeServiceArea;

public class ResumeServiceAreaVo extends ResumeServiceArea {
	
	public String provinceName;
	
	public String cityName;
	
	public String regionName;

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
}

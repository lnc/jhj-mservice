package com.jhj.provider.resume.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.resume.po.b.model.ResumeZan;
import com.jhj.provider.resume.service.ResumeZanService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;

@RestController
public class ResumeZanController {
	
	@Autowired
	private ResumeZanService resumeZanService;
	
	/**
	 * @param userId 用户id
	 * @param pageNo 页数
	 * @param pageSize 页面大小
	 * 
	 * @return page
	 * 
	 * */
	@RequestMapping(value="/api/resume/zan/list.json",method=RequestMethod.GET)
	public AppResultData<Object> getZanList(
			@RequestParam("userId")Integer userId,
			@RequestParam(value="pageNo",defaultValue="1")Integer pageNo,
			@RequestParam(value="pageSize",defaultValue="10")Integer pageSize){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		ResumeZan zan = new ResumeZan();
		zan.setActionId(userId);
		PageInfo<?> page = resumeZanService.selectByListPage(zan, pageNo, pageSize, "add_time desc");
		data.setData(page);
		
		return data;
	}
	
	/**
	 * @param resume_id 人才id
	 * @param resume_no 人才编号
	 * @param action_type
	 * @param action 备注
	 * @param action_id 操作id
	 * @param action_name 操作人名称
	 * @param remark 备注
	 * @return id
	 * 
	 * */
	@RequestMapping(value="/api/resume/zan/add.json",method=RequestMethod.POST)
	public AppResultData<Object> getZanList(
			@RequestParam("resume_id")Integer resumeId,
			@RequestParam("resume_no")String resumeNo,
			@RequestParam(value="action_type",required=false,defaultValue="0")Integer actionType,
			@RequestParam(value="action",required=false,defaultValue="")String action,
			@RequestParam(value="action_id",required=false,defaultValue="")Integer actionId,
			@RequestParam(value="action_name",required=false,defaultValue="")String actionName,
			@RequestParam(value="remark",required=false,defaultValue="")String remark){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		
		ResumeZan searchVo = new  ResumeZan();
		searchVo.setResumeId(resumeId);
		searchVo.setActionId(actionId);
		List<ResumeZan> list = resumeZanService.selectBySearchVo(searchVo, "");
		if (!list.isEmpty()) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("你已经支持过了!");
			return data;
		}

		ResumeZan zan = new ResumeZan();
		zan.setResumeId(resumeId);
		zan.setResumeNo(resumeNo);
		zan.setActionType(actionType);
		zan.setAction(action);
		zan.setActionId(actionId);
		zan.setActionName(actionName);
		zan.setRemark(remark);
		zan.setAddTime(TimeStampUtil.getNowSecond().intValue());
		resumeZanService.insert(zan);
		
		data.setData(zan.getId());
		return data;
	}

}

package com.jhj.provider.resume.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.resume.po.b.model.ResumeServiceArea;
import com.jhj.provider.resume.service.ResumeServiceAreaService;

/**
 * ResumeService实现类
 */
@Service
public class ResumeServiceAreaServiceImpl extends BaseServiceImpl<ResumeServiceArea> implements ResumeServiceAreaService  {
	

}

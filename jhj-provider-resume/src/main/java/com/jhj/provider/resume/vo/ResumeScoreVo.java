package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.ResumeScore;

public class ResumeScoreVo extends ResumeScore {
	
	public String doActionName;

	public String getDoActionName() {
		return doActionName;
	}

	public void setDoActionName(String doActionName) {
		this.doActionName = doActionName;
	}



}

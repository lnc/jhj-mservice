package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeScore;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeScoreMapper extends Mapper<ResumeScore> {
}
package com.jhj.provider.resume.utils;

public class ResumeUtil {
	
	
	public static String getLogTypeName(String logType){
		String logTypeName = "";
		switch (logType) {
			case "resume_add" :
				logTypeName = "新增";
				break;
			case "resume_update" :
				logTypeName = "修改";
				break;	
			case "resume_active" :
				logTypeName = "激活";
				break;	
			case "resume_contract" :
				logTypeName = "上户";
				break;		
			case "resume_publish" :
				logTypeName = "已推荐";
				break;	
			default:
				logTypeName = "";
		}
		return logTypeName;
	}
	
	public static String getSexName(Integer sex) {
		String sexName = "";
		switch (sex) {
			case 0 :
				sexName = "未知";
				break;
			case 1 :
				sexName = "男";
				break;	
			case 2 :
				sexName = "女";
				break;	
			default:
				sexName = "";
		}
		return sexName;
	}
	
	public static String getResumeFromName(Integer resumeFrom) {
		String resumeFromName = "";
		switch (resumeFrom) {
			case 0 :
				resumeFromName = "运营人员";
				break;
			case 1 :
				resumeFromName = "老师推荐";
				break;	
			case 2 :
				resumeFromName = "客服推荐";
				break;	
			default:
				resumeFromName = "";
		}
		return resumeFromName;
	}
	
	public static String getActinName(String action) {
		String actionName;
		switch (action) {
		case "resume_add" :
			actionName = "增加";
			break;
		case "resume_refresh" :
			actionName = "刷新";
			break;	
		case "resume_sign" :
			actionName = "签到";
			break;		
		case "resume_head_img" :
			actionName = "上传头像";
			break;	
		case "resume_cert" :
			actionName = "上传照片";
			break;		
		case "resume_noactive" :
			actionName = "未签到";
			break;	
		case "resume_active" :
			actionName = "手动激活";
			break;	
		default:
			actionName = "";
	}
		
		return actionName;
	}
	
	public static String getResumeStatus(Integer status) {
		String statusName = "";
		switch (status) {
			case 0 :
				statusName = "上户";
				break;
			case 1 :
				statusName = "空闲";
				break;	
			default:
				statusName = "";
		}
		return statusName;
	}
}

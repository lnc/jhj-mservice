package com.jhj.provider.resume.utils;

public class Constants {

	public static String URL_ENCODE = "UTF-8";
	public static int PAGE_MAX_NUMBER = 12;
	
	public static int SUCCESS_0 = 0;
	public static int ERROR_999 = 999;
	public static int ERROR_100 = 100;
	public static int ERROR_101 = 101;
	public static int ERROR_102 = 102;
	public static int ERROR_103 = 103;
	public static int ERROR_104 = 104;
	
	//小程序微信配置 -阿姨端配置
	public static String MP_RESUME_APP_ID = "wx6f00f70ea215500d";
	public static String MP_RESUME_APP_SECERT = "20df3180aed615d481f5a924717947ea";
	
	//小程序微信配置 -用户端配置
	public static String MP_USER_APP_ID = "wx2b2a7241d84bc4c5";
	public static String MP_USER_APP_SECERT = "f07928818b4b917891989aa43ece1c08";
	
	//默认头像
	public static String DEFAULT_HEAD_IMG = "https://www.jia-he-jia.com/u/img/default-head-img.png";
	
	//初始化鲜活度值
	public static int SCORE_DEFAULT = 70;
	
	//手动触发鲜活度值
	public static int SCORE_ACTIVE = 100;
	
	//鲜活度最大值
	public static int SCORE_MAX = 100;
	
	//手动刷新的鲜活度值
	public static int SCORE_SIGN = 1;
	
	//一天不刷新扣除鲜活度值
	public static int SCORE_NOT_ACTIVE = 1;
	
	//身份证分数
	public static int SCORE_COMPLETE_IDCARD = 2;
	//性别分数
	public static int SCORE_COMPLETE_SEX = 1;
	//头像分数
	public static int SCORE_COMPLETE_HEADIMG = 2;
	//证书分数
	public static int SCORE_COMPLETE_CERT = 2;
	//期望工资分数
	public static int SCORE_COMPLETE_HOPESALARY = 1;
	//工作经验分数
	public static int SCORE_COMPLETE_WORKYEAR = 1;
	
	//鲜活度操作记录
	
	//增加
	public static String ACTION_PLUS_RESUME_ADD = "resume_add";
	
	//刷新签到
	public static String ACTION_PLUS_RESUME_SIGN = "resume_sign";
	
	//更新
	public static String ACTION_PLUS_RESUME_HEADIMG = "resume_head_img";
	
	//证书
	public static String ACTION_PLUS_RESUME_CERT = "resume_cert";
	
	//未签到
	public static String ACTION_SUB_RESUME_NOTACITVIE = "resume_noactive";
	
	//手动触发排名靠前
	public static String ACTION_PLUS_RESUME_ACTIVE = "resume_active";
	
	//人才日志类别定义
	
	//人才添加
	public static String LOG_TYPE_ADD = "resume_add";
	//人才修改
	public static String LOG_TYPE_UPDATE = "resume_update";
	//人才激活
	public static String LOG_TYPE_ACTIVE = "resume_active";
	
	//人才上户
	public static String LOG_TYPE_CONTRACT = "resume_contract";
	
	//人才变为已推荐
	public static String LOG_TYPE_RESUME_PUBLISH = "resume_publish";
	
	//字典库类型定义
	
	//服务类型
	public static String DICT_SERIVCE_TYPE = "service_type";
	
	//省份
	public static String DICT_PROVICE = "province";
	
	//城市
	public static String DICT_CITY = "city";
	
	//区县
	public static String DICT_REGION = "region";
	
	//年龄区间
	public static String DICT_AGE = "age";
	
	//工作年限区间
	public static String DICT_WORK_YEAR = "work_year";
	
	//期望工资区间
	public static String DICT_HOPE_SALARY = "hope_salary";
	
	//证书类型
	public static String DICT_CERT = "cert";
	
	//民族
	public static String DICT_NATION = "nation";
	
	//学历
	public static String DICT_DEGREE = "degree";
	
	//星座
	public static String DICT_ASTRO = "astro";
	
	//资源的定义
	//头像
	public static String RES_RESUME_HEAD_IMG = "resume_head_img";
	
	//个人近照
	public static String RES_RESUME_LAST_IMG = "resume_last_img";
	
	//个人生活照
	public static String RES_RESUME_LIVE_IMG = "resume_live_img";
	
	//证书照片
	public static String RES_RESUME_CERT_IMG = "resume_cert_img";
	
	//个人视频
	public static String RES_RESUME_VIDEO = "resume_video";
	
	//身份证正面
	public static String RES_IDCARD_FRONT_IMG = "id_card_front_img";
	
	//身份证反面
	public static String RES_IDCARD_BACK_IMG = "id_card_back_img";
	
	
	
}

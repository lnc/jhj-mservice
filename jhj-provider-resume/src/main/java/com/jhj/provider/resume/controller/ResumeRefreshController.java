package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeLog;
import com.jhj.provider.resume.po.b.model.ResumeScore;
import com.jhj.provider.resume.service.ResumeLogService;
import com.jhj.provider.resume.service.ResumeScoreService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.utils.ResumeUtil;
import com.jhj.provider.resume.vo.ResumeScoreSearchVo;
import com.jhj.provider.resume.vo.ResumeScoreVo;
import com.jhj.provider.resume.vo.ResumeSearchVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.DateUtil;
import com.jhj.utils.TimeStampUtil;


/**
 *
 */
@RestController
public class ResumeRefreshController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeScoreService resumeScoreService;
	
	@Autowired
	ResumeLogService resumeLogService;
	
	/**
	 * 鲜活度列表
	 * @param resumeNo
	 * @param action
	 * @param page
	 * 
	 * */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/api/resume/score_list.json", method = RequestMethod.GET)
	public AppResultData<Object> list(
		@RequestParam("resumeNo") String resumeNo,
		@RequestParam(value = "action", required = false, defaultValue = "") String action,
		@RequestParam(value = "page", required = false, defaultValue = "1") Integer page
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		if (page.equals(0)) page = 1;
		ResumeScore searchVo = new ResumeScore();
		searchVo.setResumeNo(resumeNo);
		if (!StringUtil.isEmpty(action)) {
			searchVo.setAction(action);
		}
		PageInfo pageInfo = resumeScoreService.selectByListPage(searchVo, page,  Constants.PAGE_MAX_NUMBER, "id desc");
		
		List<ResumeScore> list = pageInfo.getList();
		int len = list.size();
		for (int i = 0; i < len ; i++) {
			ResumeScore item = list.get(i);
			ResumeScoreVo vo = resumeScoreService.getVo(item);
			list.set(i, vo);
		}
		pageInfo = new PageInfo(list);
		result.setData(pageInfo);
		return result;
	}
	/**
	 * 签到刷新
	 * @param resumeNo    人才库编号
	 * @param mobile		 人才库手机号
	 * @param needAddScore  0 = 需要增加鲜活度值  1 = 不需要增加鲜活度值
	 * */
	@RequestMapping(value = "/api/resume/refresh.json", method = RequestMethod.POST)
	public AppResultData<Object> resumereFresh(
		@RequestParam("resumeNo") String resumeNo,
		@RequestParam("mobile") String mobile,
		@RequestParam(value = "action", required = false, defaultValue = "resume_sign") String action

		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//校验1.判断今天是否已经满3次，三次后则提示一天只能刷新三次
		ResumeScoreSearchVo searchVo = new ResumeScoreSearchVo();
		searchVo.setResumeNo(resumeNo);
		searchVo.setAction(action);
		Integer startTime = TimeStampUtil.getBeginOfToday().intValue();
		Integer endTime = TimeStampUtil.getEndOfToday().intValue();
		searchVo.setStartTime(startTime);
		searchVo.setEndTime(endTime);
		List<ResumeScore> list = resumeScoreService.selectBySearchVoSelf(searchVo, "add_time desc");
		
		Integer nowTime = TimeStampUtil.getNowSecond().intValue();
		//最近一次刷新要超过3小时间隔
		if (!list.isEmpty()) {
			if (list.size() >= 3) {
				result.setStatus(Constants.ERROR_999);
				result.setMsg("一天只能刷新三次");
				return result;
			}
			
			ResumeScore lastRecord = list.get(0);
			Integer addTime = lastRecord.getAddTime();
			
			if ( (nowTime - addTime) < 3 * 3600) {
				Integer remindTime = addTime + 3 * 3600 - nowTime;
				String afterTimeStr = DateUtil.formatDateTime(Long.valueOf(remindTime));
				result.setStatus(Constants.ERROR_999);
				result.setMsg("刷新间隔为3小时，请在" + afterTimeStr + "再刷新");
				return result;
			}
		}
		
		Resume resumeSearchVo = new Resume();
		resumeSearchVo.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(resumeSearchVo);
		
		if (action.equals(Constants.ACTION_PLUS_RESUME_SIGN)) {
			Integer totalScore = resume.getTotalScore();
			totalScore = totalScore + Constants.SCORE_SIGN;
			if (totalScore > 100) totalScore = 100;
			resume.setTotalScore(totalScore);
		}
		resume.setRefreshTime(nowTime);
		resume.setUpdateTime(nowTime);
		resumeService.update(resume);
		
		Integer resumeId = resume.getResumeId();

		//鲜活度明细
		ResumeScore  resumeScore = resumeScoreService.initPo(new ResumeScore());
		resumeScore.setResumeId(resumeId);
		resumeScore.setResumeNo(resumeNo);
		resumeScore.setActionType(0);
		resumeScore.setAction(action);
		if (action.equals(Constants.ACTION_PLUS_RESUME_SIGN)) {
			resumeScore.setScore(Constants.SCORE_SIGN);
		}
		resumeScore.setRemark("");
		resumeScore.setAddTime(nowTime);
		resumeScoreService.insert(resumeScore);
		
		
		Integer refreshTime = resume.getRefreshTime();
		nowTime = TimeStampUtil.getNowSecond().intValue();
		Long lastTime = nowTime - Long.valueOf(refreshTime);
		String refreshTimeFormat = DateUtil.formatDateTimeBefore(lastTime);
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("resumeId", resumeId);
		data.put("resumeNo", resumeNo);
		data.put("name", resume.getName());
		//总数
		ResumeSearchVo rsearchVo = new ResumeSearchVo();
		Integer total = resumeService.selectCountBySearchVo(rsearchVo);
		
		//排名
		Integer rowNum = resumeService.selectByRowNum(resumeId);
		
		data.put("total", total);
		data.put("rowNum", rowNum);
		data.put("totalScore", resume.getTotalScore());
		data.put("refreshTimeFormat", refreshTimeFormat);
		
		result.setData(data);
		
		return result;
	}
	
	/**
	 * 长时间不用激活
	 * @param resumeNo
	 * @param actionMobile
	 * @param actionId
	 * @param actionName
	 * 
	 * */
	@RequestMapping(value = "/api/resume/active.json", method = RequestMethod.POST)
	public AppResultData<Object> resumereActive(
		@RequestParam("resumeNo") String resumeNo,
		@RequestParam("actionMobile") String actionMobile,
		@RequestParam("actionId") Integer actionId,
		@RequestParam("actionName") String actionName
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//校验1.判断一个月内只能激活一次
		ResumeScoreSearchVo searchVo = new ResumeScoreSearchVo();
		searchVo.setResumeNo(resumeNo);
		searchVo.setAction(Constants.ACTION_PLUS_RESUME_ACTIVE);
		List<ResumeScore> list = resumeScoreService.selectBySearchVoSelf(searchVo, "add_time desc");
		
		Integer nowTime = TimeStampUtil.getNowSecond().intValue();
		String today = DateUtil.getToday();
		//最近一次刷新要超过3小时间隔
		if (!list.isEmpty()) {
			
			ResumeScore lastRecord = list.get(0);
			Integer addTime = lastRecord.getAddTime() * 1000;
			String addTimeStr = TimeStampUtil.timeStampToDateStr(addTime.longValue());
			int days = DateUtil.daysOfTwo(DateUtil.parse(addTimeStr), DateUtil.parse(today));
			if (days < 30) {
				
				result.setStatus(Constants.ERROR_999);
				result.setMsg("一个月只能激活一次");
				return result;
			}
		}
		
		Resume resumeSearchVo = new Resume();
		resumeSearchVo.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(resumeSearchVo);
		
		Integer totalScore = resume.getTotalScore();
		totalScore = totalScore + Constants.SCORE_ACTIVE;
		if (totalScore > 100) totalScore = 100;
		resume.setRefreshTime(nowTime);
		resume.setTotalScore(totalScore);
		resume.setUpdateTime(nowTime);
		resumeService.update(resume);
		
		Integer resumeId = resume.getResumeId();
		//鲜活度明细
		ResumeScore  resumeScore = resumeScoreService.initPo(new ResumeScore());
		resumeScore.setResumeId(resumeId);
		resumeScore.setResumeNo(resumeNo);
		resumeScore.setActionType(0);
		resumeScore.setAction(Constants.ACTION_PLUS_RESUME_ACTIVE);
		resumeScore.setScore(Constants.SCORE_ACTIVE);
		resumeScore.setActionId(actionId);
		resumeScore.setActionMobile(actionMobile);
		resumeScore.setActionName(actionName);
		resumeScore.setRemark("");
		resumeScore.setAddTime(nowTime);
		resumeScoreService.insert(resumeScore);
		
		//记录日志
		ResumeLog resumeLog = resumeLogService.initPo(new ResumeLog());
		resumeLog.setResumeId(resumeId);
		resumeLog.setResumeNo(resumeNo);
		resumeLog.setLogType(Constants.LOG_TYPE_ACTIVE);
		String logTypeName = ResumeUtil.getLogTypeName(Constants.LOG_TYPE_ACTIVE);
		resumeLog.setLogName(logTypeName);
		resumeLog.setActionId(actionId);
		resumeLog.setActionName(actionName);
		resumeLog.setRemark(logTypeName + "加" + Constants.SCORE_ACTIVE + "分");
		resumeLog.setAddTime(nowTime);
		resumeLogService.insert(resumeLog);
		
		return result;
	}
	
	/**
	 * 鲜活度减分，用于每天没有签到
	 * @param resumeNo
	 * @return result
	 * 
	 * */
	@RequestMapping(value = "/api/resume/score_sub.json", method = RequestMethod.POST)
	public AppResultData<Object> scoreSub(
		@RequestParam("resumeNo") String resumeNo
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Integer nowTime = TimeStampUtil.getNowSecond().intValue();
		Resume resumeSearchVo = new Resume();
		resumeSearchVo.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(resumeSearchVo);
		
		Integer totalScore = resume.getTotalScore();
		totalScore = totalScore - Constants.SCORE_NOT_ACTIVE;
		resume.setTotalScore(totalScore);
		resume.setUpdateTime(nowTime);
		resumeService.update(resume);
		
		Integer resumeId = resume.getResumeId();
		//鲜活度明细
		ResumeScore  resumeScore = resumeScoreService.initPo(new ResumeScore());
		resumeScore.setResumeId(resumeId);
		resumeScore.setResumeNo(resumeNo);
		resumeScore.setActionType(1);
		resumeScore.setAction(Constants.ACTION_SUB_RESUME_NOTACITVIE);
		resumeScore.setRemark("");
		resumeScore.setAddTime(nowTime);
		resumeScoreService.insert(resumeScore);
		
		//todo 提醒：7天不刷新短信提醒，15天提醒一次，30天提醒一次  90天提醒一次（最后一次）
		
		return result;
	}
}
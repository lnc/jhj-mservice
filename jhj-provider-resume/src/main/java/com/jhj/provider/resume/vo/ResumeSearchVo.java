package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.Resume;

public class ResumeSearchVo extends Resume {
	
	public Integer serviceCityId;
	
	public String serviceRegionIds;
	
	public String serviceTypes;
	
	public Integer ageId;
	
	public Integer serviceTypeId;
	
	public Integer ageFrom;
	
	public Integer ageTo;

	public Integer getServiceCityId() {
		return serviceCityId;
	}

	public void setServiceCityId(Integer serviceCityId) {
		this.serviceCityId = serviceCityId;
	}

	public String getServiceTypes() {
		return serviceTypes;
	}

	public void setServiceTypes(String serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	public Integer getAgeId() {
		return ageId;
	}

	public void setAgeId(Integer ageId) {
		this.ageId = ageId;
	}

	public String getServiceRegionIds() {
		return serviceRegionIds;
	}

	public void setServiceRegionIds(String serviceRegionIds) {
		this.serviceRegionIds = serviceRegionIds;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public Integer getAgeFrom() {
		return ageFrom;
	}

	public void setAgeFrom(Integer ageFrom) {
		this.ageFrom = ageFrom;
	}

	public Integer getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(Integer ageTo) {
		this.ageTo = ageTo;
	}

}

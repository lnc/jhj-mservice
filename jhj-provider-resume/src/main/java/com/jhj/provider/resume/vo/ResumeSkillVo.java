package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.ResumeSkill;

public class ResumeSkillVo extends ResumeSkill {
	
	public String skillName;

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
}

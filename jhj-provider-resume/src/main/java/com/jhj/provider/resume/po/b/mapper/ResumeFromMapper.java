package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeFrom;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeFromMapper extends Mapper<ResumeFrom> {
}
package com.jhj.provider.resume.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Dict;
import com.jhj.provider.resume.service.DictService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.StringUtil;

/**
 *
 */
@RestController
public class DictController {
	
	@Autowired
	DictService dictService;
	
	/**
	 * 查询字典信息
	 * @param dictType
	 * @param pid
	 * @return list<Dict>
	 * 
	 * */
	@RequestMapping(value = "/api/resume/get_dict.json", method = RequestMethod.GET)
	public AppResultData<Object> dictList(
			@RequestParam(value = "dictType", required = false, defaultValue = "") String dictType,
			@RequestParam(value = "pid", required = false, defaultValue = "") String pid) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		if (StringUtil.isEmpty(dictType)) return result;
		Dict searchVo = new Dict();
		searchVo.setDictType(dictType);
		
		if(!StringUtil.isEmpty(pid))searchVo.setPid(pid);
		
		searchVo.setEnable(1);
		List<Dict> list = dictService.selectBySearchVo(searchVo, "id asc");
		result.setData(list);
		return result;
	}
	
	/**
	 * 根据类型集合查询
	 * @param dictTypes
	 * @param pid
	 * @return list<Dict>
	 * */
	@RequestMapping(value = "/api/resume/dict_types.json", method = RequestMethod.GET)
	public AppResultData<Object> dictListByDictTypes(
			@RequestParam(value = "dictTypes", required = false, defaultValue = "") String dictTypes,
			@RequestParam(value = "pid", required = false, defaultValue = "") String pid) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		if (StringUtil.isEmpty(dictTypes)) return result;
		Dict searchVo = new Dict();
		searchVo.setDictType(dictTypes);
		if(!StringUtil.isEmpty(pid))searchVo.setPid(pid);
		searchVo.setEnable(1);
		List<String> dictTypeList = new ArrayList<>();
		if(dictTypes!=null && !"".equals(dictTypes)) {
			String[] serviceTypeAry = StringUtil.convertStrToArray(dictTypes);
			dictTypeList = Arrays.asList(serviceTypeAry);
		}
		List<Dict> list = dictService.selectByDictTypes(searchVo,dictTypeList);
		result.setData(list);
		return result;
	}
	

}
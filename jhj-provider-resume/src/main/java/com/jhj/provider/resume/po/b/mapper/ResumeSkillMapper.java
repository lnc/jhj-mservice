package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeSkill;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeSkillMapper extends Mapper<ResumeSkill> {
}
package com.jhj.provider.resume.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jhj.utils.AppResultData;

@FeignClient("jhj-provider-user")
public interface UserOpenidClient {
	
	@RequestMapping(value="/api/user/addOpenid",method=RequestMethod.POST)
	public AppResultData<Object> addUserOpenid(
			@RequestParam(value="mobile") String mobile,
			@RequestParam(value="openid") String openid,
			@RequestParam(value="userType",defaultValue="0") Integer userType,
			@RequestParam("appkey") String appkey,
			@RequestParam("oncestr") String oncestr,
			@RequestParam("sign") String sign);

}

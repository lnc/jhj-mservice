package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "resume_share")
public class ResumeShare {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 人才ID
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * 销售老师ID
     */
    @Column(name = "sale_id")
    private Integer saleId;

    /**
     * 手机号
     */
    @Column(name = "sale_mobile")
    private String saleMobile;

    /**
     * 姓名
     */
    @Column(name = "sale_name")
    private String saleName;

    /**
     * 推荐人评价
     */
    @Column(name = "share_rate")
    private String shareRate;
    
    /**
     * 二维码图片
     */
    private String qrcode;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取人才ID
     *
     * @return resume_id - 人才ID
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才ID
     *
     * @param resumeId 人才ID
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取销售老师ID
     *
     * @return sale_id - 销售老师ID
     */
    public Integer getSaleId() {
        return saleId;
    }

    /**
     * 设置销售老师ID
     *
     * @param saleId 销售老师ID
     */
    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    /**
     * 获取手机号
     *
     * @return sale_mobile - 手机号
     */
    public String getSaleMobile() {
        return saleMobile;
    }

    /**
     * 设置手机号
     *
     * @param saleMobile 手机号
     */
    public void setSaleMobile(String saleMobile) {
        this.saleMobile = saleMobile;
    }

    /**
     * 获取姓名
     *
     * @return sale_name - 姓名
     */
    public String getSaleName() {
        return saleName;
    }

    /**
     * 设置姓名
     *
     * @param saleName 姓名
     */
    public void setSaleName(String saleName) {
        this.saleName = saleName;
    }

    /**
     * 获取推荐人评价
     *
     * @return share_rate - 推荐人评价
     */
    public String getShareRate() {
        return shareRate;
    }

    /**
     * 设置推荐人评价
     *
     * @param shareRate 推荐人评价
     */
    public void setShareRate(String shareRate) {
        this.shareRate = shareRate;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
}
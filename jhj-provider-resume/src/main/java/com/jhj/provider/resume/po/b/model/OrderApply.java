package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "order_apply")
public class OrderApply {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 人才id
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * 人才手机号
     */
    @Column(name = "resume_mobile")
    private String resumeMobile;

    /**
     * 人才名称
     */
    @Column(name = "resume_name")
    private String resumeName;

    /**
     * 应聘时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单id
     *
     * @return order_id - 订单id
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单id
     *
     * @param orderId 订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取人才id
     *
     * @return resume_id - 人才id
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才id
     *
     * @param resumeId 人才id
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取人才手机号
     *
     * @return resume_mobile - 人才手机号
     */
    public String getResumeMobile() {
        return resumeMobile;
    }

    /**
     * 设置人才手机号
     *
     * @param resumeMobile 人才手机号
     */
    public void setResumeMobile(String resumeMobile) {
        this.resumeMobile = resumeMobile;
    }

    /**
     * 获取人才名称
     *
     * @return resume_name - 人才名称
     */
    public String getResumeName() {
        return resumeName;
    }

    /**
     * 设置人才名称
     *
     * @param resumeName 人才名称
     */
    public void setResumeName(String resumeName) {
        this.resumeName = resumeName;
    }

    /**
     * 获取应聘时间
     *
     * @return add_time - 应聘时间
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置应聘时间
     *
     * @param addTime 应聘时间
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
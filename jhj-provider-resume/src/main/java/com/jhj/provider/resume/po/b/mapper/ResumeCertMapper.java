package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeCert;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeCertMapper extends Mapper<ResumeCert> {
}
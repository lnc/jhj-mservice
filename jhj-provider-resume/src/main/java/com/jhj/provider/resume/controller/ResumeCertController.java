package com.jhj.provider.resume.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeCert;
import com.jhj.provider.resume.po.b.model.ResumeScore;
import com.jhj.provider.resume.service.ResumeCertService;
import com.jhj.provider.resume.service.ResumeLogService;
import com.jhj.provider.resume.service.ResumeScoreService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.ImgServerUtil;
import com.jhj.utils.TimeStampUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 *
 */
@RestController
public class ResumeCertController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeScoreService resumeScoreService;
	
	@Autowired
	ResumeLogService resumeLogService;
	
	@Autowired
	ResumeCertService resumeCertService;
	
	@Value("${img.host}")
	private String imgHost;
	
	/**
	 * 上传证书接口
	 * @param resumeNo
	 * @param resourceType
	 * @param canMulti      0 = 仅允许一张 1 = 允许多张
	 * @param imgs
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/api/resume/add_cert.json", method = RequestMethod.POST)
	public AppResultData<Object> uploadImg(
		@RequestParam("resumeNo") String resumeNo,
		@RequestParam("certName") String certName,
		@RequestParam(value = "certType", required = false, defaultValue = "") String certType,
		@RequestParam(value = "certImgs", required = false) MultipartFile[] imgs
		) throws JsonParseException, JsonMappingException, IOException {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//校验1. 判断人才是否存在
		Resume searchVo = new Resume();
		searchVo.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}
		Integer resumeId = resume.getResumeId();
		
		
		
		//处理上传图片
		for (int i = 0; i < imgs.length; i++) {
			String url = imgHost + "/upload/";
			String fileName = imgs[i].getOriginalFilename();
			String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
			fileType = fileType.toLowerCase();
			String sendResult = ImgServerUtil.sendPostBytes(url, imgs[i].getBytes(), fileType);

			ObjectMapper mapper = new ObjectMapper();
			HashMap<String, Object> o = mapper.readValue(sendResult, HashMap.class);
//					String ret = o.get("ret").toString();
			HashMap<String, String> info = (HashMap<String, String>) o.get("info");
			String imgUrl = imgHost + "/" + info.get("md5").toString();
			
			ResumeCert record = resumeCertService.initPo(new ResumeCert());
			record.setResumeId(resumeId);
			record.setResumeNo(resumeNo);
			record.setCertName(certName);
			record.setCertType(certType);
			record.setCertImg(imgUrl);
			record.setAddTime(TimeStampUtil.getNowSecond().intValue());
			resumeCertService.insert(record);
		}
		
		//信息完整度
		Integer totalComplete = resumeScoreService.getTotalComplete(resume);
		resume.setTotalComplete(totalComplete);
		
		Integer totalScore = resume.getTotalScore();
		totalScore = totalScore + Constants.SCORE_COMPLETE_HEADIMG;
		if (totalScore > Constants.SCORE_MAX) totalScore = Constants.SCORE_MAX;
		resume.setTotalScore(totalScore);
		
		resume.setHasCert(1);
		
		resume.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		resumeService.update(resume);
		
		//检查鲜活度明细是否已经有过加分
		
		ResumeScore resumeScoreSearchVo = new ResumeScore();
		resumeScoreSearchVo.setResumeId(resumeId);
		resumeScoreSearchVo.setAction(Constants.ACTION_PLUS_RESUME_CERT);
		List<ResumeScore> list = resumeScoreService.selectBySearchVo(resumeScoreSearchVo, "");
		if (list.isEmpty()) {
			//鲜活度明细
			ResumeScore  resumeScore = resumeScoreService.initPo(new ResumeScore());
			resumeScore.setResumeId(resumeId);
			resumeScore.setResumeNo(resumeNo);
			resumeScore.setActionType(0);
			resumeScore.setAction(Constants.ACTION_PLUS_RESUME_CERT);
			resumeScore.setScore(Constants.SCORE_COMPLETE_CERT);
			resumeScore.setRemark("上传证书增加"+ String.valueOf(Constants.SCORE_COMPLETE_CERT));
			resumeScore.setAddTime(TimeStampUtil.getNowSecond().intValue());
			resumeScoreService.insert(resumeScore);
		}
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("resumeId", resumeId);
		data.put("resumeNo", resumeNo);
		
		result.setData(data);		
		return result;
	}
	
	/**
	 * @param resumeNo
	 * @param id
	 * @return result
	 * 
	 * */
	@RequestMapping(value = "/api/resume/cert_del.json", method = RequestMethod.POST)
	public AppResultData<Object> updateDelete(
		@RequestParam("resumeNo") String resumeNo,
		@RequestParam("id") Integer id){
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//校验1. 判断人才是否存在
		Resume searchVo = new Resume();
		searchVo.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}
		Integer resumeId = resume.getResumeId();
	
		resumeCertService.deleteByPrimaryKey(id);
		
		//判断删除之后还有没有证书
		ResumeCert resumeCertSearchVo = new ResumeCert();
		resumeCertSearchVo.setResumeId(resumeId);
		List<ResumeCert> list = resumeCertService.selectBySearchVo(resumeCertSearchVo, "");
		if (list.isEmpty()) {
			resume.setHasCert(2);
			resume.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
			resumeService.update(resume);
		}
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("resumeId", resumeId);
		data.put("resumeNo", resumeNo);
		
		result.setData(data);		
		return result;
	}
}
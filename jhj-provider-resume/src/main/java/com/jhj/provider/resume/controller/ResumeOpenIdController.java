package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.util.StringUtil;
import com.jhj.provider.resume.po.b.model.ResumeOpenid;
import com.jhj.provider.resume.service.ResumeOpenidService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;


/**
 *
 */
@RestController
public class ResumeOpenIdController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeOpenidService resumeOpenidService;
	
	/**
	 * 绑定微信信息 
	 * 1. 如果只有openid ,则先查询是否已经绑定，没有则会插入resume_openid, 返回为空数据.
	 * 2. 如果有mobile 或者 resumeNo, 则查询数据是完整，没有则会插入.
	 * @param openid
	 * @param mobile
	 * @param resumeNo
	 * @return
	 */
	@RequestMapping(value = "/api/resume/openid.json", method = RequestMethod.POST)
	public AppResultData<Object> resumereFresh(
		@RequestParam("openid") String openid
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		if (StringUtil.isEmpty(openid)) {
			return result;
		}
		
		//查询reusme_openid
		ResumeOpenid resumeOpenidSearchVo = new ResumeOpenid();
		resumeOpenidSearchVo.setOpenId(openid);
		List<ResumeOpenid> list = resumeOpenidService.selectBySearchVo(resumeOpenidSearchVo, "");
		
		String resumeNo = "";
		Integer resumeId = 0;
		if (list.isEmpty()) {
			ResumeOpenid record = resumeOpenidService.initPo(new ResumeOpenid());
			record.setOpenId(openid);
			record.setAddTime(TimeStampUtil.getNowSecond().intValue());
			resumeOpenidService.insert(record);
		} else {
			ResumeOpenid item = list.get(0);
			resumeNo = item.getResumeNo();
			resumeId = item.getResumeId();
		}
		
		if (resumeId > 0) {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("resumeId", resumeId);
			data.put("resumeNo", resumeNo);
			result.setData(data);
		}
		
		
		return result;
	}
}
package com.jhj.provider.resume.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeShare;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.service.ResumeShareService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.utils.AppResultData;


/**
 *
 */
@RestController
public class ResumeShareController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeShareService resumeShareService;
	
	/**
	 * 推荐人信息
	 * @param resumeId
	 * @param saleId
	 * @return resumeShare
	 * */
	@RequestMapping(value = "/api/resume/getShareInfo.json", method = RequestMethod.GET)
	public AppResultData<Object> saveFromRate(
			@RequestParam("resumeId") Integer resumeId,
			@RequestParam(value = "saleId", required = false, defaultValue = "0") Integer saleId) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Resume searchVo = new Resume();
		searchVo.setResumeId(resumeId);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}
		
		
		//推荐人信息， 如果有跟saleId 一致的，则获取，否则获取最早的一条记录，如果都没有则返回空
		ResumeShare resumeShareSearchVo = new ResumeShare();
		resumeShareSearchVo.setResumeId(resumeId);
		List<ResumeShare> list = resumeShareService.selectBySearchVo(resumeShareSearchVo, "id asc");
		
		ResumeShare resumeShare = null;
		if (saleId > 0) {
			for (ResumeShare item : list) {
				if (item.getSaleId() == saleId) {
					resumeShare = item;
					break;
				}
			}
		} 
		
		
		if (resumeShare == null && !list.isEmpty()) {
			resumeShare = list.get(0);
		}
		
		if (resumeShare != null) {

			String mobile = resumeShare.getSaleMobile();
			StringBuffer bf = new StringBuffer();
			bf.append(mobile.substring(0, 2)).append("*****").append(mobile.substring(8));
			resumeShare.setSaleMobile(mobile);
			result.setData(resumeShare);
		}
				
		return result;
	}
		
	/**
	 * 更新推荐人评价
	 * @param resumeNo
	 * @param shareRate
	 * @param saleId
	 * @param saleName
	 * @param saleMobile
	 * 
	 * @return result
	 * */
	@RequestMapping(value = "/api/resume/saveShareRate.json", method = RequestMethod.POST)
	public AppResultData<Object> saveFromRate(
			@RequestParam("resumeNo") String resumeNo,
			@RequestParam("shareRate") String shareRate,
			@RequestParam("saleId") Integer saleId,
			@RequestParam("saleName") String saleName,
			@RequestParam("saleMobile") String saleMobile) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Resume searchVo = new Resume();
		searchVo.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}
		Integer resumeId = resume.getResumeId();
		
		//推荐人信息更新
		ResumeShare resumeShareSearchVo = new ResumeShare();
		resumeShareSearchVo.setResumeId(resumeId);
		resumeShareSearchVo.setSaleId(saleId);
		ResumeShare resumeShare = resumeShareService.selectOne(resumeShareSearchVo);
		if (resumeShare == null) {
			resumeShare = resumeShareService.initPo(new ResumeShare());
			resumeShare.setResumeId(resumeId);
			resumeShare.setResumeNo(resumeNo);
			resumeShare.setSaleId(saleId);
			resumeShare.setSaleMobile(saleMobile);
			resumeShare.setSaleName(saleName);
			resumeShare.setShareRate(shareRate);
			
			resumeShareService.insert(resumeShare);
		} else {
			resumeShare.setShareRate(shareRate);
			resumeShareService.update(resumeShare);
		}
				
		return result;
	}
}
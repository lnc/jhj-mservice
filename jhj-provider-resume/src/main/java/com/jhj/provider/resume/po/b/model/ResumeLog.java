package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "resume_log")
public class ResumeLog {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 人才ID
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * resume_add = 简历添加 resuem_update= 简历修改
     */
    @Column(name = "log_type")
    private String logType;

    /**
     * 操作名称
     */
    @Column(name = "log_name")
    private String logName;

    /**
     * 操作人ID  0 = 系统操作
     */
    @Column(name = "action_id")
    private Integer actionId;

    /**
     * 操作人名称
     */
    @Column(name = "action_name")
    private String actionName;

    /**
     * 操作说明
     */
    private String remark;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取人才ID
     *
     * @return resume_id - 人才ID
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才ID
     *
     * @param resumeId 人才ID
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取resume_add = 简历添加 resuem_update= 简历修改
     *
     * @return log_type - resume_add = 简历添加 resuem_update= 简历修改
     */
    public String getLogType() {
        return logType;
    }

    /**
     * 设置resume_add = 简历添加 resuem_update= 简历修改
     *
     * @param logType resume_add = 简历添加 resuem_update= 简历修改
     */
    public void setLogType(String logType) {
        this.logType = logType;
    }

    /**
     * 获取操作名称
     *
     * @return log_name - 操作名称
     */
    public String getLogName() {
        return logName;
    }

    /**
     * 设置操作名称
     *
     * @param logName 操作名称
     */
    public void setLogName(String logName) {
        this.logName = logName;
    }

    /**
     * 获取操作人ID  0 = 系统操作
     *
     * @return action_id - 操作人ID  0 = 系统操作
     */
    public Integer getActionId() {
        return actionId;
    }

    /**
     * 设置操作人ID  0 = 系统操作
     *
     * @param actionId 操作人ID  0 = 系统操作
     */
    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    /**
     * 获取操作人名称
     *
     * @return action_name - 操作人名称
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * 设置操作人名称
     *
     * @param actionName 操作人名称
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    /**
     * 获取操作说明
     *
     * @return remark - 操作说明
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置操作说明
     *
     * @param remark 操作说明
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
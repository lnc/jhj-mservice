package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.support.json.JSONUtils;
import com.jhj.provider.resume.client.UserOpenidClient;
import com.jhj.provider.resume.client.UserSmsTokenClient;
import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.vo.ResumeSearchVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.HttpClientUtil;
import com.jhj.utils.sign.JhjSignConfig;
import com.jhj.utils.sign.JhjSignUtils;


@RestController
public class ResumeLoginController {
	
	//用户不存在，记录在openid表中
	@Autowired
	private ResumeService resumeService;
	
	@Autowired
	private UserSmsTokenClient userSmsTokenClient;
	
	@Autowired
	private UserOpenidClient userOpenidClient;
	
	/**
	 * @param mobile
	 * @param smsToken
	 * @param userType
	 * @param code
	 * 
	 * @return map
	 * 
	 * */
	@RequestMapping(value="/api/resume/login.json",method=RequestMethod.POST)
	public AppResultData<Object> login(
			HttpServletRequest request, 
			@RequestParam("mobile") String mobile, 
			@RequestParam("smsToken") String smsToken,
			@RequestParam(value = "userType", required = false, defaultValue = "1") int userType,
			@RequestParam("code") String wxcode){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		String url = "https://api.weixin.qq.com/sns/jscode2session";
		Map<String,String> params = new HashMap<>();
		params.put("appid", "wx6f00f70ea215500d");
		params.put("secret", "20df3180aed615d481f5a924717947ea");
		params.put("js_code", wxcode);
		params.put("grant_type", "authorization_code");
		String string = HttpClientUtil.get(url, params);
		Map<String,String> map = (Map<String,String>)JSONUtils.parse(string);
		String openid = map.get("openid");
		
		HashMap<String,String> param = new HashMap<>();
		param.put("mobile", mobile);
		param.put("smsType", "0");
		param.put("smsToken", smsToken);
		Map<String,String> param1 = JhjSignUtils.getSignParams(param);
		String appkey = JhjSignConfig.appkey;
		String oncestr = param1.get("oncestr").toString();
		String sign = param1.get("sign").toString();
		
		//检测验证码是否正确
		
		AppResultData<String> checkResult = userSmsTokenClient.checkCode(mobile, smsToken, 0, appkey, oncestr, sign);
		if (checkResult.getStatus() == Constants.ERROR_999) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg(checkResult.getMsg());
			return data;
		}
		
		Resume searchVo = new Resume();
		searchVo.setMobile(mobile);
		Resume resume = resumeService.selectOne(searchVo);
		if(resume==null) {
			HashMap<String,String> p1 = new HashMap<>();
			p1.put("mobile", mobile);
			p1.put("openid", openid);
			p1.put("userType", String.valueOf(userType));

			Map<String,String> p = JhjSignUtils.getSignParams(p1);
			userOpenidClient.addUserOpenid(mobile, openid, userType, appkey, p.get("oncestr"), p.get("sign"));
			p1.put("resumeNo", "");
			p1.put("name", "");
			p1.put("total", "0");
			p1.put("rowNum", "0");
			p1.put("totalScore", "0");
			data.setData(p1);
			data.setMsg("请先注册");
			return data;
		}
		map.put("mobile", resume.getMobile());
		map.put("resumeNo", resume.getResumeNo());
		map.put("resumeId", String.valueOf(resume.getResumeId()));
		map.put("openid", openid);
		map.put("name", resume.getName());
		
		//总人数，排名，鲜活度值
		//总数
		ResumeSearchVo resumeSearchVo = new ResumeSearchVo();
		Integer total = resumeService.selectCountBySearchVo(resumeSearchVo);
				
		//排名
		Integer rowNum = resumeService.selectByRowNum(resume.getResumeId());
		map.put("total", String.valueOf(total));
		map.put("rowNum", String.valueOf(rowNum));
		map.put("totalScore", String.valueOf(resume.getTotalScore()));
		data.setData(map);
		return data;
	}

}

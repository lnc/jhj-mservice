package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeFrom;
import com.jhj.provider.resume.po.b.model.ResumeLog;
import com.jhj.provider.resume.po.b.model.ResumeResource;
import com.jhj.provider.resume.po.b.model.ResumeScore;
import com.jhj.provider.resume.po.b.model.ResumeServiceArea;
import com.jhj.provider.resume.po.b.model.ResumeSkill;
import com.jhj.provider.resume.service.DictService;
import com.jhj.provider.resume.service.ResumeFromService;
import com.jhj.provider.resume.service.ResumeLogService;
import com.jhj.provider.resume.service.ResumeResourceService;
import com.jhj.provider.resume.service.ResumeScoreService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.service.ResumeServiceAreaService;
import com.jhj.provider.resume.service.ResumeSkillService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.utils.ResumeUtil;
import com.jhj.provider.resume.vo.ResumeSearchVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.IdcardInfoExtractor;
import com.jhj.utils.OrderNoUtil;
import com.jhj.utils.StringUtil;
import com.jhj.utils.TimeStampUtil;


/**
 *
 */
@RestController
public class ResumeController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeScoreService resumeScoreService;
	
	@Autowired
	ResumeLogService resumeLogService;
	
	@Autowired
	ResumeFromService resumeFromService;
	
	@Autowired
	ResumeSkillService resumeSkillService;
	
	@Autowired
	ResumeServiceAreaService resumeServiceAreaService;
	
	@Autowired
	ResumeResourceService resumeResourceService;
	
	@Autowired
	DictService dictService;
	
	/**
	 * 新增接口
	 * @param mobile
	 * @param name
	 * @param sex
	 * @param workYear
	 * @param serviceTypes
	 * @param serviceAreas
	 * @param resumeFrom
	 * @param fromId
	 * @param fromMobile
	 * @param fromName
	 * @param provinceId
	 * @param cityId
	 * @param idCard
	 * @param addr
	 * @param remark
	 * @return
	 */
	
	@RequestMapping(value = "/api/resume/add.json", method = RequestMethod.POST)
	public AppResultData<Object> add(
		@RequestParam("mobile") String mobile,
		@RequestParam("name") String name,
		@RequestParam("sex") Integer sex,
		@RequestParam("workYear") Integer workYear,
		@RequestParam("serviceTypes") String serviceTypes,
		@RequestParam(value = "serviceProvinceId", required = false, defaultValue = "0") Integer serviceProvinceId,
		@RequestParam(value = "serviceCityId", required = false, defaultValue = "0") Integer serviceCityId,
		@RequestParam(value = "regionids", required = false, defaultValue = "0") String regionids,
		@RequestParam("resumeFrom") Integer resumeFrom,
		@RequestParam(value = "serviceCanDate", required = false, defaultValue = "") String serviceCanDate,
		@RequestParam(value = "fromId", required = false, defaultValue = "0") Integer fromId,
		@RequestParam(value = "fromMobile", required = false, defaultValue = "") String fromMobile,
		@RequestParam(value = "fromName", required = false, defaultValue = "") String fromName,
		@RequestParam(value = "fromRate", required = false, defaultValue = "") String fromRate,
		@RequestParam(value = "provinceId", required = false, defaultValue = "0") Integer provinceId,
		@RequestParam(value = "cityId", required = false, defaultValue = "0") Integer cityId,
		@RequestParam(value = "hukou", required = false, defaultValue = "") String hukou,
		@RequestParam(value = "degreeId", required = false, defaultValue = "0") Integer degreeId,
		@RequestParam(value = "idCard", required = false, defaultValue = "") String idCard,
		@RequestParam(value = "addr", required = false, defaultValue = "") String addr,
		@RequestParam(value = "remark", required = false, defaultValue = "") String remark,
		@RequestParam(value = "headImg", required = false, defaultValue = "") String headImg,
		@RequestParam(value = "nationId", required = false, defaultValue = "") Integer nationId,
		@RequestParam(value = "hopeSalary", required = false, defaultValue = "") Integer hopeSalary
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//校验1. 判断手机号是否已经存在
		Resume searchVo = new Resume();
		searchVo.setMobile(mobile);
		Resume isExistResume = resumeService.selectOne(searchVo);
		if (isExistResume != null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("此手机号码已经注册过.");
			return result;
		}
		//校验2.判断身份照是否已经存在.
		if (!StringUtil.isEmpty(idCard)) {
			searchVo = new Resume();
			searchVo.setIdCard(idCard);
			isExistResume = resumeService.selectOne(searchVo);
			if (isExistResume != null) {
				result.setStatus(Constants.ERROR_999);
				result.setMsg("身份证号码已经注册过.");
				return result;
			}
		}
		
		//生成人才唯一编号.
		String resumeNo = String.valueOf(OrderNoUtil.getResumeNo());
		
		Integer nowTime = TimeStampUtil.getNowSecond().intValue();
		
		Resume record = resumeService.initPo(new Resume());		
		record.setResumeNo(resumeNo);
		record.setMobile(mobile);
		record.setName(name);
		record.setSex(sex);
		
		record.setRefreshTime(nowTime);
		record.setWorkYear(workYear);
		
		record.setProvinceId(provinceId);
		record.setCityId(cityId);
		if (StringUtil.isEmpty(hukou)) {
			if (provinceId > 0) {
				hukou+= dictService.getDictName(provinceId);
			}
			if (cityId > 0) {
				hukou+= " " + dictService.getDictName(cityId);
			}
		}
		
		
		if (!StringUtil.isEmpty(hukou)) {
			String[] hukouAry = StringUtil.convertStrToArrayByDelimiter(hukou, " ");
			if (provinceId == 0) {
				String provinceName = hukouAry[0];
				provinceId = dictService.getIdByName(Constants.DICT_PROVICE, provinceName);
				if (provinceId > 0) record.setProvinceId(provinceId);
			}
			
			if (cityId == 0) {
				String cityName = hukouAry[1];
				cityId = dictService.getIdByName(Constants.DICT_CITY, cityName);
				if (cityId > 0) record.setCityId(cityId);
			}
		}
		
		
		record.setHukou(hukou);
		record.setIdCard(idCard);
		if (!StringUtil.isEmpty(idCard)) {
			//根据身份证号计算出年龄
			IdcardInfoExtractor ie = new IdcardInfoExtractor(idCard);
			Integer age = ie.getAge();
			record.setAge(age);
		}
		
		record.setAddr(addr);
		record.setRemark(remark);
		record.setDegreeId(degreeId);
		record.setServiceCanDate(serviceCanDate);
		record.setAddTime(nowTime);
		record.setUpdateTime(nowTime);
		record.setStatus(1);
		record.setNationId(nationId);
		record.setHopeSalary(hopeSalary);
		
		resumeService.insert(record);
		Integer resumeId = record.getResumeId();
		
		
		
		
		//如果有头像，则直接插入
		if (!StringUtil.isEmpty(headImg)) {
			ResumeResource resourceSearchVo = new ResumeResource();
			resourceSearchVo.setResumeId(resumeId);
			resourceSearchVo.setResourceType("resume_head_img");
			resumeResourceService.delete(resourceSearchVo);
			
			ResumeResource resumeResource = resumeResourceService.initPo(new ResumeResource());
			resumeResource.setResumeId(resumeId);
			resumeResource.setResumeNo(resumeNo);
			resumeResource.setResourceType("resume_head_img");
			resumeResource.setResourceUrl(headImg);
			resumeResource.setAddTime(TimeStampUtil.getNowSecond().intValue());
			resumeResourceService.insert(resumeResource);
		}
		
		//信息完整度
		Integer totalComplete = resumeScoreService.getTotalComplete(record);
		record.setTotalComplete(totalComplete);
		record.setTotalScore(Constants.SCORE_DEFAULT + totalComplete);
		resumeService.update(record);
		
		//绑定推荐人
		ResumeFrom resumeFromRecord = resumeFromService.initPo(new ResumeFrom());
		resumeFromRecord.setResumeId(resumeId);
		resumeFromRecord.setResumeNo(resumeNo);
		resumeFromRecord.setResumeFrom(resumeFrom);
		resumeFromRecord.setFromId(fromId);
		resumeFromRecord.setFromMobile(fromMobile);
		resumeFromRecord.setFromName(fromName);
		resumeFromRecord.setFromRate(fromRate);
		resumeFromRecord.setAddTime(nowTime);
		resumeFromService.insert(resumeFromRecord);
		
		//处理服务技能
		if (!StringUtil.isEmpty(serviceTypes)) {
			String[] serviceTypeAry = StringUtil.convertStrToArray(serviceTypes);
			int len = serviceTypeAry.length;
			for (int i = 0 ; i < len; i++) {
				String serviceTypeIdStr = serviceTypeAry[i];
				if (StringUtil.isEmpty(serviceTypeIdStr)) continue;
				Integer serviceTypeId = Integer.valueOf(serviceTypeIdStr);
				ResumeSkill resumeSkill = resumeSkillService.initPo(new ResumeSkill());
				resumeSkill.setResumeId(resumeId);
				resumeSkill.setResumeNo(resumeNo);
				resumeSkill.setSkillId(serviceTypeId);
				resumeSkill.setAddTime(nowTime);
				resumeSkillService.insert(resumeSkill);
			}
		}
		
		//处理服务区域
		if (!StringUtil.isEmpty(regionids)) {
			String[] serviceRegionIdAry = StringUtil.convertStrToArray(regionids);
			int len = serviceRegionIdAry.length;
			for (int i = 0 ; i < len; i++) {
				String regionIdStr = serviceRegionIdAry[i];
				if (StringUtil.isEmpty(regionIdStr)) continue;
				if (regionIdStr.equals("0")) continue;
				Integer serviceRegionId = Integer.valueOf(regionIdStr);
				ResumeServiceArea serviceArea = resumeServiceAreaService.initPo(new ResumeServiceArea());
				serviceArea.setResumeId(resumeId);
				serviceArea.setResumeNo(resumeNo);
				serviceArea.setProvinceId(serviceProvinceId);
				serviceArea.setCityId(serviceCityId);
				serviceArea.setRegionId(serviceRegionId);
				serviceArea.setAddTime(nowTime);
				resumeServiceAreaService.insert(serviceArea);
			}
		}
		
		
		//鲜活度明细
		ResumeScore  resumeScore = resumeScoreService.initPo(new ResumeScore());
		resumeScore.setResumeId(resumeId);
		resumeScore.setResumeNo(resumeNo);
		resumeScore.setActionType(0);
		resumeScore.setAction(Constants.ACTION_PLUS_RESUME_ADD);
		resumeScore.setScore(Constants.SCORE_DEFAULT + totalComplete);
		resumeScore.setRemark("");
		resumeScore.setAddTime(nowTime);
		resumeScoreService.insert(resumeScore);
		
		
		//人才库日志
		ResumeLog resumeLog = resumeLogService.initPo(new ResumeLog());
		resumeLog.setResumeId(resumeId);
		resumeLog.setResumeNo(resumeNo);
		resumeLog.setLogType(Constants.LOG_TYPE_ADD);
		String logTypeName = ResumeUtil.getLogTypeName(Constants.LOG_TYPE_ADD);
		resumeLog.setLogName(logTypeName);
		resumeLog.setActionId(fromId);
		resumeLog.setActionName(fromName);
		resumeLog.setRemark(logTypeName);
		resumeLog.setAddTime(nowTime);
		resumeLogService.insert(resumeLog);
		
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("resumeId", resumeId);
		data.put("resumeNo", resumeNo);
		data.put("name", name);
		//总数
		ResumeSearchVo resumeSearchVo = new ResumeSearchVo();
		Integer total = resumeService.selectCountBySearchVo(resumeSearchVo);
		
		//排名
		Integer rowNum = resumeService.selectByRowNum(resumeId);
		
		data.put("total", total);
		data.put("rowNum", rowNum);
		data.put("totalScore", record.getTotalScore());
		
		result.setData(data);		
		return result;
	}
}
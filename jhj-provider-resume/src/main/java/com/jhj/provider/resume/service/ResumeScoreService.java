package com.jhj.provider.resume.service;

import java.util.List;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeScore;
import com.jhj.provider.resume.vo.ResumeScoreSearchVo;
import com.jhj.provider.resume.vo.ResumeScoreVo;

/**
 * 通用Service
 */
public interface ResumeScoreService extends BaseService<ResumeScore> {

	Integer getTotalComplete(Resume entity);

	List<ResumeScore> selectBySearchVoSelf(ResumeScoreSearchVo searchVo, String orderBy);

	ResumeScoreVo getVo(ResumeScore item);

	
	
}

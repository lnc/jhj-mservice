package com.jhj.provider.resume.cron;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeScore;
import com.jhj.provider.resume.service.ResumeLogService;
import com.jhj.provider.resume.service.ResumeScoreService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.vo.ResumeScoreSearchVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;


/**
 *
 */
@RestController
public class CronScorehController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeScoreService resumeScoreService;
	
	@Autowired
	ResumeLogService resumeLogService;
	
	/**
	 * 鲜活度定时任务
	 * 每天不刷新扣除1分
	 * */
	@RequestMapping(value = "/api/resume/cron_score.json", method = RequestMethod.GET)
	public AppResultData<Object> list() {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//找出当天有刷新纪录的信息
		ResumeScoreSearchVo searchVo = new ResumeScoreSearchVo();
		searchVo.setAction(Constants.ACTION_PLUS_RESUME_SIGN);
		Integer startTime = TimeStampUtil.getBeginOfToday().intValue();
		Integer endTime = TimeStampUtil.getEndOfToday().intValue();
		searchVo.setStartTime(startTime);
		searchVo.setEndTime(endTime);
		List<ResumeScore> list = resumeScoreService.selectBySearchVoSelf(searchVo, "add_time desc");
		
		List<Integer> activeResumeIds = new ArrayList<Integer>();
		for (ResumeScore item : list) {
			Integer resumeId = item.getResumeId();
			if (!activeResumeIds.contains(resumeId)) activeResumeIds.add(resumeId);
 		}
		
		//找出所有的resumeId
		Integer nowTime = TimeStampUtil.getNowSecond().intValue();
		List<Resume> allList = resumeService.selectBySearchVo(new Resume(), "");
		for (Resume resume : allList) {
			Integer resumeId = resume.getResumeId();
			String resumeNo = resume.getResumeNo();
			if (!activeResumeIds.contains(resumeId)) {
				
				//查询今天是否已经扣除过，避免重复扣除
				searchVo = new ResumeScoreSearchVo();
				searchVo.setResumeId(resumeId);
				searchVo.setAction(Constants.ACTION_SUB_RESUME_NOTACITVIE);
				searchVo.setStartTime(startTime);
				searchVo.setEndTime(endTime);
				
				List<ResumeScore> elist = resumeScoreService.selectBySearchVoSelf(searchVo, "add_time desc");
				if (!elist.isEmpty()) continue;
				
				Integer totalScore = resume.getTotalScore();
				if (totalScore == 0) continue;
				totalScore = totalScore - Constants.SCORE_NOT_ACTIVE;
				if (totalScore < 0) totalScore = 0;
				
				resume.setTotalScore(totalScore);
				resumeService.update(resume);
				
				//鲜活度明细
				ResumeScore  resumeScore = resumeScoreService.initPo(new ResumeScore());
				resumeScore.setResumeId(resumeId);
				resumeScore.setResumeNo(resumeNo);
				resumeScore.setActionType(1);  // 0 = 加 1 = 减
				resumeScore.setAction(Constants.ACTION_SUB_RESUME_NOTACITVIE);
				resumeScore.setScore(Constants.SCORE_NOT_ACTIVE);
				resumeScore.setRemark("");
				resumeScore.setAddTime(nowTime);
				resumeScoreService.insert(resumeScore);
			}
		}

		return result;
	}
}
package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeLog;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeLogMapper extends Mapper<ResumeLog> {
}
package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.OrderApply;

public class OrderApplyVo extends OrderApply{
	
	private String resourceUrl;
	
	private String addTimeStr;
	
	private String serviceTypeName;

	public String getResourceUrl() {
		return resourceUrl;
	}

	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	public String getAddTimeStr() {
		return addTimeStr;
	}

	public void setAddTimeStr(String addTimeStr) {
		this.addTimeStr = addTimeStr;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}
	

}

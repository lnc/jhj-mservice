package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "resume_resource")
public class ResumeResource {
    /**
     * 资源ID
     */
    @Id
    @Column(name = "resource_id")
    private Integer resourceId;

    /**
     * 资源类型 resume_cert   证书类型 resume_head_img  人才库头像 resume_live           人才库生活照 resume_video        人才库视频
     */
    @Column(name = "resource_type")
    private String resourceType;

    /**
     * 资源地址
     */
    @Column(name = "resource_url")
    private String resourceUrl;

    /**
     * 人才库ID
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才库编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取资源ID
     *
     * @return resource_id - 资源ID
     */
    public Integer getResourceId() {
        return resourceId;
    }

    /**
     * 设置资源ID
     *
     * @param resourceId 资源ID
     */
    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * 获取资源类型 resume_cert   证书类型 resume_head_img  人才库头像 resume_live           人才库生活照 resume_video        人才库视频
     *
     * @return resource_type - 资源类型 resume_cert   证书类型 resume_head_img  人才库头像 resume_live           人才库生活照 resume_video        人才库视频
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * 设置资源类型 resume_cert   证书类型 resume_head_img  人才库头像 resume_live           人才库生活照 resume_video        人才库视频
     *
     * @param resourceType 资源类型 resume_cert   证书类型 resume_head_img  人才库头像 resume_live           人才库生活照 resume_video        人才库视频
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * 获取资源地址
     *
     * @return resource_url - 资源地址
     */
    public String getResourceUrl() {
        return resourceUrl;
    }

    /**
     * 设置资源地址
     *
     * @param resourceUrl 资源地址
     */
    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    /**
     * 获取人才库ID
     *
     * @return resume_id - 人才库ID
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才库ID
     *
     * @param resumeId 人才库ID
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才库编号
     *
     * @return resume_no - 人才库编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才库编号
     *
     * @param resumeNo 人才库编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
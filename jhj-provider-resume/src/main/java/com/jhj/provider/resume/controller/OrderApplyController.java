package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.resume.client.OrderClient;
import com.jhj.provider.resume.client.vo.OrderBaseVo;
import com.jhj.provider.resume.po.b.model.OrderApply;
import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeResource;
import com.jhj.provider.resume.service.OrderApplyService;
import com.jhj.provider.resume.service.ResumeResourceService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.vo.OrderApplyVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.BeanUtilsExp;
import com.jhj.utils.TimeStampUtil;
import com.jhj.utils.sign.JhjSignConfig;
import com.jhj.utils.sign.JhjSignUtils;

@RestController
public class OrderApplyController {
	
	@Autowired
	private OrderApplyService orderApplyService;
	
	@Autowired
	private ResumeService resumeService;
	
	@Autowired
	private ResumeResourceService resumeResourceService;
	
	@Autowired
	public OrderClient orderClient;
	
	/**
	 * 应聘记录添加
	 * @param orderId
	 * @param orderNo
	 * @param resumeId
	 * @param resumeNo
	 * @param resumeName
	 * @param resumeMobile
	 * @return id
	 * 
	 * */
	@RequestMapping(value="/api/resume/apply/add.json",method=RequestMethod.POST)
	public AppResultData<Object> addApply(
			@RequestParam(value="orderId",required=false,defaultValue="") Integer orderId,
			@RequestParam(value="orderNo",required=false,defaultValue="") String orderNo,
			@RequestParam(value="resumeNo") String resumeNo){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		Resume r = new Resume();
		r.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(r);
		if(resume==null) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("用户错误");
			return data;
		}
		
		OrderApply applySearch = new OrderApply();
		applySearch.setOrderNo(orderNo);
		applySearch.setResumeNo(resumeNo);
		OrderApply orderApply = orderApplyService.selectOne(applySearch);
		if(orderApply!=null) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("已经应聘过");
			return data;
		}
		OrderApply apply = new OrderApply();
		apply.setOrderId(orderId);
		apply.setOrderNo(orderNo);
		apply.setResumeId(resume.getResumeId());
		apply.setResumeNo(resumeNo);
		apply.setResumeName(resume.getName());
		apply.setResumeMobile(resume.getMobile());
		apply.setAddTime(TimeStampUtil.getNowSecond().intValue());
		orderApplyService.insert(apply);
		
		data.setData(apply.getId());
		return data;
	}
	
	/**
	 * 查询服务人员的应聘记录
	 * @param resumeNo
	 * @param pageNo
	 * @param pageSize
	 * @return page
	 * */
	@RequestMapping(value="/api/resume/apply/list.json",method=RequestMethod.GET)
	public AppResultData<Object> listApply(
			@RequestParam(value="resumeNo") String resumeNo,
			@RequestParam(value="pageNo",defaultValue="1") Integer pageNo,
			@RequestParam(value="pageSize",defaultValue="10") Integer pageSize){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		Resume r = new Resume();
		r.setResumeNo(resumeNo);
		Resume resume = resumeService.selectOne(r);
		if(resume==null) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("用户错误");
		}
		
		OrderApply apply = new OrderApply();
		apply.setResumeNo(resumeNo);
		PageInfo page = orderApplyService.selectByListPage(apply, pageNo, pageSize, "add_time desc");
		List<OrderApply> list = page.getList();
		for (int i = 0; i < list.size(); i++) {
			OrderApply item = list.get(i);
			OrderApplyVo vo = new OrderApplyVo();
			BeanUtilsExp.copyPropertiesIgnoreNull(item, vo);
			String orderNo = vo.getOrderNo();
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("orderNo", orderNo);
			
			params = JhjSignUtils.getSignParams(params);
			String appkey = JhjSignConfig.appkey;
			String oncestr = params.get("oncestr").toString();
			String sign = params.get("sign").toString();
			
			AppResultData<OrderBaseVo> orderData = orderClient.orderBaseInfo(orderNo, appkey, oncestr, sign);
			OrderBaseVo orderBaseInfo = orderData.getData();
			
			vo.setServiceTypeName(orderBaseInfo.getServiceTypeName());
			list.set(i, vo);
		}
		page = new PageInfo(list);
		data.setData(page);
		return data;
	}
	
	/**
	 * 查询订单应聘人员
	 * @param resumeNo
	 * @param pageNo
	 * @param pageSize
	 * @return page
	 * 
	 * */
	@RequestMapping(value="/api/resume/apply/listByOrderNo.json",method=RequestMethod.GET)
	public AppResultData<Object> applyListByOrderNo(
			@RequestParam(value="orderNo") String orderNo,
			@RequestParam(value="pageNo",defaultValue="1") Integer pageNo,
			@RequestParam(value="pageSize",defaultValue="10") Integer pageSize){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		OrderApply apply = new OrderApply();
		apply.setOrderNo(orderNo);
		PageInfo<OrderApply> page = (PageInfo<OrderApply>)orderApplyService.selectByListPage(apply, pageNo, pageSize, "add_time desc");
		List<OrderApply> list =page.getList();
		Set<Integer> resumeIdSet = new HashSet<>();
		for(Integer i=0;i<list.size();i++) {
			OrderApply l = list.get(i);
			Integer resumeId = l.getResumeId();
			resumeIdSet.add(resumeId);
			
			ResumeResource rr = new ResumeResource();
			rr.setResumeId(resumeId);
			rr.setResourceType("resume_head_img");
			List<ResumeResource> resumeResourceList = resumeResourceService.selectBySearchVo(rr, "add_time desc");
			
			OrderApplyVo vo = new OrderApplyVo();
			BeanUtilsExp.copyPropertiesIgnoreNull(l, vo);
			
			if(!resumeResourceList.isEmpty()) {
				vo.setResourceUrl(resumeResourceList.get(0).getResourceUrl());
			}
			Integer addTime = vo.getAddTime()*1000;
			vo.setAddTimeStr(TimeStampUtil.timeStampToDateStr(addTime.longValue()));
			list.set(i, vo);
		}
		
		page.setList(list);
		data.setData(page);
		return data;
	}


}

package com.jhj.provider.resume.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.resume.po.b.model.ResumeLog;
import com.jhj.provider.resume.service.ResumeLogService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.utils.AppResultData;


/**
 *
 */
@RestController
public class ResumeLogController {
	
	@Autowired
	ResumeService resumeService;
	
	@Autowired
	ResumeLogService resumeLogService;
		
	/**
	 * 日志列表接口
	 * @param resumeNo
	 * @param page
	 * @return pageInfo
	 */
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(value = "/api/resume/log_list.json", method = RequestMethod.GET)
	public AppResultData<Object> list(
		@RequestParam("resumeNo") String resumeNo,
		@RequestParam(value = "page", required = false, defaultValue = "1") Integer page
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		if (page.equals(0)) page = 1;
		ResumeLog searchVo = new ResumeLog();
		searchVo.setResumeNo(resumeNo);
		PageInfo pageInfo = resumeLogService.selectByListPage(searchVo, page,  Constants.PAGE_MAX_NUMBER, "id desc");

		result.setData(pageInfo);
		return result;
	}
	
}
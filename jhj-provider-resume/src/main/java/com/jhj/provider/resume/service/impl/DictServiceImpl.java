package com.jhj.provider.resume.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jhj.provider.resume.po.b.mapper.DictMapper;
import com.jhj.provider.resume.po.b.model.Dict;
import com.jhj.provider.resume.service.DictService;

/**
 * ResumeService实现类
 */
@Service
public class DictServiceImpl extends BaseServiceImpl<Dict> implements DictService  {
	
	@Autowired
	private DictMapper dictMapper;
	
	@Override
	public String getDictName(Integer dictId) {
		String dictName = "";
		if (dictId > 0) {
			Dict dictRecord = mapper.selectByPrimaryKey(dictId);
			if (dictRecord != null) dictName = dictRecord.getName();
		}
		return dictName;
	}
	
	@Override
	public Integer getIdByName(String dictType, String dictName) {
		Integer dictId = 0;
		Dict searchVo = new Dict();
		searchVo.setDictType(dictType);
		searchVo.setName(dictName);
		Dict dictRecord = mapper.selectOne(searchVo);	
		if (dictRecord != null) dictId = dictRecord.getId();
		return dictId;
	}

	@Override
	public List<Dict> selectByDictTypes(Dict dict, List<String> dictTypeList) {
		return dictMapper.selectByDictTypes(dict,dictTypeList);
	}
}

package com.jhj.provider.resume.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jhj.provider.resume.client.vo.OrderBaseVo;
import com.jhj.utils.AppResultData;

@FeignClient("jhj-provider-bm")
public interface OrderClient {
	
	@RequestMapping(value="/api/bm/order/baseinfo.json", method=RequestMethod.GET)
	public AppResultData<OrderBaseVo> orderBaseInfo(
			@RequestParam("orderNo") String orderNo,
			@RequestParam("appkey") String appkey,
			@RequestParam("oncestr") String oncestr,
			@RequestParam("sign") String sign);
	
}

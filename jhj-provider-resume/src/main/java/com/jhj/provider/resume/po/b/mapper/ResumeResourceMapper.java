package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeResource;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeResourceMapper extends Mapper<ResumeResource> {
}
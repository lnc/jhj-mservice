package com.jhj.provider.resume.po.b.mapper;

import java.util.Map;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.vo.ResumeSearchVo;

import tk.mybatis.mapper.common.Mapper;

public interface ResumeMapper extends Mapper<Resume> {
	
	Integer selectCountBySearchVo(ResumeSearchVo searchVo);
	
	Integer selectByRowNum(Integer resumeId);
	
	Map<String, Integer> statNumBySearchVo(ResumeSearchVo searchVo);
}
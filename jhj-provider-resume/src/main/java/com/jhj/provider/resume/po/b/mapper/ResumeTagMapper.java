package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeTag;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeTagMapper extends Mapper<ResumeTag> {
}
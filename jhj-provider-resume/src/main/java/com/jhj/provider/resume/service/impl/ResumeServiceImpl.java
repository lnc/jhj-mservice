package com.jhj.provider.resume.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.github.pagehelper.util.StringUtil;
import com.jhj.provider.resume.po.b.mapper.ResumeMapper;
import com.jhj.provider.resume.po.b.model.Dict;
import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeCert;
import com.jhj.provider.resume.po.b.model.ResumeFrom;
import com.jhj.provider.resume.po.b.model.ResumeResource;
import com.jhj.provider.resume.po.b.model.ResumeServiceArea;
import com.jhj.provider.resume.po.b.model.ResumeSkill;
import com.jhj.provider.resume.service.DictService;
import com.jhj.provider.resume.service.ResumeCertService;
import com.jhj.provider.resume.service.ResumeFromService;
import com.jhj.provider.resume.service.ResumeResourceService;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.service.ResumeServiceAreaService;
import com.jhj.provider.resume.service.ResumeSkillService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.utils.ResumeUtil;
import com.jhj.provider.resume.vo.ResumeDetailVo;
import com.jhj.provider.resume.vo.ResumeListVo;
import com.jhj.provider.resume.vo.ResumeSearchVo;
import com.jhj.provider.resume.vo.ResumeServiceAreaVo;
import com.jhj.provider.resume.vo.ResumeSkillVo;
import com.jhj.utils.BeanUtilsExp;
import com.jhj.utils.DateUtil;
import com.jhj.utils.TimeStampUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * ResumeService实现类
 */
@Service
public class ResumeServiceImpl extends BaseServiceImpl<Resume> implements ResumeService {

	@Autowired
	ResumeResourceService resumeResourceService;

	@Autowired
	ResumeCertService resumeCertService;

	@Autowired
	ResumeSkillService resumeSkillService;

	@Autowired
	ResumeFromService resumeFromService;

	@Autowired
	ResumeServiceAreaService resumeServiceAreaService;

	@Autowired
	DictService dictService;

	@Autowired
	protected ResumeMapper resumeMapper;

	@SuppressWarnings({ "rawtypes" })
	@Override
	public PageInfo selectByListPageSelf(ResumeSearchVo searchVo, int pageNo, int pageSize, String orderBy) {
		PageMethod.startPage(pageNo, pageSize);

		Example example = this.getSearchVoExample(searchVo);
		// 排序
		if (!StringUtil.isEmpty(orderBy)) {
			example.setOrderByClause(orderBy);
		}

		List<Resume> list = mapper.selectByExample(example);
		PageInfo<Resume> pageInfo = new PageInfo<Resume>(list);
		return pageInfo;
	}
	
	@Override
	public Map<String, Integer> statNumBySearchVo(ResumeSearchVo searchVo) {
		
		// 年龄范围
		if (searchVo.getAgeId() > 0) {
			Dict dictSearchVo = new Dict();
			dictSearchVo.setId(searchVo.getAge());
			Dict ageDict = dictService.selectOne(dictSearchVo);
			Integer ageFrom = ageDict.getRangeFrom();
			Integer ageTo = ageDict.getRangeTo();

			if (ageFrom >= 0) {
				searchVo.setAgeFrom(ageFrom);
			}

			if (ageTo > 0) {
				searchVo.setAgeTo(ageTo);
			}
		}
		
		return resumeMapper.statNumBySearchVo(searchVo);
	}
	
	
	private Example getSearchVoExample(ResumeSearchVo searchVo) {
		
		Example example = new Example(Resume.class);
		Criteria criteria = example.createCriteria();

		// 查询服务城市
		if (searchVo.getServiceCityId() > 0) {
			criteria.andCondition("resume_id in (select resume_id from resume_service_area where city_id = "
					+ searchVo.getServiceCityId() + " )");
		}

		// 查询服务城市
		if (!StringUtil.isEmpty(searchVo.getServiceRegionIds())) {
			criteria.andCondition("resume_id in (select resume_id from resume_service_area where region_id in  ( "
					+ searchVo.getServiceRegionIds() + ") )");
		}

		// 查询服务技能
		if (!StringUtil.isEmpty(searchVo.getServiceTypes())) {
			criteria.andCondition("resume_id in (select resume_id from resume_skill where skill_id in ("
					+ searchVo.getServiceTypes() + ") )");
		}

		// 籍贯
		if (searchVo.getProvinceId() > 0) {
			criteria.andCondition("province_id = " + searchVo.getProvinceId());
		}

		// 期望薪水
		if (searchVo.getHopeSalary() > 0) {
			criteria.andCondition("hope_salary = " + searchVo.getHopeSalary());
		}

		// 工作年限
		if (searchVo.getWorkYear() > 0) {
			criteria.andCondition("work_year = " + searchVo.getWorkYear());
		}

		// 学历
		if (searchVo.getDegreeId() > 0) {
			criteria.andCondition("degree_id = " + searchVo.getDegreeId());
		}

		// 年龄范围
		if (searchVo.getAgeId() > 0) {
			Dict dictSearchVo = new Dict();
			dictSearchVo.setId(searchVo.getAge());
			Dict ageDict = dictService.selectOne(dictSearchVo);
			Integer ageFrom = ageDict.getRangeFrom();
			Integer ageTo = ageDict.getRangeTo();

			if (ageFrom >= 0) {
				criteria.andGreaterThanOrEqualTo("age", ageFrom);
			}

			if (ageTo > 0) {
				criteria.andLessThanOrEqualTo("age", ageTo);
			}
		}

		// 手机号
		if (!StringUtil.isEmpty(searchVo.getMobile())) {
			criteria.andCondition("mobile = " + searchVo.getMobile().trim());
		}

		// 姓名全文检索
		if (!StringUtil.isEmpty(searchVo.getName())) {
			criteria.andCondition("MATCH(name) AGAINST (\'" + searchVo.getName() + "\' IN BOOLEAN MODE)");
		}

		// 性别
		if (searchVo.getSex() > 0) {
			criteria.andCondition("sex = " + searchVo.getSex());
		}

		// 是否有证书
		if (searchVo.getHasCert() > 0) {
			criteria.andCondition("has_cert = " + searchVo.getHasCert());
		}

		// 状态
		if (searchVo.getStatus() != null) {
			criteria.andCondition("status = " + searchVo.getStatus());
		}

		// 是否已推荐
		if (searchVo.getPublish() != null) {
			criteria.andCondition("publish = " + searchVo.getPublish());
		}
		return example;
	}

	@Override
	public ResumeListVo getListVo(Resume item) {
		ResumeListVo vo = new ResumeListVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(item, vo);
		Integer resumeId = vo.getResumeId();

		// 刷新时间格式化
		Integer refreshTime = vo.getRefreshTime();
		Long nowTime = TimeStampUtil.getNowSecond();
		Long lastTime = nowTime - Long.valueOf(refreshTime);
		String refreshTimeFormat = DateUtil.formatDateTimeBefore(lastTime);
		vo.setRefreshTimeFormat(refreshTimeFormat);

		// 性别名称
		String sexName = ResumeUtil.getSexName(vo.getSex());
		vo.setSexName(sexName);

		// 头像
		String headImg = Constants.DEFAULT_HEAD_IMG;
		ResumeResource resSearchVo = new ResumeResource();
		resSearchVo.setResumeId(resumeId);
		resSearchVo.setResourceType(Constants.RES_RESUME_HEAD_IMG);
		List<ResumeResource> resHeadImgList = resumeResourceService.selectBySearchVo(resSearchVo, "add_time desc");
		if (resHeadImgList != null && resHeadImgList.size()>0) {
			headImg = resHeadImgList.get(0).getResourceUrl();
		}
		vo.setHeadImg(headImg);

		// 工作年限名称
		String workYearName = dictService.getDictName(vo.getWorkYear());
		vo.setWorkYearName(workYearName);

		// 期望工资名称
		String hopeSalaryName = dictService.getDictName(vo.getHopeSalary());
		vo.setHopeSalaryName(hopeSalaryName);
		
		//学历名称 
		String degreeName = dictService.getDictName(vo.getDegreeId());
		vo.setDegreeName(degreeName);

		// 户口省份
		String provinceName = dictService.getDictName(vo.getProvinceId());
		vo.setProvinceName(provinceName);

		// 户口城市
		String cityName = dictService.getDictName(vo.getCityId());
		vo.setCityName(cityName);

		// 服务类型名称
		String serviceTypeNames = "";
		String serviceTypeIds = "";
		ResumeSkill resumeSkillSearchVo = new ResumeSkill();
		resumeSkillSearchVo.setResumeId(resumeId);
		List<ResumeSkill> resumeSkills = resumeSkillService.selectBySearchVo(resumeSkillSearchVo, "skill_id asc");
		if (!resumeSkills.isEmpty()) {
			for (ResumeSkill resumeSkill : resumeSkills) {
				serviceTypeIds += resumeSkill.getSkillId() + ",";
				Dict dictSearchVo = new Dict();
				dictSearchVo.setId(resumeSkill.getSkillId());
				Dict dictRecord = dictService.selectOne(dictSearchVo);
				if (dictRecord != null) {
					serviceTypeNames += dictRecord.getName() + " ";
				}
			}
		}
		vo.setServiceTypeNames(serviceTypeNames);
		vo.setServiceTypeIds(serviceTypeIds);

		// 状态 1 = 空闲 0 = 上户
		String statusName = ResumeUtil.getResumeStatus(vo.getStatus());
		vo.setStatusName(statusName);
		return vo;
	}

	@Override
	public ResumeDetailVo getDetailVo(Resume item) {
		ResumeListVo listVo = new ResumeListVo();
		listVo = this.getListVo(item);

		ResumeDetailVo vo = new ResumeDetailVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(listVo, vo);

		Integer resumeId = vo.getResumeId();

		// 完整度百分比
		String totalPercent = String.valueOf(Constants.SCORE_DEFAULT + vo.getTotalComplete()) + "%";
		vo.setTotalPercent(totalPercent);

		// 添加时间格式化为yyyy-MM-dd HH:mm:ss
		Long addTime = Long.valueOf(vo.getAddTime() * 1000);
		String addTimeFormat = TimeStampUtil.timeStampToDateStr(addTime, DateUtil.DEFAULT_FULL_PATTERN);
		vo.setAddTimeFormat(addTimeFormat);

		// 排名
		Integer rowNum = 1;
		rowNum = this.selectByRowNum(resumeId);
		vo.setRowNum(rowNum);

		Integer total = 0;
		ResumeSearchVo searchVo = new ResumeSearchVo();
		total = this.selectCountBySearchVo(searchVo);
		vo.setTotal(total);

		// 民族名称
		String nationName = dictService.getDictName(vo.getNationId());
		vo.setNationName(nationName);

		// 学历名称
		String degreeName = dictService.getDictName(vo.getDegreeId());
		vo.setDegreeName(degreeName);

		// 婚否名称
		String maritalName = dictService.getDictName(vo.getMaritalId());
		vo.setMaritalName(maritalName);

		// 星座名称
		String astroName = dictService.getDictName(vo.getAstro());
		vo.setAstroName(astroName);

		// 推荐人信息
		ResumeFrom resumeFromSearchVo = new ResumeFrom();
		resumeFromSearchVo.setResumeId(resumeId);
		ResumeFrom resumeFrom = resumeFromService.selectOne(resumeFromSearchVo);
		if (resumeFrom != null) {
			// 推荐人来源渠道
			String resumeFromName = ResumeUtil.getResumeFromName(resumeFrom.getResumeFrom());
			vo.setResumeFromName(resumeFromName);

			// 推荐人ID
			vo.setFromId(resumeFrom.getFromId());

			// 推荐人手机号
			vo.setFromMobile(resumeFrom.getFromMobile());

			// 推荐人名称
			vo.setFromName(resumeFrom.getFromName());

			// 推荐人评价
			vo.setFromRate(resumeFrom.getFromRate());

			// 推荐时间
			Long fromTime = Long.valueOf(resumeFrom.getAddTime() * 1000);
			String fromTimeFormat = TimeStampUtil.timeStampToDateStr(fromTime, DateUtil.DEFAULT_FULL_PATTERN);
			vo.setFromTimeFormat(fromTimeFormat);
		}

		// 服务区域
		Integer serviceCityId = 0;
		String regionIds = "";
		ResumeServiceArea resumeServiceAreaSearchVo = new ResumeServiceArea();
		resumeServiceAreaSearchVo.setResumeId(resumeId);
		List<ResumeServiceArea> resumeServiceAreas = resumeServiceAreaService
				.selectBySearchVo(resumeServiceAreaSearchVo, "region_id asc");
		List<ResumeServiceAreaVo> resumeServiceAreaVos = new ArrayList<ResumeServiceAreaVo>();
		for (ResumeServiceArea serviceArea : resumeServiceAreas) {
			serviceCityId = serviceArea.getCityId();
			regionIds += serviceArea.getRegionId().toString() + ",";

			ResumeServiceAreaVo resumeServiceAreaVo = new ResumeServiceAreaVo();
			BeanUtilsExp.copyPropertiesIgnoreNull(serviceArea, resumeServiceAreaVo);
			String provinceName = dictService.getDictName(resumeServiceAreaVo.getProvinceId());
			resumeServiceAreaVo.setProvinceName(provinceName);

			String cityName = dictService.getDictName(resumeServiceAreaVo.getCityId());
			resumeServiceAreaVo.setCityName(cityName);

			String regionName = dictService.getDictName(resumeServiceAreaVo.getRegionId());
			resumeServiceAreaVo.setRegionName(regionName);

			resumeServiceAreaVos.add(resumeServiceAreaVo);
		}
		vo.setServiceCityId(serviceCityId);
		vo.setRegionIds(regionIds);
		vo.setResumeServiceAreas(resumeServiceAreaVos);

		// 服务技能
		List<ResumeSkillVo> resumeSkillVos = new ArrayList<ResumeSkillVo>();
		String serviceTypeNames = "";
		ResumeSkill resumeSkillSearchVo = new ResumeSkill();
		resumeSkillSearchVo.setResumeId(resumeId);
		List<ResumeSkill> resumeSkills = resumeSkillService.selectBySearchVo(resumeSkillSearchVo, "skill_id asc");
		if (!resumeSkills.isEmpty()) {
			for (ResumeSkill resumeSkill : resumeSkills) {
				ResumeSkillVo resumeSkillVo = new ResumeSkillVo();
				BeanUtilsExp.copyPropertiesIgnoreNull(resumeSkill, resumeSkillVo);
				String skillName = dictService.getDictName(resumeSkillVo.getSkillId());
				resumeSkillVo.setSkillName(skillName);
				resumeSkillVos.add(resumeSkillVo);

			}
		}
		vo.setResumeSkills(resumeSkillVos);

		// 证书信息
		ResumeCert resumeCertSearchVo = new ResumeCert();
		resumeCertSearchVo.setResumeId(resumeId);
		List<ResumeCert> resumeCerts = resumeCertService.selectBySearchVo(resumeCertSearchVo, "id asc");
		vo.setResumeCerts(resumeCerts);

		// 图片资源信息
		ResumeResource resumeResourceSearchVo = new ResumeResource();
		resumeResourceSearchVo.setResumeId(resumeId);
		List<ResumeResource> resumeResourceList = resumeResourceService.selectBySearchVo(resumeResourceSearchVo,
				"resource_id desc");
		if (resumeResourceList.isEmpty())
			resumeResourceList = new ArrayList<ResumeResource>();
		vo.setResumeResource(resumeResourceList);

		return vo;
	}

	/**
	 * 获取服务人员总数，最小值1567.
	 */
	@Override
	public Integer selectCountBySearchVo(ResumeSearchVo searchVo) {
		Integer total = resumeMapper.selectCountBySearchVo(searchVo);
		if (total == null)
			total = 0;
		total = 1567 + total;
		return total;
	}

	/**
	 * 计算排名
	 */
	@Override
	public Integer selectByRowNum(Integer resumeId) {
		return resumeMapper.selectByRowNum(resumeId);
	}

	/**
	 * 查询在特定地理位置，特定服务类型的人数
	 */
	@Override
	public Integer getServiceCount(Integer serviceTypeId, String lat, String lng) {
		// todo
		Integer total = 0;
		Random rand = new Random();
		total = rand.nextInt(10) + 20;
		return total;
	}
}

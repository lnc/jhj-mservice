package com.jhj.provider.resume.service;

import java.util.HashMap;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.vo.ResumeDetailVo;
import com.jhj.provider.resume.vo.ResumeListVo;
import com.jhj.provider.resume.vo.ResumeSearchVo;

/**
 * 通用Service
 */
public interface ResumeService extends BaseService<Resume> {

	PageInfo selectByListPageSelf(ResumeSearchVo searchVo, int pageNo, int pageSize, String orderBy);

	ResumeListVo getListVo(Resume item);

	ResumeDetailVo getDetailVo(Resume item);

	Integer selectByRowNum(Integer resumeId);

	Integer getServiceCount(Integer serviceTypeId, String lat, String lng);

	Integer selectCountBySearchVo(ResumeSearchVo searchVo);

	Map<String, Integer> statNumBySearchVo(ResumeSearchVo searchVo);
	
	
}

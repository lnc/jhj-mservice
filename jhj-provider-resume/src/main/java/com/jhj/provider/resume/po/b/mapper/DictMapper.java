package com.jhj.provider.resume.po.b.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jhj.provider.resume.po.b.model.Dict;
import tk.mybatis.mapper.common.Mapper;

public interface DictMapper extends Mapper<Dict> {
	
	public List<Dict> selectByDictTypes(@Param("pojo") Dict dict, @Param("dictTypeList")List<String> dictTypeList);
	
}
package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.Resume;

public class ResumeListVo extends Resume {
	
	//刷新时间
	public String refreshTimeFormat;
	
	//头像
	public String headImg;
	
	//性别名称
	public String sexName;
	
	//工作年限名称
	public String workYearName;
	
	//期望工资名称
	public String hopeSalaryName;
	
	//籍贯省份名称
	public String provinceName;
	
	//籍贯城市名称
	public String cityName;
	
	//学历名称
	public String degreeName;
	
	//服务类型技能名称集合
	public String serviceTypeNames;
	
	public String serviceTypeIds;
	
	//当前状态 1 = 空闲 0 = 上户
	public String statusName;

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getWorkYearName() {
		return workYearName;
	}

	public void setWorkYearName(String workYearName) {
		this.workYearName = workYearName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getServiceTypeNames() {
		return serviceTypeNames;
	}

	public void setServiceTypeNames(String serviceTypeNames) {
		this.serviceTypeNames = serviceTypeNames;
	}

	public String getSexName() {
		return sexName;
	}

	public void setSexName(String sexName) {
		this.sexName = sexName;
	}

	public String getRefreshTimeFormat() {
		return refreshTimeFormat;
	}

	public void setRefreshTimeFormat(String refreshTimeFormat) {
		this.refreshTimeFormat = refreshTimeFormat;
	}

	public String getServiceTypeIds() {
		return serviceTypeIds;
	}

	public void setServiceTypeIds(String serviceTypeIds) {
		this.serviceTypeIds = serviceTypeIds;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getHopeSalaryName() {
		return hopeSalaryName;
	}

	public void setHopeSalaryName(String hopeSalaryName) {
		this.hopeSalaryName = hopeSalaryName;
	}

	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}
}

package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "dict")
public class Dict {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 字典类型
     */
    @Column(name = "dict_type")
    private String dictType;

    /**
     * 代码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 下限
     */
    @Column(name = "range_from")
    private Integer rangeFrom;

    /**
     * 上限
     */
    @Column(name = "range_to")
    private Integer rangeTo;

    /**
     * 上级ID
     */
    private String pid;

    /**
     * 级别
     */
    private Integer level;

    /**
     * 是否有效 0  = 无效 1 = 有效
     */
    private Integer enable;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取字典类型
     *
     * @return dict_type - 字典类型
     */
    public String getDictType() {
        return dictType;
    }

    /**
     * 设置字典类型
     *
     * @param dictType 字典类型
     */
    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    /**
     * 获取代码
     *
     * @return code - 代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置代码
     *
     * @param code 代码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取下限
     *
     * @return range_from - 下限
     */
    public Integer getRangeFrom() {
        return rangeFrom;
    }

    /**
     * 设置下限
     *
     * @param rangeFrom 下限
     */
    public void setRangeFrom(Integer rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    /**
     * 获取上限
     *
     * @return range_to - 上限
     */
    public Integer getRangeTo() {
        return rangeTo;
    }

    /**
     * 设置上限
     *
     * @param rangeTo 上限
     */
    public void setRangeTo(Integer rangeTo) {
        this.rangeTo = rangeTo;
    }

    /**
     * 获取上级ID
     *
     * @return pid - 上级ID
     */
    public String getPid() {
        return pid;
    }

    /**
     * 设置上级ID
     *
     * @param pid 上级ID
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * 获取级别
     *
     * @return level - 级别
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * 设置级别
     *
     * @param level 级别
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * 获取是否有效 0  = 无效 1 = 有效
     *
     * @return enable - 是否有效 0  = 无效 1 = 有效
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * 设置是否有效 0  = 无效 1 = 有效
     *
     * @param enable 是否有效 0  = 无效 1 = 有效
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
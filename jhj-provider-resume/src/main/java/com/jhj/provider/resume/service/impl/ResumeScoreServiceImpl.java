package com.jhj.provider.resume.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.util.StringUtil;
import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.po.b.model.ResumeCert;
import com.jhj.provider.resume.po.b.model.ResumeResource;
import com.jhj.provider.resume.po.b.model.ResumeScore;
import com.jhj.provider.resume.service.ResumeCertService;
import com.jhj.provider.resume.service.ResumeResourceService;
import com.jhj.provider.resume.service.ResumeScoreService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.utils.ResumeUtil;
import com.jhj.provider.resume.vo.ResumeScoreSearchVo;
import com.jhj.provider.resume.vo.ResumeScoreVo;
import com.jhj.utils.BeanUtilsExp;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * ResumeService实现类
 */
@Service
public class ResumeScoreServiceImpl extends BaseServiceImpl<ResumeScore> implements ResumeScoreService  {
	
	@Autowired
	ResumeResourceService resumeResourceService;
	
	@Autowired
	ResumeCertService resumeCertService;
	
	
	@Override
	public List<ResumeScore> selectBySearchVoSelf(ResumeScoreSearchVo searchVo, String orderBy) {
		Example example = new Example(ResumeScore.class);
		Criteria criteria = example.createCriteria();
		
		if (searchVo.getResumeId() != null) {
			criteria.andCondition("resume_id = " + searchVo.getResumeId());
		}
		
		if (!StringUtil.isEmpty(searchVo.getResumeNo()) ) {
			criteria.andCondition("resume_no = " + searchVo.getResumeNo());
		}
		
		if (!StringUtil.isEmpty(searchVo.getAction())) {
			criteria.andCondition("action = '" + searchVo.getAction() + "'");
		}
		
		if (searchVo.getActionType() != null) {
			criteria.andCondition("action_type = " + searchVo.getActionType() );
		}
		
		if (searchVo.getStartTime() != null) {
			criteria.andCondition("add_time >= " + searchVo.getStartTime());
		}
		
		if (searchVo.getEndTime() != null) {
			criteria.andCondition("add_time <= " + searchVo.getEndTime());
		}
		
		// 排序
		if (!StringUtil.isEmpty(orderBy)) {
			example.setOrderByClause(orderBy);
		}

		return mapper.selectByExample(example);
	}
	
	
	
	/**
	 * 获取信息完整度
	 */
	@Override
	public Integer getTotalComplete(Resume entity) {
		int totalComplete = 0;
		
		//身份证号分数  = 2
		String idCard = entity.getIdCard();
		if (StringUtil.isEmpty(idCard)) {
			totalComplete = totalComplete + Constants.SCORE_COMPLETE_IDCARD;
		}
		
		//性别 +1
		Integer sex = entity.getSex();
		if (sex > 0) {
			totalComplete = totalComplete + Constants.SCORE_COMPLETE_SEX;
		}
		
		//期望工资 + 1
		Integer hopeSalary = entity.getHopeSalary();
		if (hopeSalary > 0) {
			totalComplete = totalComplete + Constants.SCORE_COMPLETE_HOPESALARY;
		}
		
		//工作经验 + 1
		Integer workYear = entity.getWorkYear();
		if (workYear > 0) {
			totalComplete = totalComplete + Constants.SCORE_COMPLETE_WORKYEAR;
		}
		
		Integer resumeId = entity.getResumeId();
		
		
		
		//头像+2；
		if (resumeId > 0) {
			ResumeResource resSearchVo = new ResumeResource();
			resSearchVo.setResumeId(resumeId);
			resSearchVo.setResourceType(Constants.RES_RESUME_HEAD_IMG);
			ResumeResource resHeadImg = resumeResourceService.selectOne(resSearchVo);
			if (resHeadImg != null) {
				totalComplete = totalComplete + Constants.SCORE_COMPLETE_HEADIMG;
			}
		}
		
		//证书 + 2
		if (resumeId > 0) {
			ResumeCert certSearchVo = new ResumeCert();
			certSearchVo.setResumeId(resumeId);
			List<ResumeCert> certList = resumeCertService.selectBySearchVo(certSearchVo, "");
			if (!certList.isEmpty()) {
				totalComplete = totalComplete + Constants.SCORE_COMPLETE_CERT;
			}
		}
		
		return totalComplete;
	}
	
	@Override
	public ResumeScoreVo getVo(ResumeScore item) {
		ResumeScoreVo vo = new ResumeScoreVo();
		BeanUtilsExp.copyPropertiesIgnoreNull(item, vo);
		
		String action = vo.getAction();
		
		String doActionName = ResumeUtil.getActinName(action);
		vo.setDoActionName(doActionName);
		
		return vo;
	}
}

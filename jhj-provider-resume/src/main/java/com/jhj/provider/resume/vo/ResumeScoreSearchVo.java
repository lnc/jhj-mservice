package com.jhj.provider.resume.vo;

import com.jhj.provider.resume.po.b.model.ResumeScore;

public class ResumeScoreSearchVo extends ResumeScore {
	
	public Integer startTime;
	
	public Integer endTime;

	public Integer getStartTime() {
		return startTime;
	}

	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	

}

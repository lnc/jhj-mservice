package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.OrderApply;
import tk.mybatis.mapper.common.Mapper;

public interface OrderApplyMapper extends Mapper<OrderApply> {
}
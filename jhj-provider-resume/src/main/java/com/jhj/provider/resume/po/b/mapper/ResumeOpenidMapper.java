package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeOpenid;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeOpenidMapper extends Mapper<ResumeOpenid> {
}
package com.jhj.provider.resume.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
public class HelloController {

    @RequestMapping("/api/resume/hello")
    public String index() {
        return "Hello World";
    }

}
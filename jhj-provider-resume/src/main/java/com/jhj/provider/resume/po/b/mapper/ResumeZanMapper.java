package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeZan;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeZanMapper extends Mapper<ResumeZan> {
}
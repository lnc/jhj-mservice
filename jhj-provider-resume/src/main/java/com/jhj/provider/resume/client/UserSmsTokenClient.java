package com.jhj.provider.resume.client;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jhj.provider.resume.client.vo.UserSmsTokenVO;
import com.jhj.utils.AppResultData;

@FeignClient("jhj-provider-user")
public interface UserSmsTokenClient {
	
	@RequestMapping(value="/api/user/selectCode.json", method=RequestMethod.GET)
	public AppResultData<UserSmsTokenVO> getCode(
			@RequestParam("mobile") String mobile,
			@RequestParam(value = "user_type", required = false, defaultValue = "0") int userType,
			@RequestParam("appkey") String appkey,
			@RequestParam("oncestr") String oncestr,
			@RequestParam("sign") String sign);
	
	@RequestMapping(value="/api/user/checkCode.json", method=RequestMethod.GET)
	public AppResultData<String> checkCode(
			@RequestParam("mobile") String mobile,
			@RequestParam("smsToken") String smsToken,
			@RequestParam(value = "smsType", required = false, defaultValue = "0") int smsType,
			@RequestParam("appkey") String appkey,
			@RequestParam("oncestr") String oncestr,
			@RequestParam("sign") String sign);

}

package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeServiceArea;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeServiceAreaMapper extends Mapper<ResumeServiceArea> {
}
package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "resume_openid")
public class ResumeOpenid {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 人才ID
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * wx = 微信 taobao = 淘宝
     */
    @Column(name = "open_type")
    private String openType;

    /**
     * 唯一标识
     */
    @Column(name = "open_id")
    private String openId;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取人才ID
     *
     * @return resume_id - 人才ID
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才ID
     *
     * @param resumeId 人才ID
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取wx = 微信 taobao = 淘宝
     *
     * @return open_type - wx = 微信 taobao = 淘宝
     */
    public String getOpenType() {
        return openType;
    }

    /**
     * 设置wx = 微信 taobao = 淘宝
     *
     * @param openType wx = 微信 taobao = 淘宝
     */
    public void setOpenType(String openType) {
        this.openType = openType;
    }

    /**
     * 获取唯一标识
     *
     * @return open_id - 唯一标识
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 设置唯一标识
     *
     * @param openId 唯一标识
     */
    public void setOpenId(String openId) {
        this.openId = openId;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
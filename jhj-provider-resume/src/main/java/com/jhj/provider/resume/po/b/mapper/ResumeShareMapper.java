package com.jhj.provider.resume.po.b.mapper;

import com.jhj.provider.resume.po.b.model.ResumeShare;
import tk.mybatis.mapper.common.Mapper;

public interface ResumeShareMapper extends Mapper<ResumeShare> {
}
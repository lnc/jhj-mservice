package com.jhj.provider.resume.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.vo.ResumeDetailVo;
import com.jhj.provider.resume.vo.ResumeListVo;
import com.jhj.utils.AppResultData;
import com.jhj.utils.StringUtil;

/**
 *
 */
@RestController
public class ResumeDetailController {

	@Autowired
	ResumeService resumeService;

	/**
	 * @param resumeNo
	 * @param resumeId
	 * @param sceretPhone 手机号是否标星 0 = 否  1= 是
	 * @return ResumeDetailVo
	 * 
	 */
	@RequestMapping(value = "/api/resume/detail.json", method = RequestMethod.GET)
	public AppResultData<Object> detail(
			@RequestParam(value = "resumeNo", required = false, defaultValue = "") String resumeNo,
			@RequestParam(value = "resumeId", required = false, defaultValue = "0") Integer resumeId,
			@RequestParam(value = "sceretPhone", required = false, defaultValue = "1") Integer sceretPhone) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		if (StringUtil.isEmpty(resumeNo) && resumeId == 0) {
			return result;
		}

		Resume searchVo = new Resume();
		if (!StringUtil.isEmpty(resumeNo))
			searchVo.setResumeNo(resumeNo);
		if (resumeId > 0)
			searchVo.setResumeId(resumeId);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}

		ResumeDetailVo vo = resumeService.getDetailVo(resume);
		if (sceretPhone == 1) {
			String m = vo.getMobile();
			StringBuffer bf = new StringBuffer();
			bf.append(m.substring(0, 2)).append("*****").append(m.substring(8));
			vo.setMobile(bf.toString());
		}
		result.setData(vo);
		return result;
	}

	/**
	 * @param resumeNo
	 * @param mobile
	 * @return ResumeListVo
	 * 
	 */
	@RequestMapping(value = "/api/resume/baseinfo.json", method = RequestMethod.GET)
	public AppResultData<Object> baseinfo(
			@RequestParam(value = "resumeNo", required = false, defaultValue = "") String resumeNo,
			@RequestParam(value = "mobile", required = false, defaultValue = "") String mobile) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		Resume searchVo = new Resume();

		if (!StringUtil.isEmpty(resumeNo))
			searchVo.setResumeNo(resumeNo);
		if (!StringUtil.isEmpty(mobile))
			searchVo.setMobile(mobile);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}

		ResumeListVo vo = resumeService.getListVo(resume);
		result.setData(vo);
		return result;
	}

	/**
	 * @param mobile
	 * @return resume
	 * 
	 */
	@RequestMapping(value = "/api/resume/get.json", method = RequestMethod.GET)
	public AppResultData<Resume> getResume(
			@RequestParam(value = "mobile", required = false, defaultValue = "") String mobile) {
		AppResultData<Resume> result = new AppResultData<Resume>(Constants.SUCCESS_0, "ok", new Resume());

		Resume searchVo = new Resume();
		if (!StringUtil.isEmpty(mobile))
			searchVo.setMobile(mobile);
		Resume resume = resumeService.selectOne(searchVo);
		if (resume == null) {
			result.setStatus(Constants.ERROR_999);
			result.setMsg("人才信息不存在");
			return result;
		}
		result.setData(resume);
		return result;
	}
}
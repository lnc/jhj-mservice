package com.jhj.provider.resume.po.b.model;

import javax.persistence.*;

@Table(name = "resume_from")
public class ResumeFrom {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 人才ID
     */
    @Column(name = "resume_id")
    private Integer resumeId;

    /**
     * 人才编号
     */
    @Column(name = "resume_no")
    private String resumeNo;

    /**
     * 0 = 后台运营人员 1 = 老师推荐  2 = 客服推荐 
     */
    @Column(name = "resume_from")
    private Integer resumeFrom;

    /**
     * 推荐人ID
     */
    @Column(name = "from_id")
    private Integer fromId;

    /**
     * 推荐人手机号
     */
    @Column(name = "from_mobile")
    private String fromMobile;

    /**
     * 推荐人姓名
     */
    @Column(name = "from_name")
    private String fromName;
    
    /**
     * 推荐人评价
     */
    @Column(name = "from_rate")
    private String fromRate;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取人才ID
     *
     * @return resume_id - 人才ID
     */
    public Integer getResumeId() {
        return resumeId;
    }

    /**
     * 设置人才ID
     *
     * @param resumeId 人才ID
     */
    public void setResumeId(Integer resumeId) {
        this.resumeId = resumeId;
    }

    /**
     * 获取人才编号
     *
     * @return resume_no - 人才编号
     */
    public String getResumeNo() {
        return resumeNo;
    }

    /**
     * 设置人才编号
     *
     * @param resumeNo 人才编号
     */
    public void setResumeNo(String resumeNo) {
        this.resumeNo = resumeNo;
    }

    /**
     * 获取0 = 后台运营人员 1 = 老师推荐  2 = 客服推荐 
     *
     * @return resume_from - 0 = 后台运营人员 1 = 老师推荐  2 = 客服推荐 
     */
    public Integer getResumeFrom() {
        return resumeFrom;
    }

    /**
     * 设置0 = 后台运营人员 1 = 老师推荐  2 = 客服推荐 
     *
     * @param resumeFrom 0 = 后台运营人员 1 = 老师推荐  2 = 客服推荐 
     */
    public void setResumeFrom(Integer resumeFrom) {
        this.resumeFrom = resumeFrom;
    }

    /**
     * 获取推荐人ID
     *
     * @return from_id - 推荐人ID
     */
    public Integer getFromId() {
        return fromId;
    }

    /**
     * 设置推荐人ID
     *
     * @param fromId 推荐人ID
     */
    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    /**
     * 获取推荐人手机号
     *
     * @return from_mobile - 推荐人手机号
     */
    public String getFromMobile() {
        return fromMobile;
    }

    /**
     * 设置推荐人手机号
     *
     * @param fromMobile 推荐人手机号
     */
    public void setFromMobile(String fromMobile) {
        this.fromMobile = fromMobile;
    }

    /**
     * 获取推荐人姓名
     *
     * @return from_name - 推荐人姓名
     */
    public String getFromName() {
        return fromName;
    }

    /**
     * 设置推荐人姓名
     *
     * @param fromName 推荐人姓名
     */
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

	public String getFromRate() {
		return fromRate;
	}

	public void setFromRate(String fromRate) {
		this.fromRate = fromRate;
	}
}
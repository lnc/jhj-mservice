package com.jhj.provider.resume.service;

import java.util.List;

import com.jhj.provider.resume.po.b.model.Dict;

/**
 * 通用Service
 */
public interface DictService extends BaseService<Dict> {

	String getDictName(Integer dictId);
	
	List<Dict> selectByDictTypes(Dict dict, List<String> dictTypeList);

	Integer getIdByName(String dictType, String dictName);

	
	
}

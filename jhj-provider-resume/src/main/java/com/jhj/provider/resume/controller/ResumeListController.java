package com.jhj.provider.resume.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.resume.po.b.model.Resume;
import com.jhj.provider.resume.service.ResumeService;
import com.jhj.provider.resume.utils.Constants;
import com.jhj.provider.resume.vo.ResumeListVo;
import com.jhj.provider.resume.vo.ResumeSearchVo;
import com.jhj.utils.AppResultData;
import com.github.pagehelper.PageInfo;


/**
 *
 */
@RestController
public class ResumeListController {
	
	@Autowired
	ResumeService resumeService;
		
	/**
	 * 查询接口
	 * @param serviceCityId		服务城市
	 * @param serviceTypes		服务类别
	 * @param cityId				籍贯
	 * @param hopeSalary			期望薪水
	 * @param workYear			工作年限
	 * @param degreeId			学历
	 * @param ageId				年龄范围
	 * @param mobile				手机号
	 * @param name				姓名
	 * @param sex				性别
	 * @param page				页码
	 * @param sceretPhone  0 = 显示完整手机号  1 = 手机号加星
	 * @param publish      0 = 显示全部 1 = 仅显示已推荐的
	 * @param orderByTime  0 = 按照刷新时间和活跃度排序 1 = 仅按照刷新时间排序
	 * @return
	 */
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/api/resume/list.json", method = RequestMethod.GET)
	public AppResultData<Object> list(
		@RequestParam(value = "serviceCityId", required = false, defaultValue = "0") Integer serviceCityId,
		@RequestParam(value = "serviceRegionIds", required = false, defaultValue = "") String serviceRegionIds,
		@RequestParam(value = "serviceTypes", required = false, defaultValue = "") String serviceTypes,
		@RequestParam(value = "provinceId", required = false, defaultValue = "0") Integer provinceId,
		@RequestParam(value = "salaryId", required = false, defaultValue = "0") Integer salaryId,
		@RequestParam(value = "workYearId", required = false, defaultValue = "0") Integer workYearId,
		@RequestParam(value = "degreeId", required = false, defaultValue = "0") Integer degreeId,
		@RequestParam(value = "ageId", required = false, defaultValue = "0") Integer ageId,
		@RequestParam(value = "mobile", required = false, defaultValue = "") String mobile,
		@RequestParam(value = "name", required = false, defaultValue = "") String name,
		@RequestParam(value = "sex", required = false, defaultValue = "0") Integer sex,
		@RequestParam(value = "hasCert", required = false, defaultValue = "0") Integer hasCert,
		@RequestParam(value = "status", required = false, defaultValue = "1") Integer status,
		@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
		@RequestParam(value = "sceretPhone", required = false, defaultValue = "1") Integer sceretPhone,
		@RequestParam(value = "publish", required = false, defaultValue = "0") Integer publish,
		@RequestParam(value = "orderByTime", required = false, defaultValue = "0") Integer orderByTime,
		@RequestParam(value = "pageSize", required = false, defaultValue = "12") Integer pageSize
		) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		ResumeSearchVo searchVo = new ResumeSearchVo();
		searchVo.setServiceCityId(serviceCityId);
		searchVo.setServiceRegionIds(serviceRegionIds);
		searchVo.setServiceTypes(serviceTypes);
		searchVo.setProvinceId(provinceId);
		searchVo.setHopeSalary(salaryId);
		searchVo.setWorkYear(workYearId);
		searchVo.setDegreeId(degreeId);
		searchVo.setAgeId(ageId);
		searchVo.setMobile(mobile);
		searchVo.setName(name);
		searchVo.setSex(sex);
		searchVo.setHasCert(hasCert);
		searchVo.setStatus(status);
		
		// 0 = 显示全部 1 = 仅显示已推荐的
		if (publish == 1) {
			searchVo.setPublish(1);
		}
		
		String orderBy = "total_score desc, refresh_time desc";
		// 0 = 按照刷新时间和活跃度排序 1 = 仅按照刷新时间排序
		if (orderByTime == 1) {
			orderBy =  "refresh_time desc";
		}
		
		
		if (page.equals(0)) page = 1;
		PageInfo pageInfo = resumeService.selectByListPageSelf(searchVo, page, pageSize, orderBy);
		
		List<Resume> list = pageInfo.getList();
		int len = list.size();
		for (int i = 0; i < len; i++) {
			Resume item = list.get(i);
			ResumeListVo vo = resumeService.getListVo(item);
			if(sceretPhone==1) {
				String m = vo.getMobile();
				StringBuffer bf = new StringBuffer();
				bf.append(m.substring(0, 2)).append("*****").append(m.substring(8));
				vo.setMobile(bf.toString());
			}
			list.set(i, vo);
		}
		pageInfo = new PageInfo(list);
		result.setData(pageInfo);
		return result;
	}
}
package com.jhj.provider.resume.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import com.jhj.provider.resume.Application;
import com.jhj.provider.resume.controller.HelloController;


/**
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class, MockServletContext.class })
@WebAppConfiguration()
public class ApplicationTests {

	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(
				new HelloController()).build();
	}

	@Test
	public void getHello() throws Exception {
		String url = "/hello";

		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		RequestBuilder e = get(url);
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);
	}

}

package com.jhj.provider.resume.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import com.jhj.provider.resume.Application;
import com.jhj.provider.resume.controller.HelloController;


/**
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class, MockServletContext.class })
@WebAppConfiguration()
public class DictControllerTests {

	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(
				new HelloController()).build();
	}

	@Test
	public void resumeList() throws Exception {

		String url = "/api/resume/get_dict.json";
		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>(); 
		params.add("dictType", "service_type");

		
		RequestBuilder e = get(url).params(params);
		
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);
	}
	
	
	@Test
	public void resumeDetail() throws Exception {

		String url = "/resume/detail.json";
		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>(); 
		params.add("mobile", "18302286437");
		params.add("resumeNo", "950070401252208640");
		
		RequestBuilder e = get(url).params(params);
		
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);
	}	


}

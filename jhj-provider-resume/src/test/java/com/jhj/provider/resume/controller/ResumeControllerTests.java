package com.jhj.provider.resume.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import com.jhj.provider.resume.Application;
import com.jhj.provider.resume.controller.HelloController;


/**
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class, MockServletContext.class })
@WebAppConfiguration()
public class ResumeControllerTests {

	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(
				new HelloController()).build();
	}

	@Test
	public void resumeAdd() throws Exception {

		String url = "/resume/add.json";
		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>(); 
		params.add("mobile", "18302286437");
		params.add("name", "王海龙");
		params.add("sex", "0");
		params.add("workYear", "4679");
		params.add("serviceTypes", "4670,4672");
		params.add("serviceProvinceId", "1");
		params.add("serviceCityId", "64");
		params.add("serviceRegionIds", "575,576");
		params.add("resumeFrom", "0");
		params.add("fromId", "18");
		params.add("fromMobile", "18612514665");
		params.add("fromName", "lianningchang");
		params.add("provinceId", "1");
		params.add("cityId", "64");
		params.add("idCard", "13262719970405001X");
		params.add("addr", "珠江摩尔");
		params.add("remark", "测试测试测试");
		
		RequestBuilder e = post(url).params(params);
		
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);
	}


}

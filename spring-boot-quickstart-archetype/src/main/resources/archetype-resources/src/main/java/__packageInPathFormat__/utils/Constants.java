package ${package}.utils;

public class Constants {

	public static String URL_ENCODE = "UTF-8";
	public static int PAGE_MAX_NUMBER = 10;
	/**
	 * 验证码最大值
	 */
	public static int CHECK_CODE_MAX_LENGTH = 999999;

	// '0' COMMENT '用户来源 0 = APP 1 = 微网站 2 = 管理后台 3= 到位用户'
	public static int USER_APP = 0;
	public static int USER_NET = 1;
	public static int USER_BACK = 2;
	public static int USER_DAOWAY = 3;

	// 充值卡类型:1,无忧管家;2,快乐管家;3,超级管家
	public static long DICT_CARD_TYPE_1 = 1;
	public static long DICT_CARD_TYPE_2 = 2;
	public static long DICT_CARD_TYPE_3 = 3;

	// 是否使用 0 = 未使用 1= 已使用
	public static short IS_USER_0 = 0;
	public static short IS_USER_1 = 1;

	// 地址默认 默认地址 1 = 默认 0 = 不默认
	public static int ADDRESS_DEFAULT_1 = 1;
	public static int ADDRESS_DEFAULT_NOT_0 = 0;

	// 0 = 发送失败 1 = 发送成功
	public static short SMS_SUCCESS = 1; // 2015-11-24 15:46:10， 发送成功和失败都是 0 ？？？？
	public static short SMS_FAIL = 0;
	public static String SMS_SUCCESS_CODE = "000000";
	public static String SMS_STATUS_CODE = "statusCode";
	public static String SMS_STATUS_MSG = "msg";

	/*
	 * 短信验证码 类型 2016年3月28日12:13:13
	 */
	public static int SMS_TYPE_0 = 0; // app登录
	public static int SMS_TYPE_1 = 1; // app 支付
	public static int SMS_TYPE_2 = 2;
	public static int SMS_TYPE_3 = 3; // 运营平台。 会员充值

	// 短信模板定义
	public static String PAY_SUCCESS_SMS_TEMPLE_ID = "9282";
	// public static String GET_CODE_TEMPLE_ID= "8429";
	public static String GET_CODE_TEMPLE_ID = "30064";
	public static String STAFF_POST_BEGIN = "64740";// 服务人员点击开始服务 64740
	public static String STAFF_POST_DONE = "64744";// 服务人员点击完成服务 64740
	public static String STAFF_JOIN_BLACK = "80275";// 服务人员欠款大于1000元被加入黑名单
	public static String STAFF_OUT_BLACK = "65016";// 服务人员已经被移除黑名单
	public static String GET_USER_VERIFY_ID = "30064";// jhj 用户获取验证码短信模板Id
	public static String GET_AM_EXP_CLEAN_ORDER_ID = "29167";// jhj 助理获得深度保洁订单通知
	public static String GET_USER_NO_PAY_ORDER_ID = "29163";// jhj 用户收到未支付的预约单通知
	public static String GET_CODE_REMIND_ID = "10923";
	public static String GET_CODE_MAX_VALID = "30";// 短信有效时间
	public static String NOTICE_CUSTOMER_Message_ID = "9280";
	public static String NOTICE_STAFF__Message_ID = "15284";
	public static String GET_USER_ORDER_AM_ID = "29161";// 用户预约下单后给助理发短信
	public static String CANCEL_ORDER_SATUS = "cancel";
	public static String AM_NOTICE_CUSTOMER_Message = "64742";// 助理修改助理预约单后给用户发短信
	public static String AM_CLEAN_NOTICE_CUSTOMER_Message = "29160";// 助理修改助理预约单后给用户发短信
	public static String NOTICE_USER_REST_MONEY_NOT_ENOUGH = "34652";
	public static String STAFF_PAY_DEPT_SUCCESS = " 80274"; // 服务人员支付欠款成功
	public static String STAFF_CASE_REQ = "80272"; // 服务人员申请提现
	public static String PAY_SUCCESS_ORDER_SMS = "151019";

	public static int SUCCESS_0 = 0;
	public static int ERROR_999 = 999;
	public static int ERROR_100 = 100;
	public static int ERROR_101 = 101;
	public static int ERROR_102 = 102;
	public static int ERROR_103 = 103;
	public static int ERROR_104 = 104;

}

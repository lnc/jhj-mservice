package ${package}.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

/**
 * 通用Service
 */
public interface BaseService<T> {
	
	int deleteByPrimaryKey(Integer id);
	
	int delete(T entity);
	
	int insert(T entity);
	
	int update(T entity);
	
	T initPo(T entity);
	
	T selectByPrimaryKey(Integer id);

	T selectOne(T entity);

	PageInfo selectByListPage(T entity, int pageNo, int pageSize, String orderBy);

	List<T> selectBySearchVo(T entity, String orderBy);

	
	
}

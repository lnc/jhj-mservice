package ${package};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 修改启动类，继承 SpringBootServletInitializer 并重写 configure 方法
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    @Override  
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) { 
        return application.sources(Application.class);  
    }  

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}

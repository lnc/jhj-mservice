#合同增加开始时间和结束时间
ALTER TABLE `order_contract` ADD `from_date` VARCHAR(20) NOT NULL COMMENT '合同开始时间' AFTER `status`, ADD `to_date` VARCHAR(20) NOT NULL COMMENT '合同结束时间' AFTER `from_date`;


ALTER TABLE `sale` ADD `remark` VARCHAR(255) NOT NULL COMMENT '备注' AFTER `head_img`;
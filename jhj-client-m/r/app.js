//app.js
import Touches from './utils/Touches.js'
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
       var code = res.code;
       console.log(res.code + "--------------------");
      //  发送 res.code 到后台换取 openId, sessionKey, unionId
      //  if(code){
      //    wx.request({
      //      url: 'https://test.jia-he-jia.com/api/user/getWxOpenidAndSessionKey.json',
      //      data: {
      //        code: res.code
      //      },
      //      success:function(res){
      //        debugger
      //        console.log(res)
      //        wx.setStorageSync("token", res.data.data);
      //      }
      //    })
      //  }
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    baseUrl:"https://test.jia-he-jia.com",
    shareTitle:'好活多多',
    currentIndex: '',
  },
  Touches: new Touches()
})
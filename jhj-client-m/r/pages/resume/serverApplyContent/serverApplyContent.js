// pages/serverApplyContent/serverApplyContent.js
Page({

	/**
	 * 页面的初始数据
	 */
	data : {
		tabBars : {
			left : {
				url : '../../order/list/list',
				icon : '../../img/apply.png',
				text : "招聘头条",

			},
			right : {
				url : '../../user/index/index',
				icon : '../../img/mine.png',
				text : "我的",

			},
		},
		submitSucess : false,
		genderItems : [
				{
					name : '男',
					value : '0'
				}, {
					name : '女',
					value : '1',
					checked : 'true'
				},

		],
		serverItems : [
				{
					name : '保姆',
					value : '0'
				}, {
					name : '育儿嫂',
					value : '1',
					checked : 'true'
				}, {
					name : '月嫂',
					value : '2'
				}, {
					name : '催乳师',
					value : '3'
				}, {
					name : '小儿推拿',
					value : '4'
				}, {
					name : '深度养护',
					value : '5'
				}, {
					name : '家电清洗',
					value : '6'
				},

		],

		jobYear : [
				{
					name : '新手',
					value : '0'
				}, {
					name : '1-2年',
					value : '1',
					checked : 'true'
				}, {
					name : '3-5年',
					value : '2'
				}, {
					name : '5年以上',
					value : '3'
				},

		],

		serverArea : [
				{
					name : '朝阳区',
					value : '0'
				}, {
					name : '海淀区',
					value : '1',
					checked : 'true'
				}, {
					name : '丰台区',
					value : '2'
				}, {
					name : '大兴区',
					value : '3'
				}, {
					name : '昌平区',
					value : '4'
				}, {
					name : '通州区',
					value : '5'
				}, {
					name : '房山区',
					value : '6'
				}, {
					name : '石景山区',
					value : '7'
				}, {
					name : '顺义区',
					value : '8'
				},

		],
	},

	gender : function(e) {
		var checked = e.detail.value
		var changed = {}
		for (var i = 0; i < this.data.genderItems.length; i++) {
			if (checked == this.data.genderItems[i].name) {
				changed['genderItems[' + i + '].checked'] = true
			} else {
				changed['genderItems[' + i + '].checked'] = false
			}
		}
		this.setData(changed)
	},

	serverItem : function(e) {
		var checked = e.detail.value
		var changed = {}
		for (var i = 0; i < this.data.serverItems.length; i++) {
			if (checked.indexOf(this.data.serverItems[i].name) !== -1) {
				changed['serverItems[' + i + '].checked'] = true
			} else {
				changed['serverItems[' + i + '].checked'] = false
			}
		}
		this.setData(changed)
	},

	jobYear : function(e) {
		var checked = e.detail.value
		var changed = {}
		for (var i = 0; i < this.data.jobYear.length; i++) {
			if (checked == this.data.jobYear[i].name) {
				changed['jobYear[' + i + '].checked'] = true
			} else {
				changed['jobYear[' + i + '].checked'] = false
			}
		}
		this.setData(changed)
	},

	serverArea : function(e) {
		var checked = e.detail.value
		var changed = {}
		for (var i = 0; i < this.data.serverArea.length; i++) {
			if (checked.indexOf(this.data.serverArea[i].name) !== -1) {
				changed['serverArea[' + i + '].checked'] = true
			} else {
				changed['serverArea[' + i + '].checked'] = false
			}
		}
		this.setData(changed)
	},
	submitApply : function() {
		this.setData({
			submitSucess : true,
		})
	},
	navig1 : function() {
		this.setData({
			submitSucess : false,
		})
	},
	navig2 : function() {
		this.setData({
			submitSucess : false,
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad : function(options) {

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady : function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow : function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide : function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload : function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh : function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom : function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage : function() {

	}
})
var sign = require('../../../utils/lib/sign.js');
var app = getApp();
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,
  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../list/list',
        icon: '../../img/apply.png',
        text: "招聘头条",

      },
      right: {
        url: '../../user/index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
      currentIndex:"0",
    },
    list: [],
    lastPage: 1,
    pageNum: 1,
    serviceTypes: '',
    //服务工种多选  初始化数据
    serviceTypeList: [],
  },

  //多选服务工种事件
  serverItem: function (e) {
    var checked = e.detail.value
    var changed = {}
    var serviceTypeList = this.data.serviceTypeList;
    for (var i = 0; i < serviceTypeList.length; i++) {
      var ty = serviceTypeList[i];
      if (checked.indexOf(ty['id'].toString()) !== -1) {
        changed['serviceTypeList[' + i + '].checked'] = true
      } else {
        changed['serviceTypeList[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
    if (checked.length > 0) {
      this.setData({
        serviceTypes:checked.toString()
      });
    }

    this.getData(1, checked.toString());
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getServiceType();
    this.getData(1,"");
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var pageNum = this.data.pageNum;
    pageNum++;

    var serviceTypes = this.data.serviceTypes;

    if (pageNum > this.data.lastPage) {
      return false;
    }

    this.getData(pageNum, serviceTypes);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 获取服务类别
   * 
   */
  getServiceType: function () {
    var that = this;
    var params = {
      dictType: "service_type"
    };
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + "/api/resume/get_dict.json",
      data: params,
      method: 'GET',
      dataType: 'json',
      success: function (res) {
        var result = res.data.data;
        if (result.length > 0) {
          console.log(result);
          var lists = [];
          for (var i = 0; i < result.length; i++) {
            var obj = {};
            obj.name = result[i].name;
            obj.id = result[i].id.toString();
            lists.push(obj);
          }
          that.setData({
            serviceTypeList: lists,
          })
        }
      },
      fail: function (res) {
        console.log("失败")
      }
    })
  },
  /**
   * 首页数据
   *  
   */
  getData: function (pageNo, serviceTypes){
    var pageNo = pageNo || 1;
    var that = this;
    var params = {
      serviceTypeIds: serviceTypes,
      pageNo: pageNo
    };
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl+'/api/bm/order/list.json',
      data: params,
      success: function(res) {
    	  	console.log(res);
        if(res.data.status==0){
          var result = res.data.data;
          if (result.pageNum==1){
            that.setData({
              list: result.list,
              lastPage: result.lastPage,
              pageNum: result.pageNum
            })
          }else{
            that.setData({
              list: that.data.list.concat(result.list),
              lastPage: result.lastPage,
              pageNum: result.pageNum
            })
          }
        }
      },
    })
  }
})
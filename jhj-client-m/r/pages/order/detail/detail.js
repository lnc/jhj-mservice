var sign = require('../../../utils/lib/sign.js');
var common = require("../../../utils/common.js");
var app = getApp();
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor

Page({
  changeColor: a,

	/**
	 * 页面的初始数据
	 */
	data : {
		tabBars : {
			left : {
				url : '../list/list',
				icon : '../../img/apply.png',
				text : "招聘头条",

			},
			right : {
				url : '../../user/index/index',
				icon : '../../img/mine.png',
				text : "我的",

			},
			order : {}
		},
		isShowApply : false,
		isShowConfirm : false,
		orderId: 0
	},
	apply : function() {

		this.setData({
			isShowApply : true,
		})

	},
	applyPost : function() {
		this.setData({
			isShowApply : false,
			isShowConfirm : true
		})
	},
	applyDelete : function() {
		this.setData({
			isShowApply : false,
			isShowConfirm : false,
		})
	},
	applyConfirm : function() {
		this.setData({

			isShowConfirm : false,
		})
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad : function(options) {
		console.log("order-detail onload")
		console.log(options)
	    this.setData({
	      "tabBars.currentIndex": app.globalData.currentIndex
	    })
		var orderId = options.orderId;
		
		/**
		 * 判断是否有扫码进入的，按照扫码进入的情况进行解析
		 * https://mp.weixin.qq.com/debug/wxadoc/dev/api/qrcode.html
		 * scene格式为
		 * [orderId]-[resumeFrom]-[fromId]-[fromMobile]
		 */
		var scene = options.scene;
		if (!common.isEmpty(scene)) {
			var params = scene.split("-");
			orderId = params[0];
			var resumeFrom = params[1];
			var fromId = params[2];
			var fromMobile = params[3];
			if (!common.isEmpty(fromMobile)) {
				//存入本地
				// resumeFrom 0 = 后台人员 1 = 老师推荐 2 = 客服推荐 3 = 阿姨推荐 4 = 用户推荐
				wx.setStorageSync("resumeFrom", resumeFrom);
				wx.setStorageSync("fromId", fromId);
				wx.setStorageSync("fromMobile", fromMobile);
			}
		}
		
		/**
		 * 判断是否从分享页面点击进入
		 */
		var shareMobile = options.shareMobile;
		console.log(shareMobile)
		if (!common.isEmpty(shareMobile)) {
			orderId = options.orderId;
			// resumeFrom 0 = 后台人员 1 = 老师推荐 2 = 客服推荐 3 = 阿姨推荐 4 = 用户推荐
			var resumeFrom = options.shareFrom;
			var fromId = options.shareId;
			wx.setStorageSync("resumeFrom", resumeFrom);
			wx.setStorageSync("fromId", fromId);
			wx.setStorageSync("fromMobile", shareMobile);
			console.log("resumeFrom ==" + resumeFrom);
			//根据不同的resumeFrom获取推荐人名称
			// resumeFrom 0 = 后台人员 1 = 老师推荐 2 = 客服推荐 3 = 阿姨推荐 4 = 用户推荐
			if (!common.isEmpty(fromId)) {
				if (resumeFrom == "0") this.getFromNameByAdminAccount(fromId);
				
				if (resumeFrom == "1") this.getFromNameBySale(shareMobile);
				
				if (resumeFrom == "3") this.getFromNameByResume(shareMobile);
			}
			
			
			
		}
		console.log("orderId = " + orderId);
		this.setData({orderId : orderId})
		this.getDetail(orderId);
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady : function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow : function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide : function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload : function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh : function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom : function() {

	},

	 /**
     * 用户点击右上角分享或推荐给好友按钮
     */
    onShareAppMessage: function (options) {
    	  var that = this;
      var pages = getCurrentPages();
      var url = "pages/order/detail/detail?orderId="+that.data.orderId;
      var shareUrl = common.getShareUrl(url);
      console.log(shareUrl);
      console.log(options)
      return {
        title: app.globalData.shareTitle,
        path: shareUrl,
        success: function (res) {
          wx.showToast({
            title: '转发成功',
          })
        },
        fail: function (res) {
          wx.showToast({
            title: '转发失败',
          })
        }
      }
    },
	getDetail : function(orderId) {
		console.log("getDetail");
		console.log("orderId = " + orderId);
		var that = this;
		var params = {};
		params.orderId = orderId;
		
		params = sign.getSignParams(params)
		wx.request({
			url : app.globalData.baseUrl + '/api/bm/order/detail.json',
			data : params,
			success : function(res) {
				var result = res.data.data;
				that.setData({
					order : result
				})
			},
		})
	},
	apply : function(e) {
		var that = this;
		var params = e.currentTarget.dataset;
		var resumeNo = wx.getStorageSync('resumeNo');
		var mobile = wx.getStorageSync('mobile');
		
		
		var returnUrl = '../../order/detail/detail?orderId='+ that.data.orderId;
		returnUrl = common.base64Encoder(returnUrl);  
		
		if (common.isEmpty(mobile) && common.isEmpty(resumeNo)) {
			wx.redirectTo({
				url : '../../user/login/login?returnUrl=' + returnUrl,
			})
			return false;
		} else if (common.isEmpty(resumeNo)) {
			wx.redirectTo({
				url : '../../user/reg/reg?returnUrl=' + returnUrl ,
			})
			return false;
		}
		var params = {
			orderId : params.orderId,
			orderNo : params.orderNo,
			resumeNo : resumeNo,
		};
		params = sign.getSignParams(params)
    wx.showModal({
      title: '',
      content: '确定应聘吗？',
      success: function (res) {
        if(res.cancel){
          return false;
        } else if (res.confirm){
          wx.request({
            url: app.globalData.baseUrl + '/api/resume/apply/add.json',
            data: params,
            header: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            success: function (res) {
              var result = res.data;
              if (result.status == 0) {
                wx.showToast({
                  title: '应聘成功',
                  icon: 'success'
                })
              } else {
                wx.showModal({
                  title: '',
                  showCancel: false,
                  content: result.msg,
                })
              }
            },
          })
        }
    
      }
    })

	},
	
	getFromNameByAdminAccount: function(id)  {
		var that = this;
		var params = {};
		params.adminId = id;
		params = sign.getSignParams(params)
		wx.request({
			url : app.globalData.baseUrl + '/api/user/get_adminnamel.json',
			data : params,
			success : function(res) {
				var result = res.data.data;
				if (!common.isEmpty(result)) {
					wx.setStorageSync("fromName", result);
				}
			},
		})
	},
	
    getFromNameBySale: function(mobile) {
    	    console.log("getFromNameBySale");
    		var that = this;
		var params = {};
		params.mobile = mobile;
		params = sign.getSignParams(params)
		wx.request({
			url : app.globalData.baseUrl + '/api/bm/sale/get_bymobile.json',
			data : params,
			success : function(res) {
				var result = res.data.data;
				if (!common.isEmpty(result)) {
					wx.setStorageSync("fromName", result.name);
				}
			},
		})
	},
	
	getFromNameByResume: function(mobile){
		var that = this;
		var params = {};
		params.mobile = mobile;
		params = sign.getSignParams(params)
		wx.request({
			url : app.globalData.baseUrl + '/api/resume/get.json',
			data : params,
			success : function(res) {
				var result = res.data.data;
				if (!common.isEmpty(result)) {
					wx.setStorageSync("fromName", result.name);
				}
			},
		})
	}
	
	
	
	
})
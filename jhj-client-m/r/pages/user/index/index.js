// pages/serverMine/serverMine.js
var sign = require('../../../utils/lib/sign.js');
var common = require("../../../utils/common.js");
var util = require("../../../utils/util.js");
var app = getApp();
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../../order/list/list',
        icon: '../../img/apply.png',
        text: "招聘头条",

      },
      right: {
        url: '..//index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
      order: {}
    },
    resume: {},
    applyList: [],
    orderResumeList: [],
    videoUrl: "",
    scrollTop:0,
    scrollTopInterview:0,
    videoIsShow:false,
    videoText:"展示",
    applyIsShow:false,
    applyText:'展示',
    interviewIsShow:false,
    interview:"展示"
  },
  /**
   * 应聘记录滚动
   */
  tapMove: function (e) {
    var length = this.data.applyList.length;
    if (this.data.scrollTop > 27*(length-5)) {
      this.setData({
        scrollTop: 0,
      })
    }
    else {
      this.setData({
        scrollTop: this.data.scrollTop +27
      })
    }

    console.log(this.data.scrollTop);
  },
    /**
   * 面试记录滚动
   */
  tapMoveInterview: function (e) {
    var length = this.data.orderResumeList.length;
    if (this.data.scrollTopInterview > 27 * (length - 5)) {
      this.setData({
        scrollTopInterview: 0,
      })
    }
    else {
      this.setData({
        scrollTopInterview: this.data.scrollTopInterview + 27
      })
    }

    console.log(this.data.scrollTop);
  },
  /**
   * 视频显示和隐藏
   */
  videoShow:function(e){
    if (this.data.videoIsShow){
        this.setData({
          videoIsShow:false,
          videoText:'展示'
        })
    }else{
      this.setData({
        videoIsShow: true,
        videoText:'隐藏'
      })
    }
  },
/**
 * 应聘记录展示和隐藏
 */
  applyShow: function (e) {
    if (this.data.applyIsShow) {
      this.setData({
        applyIsShow: false,
        applyText:"展示"
      })
    } else {
      this.setData({
        applyIsShow: true,
        applyText:"隐藏"
      })
    }
  },
  /**
   * 面试记录显示和隐藏
   */
  interViewShow: function (e) {
    if (this.data.interviewIsShow) {
      this.setData({
        interviewIsShow: false,
        interview:"展示"
      })
    } else {
      this.setData({
        interviewIsShow: true,
        interview:"隐藏"
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getResume();
    this.getApply();
    this.getOrderResumeList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getResume: function () {
    var that = this;
    var mobile = wx.getStorageSync('mobile');
    var resumeNo = wx.getStorageSync('resumeNo');

    if (common.isEmpty(mobile) && common.isEmpty(resumeNo)) {
      wx.redirectTo({
        url: '../../user/login/login',
      })
      return false;
    } else if (common.isEmpty(resumeNo)) {
      wx.redirectTo({
        url: '../../user/reg/reg',
      })
      return false;
    }


    var params = {
      resumeNo: resumeNo,
      mobile: mobile,
    };
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + '/api/resume/detail.json',
      data: params,
      success: function (res) {
        var result = res.data;
        var videos = [];
        if (result.status == 0) {
          var resourceList = result.data.resumeResource;
          if (resourceList != null && resourceList.length) {
            for (var i = 0; i < resourceList.length; i++) {
              var r = resourceList[i];
              if (r.resourceType == 'resume_video') {
                that.getVideoUrlToken(r.resourceUrl)
              }
            }
          }

          that.setData({
            resume: result.data,
          })

          wx.setStorage({
            key: 'resume',
            data: result.data,
          })
        }
      },

    })
  },
  /**
   * 签到刷新
   */
  signRefresh: function () {
    var that = this;
    // var resumeNo = '952456210701889536';
    // var mobile = '17331261128';
    var resumeNo = wx.getStorageSync('resumeNo');
    var mobile = wx.getStorageSync('mobile');

    var params = {
      resumeNo: resumeNo,
      mobile: mobile,
    };
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + '/api/resume/refresh.json',
      data: params,
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        var result = res.data;
        if (result.status == 0) {
          var resume = that.data.resume;
          resume.total = result.data.total;
          resume.rowNum = result.data.rowNum;
          resume.totalScore = result.data.totalScore;
          that.setData({
            resume: resume
          })
          wx.showToast({
            title: '签到成功',
          })
        }
        if (result.status == 999) {
          wx.showModal({
            title: '',
            showCancel: false,
            content: result.msg,
            showCancel: false
          })
        }
      },
    })
  },
  /**
   * 退出登录
   */
  logout: function () {
//    wx.removeStorage("resumeNo");
//    wx.removeStorage("openid");
//    wx.removeStorage("resume");
//    wx.removeStorage("mobile");
//    wx.removeStorage("name");
//    wx.removeStorage("total");
//    wx.removeStorage("totalScore");
//    wx.removeStorage("rowNum");
	  wx.clearStorage();
    wx.redirectTo({
      url: '../login/login',
    })
  },
  /**
   * 应聘记录
   * 
   */
  getApply:function() {
    var that = this;
    var resumeNo = wx.getStorageSync('resumeNo');
    var params = {
      resumeNo: resumeNo,
      pageNo: 1,
    }
    params = sign.getSignParams(params);
    wx.request({
      url: app.globalData.baseUrl + '/api/resume/apply/list.json',
      data: params,
      success: function (res) {
        var result = res.data;
        if(result.status==0){
          var arry = result.data.list;
          if (arry.length>0){
            for(var i=0;i<arry.length;i++){
              var o = arry[i];
              arry[i].addTimeStr = util.longDateToStr(o.addTime*1000);
            }
          }
          that.setData({
            applyList: arry
          })
        }
      },
    })
  },
  /**
   *  面试记录
   *  
   */
  getOrderResumeList: function() {
    var that = this;
    var resumeNo = wx.getStorageSync('resumeNo');
    var params = {
      resumeNo: resumeNo,
      pageNo: 1,
    }
    params = sign.getSignParams(params);
    wx.request({
      url: app.globalData.baseUrl + '/api/bm/order/resume/list.json',
      data: params,
      success: function (res) {
        var result = res.data;
        if(result.status==0){
          var arry = result.data.list;
//          if (arry.length > 0) {
//            for (var i = 0; i < arry.length; i++) {
//              var o = arry[i];
//              arry[i].interviewTimeStr = util.longDateToStr(o.interviewTime*1000);
//            }
//          }
          that.setData({
            orderResumeList:arry
          })
        }
      },

    })
  },
  /**
   * 
   * 跳转修改
   */
  updateTo: () => {
    var resumeNo = wx.getStorageSync('resumeNo');
    if (resumeNo == null || resumeNo == '') {
      wx.redirectTo({
        url: '../login/login',
      })
    } else {
      wx.redirectTo({
        url: '../update/update',
      })
    }
  },

  getVideoUrlToken: function (videoUrl) {
    var that = this;
    var params = {};

    params.videoUrl = videoUrl;
    params = sign.getSignParams(params);

    wx.request({
      url: app.globalData.baseUrl + '/api/resume/download_token.json',
      data: params,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'GET',
      success: function (res) {
        if (res.data.status == 999) {
          common.showToast(res.data.msg);
          return false;
        }

        var videoUrl = res.data.data;
        that.setData({ videoUrl: videoUrl });

      },
      fail: function (res) {

      },
      complete: function (res) { },
    })
  }
})
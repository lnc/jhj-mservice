// pages/resume/serverLogin/serverLogin.js
var sign = require('../../../utils/lib/sign.js');
const app = getApp()
var common = require("../../../utils/common.js");
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,
	/**
	 * 页面的初始数据
	 */
	data : {
    tabBars: {
      left: {
        url: '../../order/list/list',
        icon: '../../img/apply.png',
        text: "招聘头条",

      },
      right: {
        url: '../index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
		isShowError : false,
		codeMessage : "获取验证码",
		phone : '',
		code : '',
		errorMessage : "",
		disabled : true,
		returnUrl: ""  //使用时需要经过base64解密 common.base64Decoder(returnUrl)
	},
	
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad : function(options) {
	    this.setData({
	      "tabBars.currentIndex": app.globalData.currentIndex
	    })
		//返回格式为 {url:,  params:} 返回时再拼接
	    var returnUrl = options.returnUrl;
	    if (!common.isEmpty(returnUrl)) {
	      this.setData({ returnUrl: returnUrl });
	      console.log(common.base64Decoder(returnUrl));
	    }
	},
	
	/**
	 * 获取手机号
	 */
	getPhone : function(e) {
		this.setData({
			phone : e.detail.value
		})
	},
	/**
	 * 获取输入的验证码
	 */
	getInputCode : function(e) {
		this.setData({
			code : e.detail.value
		})
	},
	/**
	 * 获取验证码
	 */
	getCode : function() {
		var that = this;
		if (that.data.disabled) {
			that.countDown();
		}
		var mobile = that.data.phone;
		var params = {};
		params.mobile = mobile;
		params.sms_type = 0;
		params = sign.getSignParams(params);
		
		wx.request({
			url : app.globalData.baseUrl + '/api/user/getCode.json',
			data : params,
			method : 'GET',
			success : function(res) {

			},
		})
	},
	/**
	 * 倒计时
	 */
	countDown : function() {
		var that = this;
		var flag = common.validatePhone(this.data.phone);
		if (flag) {
			that.changeData(false, "");
			var second = 60;
			var t = setInterval(function() {
				second--;
				if (second < 0) {
					clearInterval(t);
					that.setData({
						codeMessage : "获取验证码",
						disabled : true,
					})
				} else {
					that.setData({
						codeMessage : second + "秒",
						disabled : false,
					})
				}
			}, 1000);

		} else {
			that.changeData(true, "手机号不正确或未填写");
		}
	},

	/**
	 * 改变数据
	 */
	changeData : function(isShowError, errorMessage) {
		this.setData({
			isShowError : isShowError,
			errorMessage : errorMessage,

		})
	},
	/**
	 * 点击确定
	 */
	confirm : function() {
		var that = this;
		that.changeData(false, "");
	},

	/**
	 * 登录之前验证
	 */
	validateData : function() {
		var that = this;
		var phoneFlag = common.validatePhone(this.data.phone);
		var codeFlag = common.isEmpty(this.data.code);
		if ((!phoneFlag) || codeFlag) {
			that.changeData(true, "正确填写手机号或验证码");
		}

	},
	/**
	 * 登录
	 */
	login : function() {
		var that = this;
		that.validateData();

		var mobile = that.data.phone;
		var code = that.data.code;

		wx.login({
			success : function(res) {
				var wxcode = res.code;
				var params = {
					mobile : mobile,
					smsToken : code,
					userType : 1,
					code : wxcode
				}
				params = sign.getSignParams(params);
				console.log(params);
				wx.request({
					url : app.globalData.baseUrl + '/api/resume/login.json',
					data : params,
					header : {
						"Content-Type" : "application/x-www-form-urlencoded"
					},
					method : 'POST',
					success : function(res) {
						if (res.data.status == 0) {
							console.log(res);
							var resumeNo = res.data.data.resumeNo;
							wx.setStorageSync("mobile", res.data.data.mobile);
							wx.setStorageSync("openid", res.data.data.openid);
							wx.setStorageSync("resumeNo", resumeNo);
							wx.setStorageSync("resumeId", res.data.data.resumeId);
							wx.setStorageSync("name", res.data.data.name);
							wx.setStorageSync("total", res.data.data.total);
							wx.setStorageSync("rowNum", res.data.data.rowNum);
							wx.setStorageSync("totalScore", res.data.data.totalScore);
							
							if (common.isEmpty(resumeNo)) {
								console.log("jump to resumeNo");
								wx.redirectTo({
									 url: '../../user/reg/reg?retunUrl='+that.data.returnUrl,
								 })
							} else {

								if (!common.isEmpty(that.data.returnUrl)) {
									console.log("jump to returnUrl");
									//使用时需要经过base64解密 common.base64Decoder(returnUrl)
					                var returnUrl = common.base64Decoder(that.data.returnUrl);
					                console.log("returnUrl = " + returnUrl);
									wx.redirectTo({
										 url: returnUrl,
									 })
								} else {
									console.log("jump to orderlist");
									wx.redirectTo({
										 url: '../../order/list/list',
									 })
								}
							}
						}
						if (res.data.status == 999) {
							common.showToast(res.data.msg);
						}

					}
				})
			}
		})
	},

})
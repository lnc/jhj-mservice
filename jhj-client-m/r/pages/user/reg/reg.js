// pages/serverMyInformation/serverMyInformation.js
//获取应用实例
var common = require("../../../utils/common.js");
var sign = require('../../../utils/lib/sign.js');
var dict = require('../../../utils/dict.js');
const app = getApp();
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,

  /**
	 * 页面的初始数据
	 */
  data: {
    tabBars: {
      left: {
        url: '../../order/list/list',
        icon: '../../img/apply.png',
        text: "招聘头条",

      },
      right: {
        url: '../../user/index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
    genderItems: [
      { name: '男', value: '1', },
      { name: '女', value: '2', },
    ],
    Skills: [
      { name: '有', value: '0' },
      { name: '无', value: '1', },
    ],
    mobile : "",
    serviceTypes: [],
    workYear: [],
    serverArea: [],
    
    sex: "",
    serviceTypeIds: "",
    workYearId: "",
    submitSucess: false,
    total: 0,
    rowNum: 0,
    totalScore: 0,
    returnUrl: ""  //使用时需要经过base64解密 common.base64Decoder(returnUrl)
  },

  /**
	 * 生命周期函数--监听页面加载
	 */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    console.log("onload");
    var that = this;
    
    if (!common.isEmpty(wx.getStorageSync("mobile"))) {
    		that.setData({mobile :wx.getStorageSync("mobile")})
    }
    
    dict.getDict("service_type", "", that.initDictServiceType);
    dict.getDict("work_year", "", that.initDictWorkYear);
    dict.getDict("region", 4780, that.initDicServiceArea);
    
    //返回格式为 {url:,  params:} 返回时再拼接
    var returnUrl = options.returnUrl;
    if (!common.isEmpty(returnUrl)) {
      this.setData({ returnUrl: returnUrl });
      console.log(common.base64Decoder(returnUrl));
    }
  },

  initDictServiceType: function (res) {
    var that = this;
    that.setData({ serviceTypes: res })
  },

  initDictWorkYear: function (res) {
    var that = this;
    that.setData({ workYear: res })
  },
  
  initDicServiceArea: function (res) {
	    var that = this;
	    that.setData({ serverArea: res })
  },

  genderChange: function (e) {
    var checked = e.detail.value;
    this.setData({ sex: checked });
  },

  serviceTypeChange: function (e) {
    var checked = e.detail.value;
    var changed = {}
    for (var i = 0; i < this.data.serviceTypes.length; i++) {

      var id = this.data.serviceTypes[i].id;
      if (checked.indexOf(id.toString()) !== -1) {

        this.data.serviceTypes[i].checked = true;
      } else {
        this.data.serviceTypes[i].checked = false;
      }
    }
    this.setData({ serviceTypes: this.data.serviceTypes });

  },

  workYearChange: function (e) {
    var checked = e.detail.value;
    var changed = {}
    for (var i = 0; i < this.data.workYear.length; i++) {

      var id = this.data.workYear[i].id;
      if (checked.indexOf(id.toString()) !== -1) {

        this.data.workYear[i].checked = true;
      } else {
        this.data.workYear[i].checked = false;
      }
    }
    this.setData({ workYear: this.data.workYear });

  },

  serverAreaChange:function(e){
    var checked = e.detail.value;
    var changed = {}
    for (var i = 0; i < this.data.serverArea.length; i++) {

      var id = this.data.serverArea[i].id;
      if (checked.indexOf(id.toString()) !== -1) {

        this.data.serverArea[i].checked = true;
      } else {
        this.data.serverArea[i].checked = false;
      }
    }
    this.setData({ serverArea: this.data.serverArea });
  },

  validateRequire: function (fieldValue, msg) {
    if (common.isEmpty(fieldValue)) {
      common.showToast(msg);
      return false;
    }
    return true;
  },

  /**
	 * 验证性别 必选
	 */
  validateGender: function () {
    var that = this;

    if (that.data.sex.length == 0) {
      common.showToast("请勾选性别!");
      return false;

    }

    return true;
  },

  /**
	 * 验证服务工种 至少选一个
	 */
  validateServiceType: function () {
    var that = this;
    var index = 0;
    for (var i = 0; i < that.data.serviceTypes.length; i++) {
      if (that.data.serviceTypes[i].checked) {
        index++;
      }
    }
    if (index == 0) {
      common.showToast("请勾选服务工种!");
      return false;
    }
  },

  /**
	 * 工作经验 必选
	 */
  validateWorkYear: function () {
    var that = this;
    var index = 0;
    for (var i = 0; i < that.data.workYear.length; i++) {
      if (that.data.workYear[i].checked) {
        index++;
      }
    }
    if (index == 0) {
      common.showToast("请勾选工作经验!");
      return false;
    }
    return true;

  },

  /**
	 * 注册信息
	 */
  reg: function (e) {
    var that = this;
    console.log("reg");
    console.log(e.detail.value);
    var params = e.detail.value;

    // 性别必选
    if (that.validateGender() == false) return false;

    // 服务工种必选
    if (that.validateServiceType() == false) return false;

    // 工作经验必选
    if (that.validateWorkYear() == false) return false;
    
    // 姓名校验
    if (that.validateRequire(e.detail.value['name'], "请填写姓名!") == false) {
    		console.log("姓名校验失败");
    		return false;
    }
    var name = e.detail.value['name'].trim();
    // 手机号校验
    if (that.validateRequire(e.detail.value['mobile'], "请填写手机号!") == false) return false;
    var mobile = e.detail.value['mobile'].trim();
    if (common.validatePhone(mobile) == false) {
      common.showToast("手机号不正确!");
      return false;
    }

    that.doReg(e.detail.value);
  },

  doReg: function (formData) {
    var that = this;
    var params = {};
    //姓名
    params.name = formData['name'];
    //手机号
    params.mobile = formData['mobile'];

    //性别
    params.sex = formData['sex'];

    //服务工种
    params.serviceTypes = formData['serviceTypes'].toString();

    //工作经验
    params.workYear = formData['workYear'];
    
    //服务区域
    params.regionids = formData['regionIds'].toString();

    //简历来源  0 = 后台推荐 1 = 老师推荐 2= 客服推荐
    params.resumeFrom = 1;
    
    //判断是否有推荐人员
    var fromMobile = wx.getStorageSync("fromMobile");
    if (!common.isEmpty(fromMobile)) {
    		var resumeFrom = wx.getStorageSync("resumeFrom");
		var fromId = wx.getStorageSync("fromId");
		var fromName = wx.getStorageSync("fromName");
		params.fromId = fromId;
		params.fromMobile = fromMobile;
		params.fromName = fromName;
    }

   

    //签名
    params = sign.getSignParams(params);
    console.log(params);
    wx.request({
      url: app.globalData.baseUrl + '/api/resume/add.json',
      data: params,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function (res) {
        if (res.data.status == 999) {
          common.showToast(res.data.msg);
          return false;
        }

        var resumeData = res.data.data;

        wx.setStorageSync("resumeNo", resumeData.resumeNo);
        wx.setStorageSync("name", resumeData.name);
        wx.setStorageSync("total", resumeData.total);
        wx.setStorageSync("rowNum", resumeData.rowNum);
        wx.setStorageSync("totalScore", resumeData.totalScore);

        //进行图片上传
        that.doRegSuccess();
      },
      fail: function (res) {

      },
      complete: function (res) { },
    })
  },

  

  doRegSuccess: function () {
	console.log("doRegSuccess");
    var that = this;
    that.setData({
    	  submitSucess: true,
      total: wx.getStorageSync("total"),
      rowNum: wx.getStorageSync("rowNum"),
      totalScore: wx.getStorageSync("totalScore")
    })
  },
})
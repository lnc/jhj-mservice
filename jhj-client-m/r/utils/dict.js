var sign = require('lib/sign.js');
// 调用后台接口公用方法 dictType:向后台传的参数
  function getDict(dictType, pid, callback) {
	console.log("getDict");
	var storageKey = "dict_" + dictType;
	var datas = wx.getStorageSync(storageKey);
	
	if (datas != undefined && datas != "") {
		if (callback) {
	  		callback(datas);
	  	} else {
	  		return datas;
	  	}
	}
	
	var params = {};
	params.dictType = dictType;
	params.pid = pid;
	params = sign.getSignParams(params)
    wx.request({
      url: getApp().globalData.baseUrl + "/api/resume/get_dict.json",
	  data: params,
	  method: 'GET',
	  dataType: 'json',
      success: function(res) {
    	  	if (res.data.status == 999) {
    	  		wx.showToast({
					title : res.data.msg,
			});
    	  	}
    	  	wx.getStorageSync(storageKey, res.data.data);
    	  	if (callback) {
    	  		callback(res.data.data);
    	  	} else {
    	  		return res.data.data;
    	  	}
    	  	
      }
    })
  }

module.exports = {
  getDict: getDict
}
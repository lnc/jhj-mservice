// 检测是否为空，为空 = true 不为空 = false
function isEmpty(str) {
  if (str == undefined || str == "" || str.length == 0 || str == null) {
    return true;
  }
  return false;
}

// // 获取网页参数方法
// function getUrlParam(name) {http://dhsjg.com/ddddd?param1=2&param3=3&fdskjh=
//   var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
//   var r = window.location.search.substr(1).match(reg); // 匹配目标参数
//   if (r != null)
//     return unescape(r[2]);

//     var obj = param:ss
//   return null; // 返回参数值
// }

//检验手机号是否有效  有效=true,无效=false
function validatePhone(phone) {
  if (!isEmpty(phone)) {
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
    if (myreg.test(phone)) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

//验证是否为数字  为数字=true,不为数字=false
function isNumber(value) {
  var patrn = /^[0-9]*$/;
  if (patrn.exec(value) == null || value == "") {
    return false
  } else {
    return true
  }
}

//验证身份证号  合法为true,不合法为false
// function validateIdCard(idCard) {
//   var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
//   if (reg.test(idCard)) {
//     return true;
//   }
//   return false;
// }
function validateIdCard(idCard) {
  //15位和18位身份证号码的正则表达式
  var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
   var tip = "";
   var pass = true;

  //如果通过该验证，说明身份证格式正确，但准确性还需计算
  if (regIdCard.test(idCard)) {
    if (idCard.length == 18) {
      var idCardWi = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //将前17位加权因子保存在数组里
      var idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
      var idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
      for (var i = 0; i < 17; i++) {
        idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
      }

      var idCardMod = idCardWiSum % 11;//计算出校验码所在数组的位置
      var idCardLast = idCard.substring(17);//得到最后一位身份证号码

      //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
      if (idCardMod == 2) {
        if (idCardLast == "X" || idCardLast == "x") {
           pass=true;
        } else {
          tip ="身份证号码错误";
          pass=false;
        }
      } else {
        //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
        if (idCardLast == idCardY[idCardMod]) {
          pass = true;
        } else {
      
          tip = "身份证号码错误";
          pass = false;
        }
      }
    }
  } else {
  
    tip = "身份证格式不正确!";
    pass = false;
  }
  return pass;
}


// function validateIdCard(code) {
//   var city = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江 ", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北 ", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏 ", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外 " };
//   var tip = "";
//   var pass = true;

//   if (!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)) {
//     tip = "身份证号格式错误";
//     pass = false;
//   }
//   else if (!city[code.substr(0, 2)]) {
//     tip = "地址编码错误";
//     pass = false;
//   }
//   else {
//     //18位身份证需要验证最后一位校验位
//     if (code.length == 18) {
//       code = code.split('');
//       //∑(ai×Wi)(mod 11)
//       //加权因子
//       var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
//       //校验位啊

//       var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
//       var sum = 0;
//       var ai = 0;
//       var wi = 0;
//       for (var i = 0; i < 17; i++) {
//         ai = code[i];
//         wi = factor[i];
//         sum += ai * wi;
//       }
//       var last = parity[sum % 11];
//       if (parity[sum % 11] != code[17]) {
//         tip = "校验位错误";
//         pass = false;
//       }
//     }
//   }
//   return pass;
// }
// 验证上传图片格式 图片格式为true,则为false
function validateImgformat(value) {
  if (!isEmpty(value)) {
    var myreg = /\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/;
    if (myreg.test(value)) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}
//验证视频上传格式 视频文件为true,则为false
function validateVideoformat(value) {
  if (!isEmpty(value)) {
    var myreg = /\.(rmvb|avi|mp4|ts)$/;
    if (myreg.test(value)) {
      return true;
    } else {
      return false;
    }
  }
  return false;

}

//通用提示信息，不带确认，3秒消失
//icon = success , loading, none
function showToast(msg) {
	wx.showModal({
        title: '提示信息',
        showCancel:false,
        content: msg,
        showCancel:false
    })
}

/**
 * 获得转发url. 逻辑判断为
 * // resumeFrom 0 = 后台人员 1 = 老师推荐 2 = 客服推荐 3 = 阿姨推荐 4 = 用户推荐
 * 1. 如果有 wx.getStorageSync("fromMobile") 说明是扫码进入小程序，则优先获取，并且resumeFrom = 1
 * 2. 如果有 wx.getStorageSync("mobile"), 则说明是阿姨自己分析，则resumeFrom = 3
 * @param url
 * @returns
 */
function getShareUrl(url) {
  
  var resumeFrom = 1;
  var fromMobile = wx.getStorageSync("fromMobile");
  if (!isEmpty(fromMobile)) {
	  var fromId = wx.getStorageSync("fromId");
	  resumeFrom = 1;
	  if (url.indexOf("?") < 0) {
		  url+= "?";
	  } else {
		  url+= "&"
	  }
	  url+="shareFrom="+resumeFrom+"&shareId="+fromId+"&shareMobile="+fromMobile;
	  return url;
  }
  
  var mobile = wx.getStorageSync("mobile");
  if (!isEmpty(mobile)) {
	  var resumeId =  wx.getStorageSync("resumeId");
	  resumeFrom = 3;
	  if (url.indexOf("?") < 0) {
		  url+= "?";
	  } else {
		  url+= "&"
	  }
	  url+="shareFrom="+resumeFrom+"&shareId="+resumeId+"&shareId="+mobile;
	  return url;
  }
  return url;
}

function base64Encoder (str) { // 编码，配合encodeURIComponent使用
    var c1, c2, c3;
    var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var i = 0, len = str.length, strin = '';
    while (i < len) {
        c1 = str.charCodeAt(i++) & 0xff;
        if (i == len) {
            strin += base64EncodeChars.charAt(c1 >> 2);
            strin += base64EncodeChars.charAt((c1 & 0x3) << 4);
            strin += "==";
            break;
        }
        c2 = str.charCodeAt(i++);
        if (i == len) {
            strin += base64EncodeChars.charAt(c1 >> 2);
            strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            strin += base64EncodeChars.charAt((c2 & 0xF) << 2);
            strin += "=";
            break;
        }
        c3 = str.charCodeAt(i++);
        strin += base64EncodeChars.charAt(c1 >> 2);
        strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        strin += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
        strin += base64EncodeChars.charAt(c3 & 0x3F)
    }
    return strin
}

function base64Decoder (input) { // 解码，配合decodeURIComponent使用
    var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (i < input.length) {
        enc1 = base64EncodeChars.indexOf(input.charAt(i++));
        enc2 = base64EncodeChars.indexOf(input.charAt(i++));
        enc3 = base64EncodeChars.indexOf(input.charAt(i++));
        enc4 = base64EncodeChars.indexOf(input.charAt(i++));
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);
        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }
    }
    return utf8Decode(output);
}

function utf8Decode (utftext) { // utf-8解码
    var string = '';
    let i = 0;
    let c = 0;
    let c1 = 0;
    let c2 = 0;
    while (i < utftext.length) {
        c = utftext.charCodeAt(i);
        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        } else if ((c > 191) && (c < 224)) {
            c1 = utftext.charCodeAt(i + 1);
            string += String.fromCharCode(((c & 31) << 6) | (c1 & 63));
            i += 2;
        } else {
            c1 = utftext.charCodeAt(i + 1);
            c2 = utftext.charCodeAt(i + 2);
            string += String.fromCharCode(((c & 15) << 12) | ((c1 & 63) << 6) | (c2 & 63));
            i += 3;
        }
    }
    return string;
}

module.exports = {
  isEmpty: isEmpty,
  validatePhone: validatePhone,
  isNumber: isNumber,
  validateIdCard: validateIdCard,
  validateImgformat:validateImgformat,
  validateVideoformat:validateVideoformat,
  showToast : showToast,
  getShareUrl: getShareUrl,
  base64Encoder : base64Encoder,
  base64Decoder : base64Decoder
}
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const formatDate = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return [year, month, day].map(formatNumber).join('/')
}
/**
 * 
 * Long类型日期转String类型日期
 */
const longDateToStr = long => {
  var d = new Date();
  d.setTime(long);
  return formatDate(d);
}

module.exports = {
  formatTime: formatTime,
  formatDate: formatDate,
  longDateToStr: longDateToStr
}

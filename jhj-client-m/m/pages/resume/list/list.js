//index.js
var sign = require('../../../utils/lib/sign.js');
var common = require("../../../utils/common.js");
const app = getApp()
import tempObj from '../../template/BottomTab.js'

// var indexObj = {

// }
// indexObj["changeColor"] = tempObj.changeColor
var a = tempObj.changeColor
Page({
  changeColor: a,
  data: {
	  tabBars: {
	      left: {
	        url: '../../resume/list/list',
	        icon: '../../img/aunt.png',
	        text: "优秀阿姨推荐",

	      },
	      right: {
	        url: '../../user/index/index',
	        icon: '../../img/mine.png',
	        text: "我的",

	      },
	    },
    // 服务工种多选 初始化数据
    serviceTypeList: [
      { name: '保姆', id: '4670' },
      { name: '育儿嫂', id: '4671' },
      { name: '月嫂', id: '4672' },
      { name: '催乳师', id: '4673' },
      { name: '小儿推拿', id: '4674' },
    ],
    list: [],
    lastPage: 1,
    pageNum: 1,
    serviceTypes:''
  },


  // 多选服务工种事件
  serverItem: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.serviceTypeList.length; i++) {
      if (checked.indexOf(this.data.serviceTypeList[i].id) !== -1) {
        changed['serviceTypeList[' + i + '].checked'] = true
      } else {
        changed['serviceTypeList[' + i + '].checked'] = false
      }
    }
    this.setData(changed);
    if (checked.length>0){
      var serviceTypes = checked.toString();
      this.setData({
        serviceTypes: serviceTypes
      })
    }
   
    this.getResumeList(1, serviceTypes);

  },
  // 事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getServiceType();
    this.getResumeList(1,"");
  },

  /**
	 * 页面上拉触底事件的处理函数
	 */
  onReachBottom: function () {
    var that = this;
    var pageNum = this.data.pageNum;
    pageNum++;

    var serviceTypes = this.data.serviceTypes;

    if (pageNum > this.data.lastPage) {
      return false;
    }

    console.log(pageNum)
    this.getResumeList(pageNum, serviceTypes);

  },
  /**
	 * 获取服务人员列表
	 */
  getResumeList(pageNum, serviceTypes) {
    var pageNum = pageNum | 1;
    var that = this;
    if (common.isEmpty(serviceTypes)) {
    		serviceTypes = "";
    		that.setData({serviceTypes: serviceTypes});
    }
    var params = {
      serviceTypes: serviceTypes,
      page: pageNum,
      publish : 1,
      orderByTime : 1,
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + "/api/resume/list.json",
      data: params,
      method: 'GET',
      dataType: 'json',
      success: function (res) {
        var result = res.data.data
        if (pageNum == 1) {
          that.setData({
            list: result.list,
            lastPage: result.lastPage,
            pageNum: result.pageNum
          })
        }
        if (pageNum > 1 && pageNum <= that.data.lastPage) {
          that.setData({
            list: that.data.list.concat(result.list),
            pageNum: pageNum
          })
        }

        console.log("成功")
      },
      fail: function (res) {
        console.log("失败")
      }
    })
  },
  /**
	 * 获取服务类别
	 * 
	 */
  getServiceType: function () {
    var that = this;
    var params = {
      dictType: "service_type"
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + "/api/resume/get_dict.json",
      data: params,
      method: 'GET',
      dataType: 'json',
      success: function (res) {

        var result = res.data.data;
        if (result.length > 0) {
          console.log(result);
          var lists = [];
          for (var i = 0; i < result.length; i++) {
            var obj = {};
            obj.name=result[i].name;
            obj.id = result[i].id.toString();
            lists.push(obj);
          }
          console.log(lists);
          that.setData({
            serviceTypeList: lists,
          })
        }
        console.log("成功")
      },
      fail: function (res) {
        console.log("失败")
      }
    })
  },
  /**
	 * 支持他
	 */
  support:function(e){
    var params = e.currentTarget.dataset;
    var that = this;
    var resumeId = params.resumeId;
    var resumeNo = params.resumeNo;
    var actionId = wx.getStorageSync('userId');
    if (actionId == null || actionId==''){
      wx.navigateTo({
        url: '../login/login',
      })
      return false;
    }
    var actionName = wx.getStorageSync('mobile');
    var params = {
      resume_id: resumeId,
      resume_no: resumeNo,
      action_type: 0,
      action: '',
      action_id: actionId,
      action_name: actionName,
      remark: ''
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + "/api/resume/zan/add.json",
      data: params,
      method: 'POST',
      header:{
        'Content-Type':'application/x-www-form-urlencoded'
      },
      success: function (res) {
        var result = res.data.data;
        
        wx.showToast({
          title: '成功',
        })
      },
      fail: function (res) {
        console.log("失败")
      }
    })
  }
  
})

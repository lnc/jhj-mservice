var sign = require('../../../utils/lib/sign.js');
var common = require("../../../utils/common.js");
const app = getApp();
import tempObj from '../../template/BottomTab.js'

// var indexObj = {

// }
// indexObj["changeColor"] = tempObj.changeColor
var a = tempObj.changeColor
Page(

  {
    changeColor: a,
    /**
     * 页面的初始数据
     */
    data: {
      tabBars: {
        left: {
          url: '../list/list',
          icon: '../../img/aunt.png',
          text: "优秀阿姨推荐",

        },
        right: {
          url: '../../user/index/index',
          icon: '../../img/mine.png',
          text: "我的",

        },
      },
      isVideo:false,
      
      isShow: 'hide',
      subject: {},
      liveImg: [],
      videoUrl: "",
      sale: {},
      resumeNo: "",
      resumeId: 0,
      saleName : "",
      shareRate : "",
      shareSaleId : 0,
      shareUserId : 0
    },

    returnPage: function () {
      this.setData({
        isShow: 'hide',
        isVideo:true,
      })
      
      this.videoContext.play()

    },
    //video层级处理
    videoPlay:function(){
        var that=this;
        that.setData({
          isVideo:true,
        })
        that.videoContext.play();

    },
    /**
     * 相册效果
     */
    clickImage: function (e) {
      var that = this;
      var urls=[];
      // var urls = that.data.liveImg.resourceUrl;
      for (var i=0; i < that.data.liveImg.length;i++){
        var urlsValue = (that.data.liveImg)[i].resourceUrl;
         urls.push(urlsValue);
      }
      wx.previewImage({
        urls: urls,
        current: e.currentTarget.id
      });

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      this.setData({
        "tabBars.currentIndex": app.globalData.currentIndex
      });

      console.log(options);

    
      // this.setData({
      //   isOrder:options.isOrder
      // })
      
      var resumeNo = "";
      var resumeId = 0;

      if (!common.isEmpty(options.resumeNo)) resumeNo = options.resumeNo;
      if (!common.isEmpty(options.resumeId)) resumeId = options.resumeId;
      
      //分享者老师Id
      if (!common.isEmpty(options.shareSaleId)) {
    	  	  this.setData({"shareSaleId" : options.shareSaleId});
      }
      
      //分享者用户Id
      if (!common.isEmpty(options.shareUserId)) {
    	  	  this.setData({"shareUserId" : options.shareUserId});
      }
      
      console.log("shareSaleId == " + this.data.shareSaleId);
      console.log("shareUserId == " + this.data.shareUserId);
      
      /**
		 * 判断是否有扫码进入的，按照扫码进入的情况进行解析
		 * https://mp.weixin.qq.com/debug/wxadoc/dev/api/qrcode.html
		 * scene格式为
		 * [resumeId]-[saleId]-[saleMobile]
		 */
      var scene = options.scene;
      if (!common.isEmpty(scene)) {

        var params = scene.split("-");
        resumeId = params[0].trim();
        var saleId = params[1].trim();
        var saleMobile = params[2].trim();
        if (!common.isEmpty(saleMobile)) {
          //存入本地
          wx.setStorageSync("saleId", saleId);
          wx.setStorageSync("saleMobile", saleMobile);
          
        }
      }
      console.log("resumeNo ==" + resumeNo);
      console.log("resumeId ==" + resumeId);

      this.getDetail(resumeNo, resumeId);
      this.setData({ resumeNo: resumeNo, resumeId: resumeId });
      
      
      if (resumeId > 0) {
    	  	this.getShareInfo(resumeId);
      }
      
      console.info(this.data);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
      this.videoContext = wx.createVideoContext('myVideo')

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },


    /**
     * 服务人员详情
     * 
     */
    getDetail: function (resumeNo, resumeId) {
      var that = this;
      var params = {};

      if (!common.isEmpty(resumeNo)) params.resumeNo = resumeNo;
      if (resumeId > 0) params.resumeId = resumeId;

      params = sign.getSignParams(params)
      wx.request({
        url: app.globalData.baseUrl + '/api/resume/detail.json',
        data: params,
        success: function (res) {
          var result = res.data.data;
          resumeNo = result.resumeNo;
          resumeId = result.resumeId;
          var arry = [];
          var list = result.resumeResource;
          if (list != null && list.length > 0) {
            for (var i = 0; i < list.length; i++) {
              var img = list[i];
              if (img.resourceType == 'resume_live_img') {
                arry.push(img);
              }
              if (img.resourceType == 'resume_video') {
                that.getPlayVideoUrl(img.resourceUrl);
              }
            }
          }
          that.setData({
        	    resumeId : resumeId,
        	    resumeNo : resumeNo,
            subject: result,
            liveImg: arry,
          });
          
          if (resumeId > 0) {
  	  		that.getShareInfo(resumeId);
          }
          
          console.log(that.data.subject);
        }
      })
    },
    
    /**
     * 推荐人及评价信息
     * 
     */
    getShareInfo: function (resumeId) {
      var that = this;
      var params = {};

      params.resumeId = resumeId;
      
      var saleId = wx.getStorageSync("saleId");
      if (!common.isEmpty(saleId)) {
    	  	params.saleId = saleId;
      }
      

      params = sign.getSignParams(params);
      wx.request({
        url: app.globalData.baseUrl + '/api/resume/getShareInfo.json',
        data: params,
        success: function (res) {
          var result = res.data.data;

          var saleName = result.saleName;
          var shareRate = result.shareRate;
          
          wx.setStorageSync("saleName", saleName);
          that.setData({
	        	  saleName: saleName,
	        	  shareRate: shareRate,
          });
          
        }
      })
    },
    

    getPlayVideoUrl: function (videoUrl) {
      var that = this;
      var params = {};
      params.videoUrl = videoUrl;
      params = sign.getSignParams(params)
      wx.request({
        url: app.globalData.baseUrl + '/api/resume/download_token.json',
        data: params,
        success: function (res) {
          if (res.data.status == 0) {
            console.log(res.data.data);
            that.setData({
              videoUrl: res.data.data
            })
          }
        }
      })
    },

    /**
     * 预约服务人员
     * 应该先跳到一个选择服务类型的页面
     */
    order: function () {
      var that = this;
      var userId = wx.getStorageSync('userId');
      var mobile = wx.getStorageSync('mobile');
      
      if (userId == null || userId == '') {
    	   
    	    //返回格式为 {url:,  params:{}} 返回时再拼接
    	    var returnUrl = "../../resume/detail/detail";
    	    returnUrl+= "?resumeNo=" + that.data.resumeNo + "&resumeId="+ that.data.resumeId;
    	    returnUrl = common.base64Encoder(returnUrl);  

    	    console.log(returnUrl);
        wx.redirectTo({
          url: '../../user/login/login?returnUrl='+returnUrl,
          success: function (res) { },
        })
        return false;
      }

      var userName = "";
      var lat = wx.getStorageSync('lat');
      var lng = wx.getStorageSync('lng');

      var params = {
        userId: userId,
        mobile: mobile,
        userName: userName,
        orderStatus: 1,
        lat: lat,
        lng: lng,
        addFrom: 0,
        actionId: userId,
        actionName: userName,
        resumeNo : that.data.resumeNo
      }
      
      var saleId =  wx.getStorageSync('saleId');
      if (!common.isEmpty(saleId)) params.saleId = saleId;
      
      var saleMobile = wx.getStorageSync('saleMobile');
      if (!common.isEmpty(saleMobile)) params.saleMobile = saleMobile;

      params = sign.getSignParams(params);

      wx.showModal({
        title: '',
        content: '确定预约吗？',
        success: function (res) {
          if (res.cancel) return false;
          //解决video层级
          that.setData({
            isVideo: false,
          })
          that.videoContext.pause();


          wx.request({
            url: app.globalData.baseUrl + '/api/bm/order/add.json',
            method: 'POST',
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            data: params,
            success: function (res) {
              var result = res.data;
              if (result.status == 0) {
                that.setData({
                  isShow: 'show',
                  sale: result.data
                })
              } else {
                wx.showToast({
                  title: result.msg,
                })
              }
            }
          })
        }
      })
    },

    /**
     * 用户点击右上角分享或推荐给好友按钮
     */
    onShareAppMessage: function (options) {
      var that = this;
      var pages = getCurrentPages();
      var url = "pages/resume/detail/detail?resumeId="+that.data.resumeId;
      
      var saleId = wx.getStorageSync("saleId");
      if (!common.isEmpty(saleId)) {
    	  	url+= "&shareSaleId="+saleId;
      }
      
      var userId = wx.getStorageSync('userId');
      if (!common.isEmpty(userId)) {
  	  	url+= "&shareUserId="+userId;
      }
      
      console.log(url);
      return false;
      return {
        title: app.globalData.shareTitle,
        path: url,
        success: function (res) {
          that.setResumePublish();
        },
        fail: function (res) {
          wx.showToast({
            title: '转发失败',
          })
        }
      }
    },

    /**
     * 更新人才库状态为已推荐.
     */
    setResumePublish: function (id) {
      console.log("setResumePublish");
      var that = this;
      var params = {};
      params.resumeId = that.data.resumeId;
      params.publish = 1;
      params.actionId = wx.getStorageSync("saleId");
      params.actionName = wx.getStorageSync("saleName");
      params.actionMobile = wx.getStorageSync("saleMobile");

      params = sign.getSignParams(params);
      console.log(app.globalData.baseUrl + '/api/resume/setPublish.json');
      wx.request({
    	    method: 'POST',
    	    header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
        url: app.globalData.baseUrl + '/api/resume/setPublish.json',
        data: params,
        success: function (res) {
          var result = res.data.data;
          console.log(result);
          wx.showToast({
            title: '转发成功',
          });
        },
      })
    },
  })


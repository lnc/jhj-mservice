var sign = require('../../../utils/lib/sign.js')
var app = getApp();

import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor

Page({
  changeColor: a,
	/**
	 * 页面的初始数据
	 */
  data: {
    tabBars: {
      left: {
        url: '../../resume/list/list',
        icon: '../../img/aunt.png',
        text: "优秀阿姨推荐",

      },
      right: {
        url: '../../user/index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },

    user: {}
  },
  about: function () {
    wx.redirectTo({
      url: '../about/about',
    })
  },


	/**
	 * 生命周期函数--监听页面加载
	 */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getUser();
  },

	/**
	 * 用户点击右上角分享
	 */
  onShareAppMessage: function (options) {
    var pages = getCurrentPages();
    var url = pages[pages.length - 1].route;
    console.log(options)
    return {
      title: app.globalData.shareTitle,
      path: url,
      imageUrl:'../../img/share-baner.png',
      success: function (res) {
        wx.showToast({
          title: '转发成功',
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
        })
      }
    }
  },
	/**
	 * 联系客服
	 * 
	 */
  callPhone: function () {
    wx.makePhoneCall({
      phoneNumber: '010-53511197',
    })
  },
	/**
	 * 获取用户信息
	 * 
	 */
  getUser: function () {
    var that = this;
    var userId = wx.getStorageSync("userId");
    if (userId == "" || userId == null) {
      wx.redirectTo({
        url: '../login/login',
      })
      return false;
    }

    var params = {
      userId: userId
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + "/api/user/userinfo.json",
      data: params,
      success: function (res) {
        var result = res.data.data;
        if (res.data.status == 0) {
          that.setData({
            user: result
          })
        }
        if (res.data.status == 999) {
          wx.showModal({
            title: '',
            content: res.data.msg,
            showCancel: false
          })
        }
      },
    })
  },
	/**
	 * 
	 * 退出登录
	 */
  logout: function () {
//    wx.removeStorageSync('mobile');
//    wx.removeStorageSync('userId');
//    wx.removeStorageSync('token');
	wx.clearStorage();
    wx.redirectTo({
      url: '../login/login',
    })
  },
	/**
	 * 跳转登录
	 * 
	 */
  goLogin: function () {
    wx.redirectTo({
      url: '../login/login',
    })
    return false;
  },

	/**
	 * 地址跳转
	 * 
	 */
  linkToAddr: function() {
    var that = this;
    var userId = wx.getStorageSync("userId");
    if (userId == null || userId == '') {
      that.goLogin();
    }
    wx.redirectTo({
      url: '../addr/list/list',
    })
  },
	/**
	 * 优惠券连接
	 */
  linkCoupons: function() {
    var userId = wx.getStorageSync("userId");
    if (userId == null || userId == '') {
      that.goLogin();
    }
    wx.redirectTo({
      url: '../coupons/coupons',
    })
  },
  linkToOrder:function(){
    var userId = wx.getStorageSync("userId");
    if (userId == null || userId == '') {
      that.goLogin();
    }
    wx.redirectTo({
      url: '../../order/list/list',
    })
  }

})
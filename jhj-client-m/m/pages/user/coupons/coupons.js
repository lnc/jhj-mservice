// pages/user/coupons/coupons.js
var util = require('../../../utils/util.js')
var sign = require('../../../utils/lib/sign.js')

var app = getApp();
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,
  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../../resume/list/list',
        icon: '../../img/aunt.png',
        text: "优秀阿姨推荐",

      },
      right: {
        url: '../../user/index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getCouponsList();
  },
  /**
   * 
   * 用户优惠券列表
   */
  getCouponsList:function(){
    var that = this;
    var userId = wx.getStorageSync('userId');
    var params = sign.getSignParams({ userId: userId})
    wx.request({
      url: app.globalData.baseUrl +'/api/user/coupons/list.json',
      data: params,
      method: 'GET',
      success: function(res) {
        var result = res.data.data;
        var arry = [];
        if (result.length>0){
          for (var i = 0; i < result.length;i++){
            var o = result[i];
            o.fromDateStr = util.longDateToStr(o.fromDate);
            o.toDateStr = util.longDateToStr(o.toDate);
            arry.push(o)
          }
          that.setData({
            list: arry
          })
        }
      },
    })
  }
})
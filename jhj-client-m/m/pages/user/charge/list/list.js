// pages/user/userCharge/userCharge.js
var sign = require('../../../../utils/lib/sign.js')
var app = getApp();
import tempObj from '../../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,
  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    tabBars: {
      left: {
        url: '../../../resume/list/list',
        icon: '../../../img/aunt.png',
        text: "优秀阿姨推荐",

      },
      right: {
        url: '../../../user/index/index',
        icon: '../../../img/mine.png',
        text: "我的",

      },
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getCardList();
  
  },
  
  getCardList:function(){
    var that = this;
    var params = sign.getSignParams({})
    wx.request({
      url: app.globalData.baseUrl +'/api/user/cardList.json',
      data: params,
      success: function(res) {
        if(res.data.status==0){
          var result = res.data.data;
          that.setData({
            list: result
          })
        }
        if (res.data.status == 999){
          wx.showModal({
            title: '',
            content: res.data.msg,
            showCancel:false
          })
        }
       
      },
    })
  }
})
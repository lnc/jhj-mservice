var sign = require('../../../../utils/lib/sign.js')
var app = getApp();
import tempObj from '../../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,

  /**
	 * 页面的初始数据
	 */
  data: {
    tabBars: {
      left: {
        url: '../../../resume/list/list',
        icon: '../../../img/aunt.png',
        text: "阿姨推荐",

      },
      right: {
        url: '../../index/index',
        icon: '../../../img/mine.png',
        text: "我的",

      },
    },

    address:'',
    latitude:'',
    longitude:'',
    markers: [],
  },

  /**
	 * 生命周期函数--监听页面加载
	 */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getMap();
  },

  getMap:function(){ 
    var that = this;
    wx.chooseLocation({
      success:(res)=>{
        var name = res.name;
        var address = res.address;
        var latitude = res.latitude;
        var longitude = res.longitude;
        that.setData({
          latitude: latitude,
          longitude: longitude,
          address: address,
          markers:[{
            id:0,
            latitude: latitude,
            longitude: longitude,
            iconPath:'img/marker.png',
            width:24,
            height:40
          }]
        })
      }
    })
  },
  /**
	 * 选择地址
	 */
  chooseAddress:function(){
    this.getMap()
  },
  /**
	 * 添加地址
	 */
  add:function(e){
    var param = e.detail.value;
    var userId = wx.getStorageSync("userId");
    if(userId==null || userId==''){
      wx.redirectTo({
        url: '../../login/login',
      })
      return false;
    }
    if (param.addr == null || param.addr==''){
      wx.showToast({
        title: '地址不能为空',
      })
      return false;
    }
    var mobile = wx.getStorageSync("mobile");
    var params = {
      user_id: userId,
      mobile: mobile,
      is_default: 1,
      name: param.name,
      addr: param.addr,
      longitude: param.longitude,
      latitude: param.latitude,
      city: '北京市'
    }
    params = sign.getSignParams(params);
    wx.request({
      url: app.globalData.baseUrl + '/api/user/addr/add.json',
      data: params,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function(res) {
        wx.redirectTo({
          url: '../list/list',
        })
      },
      fail: function(res) {
        wx.showToast({
          title: '失败',
        })
      }
    })
  }
})
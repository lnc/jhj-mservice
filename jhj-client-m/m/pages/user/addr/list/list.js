// pages/user/userAddress/userAddress.js
var sign = require('../../../../utils/lib/sign.js')
const app = getApp()
import tempObj from '../../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,
  /**
	 * 页面的初始数据
	 */
  data: {
	  tabBars: {
	      left: {
	        url: '../../../resume/list/list',
	        icon: '../../../img/aunt.png',
	        text: "阿姨推荐",

	      },
	      right: {
	        url: '../../index/index',
	        icon: '../../../img/mine.png',
	        text: "我的",

	      },
	    },

    itemData:[]
  },


  touchS: function (e) {  // touchstart
    let startX = app.Touches.getClientX(e)

    startX && this.setData({ startX })

  },

  touchM: function (e) {  // touchmove
    let itemData = app.Touches.touchM(e, this.data.itemData, this.data.startX)
    itemData && this.setData({ itemData })

  },
  touchE: function (e) {  // touchend
    const width = 195  // 定义操作列表宽度
    let itemData = app.Touches.touchE(e, this.data.itemData, this.data.startX, width)
    itemData && this.setData({ itemData })
  },
  itemDelete: function (e) {  // itemDelete
    var that = this;
    var id = e.target.dataset.id;
    id = parseInt(id);
    var params = sign.getSignParams({ "id": id }); 
    console.log(id);
    console.log(app.globalData.baseUrl);
 
    wx.request({
      url: app.globalData.baseUrl + '/api/user/addr/del.json',
      data: params,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: 'POST',
      success: function(res) {
        if(res.data.status==0){
          let itemData = app.Touches.deleteItem(e, that.data.itemData)
          itemData && that.setData({ itemData})
          // that.getAddressList();
        }
      },
    })
    
  },

  /**
	 * 生命周期函数--监听页面加载
	 */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getAddressList();
  },

  /**
	 * 获取用户地址列表
	 */ 
  getAddressList: function () {
    var that = this;
    var userId = wx.getStorageSync('userId');
    var params = sign.getSignParams({ userId: userId});
    wx.request({
      url: app.globalData.baseUrl + '/api/user/addr/list.json',
      data: params,
      method: 'GET',
      success: function (res) {
        var result = res.data;
        if (result.status==0){
          that.setData({
            itemData: result.data
          })
        }
        if (result.status == 999){
          wx.showModal({
            title: '',
            content: result.msg,
            showCancel: false
          })
        }
      },
    })
  },
  addAddr:()=>{
    wx.redirectTo({
      url: '../add/add',
    })
  }
})
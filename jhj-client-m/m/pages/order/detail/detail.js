var sign = require('../../../utils/lib/sign.js')
var app = getApp();
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor

Page({
  changeColor: a,
	/**
	 * 页面的初始数据
	 */
  data: {
    tabBars: {
      left: {
        url: '../../resume/list/list',
        icon: '../../img/aunt.png',
        text: "优秀阿姨推荐",

      },
      right: {
        url: '../../user/index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
    order: {},
    resumeList: [],
    orderResumeList:[],
    currentIndex: '',
    checkedAunt: [],
  },
//选择阿姨
  chooseAunt: function (e) {
  
    var checked = e.detail.value;
    var changed = {}
    for (var i = 0; i < this.data.orderResumeList.length; i++) {

      var name = this.data.orderResumeList[i].name;
      if (checked.indexOf(name) !== -1) {

        this.data.orderResumeList[i].checked = true;
      } else {
        this.data.orderResumeList[i].checked = false;
      }
    }
    var obj = {
      name: checked
    }
    var arr = this.data.checkedAunt;
    arr.splice(0, arr.length);//清空数组 
    console.log(arr); // 输出 []，空数组，即被清空了
    arr.push(obj);
    this.setData({ orderResumeList: this.data.orderResumeList,
      checkedAunt: arr
     });
  
    
   

  },

	/**
	 * 生命周期函数--监听页面加载
	 */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    var orderId = options.orderId;
    this.getOrderDetail(orderId);
    this.getResumeList(orderId);
  },

  getOrderDetail: function (orderId) {
    var that = this;
    var userId = wx.getStorageSync("userId");
    var params = sign.getSignParams({ orderId: orderId });
    wx.request({
      url: app.globalData.baseUrl + '/api/bm/order/detail.json',
      data: params,
      success: function (res) {
        var result = res.data;
        if (result.status == 0) {
          that.setData({
            order: result.data,
            orderResumeList: result.data.orderResumeList,
          })
        } else {

        }
      },
    })
  },
  //联系服务顾问
  callMobile: function (options) {
    var mobile = options.mobile;
    if (mobile == null || mobile == '') return false;
    wx.makePhoneCall({
      phoneNumber: mobile,
    })
  },
  getResumeList: function (orderId) {
    var that = this;
    var params = sign.getSignParams({ orderId: orderId });
    wx.request({
      url: app.globalData.baseUrl + '/api/bm/order/resume/get.json',
      data: params,
      success: function (res) {
        var result = res.data;
        if (result.status == 0) {
          that.setData({
            resumeList: result.data
          })
        }
      },
    })
  },

  /**
   * 选择阿姨样式变化
   */
  // chooseAunt: function (e) {
  //   var that = this;
  //   var index = e.currentTarget.dataset.index;
  //   var name = e.currentTarget.dataset.name;
  //   console.log(name);
  //   var obj = {
  //     name: name,
  //   }
  //   var arr = that.data.checkedAunt;
  //   arr.push(obj)
  //   that.setData({
  //     "currentIndex": index,
  //     checkedAunt: arr,

  //   })


  // }
})
// pages/user/order/orderList.js
var sign = require('../../../utils/lib/sign.js')
var app = getApp();
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../../resume/list/list',
        icon: '../../img/aunt.png',
        text: "优秀阿姨推荐",

      },
      right: {
        url: '../../user/index/index',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
    list:[],
    lastPage: 1,
    pageNum: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })
    this.getOrderList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var pageNum = this.data.pageNum;
    pageNum++;

    if (pageNum > this.data.lastPage) {
      return false;
    }
    this.getOrderList(pageNum);
  },

  getOrderList: function (pageNum){
    var pageNum = pageNum | 1;
    var that = this;
    var userId = wx.getStorageSync("userId");
    var params = {
      userId: userId,
      pageNo: pageNum
    }
    params = sign.getSignParams(params);
    wx.request({
      url: app.globalData.baseUrl + '/api/bm/order/list.json',
      method: 'GET',
      data:params,
      success: function (res) {
        var data = res.data.data;

        if (pageNum == 1) {
          that.setData({
            list: data.list,
            lastPage: data.lastPage,
            pageNum: data.pageNum
          })
        }

        if (pageNum > 1 && pageNum <= that.data.lastPage) {
          that.setData({
            list: that.data.list.concat(data.list),
            pageNum: pageNum
          })
        }
      },
      fail: function (res) {
        console.log("sss")
      },
      complete: function (res) { },
    })
  }
})
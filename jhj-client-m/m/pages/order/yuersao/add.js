const app = getApp();
var sign = require('../../../utils/lib/sign.js')
var common = require("../../../utils/common.js");
var bmap = require('../../../utils/lib/bmap-wx.js')
//引入模板 改变底部菜单颜色值方法
import tempObj from '../../template/BottomTab.js'
var a = tempObj.changeColor
Page({
  changeColor: a,
	/**
	 * 页面的初始数据
	 */
	data : {
		serviceTypeId : 4671,
		serviceTypeName : '育儿嫂',
		codeMessage : "获取验证码",
		countDown : false,
		userName : '',
		mobile : '',
		smsToken : '',
		serviceAge : 1,
		workHome : 0,
		btnSubmitDisable : false,
		tabBars : {
			left : {
				url : '../../resume/list/list',
				icon : '../../img/aunt.png',
        text: "优秀阿姨推荐",

			},
			right : {
				url : '../../user/index/index',
				icon : '../../img/mine.png',
				text : "我的",

			},
		},
		radioItems : [
				{
					name : '0-1岁',
					value : '0'
				}, {
					name : '1-2岁',
					value : '1',
					checked : 'true'
				}, {
					name : '2-3岁',
					value : '2'
				}, {
					name : '3岁以上',
					value : '3'
				} ],
		radioItemsHouse : [
				{
					name : '住家',
					value : '1'
				}, {
					name : '不住家',
					value : '0',
					checked : 'true'
				}

		],
	},

	/**
	 * 获取手机号
	 */
	setMobile : function(e) {
		this.setData({
			mobile : e.detail.value
		})
	},
	/**
	 * 获取输入的验证码
	 */
	setSmsToken : function(e) {
		this.setData({
			smsToken : e.detail.value
		})
	},

	/**
	 * 称呼
	 */
	setUserName : function(e) {
		this.setData({
			userName : e.detail.value
		})
	},

	radioChange : function(e) {
		var checked = e.detail.value
		var changed = {}
		for (var i = 0; i < this.data.radioItems.length; i++) {
			if (checked.indexOf(this.data.radioItems[i].name) !== -1) {
				changed['radioItems[' + i + '].checked'] = true
			} else {
				changed['radioItems[' + i + '].checked'] = false
			}
		}
		this.setData(changed);
		this.setData({
			serviceAge : e.detail.value
		})
	},
	radioChangeHouse : function(e) {
		var checked = e.detail.value
		var changed = {}
		for (var i = 0; i < this.data.radioItemsHouse.length; i++) {
			if (checked == this.data.radioItemsHouse[i].value) {
				changed['radioItemsHouse[' + i + '].checked'] = true
			} else {
				changed['radioItemsHouse[' + i + '].checked'] = false
			}
		}
		this.setData(changed);
		this.setData({
			workHome : e.detail.value
		})
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad : function(options) {
    this.setData({
      "tabBars.currentIndex": app.globalData.currentIndex
    })

		var that = this;
		/* 获取定位地理位置 */
		// 新建bmap对象
		var BMap = new bmap.BMapWX({
			ak : app.globalData.baiduAk
		});
		var fail = function(data) {
			console.log(data);
			that.getServiceTotal();
		};
		var success = function(data) {
			// 返回数据内，已经包含经纬度
			console.log(data);
			// 使用wxMarkerData获取数据
			var wxMarkerData = data.wxMarkerData;
			wx.setStorageSync("lat", wxMarkerData[0].latitude);
			wx.setStorageSync("lng", wxMarkerData[0].longitude);
			wx.setStorageSync("address", wxMarkerData[0].address);
			wx.setStorageSync("city", data.originalData.result.addressComponent.city);
			that.getServiceTotal();

		}
		// 发起regeocoding检索请求
		BMap.regeocoding({
			fail : fail,
			success : success
		});
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady : function() {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow : function() {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide : function() {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload : function() {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh : function() {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom : function() {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage : function() {

	},

	/**
	 * 获取服务人员数量
	 */
	getServiceTotal : function() {

		var lat = wx.getStorageSync('lat');
		var lng = wx.getStorageSync('lng');
		console.log(lat);
		if (common.isEmpty(lat))
			lat = "";
		if (common.isEmpty(lng))
			lng = "";

		var that = this;

		var params = {
			serviceTypeId : 4671,
			lat : lat,
			lng : lng,
		}
		params = sign.getSignParams(params)
		wx.request({
			url : app.globalData.baseUrl + '/api/resume/get_total.json',
			data : params,
			success : function(res) {
				var result = res.data.data;
				that.setData({
					data : result,
				})
			}
		})
	},

	/**
	 * 倒计时
	 */
	countDown : function() {
		var that = this;
		var flag = common.validatePhone(this.data.mobile);
		if (flag) {
			var second = 60;
			var t = setInterval(function() {
				second--;
				if (second < 0) {
					clearInterval(t);
					that.setData({
						codeMessage : "获取验证码",
						countDown : false,
					})
				} else {
					that.setData({
						codeMessage : second + "秒",
						countDown : true,
					})
				}
			}, 1000);

		}
	},

	/**
	 * 获取验证码
	 */
	getSmsToken : function() {
		var that = this;
		var mobile = that.data.mobile;
		console.log("mobile ===" + mobile);
		if (common.isEmpty(mobile) || !common.validatePhone(this.data.mobile)) {
			wx.showToast({
				title : '请填写正确手机号',
				icon : 'loading',
				duration : 2000
			})
			return false;
		}

		if (that.data.countDown) {
			return false;
		}

		console.log(mobile + "-----------------");
		var params = {};
		params.mobile = mobile;
		params.sms_type = 0;
		params = sign.getSignParams(params);
		wx.request({
			url : app.globalData.baseUrl + '/api/user/getCode.json',
			data : params,
			method : 'GET',
			success : function(res) {
				console.log(res);
				that.countDown();
			},
		})
	},

	orderSubmitConfirm : function() {
		var that = this;
		var userName = that.data.userName;
		if (common.isEmpty(userName)) {
			wx.showToast({
				title : '请填写你的称呼',
				icon : 'loading',
				duration : 2000
			})
			return false;
		}

		var mobile = that.data.mobile;
		console.log("mobile ===" + mobile);
		if (common.isEmpty(mobile) || !common.validatePhone(this.data.mobile)) {
			wx.showToast({
				title : '请填写正确手机号',
				icon : 'loading',
				duration : 2000
			})
			return false;
		}

		var smsToken = that.data.smsToken;
		if (common.isEmpty(that.data.smsToken)) {
			wx.showToast({
				title : '请填写验证码',
				icon : 'loading',
				duration : 2000
			})
			return false;
		}

		that.login();
	},

	/**
	 * 登录
	 */
	login : function() {
		var that = this;
		// that.validateData();

		wx.login({
			success : function(res) {
				var wxcode = res.code;
				var mobile = that.data.mobile;
				var code = that.data.smsToken;
				var params = {};
				params.mobile = mobile;
				params.sms_token = code;
				params.user_type = 0;
				params.code = wxcode;
				params = sign.getSignParams(params);
				wx.request({
					url : app.globalData.baseUrl + '/api/user/login.json',
					data : params,
					header : {
						"Content-Type" : "application/x-www-form-urlencoded"
					},
					method : 'POST',
					success : function(res) {
						if (res.data.status == 0) {
							wx.setStorageSync("mobile", res.data.data.mobile);
							wx.setStorageSync("userId", res.data.data.userId);
							wx.setStorageSync("token", res.data.data.token);
							that.orderSubmit();
						}
						if (res.data.status == 999) {
							wx.showToast({
								title : res.data.msg,
								icon : 'loading',
								duration : 2000
							})
						}

					}
				})
			}
		})
	},

	/**
	 * 订单提交
	 */
	orderSubmit : function() {
		var that = this;

		var params = {};
		params.userId = wx.getStorageSync('userId');
		params.mobile = that.data.mobile;
		params.userName = that.data.userName;
		params.serviceAge = that.data.serviceAge;
		params.workHome = that.data.workHome;
		params.serviceTypeId = that.data.serviceTypeId;
		params.serviceTypeName = that.data.serviceTypeName;
		params.addFrom = 0;
		params.orderStatus = 1;
		params = sign.getSignParams(params);
		console.log(params);
		
		wx.request({
			url: app.globalData.baseUrl + '/api/bm/order/add.json',
			data : params,  
			header : {
				"Content-Type" : "application/x-www-form-urlencoded"
			},
			method : 'POST',
			success : function(res) {
				if (res.data.status == 0) {
          wx.redirectTo({
						url : '../../order/success/success',
					})
				}
				if (res.data.status == 999) {
					wx.showToast({
						title : res.data.msg,
						icon : 'loading',
						duration : 2000
					})
				}

			}
		})		
	}

})

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../serverIndex/serverIndex',
        icon: '../img/apply.png',
        text: "招聘头疼",

      },
      right: {
        url: '../serverMine/serverMine',
        icon: '../img/mine.png',
        text: "我的",

      },
    },
     isShowApply:false,
     isShowConfirm:false
  },
  apply:function(){

    this.setData({
      isShowApply:true,
    })

  },
  applyPost:function(){
    this.setData({
      isShowApply: false,
      isShowConfirm: true
    })
  },
  applyDelete:function(){
    this.setData({
      isShowApply: false,
      isShowConfirm: false,
    })
  },
  applyConfirm:function(){
    this.setData({
  
      isShowConfirm: false,
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
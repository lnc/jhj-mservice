// pages/resume/serverLogin/serverLogin.js
const app = getApp()
var common = require("../../../utils/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
     isShowError:false,
     codeMessage:"获取验证码",
     phone:'',
     code:'',
     errorMessage:"",
     disabled:true,
  },
  /**
   * 获取手机号
   */
  getPhone:function(e){
     this.setData({
       phone: e.detail.value
     })
  },
  /**
 * 获取输入的验证码
 */
  getInputCode: function (e) {
    this.setData({
      code: e.detail.value
    })
  },
  /**
   * 获取验证码
   */
  getCode:function(){
    var that =this;
    if (that.data.disabled){
      that.countDown();
    }
    var mobile = that.data.phone;
    console.log(mobile)
    wx.request({
      url: app.globalData.baseUrl +'/api/user/getCode.json',
      data: {
        mobile: mobile,
        sms_type:0
      },
      method: 'GET',
      success: function(res) {

      },
    })
  },
  /**
   * 倒计时
   */
  countDown:function(){
    var that=this;
    var flag = common.validatePhone(this.data.phone);
    if (flag){
      that.changeData(false, "");
      var second = 60;
      var t=setInterval(function () {
        second--;
        if(second<0){
          clearInterval(t);
          that.setData({
            codeMessage: "获取验证码",
            disabled: true,
          })
        }else{
          that.setData({
            codeMessage: second + "秒",
            disabled:false,
          })
        }
      },
        1000);

    }else{
      that.changeData(true,"手机号不正确或未填写");
    }
  },

/**
 * 改变数据  
 */
  changeData: function (isShowError, errorMessage){
    this.setData({
      isShowError: isShowError,
      errorMessage: errorMessage,

    })
  },
/**
 * 点击确定
 */
  confirm:function(){
    var that=this;
    that.changeData(false, "");
  },


  /**
   * 登录之前验证
   */
    validateData:function(){
      var that=this;
      var phoneFlag = common.validatePhone(this.data.phone);
      var codeFlag = common.isEmpty(this.data.code);
      if ((!phoneFlag) || codeFlag){
        that.changeData(true, "正确填写手机号或验证码");
      }

    },
    /**
     * 登录
    */
    login:function(){
      var that = this;
      that.validateData();

      var mobile = that.data.phone;
      var code = taht.data.code;


      wx.request({
        url: app.globalData.baseUrl+'',
        data:{
          
        },
        header: {},
        method: GET,
        dataType: json,
        responseType: text,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    },

 
 

 
})
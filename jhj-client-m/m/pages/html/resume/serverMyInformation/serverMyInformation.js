// pages/serverMyInformation/serverMyInformation.js
//获取应用实例
const app = getApp()
var common=require("../../../utils/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../serverIndex/serverIndex',
        icon: '../../img/apply.png',
        text: "招聘头条",

      },
      right: {
        url: '../serverMine/serverMine',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
    genderItems: [
      { name: '男', value: '0' ,checked: 'true'},
      { name: '女', value: '1', 
  
  },],
    Skills: [
      { name: '有', value: '0' },
      {
        name: '无', value: '1', 

      },],
  
    serverItems: [
      { name: '保姆', value: '0' },
      { name: '育儿嫂', value: '1', },
      { name: '月嫂', value: '2' },
      { name: '催乳师', value: '3' },
      { name: '小儿推拿', value: '4' },
      { name: '深度养护', value: '5' },
      { name: '家电清洗', value: '6' },

    ],
    jobYear: [
      { name: '新手1', value: '0' },
      { name: '1-2年', value: '1',  },
      { name: '3-5年', value: '2' },
      { name: '5年以上', value: '3' },


    ],
    serverArea: [
      { name: '朝阳区11', value: '0' },
      { name: '海淀区', value: '1',  },
      { name: '丰台区', value: '2' },
      { name: '大兴区', value: '3' },
      { name: '昌平区', value: '4' },
      { name: '通州区', value: '5' },
      { name: '房山区', value: '6' },
      { name: '石景山区', value: '7' },
      { name: '顺义区', value: '8' },


    ],

    region: ['广东省', '广州市', '海珠区'],


    name:'',//姓名
    nativePlace:'',//籍贯
    idCard:'',//身份证号
    address:'',//常住地址
    date:'',//日期
    certificate:'',//证书
    phone:'',//电话号码
  


  },

/**
 * 选择籍贯
 */
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },
  gender: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.genderItems.length; i++) {
      if (checked == this.data.genderItems[i].name) {
        changed['genderItems[' + i + '].checked'] = true
      } else {
        changed['genderItems[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  serverItem: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.serverItems.length; i++) {
      if (checked.indexOf(this.data.serverItems[i].name) !== -1) {
        changed['serverItems[' + i + '].checked'] = true
      } else {
        changed['serverItems[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  jobYear: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.jobYear.length; i++) {
      if (checked == this.data.jobYear[i].name) {
        changed['jobYear[' + i + '].checked'] = true
      } else {
        changed['jobYear[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  serverArea: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.serverArea.length; i++) {
      if (checked.indexOf(this.data.serverArea[i].name) !== -1) {
        changed['serverArea[' + i + '].checked'] = true
      } else {
        changed['serverArea[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  Skills: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.Skills.length; i++) {
      if (checked == this.data.Skills[i].name) {
        changed['Skills[' + i + '].checked'] = true
      } else {
        changed['Skills[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
    if ((this.data.Skills[0].checked)==true){
  
      this.changeData('spe', true);
    }else{
  
      this.changeData('spe', false);
    }
  },


//调用后台接口公用方法 data_type:向后台传的参数
  getDatas: function (data_type,callback){
    wx.request({
      url: app.globalData.baseUrl + "/api/resume/get_dict.json",
      data: data_type,
      method: 'GET',
      dataType: 'json',
      success: callback
  })
},
//获取数据
getDatasCallBack:function(res){
  var that = this;

  var list = res.data.data;
  if(list.length>0){
    var dictType=list[0].dictType;
    //获取服务区域
    if (dictType == "region") {
      that.setData({
        serverArea: list
      })
    }
   // 获取服务工种
    if (dictType=="service_type"){
          that.setData({
            serverItems: list
          })
    }
    //获取工作经验
    if (dictType == "work_year"){
      that.setData({
        jobYear: list
      })
    }


  }
},



  /**
  * 改变数据的公用方法  dataKey要改变的数据，dataMessage是改变的值
 */
  changeData(dataKey, dataMessage) {
    var obj = {};
    obj[dataKey] = dataMessage
    this.setData(obj);
  }, 
  
/**
 * 鼠标移开改变name值，并验证
 */
getName:function(e){
  var that=this;
  that.changeData('name', e.detail.value);
  that.validateName();
},
/**
 * 校验姓名
*/
validateName: function () {
  var that = this;
  var flag = common.isEmpty(that.data.name);
  if (flag) {
    that.changeData('nameMessage', "姓名不能为空");

  } else {
    that.changeData('nameMessage', "");

  }
},
/**
 * 鼠标移开改变手机号值，并验证
 */
getMobile: function (e) {
  var that = this;
  that.changeData('phone', e.detail.value);
  that.validatePhone();
},
/**
 * 验证手机号
*/
validatePhone:function(){
  var that = this;
  var flag = common.validatePhone(that.data.phone);
  if (flag) {
    that.changeData('phoneMessage', "");


  } else {
    that.changeData('phoneMessage', "手机号未填写或无效");

  }
},
// /**
//  * 鼠标移开改变籍贯值，并验证
//  */
//   getNativePlace: function (e) {
//   var that = this;
//   that.changeData('nativePlace', e.detail.value);
//   that.validateNativePlace();
// },
/**
 * 校验籍贯
*/
// validateNativePlace:function(){
//     var that=this;
//     var flag = common.isEmpty(that.data.nativePlace);
//     if (flag) {
//       that.changeData('NativePlace', "籍贯不能为空");

//     } else {
//       that.changeData('NativePlace', "");

//     }

// },

/**
 * 鼠标移开改变身份证号，并验证
 */
  getIdCard: function (e) {
  var that = this;
  that.changeData('idCard', e.detail.value);
  that.validateIdCard();
},
/**
 * 校验身份证号
*/
validateIdCard:function(){
  var that = this;
  var flag = common.validateIdCard(that.data.idCard);
  if (!flag) {
    that.changeData('idCard', "身份证号不合法或者未填写");

  } else {
    that.changeData('idCard', "");
  }
},
/**
 * 鼠标移开改变地址，并验证
 */
  getAddress: function (e) {
  var that = this;
  that.changeData('address', e.detail.value);
  that.validateAddress();
},
/**
 * 校验地址
*/
validateAddress:function(){
  var that = this;
  var flag = common.isEmpty(that.data.address);
  if (flag) {
    that.changeData('address', "常住地址不能为空");

  } else {
    that.changeData('address', "");
  }

  
},
/**
 * 校验日期
*/
  validateDate:function(){
    var that = this;
    var flag = common.isEmpty(that.data.date);
    if(flag){
      that.changeData('dateMessage', "服务开始时间不能为空");
    }else{
      that.changeData('dateMessage', "");
    }
  },
  /**
   * 鼠标移开改变证书名称，并验证
   */
  getCertificate: function (e) {
    var that = this;
    that.changeData('certificate', e.detail.value);
    that.validateCertificate();
  },
/**
 * 校验证书名称
*/
  validateCertificate:function(){
      var that=this;
      var flag = common.isEmpty(that.data.certificate);
      if (flag) {
        that.changeData('certMessage', "证书名称不能为空");
      } else {
        that.changeData('certMessage', "");
      }
  },
/**
 * 验证性别 必选
*/
  validateGender:function(){
    var that=this;
    var index=0;
    for (var i = 0; i < that.data.genderItems.length;i++){
      if (that.data.genderItems[i].checked){
          index++;
      }
     }
     if(index>0){
       that.changeData('renderMessage', "");
     }else{
       that.changeData('renderMessage', "性别必选");
     }
  },
  /**
   * 验证服务工种 至少选一个
  */
  validateServerItem:function(){
    var that = this; 
    var index = 0;
    for (var i = 0; i < that.data.serverItems.length; i++) {
      if (that.data.serverItems[i].checked) {
        index++;
      }
    }
    if (index > 0) {
      that.changeData('serverItemMessage', "");
    } else {
      that.changeData('serverItemMessage', "服务工种至少选一个");
    }
  },

  /**
   * 工作经验 必选
  */
  validateJobYear:function(){
    var that = this;
    var index = 0;
    for (var i = 0; i < that.data.jobYear.length; i++) {
      if (that.data.jobYear[i].checked) {
        index++;
      }
    }
    if (index > 0) {
      that.changeData('jobYearMessage', "");
    } else {
      that.changeData('jobYearMessage', "工作经验必选");
    }
  },
  
  /**
   * 服务区域至少选一个
  */
  validateServerArea:function(){
    var that = this;
    var index = 0;
    for (var i = 0; i < that.data.serverArea.length; i++) {
      if (that.data.serverArea[i].checked) {
        index++;
      }
    }
    if (index > 0) {
      that.changeData('serverAreaMessage', "");
    } else {
      that.changeData('serverAreaMessage', "服务区域至少选一个");
    }
  },
  /**
 * 技能证书必勾选
*/
  validateSkills: function () {
    var that = this;
    var index = 0;
    for (var i = 0; i < that.data.Skills.length; i++) {
      if (that.data.Skills[i].checked) {
        index++;
      }
    }
    if (index > 0) {
      that.changeData('SkillsMessage', "");
    } else {
      that.changeData('SkillsMessage', "技能证书必选");
    }
  },
/**
 * 日期控件
*/
bindDateChange: function (e) {
  // console.log('picker发送选择改变，携带值为', e.detail.value)
  this.setData({
    date: e.detail.value
  })
},


/**
 * 上传头像
 */
uploadHead:function(){
  var that=this;
  wx.chooseImage({
    count: 1, // 默认9
    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
    success: function (res) {
      // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
      var headPaths = res.tempFilePaths;
      that.changeData("headPaths", headPaths);
      that.validateUploadHead(that.data.headPaths);
    }
  })
},

//验证上传图像格式  一张的
  validateUploadHead(headPaths){
      var that = this;
      if (headPaths == undefined || headPaths == "" || headPaths.length == 0 || headPaths == null){
        that.changeData("imgMessage", "请上传图片");
      }else{
        var isImg = common.validateImgformat(headPaths[0]);
        if (isImg) {
          that.changeData("imgMessage", "");
        } else {
          that.changeData("imgMessage", "请上传正确格式图片");
        }
      }
   

},
//上传生活照
  uploadLifeImg:function(){
    var that = this;
    wx.chooseImage({
      count: 5, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var lifePaths = res.tempFilePaths;
        that.changeData("lifePaths", lifePaths);
        that.validateUploadImg(that.data.lifePaths,"lifeImgMessage");
      }
    })
  },
  //上传证书图片
  certificateUploadImg:function(){
    var that = this;
    wx.chooseImage({
      count: 5, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var certificatePaths = res.tempFilePaths;
        that.changeData("certificatePaths", certificatePaths);
        that.validateUploadImg(that.data.certificatePaths, "certificateImgMessage");
      }
    })
  },

//身份证正面上传
  positiveUpload:function(){
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var  positivePaths = res.tempFilePaths;
        that.changeData("positivePaths", positivePaths);
        that.validateUploadImg(that.data.positivePaths, "positiveImgMessage");
      }
    })
  },
  //身份证反面上传
  negativeUpload: function () {
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var negativePaths = res.tempFilePaths;
        that.changeData("negativePaths", negativePaths);
        that.validateUploadImg(that.data.negativePaths, "negativeImgMessage");
      }
    })
  },
  //验证上传图像格式  多张的
  validateUploadImg(imgPaths,errMessageName) {
    var that = this;
    if (imgPaths == undefined || imgPaths == "" || imgPaths.length == 0 || imgPaths == null){
      that.changeData(errMessageName, "请上传图片");
    }else{
      for (var i = 0; i < imgPaths.length; i++) {
        var isImg = common.validateImgformat(imgPaths[i]);
        if (isImg) {
          that.changeData(errMessageName, "");
        } else {
          that.changeData(errMessageName, "请上传正确格式图片");
          return;
        }
      }
    }
  },
  //视频上传
  uploadVideo:function(){
    var that = this
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: 'back',
      success: function (res) {
       
           var videoPath=res.tempFilePath;
           var videSize=res.size;
         
           that.changeData("videoPath", videoPath);
           that.changeData("videSize", videSize);
           that.vidateVideo(that.data.videoPath, that.data.videSize);
      }
    })
  },
  //验证视频上传
  vidateVideo:function(value,size){
    var that=this;
   var isVideo= common.validateVideoformat(value);
   if(isVideo){
     that.changeData("videMessage", "");
     if (size <= (50 * 1024 * 2014)){
       that.changeData("videMessage", "");
     }else{
       that.changeData("videMessage", "规定视频大小50M以内");
     }
   }else{
     that.changeData("videMessage", "视频文件格式不正确或未上传");
   }
  },

  /**
 * 保存信息 验证数据
*/
  savePost: function () {
    var that = this;
    that.validateName();
    that.validateNativePlace();
    that.validateIdCard();
    that.validateAddress();
    that.validateDate();
    that.validateCertificate();
    that.validateGender();
    that.validateServerItem();
    that.validateJobYear();
    that.validateServerArea();
    that.validateSkills();

    that.validateGender();
    that.validatePhone();
    
    //验证图片上传
    that.validateUploadHead(that.data.headPaths);
    that.validateUploadImg(that.data.lifePaths, "lifeImgMessage");
    that.validateUploadImg(that.data.certificatePaths, "certificateImgMessage");
    that.validateUploadImg(that.data.positivePaths, "positiveImgMessage");
    that.validateUploadImg(that.data.negativePaths, "negativeImgMessage");
    //验证视频上传
    that.vidateVideo(that.data.videoPath, that.data.videSize);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;

    that.getDatas({ dictType: 'work_year'}, that.getDatasCallBack);
    that.getDatas({ dictType: 'service_type' }, that.getDatasCallBack);
    that.getDatas({ dictType: 'region', pid:4780}, that.getDatasCallBack);
   


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
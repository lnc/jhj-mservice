var sign = require('../../../utils/bin/sign.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../index/index',
        icon: '../../img/aunt.png',
        text: "阿姨推荐",

      },
      right: {
        url: '../userMine/userMine',
        icon: '../../img/mine.png',
        text: "我的",

      },
    },
    isShow:'hide',
    subject:{},
    liveImg:[],
    liveVideo:{},
    sale:{}
  },

  returnPage:function(){
    this.setData({
      isShow: 'hide',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getDetail(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享或推荐给好友按钮
   */
  onShareAppMessage: function (options) {
    var pages = getCurrentPages();
    var url = pages[pages.length-1].route;
    console.log(options)
    return {
      title: app.globalData.shareTitle,
      path: url,
      success: function (res) {
        wx.showToast({
          title: '转发成功',
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
        })
      }
    }
  },
  /**
   * 服务人员详情
   * 
   */
  getDetail:function(options){
    var resumeNo = options.resumeNo;
    var mobile = options.mobile;
    var that = this;   
   
    var params = {
      resumeNo: resumeNo,
      mobile: mobile
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl +'/api/resume/detail.json',
      data: params,
      success:function(res){
        var result = res.data.data;

        var arry = [];
        var obj = {};
        var list = result.resumeResource;
        if(list!=null && list.length>0){
          for(var i=0;i<list.length;i++){
            var img = list[i];
            if (img.resourceType =='resume_live_img'){
              arry.push(img);
            }
            if (img.resourceType == 'resume_video') {
              obj = img;
            }
          }
        }
        that.setData({
          subject:result,
          liveImg: arry,
          liveVideo: obj
        })
      }
    })
  },
  /**
   * 预约服务人员
   * 
   */
  order:function(){
    var that = this;
    var userId = wx.getStorageSync('userId');
    var mobile = wx.getStorageSync('mobile');
    if(userId==null || userId==''){
      wx.navigateTo({
        url: '../login/login',
        success: function(res) {},
      })
      return false;
    }

    var userName = "";
    var lat = "";
    var lng = "";

    var params = {
      userId: userId,
      mobile: mobile,
      userName: userName,
      orderStatus: 1,
      lat: lat,
      lng: lng,
      addFrom: 0,
      actionId: userId,
      actionName: userName
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl + '/api/bm/order/add.json',
      method:'POST',
      header:{
        "Content-Type":"application/x-www-form-urlencoded"
      },
      data: params,
      success: function (res) {
        var result = res.data;
        if (result.status==0){
          that.setData({
              isShow: 'show',
              sale: result.data
          })
        }else{
          wx.showToast({
            title: '预约失败',
          })
        }
      }
    })
  }
})
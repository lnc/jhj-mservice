var sign = require('../../../utils/bin/sign.js')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../index/index',
        icon: '../../img/aunt.png',
        text: "阿姨推荐",

      },
      right: {
        url: 'userMine',
        icon: '../../img/mine.png',
        text: "我的",
      },
    },

    user:{}
  },
  about:function(){
    wx.navigateTo({
      url: 'https://www.jia-he-jia.com/u/#!/user/aboutus.html',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUser();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var pages = getCurrentPages();
    var url = pages[pages.length - 1].route;
    console.log(options)
    return {
      title: app.globalData.shareTitle,
      path: url,
      success: function (res) {
        wx.showToast({
          title: '转发成功',
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '转发失败',
        })
      }
    }
  },
  /**
   * 联系客服
   * 
   */
  callPhone:function(){
    wx.makePhoneCall({
      phoneNumber: '010-53511197',
    })
  },
  /**
   * 获取用户信息
   * 
   */
  getUser:function(){
    var that = this;
    var userId = wx.getStorageSync("userId");

    if (userId == "" || userId==null){
      wx.navigateTo({
        url: '../login/login',
      })
      return false;
    }

    var params = {
      userId: userId
    }
    params = sign.getSignParams(params)
    wx.request({
      url: app.globalData.baseUrl +"/api/user/userinfo",
      data: {
        userId: userId
      },
      success: function(res) {
        var result = res.data.data;
        that.setData({
          user:result
        })
      },
    })
  },
  /**
   * 
   * 退出登录
   */
  logout:function(){
    wx.removeStorageSync('mobile');
    wx.removeStorageSync('userId');
    wx.removeStorageSync('token');
    wx.navigateTo({
      url: '../login/login',
    })
  },
  /**
   * 跳转登录
   *  
   */
  goLogin:function(){
    wx.navigateTo({
      url: '../login/login',
    })
    return false;
  },

  /**
   * 地址跳转
   * 
   */
  linkToAddr:function(){
    var that =this;
    var userId = wx.getStorageSync("userId");
    if (userId == null || userId==''){
      that.getLogin();
    }
    wx.navigateTo({
      url: '../userAddress/userAddress',
    })
  },
  /**
   * 优惠券连接
   */
  linkCoupons:function(){
    var userId = wx.getStorageSync("userId");
    if (userId == null || userId == '') {
      that.getLogin();
    }
    wx.navigateTo({
      url: '../coupons/coupons',
    })
  }
  
})
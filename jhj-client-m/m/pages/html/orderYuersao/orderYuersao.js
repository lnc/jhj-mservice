
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabBars: {
      left: {
        url: '../index/index',
        icon: '../img/aunt.png',
        text: "阿姨推荐",

      },
      right: {
        url: '../userMine/userMine',
        icon: '../img/mine.png',
        text: "我的",

      },
    },
    radioItems: [
      { name: '0-1岁', value: '0' },
      { name: '1-2岁', value: '1', checked: 'true' },
      { name: '2-3岁', value: '2' },
      { name: '3岁以上', value: '3' }
    ],
    radioItemsHouse: [
      { name: '住家', value: '1' },
      { name: '不住家', value: '0', checked: 'true' }
    
    ],
  },


  radioChange: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.radioItems.length; i++) {
      if (checked.indexOf(this.data.radioItems[i].name) !== -1) {
        changed['radioItems[' + i + '].checked'] = true
      } else {
        changed['radioItems[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  radioChangeHouse: function (e) {
    var checked = e.detail.value
    var changed = {}
    for (var i = 0; i < this.data.radioItemsHouse.length; i++) {
      if (checked==this.data.radioItemsHouse[i].name) {
        changed['radioItemsHouse[' + i + '].checked'] = true
      } else {
        changed['radioItemsHouse[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
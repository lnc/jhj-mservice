// 检测是否为空，为空 = true 不为空 = false
function isEmpty(str) {
  if (str == undefined || str == "" || str.length == 0 || str == null) {
    return true;
  }
//  if (str.replace(/(^\s*)|(\s*$)/g, "") == "")
//    return true;
  return false;
}

// // 获取网页参数方法
// function getUrlParam(name) {http://dhsjg.com/ddddd?param1=2&param3=3&fdskjh=
//   var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
//   var r = window.location.search.substr(1).match(reg); // 匹配目标参数
//   if (r != null)
//     return unescape(r[2]);

//     var obj = param:ss
//   return null; // 返回参数值
// }

//检验手机号是否有效  有效=true,无效=false
function validatePhone(phone) {
  if (!isEmpty(phone)) {
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
    if (myreg.test(phone)) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

//验证是否为数字  为数字=true,不为数字=false
function isNumber(value) {
  var patrn = /^[0-9]*$/;
  if (patrn.exec(value) == null || value == "") {
    return false
  } else {
    return true
  }
}

//验证身份证号  合法为true,不合法为false
function validateIdCard(idCard) {
  var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  if (reg.test(idCard)) {
    return true;
  }
  return false;
}
// 验证上传图片格式 图片格式为true,则为false
function validateImgformat(value) {
  if (!isEmpty(value)) {
    var myreg = /\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/;
    if (myreg.test(value)) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}
//验证视频上传格式 视频文件为true,则为false
function validateVideoformat(value) {
  if (!isEmpty(value)) {
    var myreg = /\.(rmvb|avi|mp4|ts)$/;
    if (myreg.test(value)) {
      return true;
    } else {
      return false;
    }
  }
  return false;

}


function base64Encoder (str) { // 编码，配合encodeURIComponent使用
    var c1, c2, c3;
    var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var i = 0, len = str.length, strin = '';
    while (i < len) {
        c1 = str.charCodeAt(i++) & 0xff;
        if (i == len) {
            strin += base64EncodeChars.charAt(c1 >> 2);
            strin += base64EncodeChars.charAt((c1 & 0x3) << 4);
            strin += "==";
            break;
        }
        c2 = str.charCodeAt(i++);
        if (i == len) {
            strin += base64EncodeChars.charAt(c1 >> 2);
            strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            strin += base64EncodeChars.charAt((c2 & 0xF) << 2);
            strin += "=";
            break;
        }
        c3 = str.charCodeAt(i++);
        strin += base64EncodeChars.charAt(c1 >> 2);
        strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        strin += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
        strin += base64EncodeChars.charAt(c3 & 0x3F)
    }
    return strin
}

function base64Decoder (input) { // 解码，配合decodeURIComponent使用
    var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (i < input.length) {
        enc1 = base64EncodeChars.indexOf(input.charAt(i++));
        enc2 = base64EncodeChars.indexOf(input.charAt(i++));
        enc3 = base64EncodeChars.indexOf(input.charAt(i++));
        enc4 = base64EncodeChars.indexOf(input.charAt(i++));
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);
        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }
    }
    return utf8Decode(output);
}

function utf8Decode (utftext) { // utf-8解码
    var string = '';
    let i = 0;
    let c = 0;
    let c1 = 0;
    let c2 = 0;
    while (i < utftext.length) {
        c = utftext.charCodeAt(i);
        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        } else if ((c > 191) && (c < 224)) {
            c1 = utftext.charCodeAt(i + 1);
            string += String.fromCharCode(((c & 31) << 6) | (c1 & 63));
            i += 2;
        } else {
            c1 = utftext.charCodeAt(i + 1);
            c2 = utftext.charCodeAt(i + 2);
            string += String.fromCharCode(((c & 15) << 12) | ((c1 & 63) << 6) | (c2 & 63));
            i += 3;
        }
    }
    return string;
}


module.exports = {
  isEmpty: isEmpty,
  validatePhone: validatePhone,
  isNumber: isNumber,
  validateIdCard: validateIdCard,
  validateImgformat:validateImgformat,
  validateVideoformat:validateVideoformat,
  base64Encoder : base64Encoder,
  base64Decoder : base64Decoder
}
package com.jhj.provider.user.po.b.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "users")
public class Users {
	/**
	 * 主键
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 手机号归属地
	 */
	@Column(name = "province_name")
	private String provinceName;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 第三方登陆类型
	 */
	@Column(name = "3rd_type")
	private String thirdType;

	/**
	 * 第三方登陆openid
	 */
	private String openid;

	/**
	 * 用户姓名
	 */
	private String name;

	/**
	 * 性别 0 = 先生 1 = 女士
	 */
	private String sex;

	/**
	 * 头像
	 */
	@Column(name = "head_img")
	private String headImg;

	/**
	 * 用户余额
	 */
	@Column(name = "rest_money")
	private BigDecimal restMoney;

	/**
	 * 用户积分
	 */
	private Integer score;

	/**
	 * 用户类型 0 = 普通用户 1= 代理商
	 */
	@Column(name = "user_type")
	private Integer userType;

	/**
	 * 是否会员 0 = 否 1 = 是
	 */
	@Column(name = "is_vip")
	private Integer isVip;

	private String remarks;

	/**
	 * 用户来源 0 = APP 1 = 微网站 2 = 管理后台
	 */
	@Column(name = "add_from")
	private Integer addFrom;

	/**
	 * 添加时间戳
	 */
	@Column(name = "add_time")
	private Integer addTime;

	/**
	 * 更新时间戳
	 */
	@Column(name = "update_time")
	private Integer updateTime;

	/**
	 * 获取主键
	 *
	 * @return id - 主键
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置主键
	 *
	 * @param id
	 *            主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取手机号归属地
	 *
	 * @return province_name - 手机号归属地
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * 设置手机号归属地
	 *
	 * @param provinceName
	 *            手机号归属地
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
	 * 获取手机号
	 *
	 * @return mobile - 手机号
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * 设置手机号
	 *
	 * @param mobile
	 *            手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 获取第三方登陆类型
	 *
	 * @return 3rd_type - 第三方登陆类型
	 */
	public String getThirdType() {
		return thirdType;
	}

	/**
	 * 设置第三方登陆类型
	 *
	 * @param thirdType
	 *            第三方登陆类型
	 */
	public void setThirdType(String thirdType) {
		this.thirdType = thirdType;
	}

	/**
	 * 获取第三方登陆openid
	 *
	 * @return openid - 第三方登陆openid
	 */
	public String getOpenid() {
		return openid;
	}

	/**
	 * 设置第三方登陆openid
	 *
	 * @param openid
	 *            第三方登陆openid
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * 获取用户姓名
	 *
	 * @return name - 用户姓名
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置用户姓名
	 *
	 * @param name
	 *            用户姓名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取性别 0 = 先生 1 = 女士
	 *
	 * @return sex - 性别 0 = 先生 1 = 女士
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * 设置性别 0 = 先生 1 = 女士
	 *
	 * @param sex
	 *            性别 0 = 先生 1 = 女士
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * 获取头像
	 *
	 * @return head_img - 头像
	 */
	public String getHeadImg() {
		return headImg;
	}

	/**
	 * 设置头像
	 *
	 * @param headImg
	 *            头像
	 */
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	/**
	 * 获取用户余额
	 *
	 * @return rest_money - 用户余额
	 */
	public BigDecimal getRestMoney() {
		return restMoney;
	}

	/**
	 * 设置用户余额
	 *
	 * @param restMoney
	 *            用户余额
	 */
	public void setRestMoney(BigDecimal restMoney) {
		this.restMoney = restMoney;
	}

	/**
	 * 获取用户积分
	 *
	 * @return score - 用户积分
	 */
	public Integer getScore() {
		return score;
	}

	/**
	 * 设置用户积分
	 *
	 * @param score
	 *            用户积分
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * 获取用户类型 0 = 普通用户 1= 代理商
	 *
	 * @return user_type - 用户类型 0 = 普通用户 1= 代理商
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * 设置用户类型 0 = 普通用户 1= 代理商
	 *
	 * @param userType
	 *            用户类型 0 = 普通用户 1= 代理商
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * 获取是否会员 0 = 否 1 = 是
	 *
	 * @return is_vip - 是否会员 0 = 否 1 = 是
	 */
	public Integer getIsVip() {
		return isVip;
	}

	/**
	 * 设置是否会员 0 = 否 1 = 是
	 *
	 * @param isVip
	 *            是否会员 0 = 否 1 = 是
	 */
	public void setIsVip(Integer isVip) {
		this.isVip = isVip;
	}

	/**
	 * @return remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * 获取用户来源 0 = APP 1 = 微网站 2 = 管理后台
	 *
	 * @return add_from - 用户来源 0 = APP 1 = 微网站 2 = 管理后台
	 */
	public Integer getAddFrom() {
		return addFrom;
	}

	/**
	 * 设置用户来源 0 = APP 1 = 微网站 2 = 管理后台
	 *
	 * @param addFrom
	 *            用户来源 0 = APP 1 = 微网站 2 = 管理后台
	 */
	public void setAddFrom(Integer addFrom) {
		this.addFrom = addFrom;
	}

	/**
	 * 获取添加时间戳
	 *
	 * @return add_time - 添加时间戳
	 */
	public Integer getAddTime() {
		return addTime;
	}

	/**
	 * 设置添加时间戳
	 *
	 * @param addTime
	 *            添加时间戳
	 */
	public void setAddTime(Integer addTime) {
		this.addTime = addTime;
	}

	/**
	 * 获取更新时间戳
	 *
	 * @return update_time - 更新时间戳
	 */
	public Integer getUpdateTime() {
		return updateTime;
	}

	/**
	 * 设置更新时间戳
	 *
	 * @param updateTime
	 *            更新时间戳
	 */
	public void setUpdateTime(Integer updateTime) {
		this.updateTime = updateTime;
	}
}
package com.jhj.provider.user.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.user.po.b.model.UserOpenid;
import com.jhj.provider.user.service.UserOpenidService;

@Service
public class UserOpenidServiceImpl extends BaseServiceImpl<UserOpenid> implements UserOpenidService {

}

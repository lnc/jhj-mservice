package com.jhj.provider.user.po.b.model;

import javax.persistence.*;

@Table(name = "user_sms_token")
public class UserSmsToken {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 0 = 验证码 1 = 支付
     */
    @Column(name = "sms_type")
    private Integer smsType;

    /**
     * 验证码
     */
    @Column(name = "sms_token")
    private String smsToken;

    /**
     * 0 = 发送失败 1 = 发送成功
     */
    @Column(name = "is_suceess")
    private Integer isSuceess;

    /**
     * 短信平台返回信息.
     */
    @Column(name = "sms_return")
    private String smsReturn;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Integer updateTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取0 = 验证码 1 = 支付
     *
     * @return sms_type - 0 = 验证码 1 = 支付
     */
    public Integer getSmsType() {
        return smsType;
    }

    /**
     * 设置0 = 验证码 1 = 支付
     *
     * @param smsType 0 = 验证码 1 = 支付
     */
    public void setSmsType(Integer smsType) {
        this.smsType = smsType;
    }

    /**
     * 获取验证码
     *
     * @return sms_token - 验证码
     */
    public String getSmsToken() {
        return smsToken;
    }

    /**
     * 设置验证码
     *
     * @param smsToken 验证码
     */
    public void setSmsToken(String smsToken) {
        this.smsToken = smsToken;
    }

    /**
     * 获取0 = 发送失败 1 = 发送成功
     *
     * @return is_suceess - 0 = 发送失败 1 = 发送成功
     */
    public Integer getIsSuceess() {
        return isSuceess;
    }

    /**
     * 设置0 = 发送失败 1 = 发送成功
     *
     * @param isSuceess 0 = 发送失败 1 = 发送成功
     */
    public void setIsSuceess(Integer isSuceess) {
        this.isSuceess = isSuceess;
    }

    /**
     * 获取短信平台返回信息.
     *
     * @return sms_return - 短信平台返回信息.
     */
    public String getSmsReturn() {
        return smsReturn;
    }

    /**
     * 设置短信平台返回信息.
     *
     * @param smsReturn 短信平台返回信息.
     */
    public void setSmsReturn(String smsReturn) {
        this.smsReturn = smsReturn;
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Integer getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }
}
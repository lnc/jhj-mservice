package com.jhj.provider.user.po.b.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "dict_card_type")
public class DictCardType {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 充值金额
     */
    @Column(name = "card_value")
    private BigDecimal cardValue;

    /**
     * 支付金额
     */
    @Column(name = "card_pay")
    private BigDecimal cardPay;

    @Column(name = "send_money")
    private BigDecimal sendMoney;

    /**
     * 充值赠送的礼包ID
     */
    @Column(name = "gift_id")
    private Integer giftId;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取充值金额
     *
     * @return card_value - 充值金额
     */
    public BigDecimal getCardValue() {
        return cardValue;
    }

    /**
     * 设置充值金额
     *
     * @param cardValue 充值金额
     */
    public void setCardValue(BigDecimal cardValue) {
        this.cardValue = cardValue;
    }

    /**
     * 获取支付金额
     *
     * @return card_pay - 支付金额
     */
    public BigDecimal getCardPay() {
        return cardPay;
    }

    /**
     * 设置支付金额
     *
     * @param cardPay 支付金额
     */
    public void setCardPay(BigDecimal cardPay) {
        this.cardPay = cardPay;
    }

    /**
     * @return send_money
     */
    public BigDecimal getSendMoney() {
        return sendMoney;
    }

    /**
     * @param sendMoney
     */
    public void setSendMoney(BigDecimal sendMoney) {
        this.sendMoney = sendMoney;
    }

    /**
     * 获取充值赠送的礼包ID
     *
     * @return gift_id - 充值赠送的礼包ID
     */
    public Integer getGiftId() {
        return giftId;
    }

    /**
     * 设置充值赠送的礼包ID
     *
     * @param giftId 充值赠送的礼包ID
     */
    public void setGiftId(Integer giftId) {
        this.giftId = giftId;
    }

    /**
     * 获取描述信息
     *
     * @return description - 描述信息
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述信息
     *
     * @param description 描述信息
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
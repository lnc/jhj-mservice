package com.jhj.provider.user.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.user.po.b.model.DictCardType;
import com.jhj.provider.user.service.DictCardTypeService;

@Service
public class DictCardTypeServiceImpl extends BaseServiceImpl<DictCardType> implements DictCardTypeService{

}

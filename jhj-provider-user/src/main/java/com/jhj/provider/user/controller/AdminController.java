package com.jhj.provider.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.po.b.model.AdminAccount;
import com.jhj.provider.user.service.AdminAccountService;
import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;

/**
 *
 */
@RestController
public class AdminController {
	
	@Autowired
	AdminAccountService adminAccountService;
	
	/**
	 * 获取账户信息
	 * @param adminId
	 * 
	 * @return adminName
	 * */
	@RequestMapping(value = "/api/user/get_adminname", method = RequestMethod.GET)
	public AppResultData<Object> userInfo(
			
			@RequestParam("adminId") Integer adminId) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		String adminName = "";
		AdminAccount searchVo = new AdminAccount();
		searchVo.setId(adminId);
		AdminAccount adminAccount = adminAccountService.selectOne(searchVo);
		if (adminAccount != null) adminName = adminAccount.getName();
		result.setData(adminName);
		return result;
	}    
}
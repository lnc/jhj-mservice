package com.jhj.provider.user.po.b.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "user_coupons")
public class UserCoupons {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 优惠券id
     */
    @Column(name = "coupon_id")
    private Integer couponId;

    /**
     * 优惠劵金额
     */
    private BigDecimal value;

    /**
     * 赠送礼包id
     */
    @Column(name = "gift_id")
    private Integer giftId;

    /**
     * 服务类型id
     */
    @Column(name = "service_type")
    private String serviceType;

    /**
     * 开始日期
     */
    @Column(name = "from_date")
    private Date fromDate;

    /**
     * 结束日期
     */
    @Column(name = "to_date")
    private Date toDate;

    /**
     * 是否使用 0 = 未使用 1= 已使用
     */
    @Column(name = "is_used")
    private Integer isUsed;

    /**
     * 使用时间戳
     */
    @Column(name = "used_time")
    private Integer usedTime;

    /**
     * 对应使用的订单号 
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取优惠券id
     *
     * @return coupon_id - 优惠券id
     */
    public Integer getCouponId() {
        return couponId;
    }

    /**
     * 设置优惠券id
     *
     * @param couponId 优惠券id
     */
    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    /**
     * 获取优惠劵金额
     *
     * @return value - 优惠劵金额
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * 设置优惠劵金额
     *
     * @param value 优惠劵金额
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * 获取赠送礼包id
     *
     * @return gift_id - 赠送礼包id
     */
    public Integer getGiftId() {
        return giftId;
    }

    /**
     * 设置赠送礼包id
     *
     * @param giftId 赠送礼包id
     */
    public void setGiftId(Integer giftId) {
        this.giftId = giftId;
    }

    /**
     * 获取服务类型id
     *
     * @return service_type - 服务类型id
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * 设置服务类型id
     *
     * @param serviceType 服务类型id
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * 获取开始日期
     *
     * @return from_date - 开始日期
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * 设置开始日期
     *
     * @param fromDate 开始日期
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * 获取结束日期
     *
     * @return to_date - 结束日期
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * 设置结束日期
     *
     * @param toDate 结束日期
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * 获取是否使用 0 = 未使用 1= 已使用
     *
     * @return is_used - 是否使用 0 = 未使用 1= 已使用
     */
    public Integer getIsUsed() {
        return isUsed;
    }

    /**
     * 设置是否使用 0 = 未使用 1= 已使用
     *
     * @param isUsed 是否使用 0 = 未使用 1= 已使用
     */
    public void setIsUsed(Integer isUsed) {
        this.isUsed = isUsed;
    }

    /**
     * 获取使用时间戳
     *
     * @return used_time - 使用时间戳
     */
    public Integer getUsedTime() {
        return usedTime;
    }

    /**
     * 设置使用时间戳
     *
     * @param usedTime 使用时间戳
     */
    public void setUsedTime(Integer usedTime) {
        this.usedTime = usedTime;
    }

    /**
     * 获取对应使用的订单号 
     *
     * @return order_no - 对应使用的订单号 
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置对应使用的订单号 
     *
     * @param orderNo 对应使用的订单号 
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }
}
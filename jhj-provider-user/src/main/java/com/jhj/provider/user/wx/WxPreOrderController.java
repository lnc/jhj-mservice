package com.jhj.provider.user.wx;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.HttpClientUtil;

@RestController
public class WxPreOrderController {
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public AppResultData<Object> wxPreOrder(
			@RequestParam("orderNo") String orderNo,
			@RequestParam("total_fee") String totalFee){
		
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
		HashMap<String,String> params = new HashMap<>();
		params.put("appid", "");
		params.put("mch_id", "");
		params.put("nonce_str", "");
		params.put("sign", "");
		params.put("sign_type", "MD5");
		params.put("body", totalFee+"充值卡");
		params.put("out_trade_no", orderNo);
		params.put("total_fee", totalFee);
		params.put("spbill_create_ip", "123.57.232.172");
		params.put("notify_url", "");
		params.put("trade_type", "APP");
		
		String post = HttpClientUtil.post(url, params);
		
		data.setData(post);
		return data;
	
	}
}

package com.jhj.provider.user.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.user.po.b.model.Users;

/**
 * 通用Service
 */
public interface UserService extends BaseService<Users> {

	Users genUser(String mobile, Short addFrom);
	
	
}

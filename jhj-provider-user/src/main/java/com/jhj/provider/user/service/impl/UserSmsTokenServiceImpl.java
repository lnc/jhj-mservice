package com.jhj.provider.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jhj.provider.user.po.b.mapper.UserSmsTokenMapper;
import com.jhj.provider.user.po.b.model.UserSmsToken;
import com.jhj.provider.user.service.UserSmsTokenService;

@Service
public class UserSmsTokenServiceImpl extends BaseServiceImpl<UserSmsToken> implements UserSmsTokenService{

	@Autowired
	private UserSmsTokenMapper userSmsTokenMapper;
	
	@Override
	public UserSmsToken selectByMobileAndType(String mobile, int userType) {
		return userSmsTokenMapper.selectByMobileAndType(mobile, userType);
	}

}

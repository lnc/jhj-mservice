package com.jhj.provider.user.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.user.po.b.model.AdminAccount;
import com.jhj.provider.user.service.AdminAccountService;

@Service
public class AdminAccountServiceImpl extends BaseServiceImpl<AdminAccount> implements AdminAccountService {

}

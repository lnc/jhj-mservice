package com.jhj.provider.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.po.b.model.UserCoupons;
import com.jhj.provider.user.service.UserCouponsService;
import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;

@RestController
public class UserCouponsController {
	
	@Autowired
	private UserCouponsService userCouponsService;
	
	/**
	 * 用户优惠券列表
	 * @param userId
	 * @return list<UserCoupons>
	 * 
	 * */
	@RequestMapping(value="/api/user/coupons/list.json",method=RequestMethod.GET)
	public AppResultData<Object> getUserCouponsList(
			@RequestParam("userId") Integer userId){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0,"ok","");
		
		UserCoupons userCoupons = new UserCoupons();
		userCoupons.setUserId(userId);
		userCoupons.setIsUsed(0);
		List<UserCoupons> list = userCouponsService.selectBySearchVo(userCoupons, "add_time desc");
		
		data.setData(list);
		return data;
	}

}

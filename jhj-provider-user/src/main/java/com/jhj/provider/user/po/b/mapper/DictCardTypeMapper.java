package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.DictCardType;
import tk.mybatis.mapper.common.Mapper;

public interface DictCardTypeMapper extends Mapper<DictCardType> {
}
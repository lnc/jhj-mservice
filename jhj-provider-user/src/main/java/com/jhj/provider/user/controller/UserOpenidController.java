package com.jhj.provider.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.po.b.model.UserOpenid;
import com.jhj.provider.user.service.UserOpenidService;
import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;

@RestController
public class UserOpenidController {
	
	@Autowired
	private UserOpenidService userOpenidService;
	
	/**
	 * 添加openid
	 * @param mobile 
	 * @param openid
	 * @param userType
	 * 
	 * @return
	 * 
	 * */
	@RequestMapping(value="/api/user/addOpenid",method=RequestMethod.POST)
	public AppResultData<Object> addUserOpenid(
			@RequestParam(value="mobile") String mobile,
			@RequestParam(value="openid") String openid,
			@RequestParam(value="userType",defaultValue="0") Integer userType){
		
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		//先做个判断，是否已经存在
		UserOpenid searchVo = new UserOpenid();
		searchVo.setOpenid(openid);
		UserOpenid userOpenid = userOpenidService.selectOne(searchVo);
		if (userOpenid == null) {
		
			userOpenid = new UserOpenid();
			userOpenid.setMobile(mobile);
			userOpenid.setOpenid(openid);
			userOpenid.setUserType(userType);
			userOpenid.setAddTime(TimeStampUtil.getNowSecond().intValue());
			userOpenid.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
			userOpenidService.insert(userOpenid);
		}
		
		return data;
	}

}

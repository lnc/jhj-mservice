package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.Users;

import tk.mybatis.mapper.common.Mapper;

public interface UsersMapper extends Mapper<Users> {
}
package com.jhj.provider.user.po.b.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "admin_account")
public class AdminAccount {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    private Integer version;

    /**
     * 是否可用 0 =不可用 1 = 可用
     */
    private Integer enable;

    private String name;

    private String email;

    private String username;

    private String password;

    @Column(name = "register_time")
    private Date registerTime;

    @Column(name = "role_id")
    private Integer roleId;

    @Column(name = "organization_id")
    private Integer organizationId;

    /**
     * 环信IM
     */
    @Column(name = "im_username")
    private String imUsername;

    /**
     * 昵称
     */
    private String nickname;

    private String mobile;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 获取是否可用 0 =不可用 1 = 可用
     *
     * @return enable - 是否可用 0 =不可用 1 = 可用
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * 设置是否可用 0 =不可用 1 = 可用
     *
     * @param enable 是否可用 0 =不可用 1 = 可用
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return register_time
     */
    public Date getRegisterTime() {
        return registerTime;
    }

    /**
     * @param registerTime
     */
    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * @return role_id
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * @param roleId
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * @return organization_id
     */
    public Integer getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId
     */
    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * 获取环信IM
     *
     * @return im_username - 环信IM
     */
    public String getImUsername() {
        return imUsername;
    }

    /**
     * 设置环信IM
     *
     * @param imUsername 环信IM
     */
    public void setImUsername(String imUsername) {
        this.imUsername = imUsername;
    }

    /**
     * 获取昵称
     *
     * @return nickname - 昵称
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * 设置昵称
     *
     * @param nickname 昵称
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
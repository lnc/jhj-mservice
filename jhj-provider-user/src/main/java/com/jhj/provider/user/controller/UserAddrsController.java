package com.jhj.provider.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.po.b.model.UserAddrs;
import com.jhj.provider.user.service.UserAddrsService;
import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.TimeStampUtil;

@RestController
public class UserAddrsController {
	
	@Autowired
	private UserAddrsService userAddrsService;
	
	/**
	 * 获取用户地址列表
	 * @param userId 用户id
	 * @return list<UserAddrs>
	 * */
	@RequestMapping("/api/user/addr/list.json")
	public AppResultData<Object> getUserAddrsList(
			@RequestParam("userId") Integer userId){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		UserAddrs userAddrs = new UserAddrs();
		userAddrs.setUserId(userId);
		List<UserAddrs> list = userAddrsService.selectBySearchVo(userAddrs, "add_time desc");
		
		data.setData(list);
		return data;
	}
	
	/**
	 * 地址详情
	 * @param id 地址id
	 * @return userAddrs
	 * */
	@RequestMapping("/api/user/addr/detail.json")
	public AppResultData<Object> getUserAddrsDetail(
			@RequestParam("id") Integer id){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		UserAddrs userAddrs = userAddrsService.selectByPrimaryKey(id);
		
		data.setData(userAddrs);
		return data;
	}
	
	/**
	 * 设置地址为默认地址
	 * @param id 地址id
	 * @param idDeafult 1=默认地址，0=否
	 * @param userId 用户id
	 * @return userAddrs
	 * */
	@RequestMapping(value = "/api/user/addr/update.json", method = RequestMethod.POST)
	public AppResultData<Object> update(
			@RequestParam("id") Integer id,
			@RequestParam(value="isDefault",defaultValue="1") Integer isDefault,
			@RequestParam("userId") Integer userId){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		UserAddrs ua = new UserAddrs();
		ua.setUserId(userId);
		ua.setIsDefault(0);
		ua.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		userAddrsService.update(ua);
		
		UserAddrs userAddrs = new UserAddrs();
		userAddrs.setId(id);
		userAddrs.setIsDefault(isDefault);
		userAddrs.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		userAddrsService.update(userAddrs);
		
		data.setData(userAddrs);
		return data;
	} 
	
	/**
	 * 添加地址
	 * @param userId 用户id
	 * @param mobile
	 * @param idDeafult 1=默认地址，0=否
	 * @param name
	 * @param addr
	 * @param longitude
	 * @param latitude
	 * @param city
	 * @param address
	 * @param uid
	 * @param poi_type
	 * @param phone
	 * @param post_code
	 * @param city_id
	 * @param org_id
	 * @return userAddrs
	 * 
	 * */
	@RequestMapping(value = "/api/user/addr/add.json", method = RequestMethod.POST)
	public AppResultData<Object> add(
			@RequestParam("user_id") Integer userId,
			@RequestParam("mobile") String mobile,
			@RequestParam("is_default") Integer isDefault,
			@RequestParam("name") String name,
			@RequestParam("addr") String addr, // 详细门牌号
			@RequestParam("longitude") String longitude,
			@RequestParam("latitude") String latitude,
			@RequestParam("city") String city,
			@RequestParam(value = "address", required = false, defaultValue = "") String address, // 百度地图详细地址
			@RequestParam(value = "uid", required = false, defaultValue = "") String uid, // 百度地图uid
			@RequestParam(value = "poi_type", required = false, defaultValue = "0") Integer poiType,
			@RequestParam(value = "phone", required = false, defaultValue = "") String phone,
			@RequestParam(value = "post_code", required = false, defaultValue = "") String postCode,
			@RequestParam(value = "city_id", required = false, defaultValue = "0") String cityId,
			@RequestParam(value = "org_id", required = false, defaultValue = "0") Integer orgId){
		
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");

		/*
		 * 添加地址之前，判断是否在北京市 范围内。不在则不让添加
		 * 未做 编码转换, 该接口下没有 编码问题。。mapcontroller 接口下有编码问题
		 */
		if (!city.equals("北京市")) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("");
			return data;
		}

		if(isDefault!=null && isDefault==1) {
			UserAddrs ua = new UserAddrs();
			ua.setUserId(userId);
			ua.setIsDefault(0);
			ua.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
			userAddrsService.update(ua);
		}
		
		UserAddrs userAddrs = new UserAddrs();
		userAddrs.setUserId(userId);
		userAddrs.setMobile(mobile);
		userAddrs.setAddr(addr);
		userAddrs.setLongitude(longitude);
		userAddrs.setLatitude(latitude);
		userAddrs.setPoiType(poiType);
		userAddrs.setName(name);
		userAddrs.setAddress(address);
		userAddrs.setCity(city);
		userAddrs.setUid(uid);
		userAddrs.setPhone(phone);
		userAddrs.setPostCode(postCode);
		userAddrs.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		userAddrs.setIsDefault(isDefault);
		userAddrs.setOrgId(orgId);
		userAddrsService.insert(userAddrs);

		data.setData(userAddrs);
		return data;
	} 
	
	/***
	 * 删除地址
	 * 
	 * @return
	 * */
	@RequestMapping(value="/api/user/addr/del.json",method=RequestMethod.POST)
	public AppResultData<Object> deleteAddr(
			@RequestParam("id") Integer id){
		
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		UserAddrs userAddr = new UserAddrs();
		userAddr.setId(id);
		userAddrsService.delete(userAddr);
		
		return data;
	}

}

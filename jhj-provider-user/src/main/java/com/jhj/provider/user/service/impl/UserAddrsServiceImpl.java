package com.jhj.provider.user.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.user.po.b.model.UserAddrs;
import com.jhj.provider.user.service.UserAddrsService;

@Service
public class UserAddrsServiceImpl extends BaseServiceImpl<UserAddrs> implements UserAddrsService {

}

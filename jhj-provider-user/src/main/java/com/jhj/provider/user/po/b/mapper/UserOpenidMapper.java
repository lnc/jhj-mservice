package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.UserOpenid;
import tk.mybatis.mapper.common.Mapper;

public interface UserOpenidMapper extends Mapper<UserOpenid> {
}
package com.jhj.provider.user.po.b.model;

import javax.persistence.*;

@Table(name = "user_addrs")
public class UserAddrs {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator="JDBC")
    private Integer id;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 用户手机号
     */
    private String mobile;

    /**
     * 地址对应门店
     */
    @Column(name = "org_id")
    private Integer orgId;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 地理位置poi类型 0 = 普通点 1 = 公交站 2 = 公交路线 3 = 地铁站 4 = 地铁线路
     */
    @Column(name = "poi_type")
    private Integer poiType;

    /**
     * 地理位置名称
     */
    private String name;

    /**
     * 地理位置详细地址
     */
    private String address;

    /**
     * 门牌号
     */
    private String addr;

    /**
     * 城市
     */
    private String city;

    /**
     * 地理位置唯一标识
     */
    private String uid;

    /**
     * 地址位置电话标注
     */
    private String phone;

    /**
     * 地址位置邮编
     */
    @Column(name = "post_code")
    private String postCode;

    /**
     * 默认地址 1 = 默认  0 = 不默认
     */
    @Column(name = "is_default")
    private Integer isDefault;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Integer updateTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取用户手机号
     *
     * @return mobile - 用户手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置用户手机号
     *
     * @param mobile 用户手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取地址对应门店
     *
     * @return org_id - 地址对应门店
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * 设置地址对应门店
     *
     * @param orgId 地址对应门店
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取纬度
     *
     * @return latitude - 纬度
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 设置纬度
     *
     * @param latitude 纬度
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 获取经度
     *
     * @return longitude - 经度
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 设置经度
     *
     * @param longitude 经度
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 获取地理位置poi类型 0 = 普通点 1 = 公交站 2 = 公交路线 3 = 地铁站 4 = 地铁线路
     *
     * @return poi_type - 地理位置poi类型 0 = 普通点 1 = 公交站 2 = 公交路线 3 = 地铁站 4 = 地铁线路
     */
    public Integer getPoiType() {
        return poiType;
    }

    /**
     * 设置地理位置poi类型 0 = 普通点 1 = 公交站 2 = 公交路线 3 = 地铁站 4 = 地铁线路
     *
     * @param poiType 地理位置poi类型 0 = 普通点 1 = 公交站 2 = 公交路线 3 = 地铁站 4 = 地铁线路
     */
    public void setPoiType(Integer poiType) {
        this.poiType = poiType;
    }

    /**
     * 获取地理位置名称
     *
     * @return name - 地理位置名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置地理位置名称
     *
     * @param name 地理位置名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取地理位置详细地址
     *
     * @return address - 地理位置详细地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地理位置详细地址
     *
     * @param address 地理位置详细地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取门牌号
     *
     * @return addr - 门牌号
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置门牌号
     *
     * @param addr 门牌号
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取城市
     *
     * @return city - 城市
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置城市
     *
     * @param city 城市
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 获取地理位置唯一标识
     *
     * @return uid - 地理位置唯一标识
     */
    public String getUid() {
        return uid;
    }

    /**
     * 设置地理位置唯一标识
     *
     * @param uid 地理位置唯一标识
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * 获取地址位置电话标注
     *
     * @return phone - 地址位置电话标注
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置地址位置电话标注
     *
     * @param phone 地址位置电话标注
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取地址位置邮编
     *
     * @return post_code - 地址位置邮编
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * 设置地址位置邮编
     *
     * @param postCode 地址位置邮编
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * 获取默认地址 1 = 默认  0 = 不默认
     *
     * @return is_default - 默认地址 1 = 默认  0 = 不默认
     */
    public Integer getIsDefault() {
        return isDefault;
    }

    /**
     * 设置默认地址 1 = 默认  0 = 不默认
     *
     * @param isDefault 默认地址 1 = 默认  0 = 不默认
     */
    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Integer getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }
}
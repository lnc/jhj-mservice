package com.jhj.provider.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.po.b.model.DictCardType;
import com.jhj.provider.user.po.b.model.OrderCards;
import com.jhj.provider.user.service.DictCardTypeService;
import com.jhj.provider.user.service.OrderCardService;
import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;
import com.jhj.utils.OrderNoUtil;
import com.jhj.utils.TimeStampUtil;

@RestController
public class OrderCardController {
	
	@Autowired
	private OrderCardService orderCardService;
	
	@Autowired
	private DictCardTypeService dictCardtypeService;
	
	/**
	 * 保存充值订单
	 * @param userId 用户id
	 * @param mobile 手机号
	 * @param cardType 充值卡类型
	 * @param payType 支付类型
	 * @param referee 推荐码
	 * 
	 * @return orderCards
	 * 
	 * */
	@RequestMapping(value="/api/user/card/save.json",method = RequestMethod.POST)
	public AppResultData<Object> save(
			@RequestParam("userId") Integer userId,
			@RequestParam("mobile") String mobile,
			@RequestParam("cardType") Integer cardType,
			@RequestParam(value = "payType",defaultValue="2") Integer payType,
			@RequestParam(value="referee",defaultValue="") String referee){
		AppResultData<Object> data = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		DictCardType ct = new DictCardType();
		ct.setId(cardType);
		DictCardType dictCardType = dictCardtypeService.selectOne(ct);
		if(dictCardType==null) {
			data.setStatus(Constants.ERROR_999);
			data.setMsg("数据错误");
			return data;
		}
		
		OrderCards orderCards = new OrderCards();
		orderCards.setUserId(userId);
		orderCards.setMobile(mobile);
		orderCards.setCardOrderNo(String.valueOf(OrderNoUtil.genOrderNo()));
		orderCards.setCardType(cardType);
		orderCards.setCardMoney(dictCardType.getCardValue());
		orderCards.setCardPay(dictCardType.getCardPay());
		orderCards.setPayType(payType);
		orderCards.setOrderStatus(0);
		orderCards.setAddTime(TimeStampUtil.getNowSecond().intValue());
		orderCards.setUpdateTime(TimeStampUtil.getNowSecond().intValue());
		orderCards.setReferee(referee);
		orderCards.setParentId(0);
		orderCards.setOrgId(0);
		orderCardService.insert(orderCards);
		
		data.setData(orderCards);
		return data;
	}

}

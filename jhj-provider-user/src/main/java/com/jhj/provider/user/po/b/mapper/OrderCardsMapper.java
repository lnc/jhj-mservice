package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.OrderCards;
import tk.mybatis.mapper.common.Mapper;

public interface OrderCardsMapper extends Mapper<OrderCards> {
}
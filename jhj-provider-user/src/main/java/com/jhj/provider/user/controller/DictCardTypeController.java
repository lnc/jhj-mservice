package com.jhj.provider.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jhj.provider.user.po.b.model.DictCardType;
import com.jhj.provider.user.service.DictCardTypeService;
import com.jhj.utils.AppResultData;

@RestController
public class DictCardTypeController {
	
	@Autowired
	private DictCardTypeService dictCardTypeService;
	
	/**
	 * 获取充值卡列表
	 * @return list<DictCardType>
	 * */
	@RequestMapping(value="/api/user/cardList.json", method=RequestMethod.GET)
	public AppResultData<Object> getCardTypeList(){
		AppResultData<Object> data = new AppResultData<>();
		DictCardType dt = new DictCardType();
		List<DictCardType> list = dictCardTypeService.selectBySearchVo(dt, "card_value asc");
		
		data.setData(list);
		return data;
	}

}

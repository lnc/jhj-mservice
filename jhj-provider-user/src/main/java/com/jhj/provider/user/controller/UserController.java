package com.jhj.provider.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.jhj.provider.user.po.b.model.Users;
import com.jhj.provider.user.service.UserService;
import com.jhj.provider.user.utils.Constants;
import com.jhj.utils.AppResultData;

/**
 *
 */
@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	/**
	 * @param userId
	 * @return user
	 * */
	@RequestMapping(value = "/api/user/userinfo", method = RequestMethod.GET)
	public AppResultData<Object> userInfo(@RequestParam("userId") Integer userId) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		if (userId == 0) return result;
		Users user = userService.selectByPrimaryKey(userId);
		if (user == null) return result;
		result.setData(user);
		return result;
	}
	
	/**
	 * 获取用户列表
	 * @param page 页数
	 * @return pageInfo
	 * 
	 * */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/api/user/list", method = RequestMethod.GET)
    public AppResultData<Object> list(
    		@RequestParam(value = "page", required = false, defaultValue = "0") int page) {
		AppResultData<Object> result = new AppResultData<Object>(Constants.SUCCESS_0, "ok", "");
		
		Users searchVo = new Users();
		searchVo.setAddFrom(1);
		PageInfo pageInfo = userService.selectByListPage(searchVo, page, Constants.PAGE_MAX_NUMBER, "add_time desc");
		
		result.setData(pageInfo);
        return result;
    }
        
}
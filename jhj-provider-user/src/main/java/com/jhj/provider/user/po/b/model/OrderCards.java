package com.jhj.provider.user.po.b.model;

import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "order_cards")
public class OrderCards {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 订单号
     */
    @Column(name = "card_order_no")
    private String cardOrderNo;

    /**
     * 充值卡类型
     */
    @Column(name = "card_type")
    private Integer cardType;

    /**
     * 充值卡金额
     */
    @Column(name = "card_money")
    private BigDecimal cardMoney;

    /**
     * 充值卡支付金额
     */
    @Column(name = "card_pay")
    private BigDecimal cardPay;

    /**
     * 付款方式 0 = 余额支付 1 = 支付宝 2 = 微信支付 3 = 智慧支付 4 = 上门刷卡（保留，站位）
     */
    @Column(name = "pay_type")
    private Integer payType;

    /**
     * 0 = 未支付 1 = 已支付
     */
    @Column(name = "order_status")
    private Integer orderStatus;

    /**
     * 添加时间戳
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 更新时间戳
     */
    @Column(name = "update_time")
    private Integer updateTime;

    /**
     * 员工编号
     */
    private String referee;

    /**
     * 门店ID
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 云店ID
     */
    @Column(name = "org_id")
    private Integer orgId;

    /**
     * 获取id
     *
     * @return id - id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取订单号
     *
     * @return card_order_no - 订单号
     */
    public String getCardOrderNo() {
        return cardOrderNo;
    }

    /**
     * 设置订单号
     *
     * @param cardOrderNo 订单号
     */
    public void setCardOrderNo(String cardOrderNo) {
        this.cardOrderNo = cardOrderNo;
    }

    /**
     * 获取充值卡类型
     *
     * @return card_type - 充值卡类型
     */
    public Integer getCardType() {
        return cardType;
    }

    /**
     * 设置充值卡类型
     *
     * @param cardType 充值卡类型
     */
    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    /**
     * 获取充值卡金额
     *
     * @return card_money - 充值卡金额
     */
    public BigDecimal getCardMoney() {
        return cardMoney;
    }

    /**
     * 设置充值卡金额
     *
     * @param cardMoney 充值卡金额
     */
    public void setCardMoney(BigDecimal cardMoney) {
        this.cardMoney = cardMoney;
    }

    /**
     * 获取充值卡支付金额
     *
     * @return card_pay - 充值卡支付金额
     */
    public BigDecimal getCardPay() {
        return cardPay;
    }

    /**
     * 设置充值卡支付金额
     *
     * @param cardPay 充值卡支付金额
     */
    public void setCardPay(BigDecimal cardPay) {
        this.cardPay = cardPay;
    }

    /**
     * 获取付款方式 0 = 余额支付 1 = 支付宝 2 = 微信支付 3 = 智慧支付 4 = 上门刷卡（保留，站位）
     *
     * @return pay_type - 付款方式 0 = 余额支付 1 = 支付宝 2 = 微信支付 3 = 智慧支付 4 = 上门刷卡（保留，站位）
     */
    public Integer getPayType() {
        return payType;
    }

    /**
     * 设置付款方式 0 = 余额支付 1 = 支付宝 2 = 微信支付 3 = 智慧支付 4 = 上门刷卡（保留，站位）
     *
     * @param payType 付款方式 0 = 余额支付 1 = 支付宝 2 = 微信支付 3 = 智慧支付 4 = 上门刷卡（保留，站位）
     */
    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    /**
     * 获取0 = 未支付 1 = 已支付
     *
     * @return order_status - 0 = 未支付 1 = 已支付
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     * 设置0 = 未支付 1 = 已支付
     *
     * @param orderStatus 0 = 未支付 1 = 已支付
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 获取添加时间戳
     *
     * @return add_time - 添加时间戳
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间戳
     *
     * @param addTime 添加时间戳
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间戳
     *
     * @return update_time - 更新时间戳
     */
    public Integer getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间戳
     *
     * @param updateTime 更新时间戳
     */
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取员工编号
     *
     * @return referee - 员工编号
     */
    public String getReferee() {
        return referee;
    }

    /**
     * 设置员工编号
     *
     * @param referee 员工编号
     */
    public void setReferee(String referee) {
        this.referee = referee;
    }

    /**
     * 获取门店ID
     *
     * @return parent_id - 门店ID
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置门店ID
     *
     * @param parentId 门店ID
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取云店ID
     *
     * @return org_id - 云店ID
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * 设置云店ID
     *
     * @param orgId 云店ID
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }
}
package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.UserCoupons;
import tk.mybatis.mapper.common.Mapper;

public interface UserCouponsMapper extends Mapper<UserCoupons> {
}
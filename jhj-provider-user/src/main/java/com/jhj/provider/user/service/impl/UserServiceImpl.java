package com.jhj.provider.user.service.impl;

import org.springframework.stereotype.Service;

import com.jhj.provider.user.po.b.model.Users;
import com.jhj.provider.user.service.UserService;

/**
 * userservice实现类
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<Users> implements UserService  {
	
	@Override
	public Users genUser(String mobile, Short addFrom) {
		Users searchVo = new Users();
		searchVo.setMobile(mobile);
		Users u = this.selectOne(searchVo);
		if (u == null) {
			// 验证手机号是否已经注册，如果未注册，则自动注册用户，
			u = this.initPo(new Users());
			u.setMobile(mobile);
			String provinceName = "";
			u.setProvinceName(provinceName);

			this.insert(u);
		}

		return u;
	}
}

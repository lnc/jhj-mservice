package com.jhj.provider.user.po.b.mapper;

import org.apache.ibatis.annotations.Param;

import com.jhj.provider.user.po.b.model.UserSmsToken;
import tk.mybatis.mapper.common.Mapper;

public interface UserSmsTokenMapper extends Mapper<UserSmsToken> {
	
	UserSmsToken selectByMobileAndType(@Param("mobile") String mobile, @Param("userType") int userType);
}
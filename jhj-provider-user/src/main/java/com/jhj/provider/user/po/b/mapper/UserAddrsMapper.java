package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.UserAddrs;
import tk.mybatis.mapper.common.Mapper;

public interface UserAddrsMapper extends Mapper<UserAddrs> {
}
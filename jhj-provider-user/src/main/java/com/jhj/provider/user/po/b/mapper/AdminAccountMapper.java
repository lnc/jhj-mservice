package com.jhj.provider.user.po.b.mapper;

import com.jhj.provider.user.po.b.model.AdminAccount;
import tk.mybatis.mapper.common.Mapper;

public interface AdminAccountMapper extends Mapper<AdminAccount> {
}
package com.jhj.provider.user.po.b.model;

import javax.persistence.*;

@Table(name = "user_openid")
public class UserOpenid {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 微信openid
     */
    private String openid;

    /**
     * 用户类型：0=用户，1=阿姨
     */
    @Column(name = "user_type")
    private Integer userType;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Integer updateTime;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取微信openid
     *
     * @return openid - 微信openid
     */
    public String getOpenid() {
        return openid;
    }

    /**
     * 设置微信openid
     *
     * @param openid 微信openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }

    /**
     * 获取用户类型：0=用户，1=阿姨
     *
     * @return user_type - 用户类型：0=用户，1=阿姨
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * 设置用户类型：0=用户，1=阿姨
     *
     * @param userType 用户类型：0=用户，1=阿姨
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Integer getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Integer getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }
}
package com.jhj.provider.user.service;

import com.jhj.provider.user.po.b.model.UserSmsToken;

public interface UserSmsTokenService extends BaseService<UserSmsToken>{

	UserSmsToken selectByMobileAndType(String mobile, int userType);
	
}

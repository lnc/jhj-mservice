package com.jhj.user.provider.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import com.jhj.provider.user.Application;
import com.jhj.provider.user.controller.UserController;

/**
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Application.class, MockServletContext.class })
@WebAppConfiguration()
public class UserControllerTest {

	private MockMvc mvc;

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(new UserController()).build();
	}

	@Test
	public void userInfo() throws Exception {

		String url = "/user/userinfo";

		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		RequestBuilder e = get(url).param("userId", "1");
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);

	}
	
	@Test
	public void userList() throws Exception {

		String url = "/user/list";

		MockMvc mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		RequestBuilder e = get(url).param("page", "1");
		String response = mvc.perform(e).andReturn().getResponse().getContentAsString();

		System.err.println(response);

	}

}
